package com.microtron.cvinventorymanagmentapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.microtron.cvinventorymanagmentapp.Helper.StaticClass;
import com.microtron.cvinventorymanagmentapp.NetworkModel.OutSideInventoryBooking.OutSideBookingDataItem;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class OutSideInventorySalerInfoActivity extends AppCompatActivity {

    private OutSideBookingDataItem outSideProductDetails;
    private ImageView imgBack;
    private TextView txtOutSideSalerInfoCompany, txtOutSideSalerInfoProdName, txtOutSideSalerInfoProdPrice, txtOutSideSalerInfoBuyDate;
    private TextView txtOutSideSalerInfoVendorName, txtOutSideSalerInfoVendorMobile, txtOutSideSalerInfoVendorShopName;
    private TextView txtOutSideSalerInfoBuyerName, txtOutSideSalerInfoBuyerMobile, txtOutSideSalerInfoBuyerEmail, txtOutSideSalerInfoBuyerDuration, txtOutSideSalerInfoBuyerRent, txtOutSideSalerInfoBuyerBookingDate, txtOutSideSalerInfoBuyerReturnDate;
    private Date date1, date2;
    private float dayCount = 0.0f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_out_side_inventory_saler_info);
        outSideProductDetails = StaticClass.outSideDetailsProduct;

        imgBack = findViewById(R.id.imgBack);
        txtOutSideSalerInfoCompany = findViewById(R.id.txtOutSideSalerInfoCompany);
        txtOutSideSalerInfoProdName = findViewById(R.id.txtOutSideSalerInfoProdName);
        txtOutSideSalerInfoProdPrice = findViewById(R.id.txtOutSideSalerInfoProdPrice);
        txtOutSideSalerInfoBuyDate = findViewById(R.id.txtOutSideSalerInfoBuyDate);
        txtOutSideSalerInfoVendorName = findViewById(R.id.txtOutSideSalerInfoVendorName);
        txtOutSideSalerInfoVendorMobile = findViewById(R.id.txtOutSideSalerInfoVendorMobile);
        txtOutSideSalerInfoVendorShopName = findViewById(R.id.txtOutSideSalerInfoVendorShopName);

        txtOutSideSalerInfoBuyerName = findViewById(R.id.txtOutSideSalerInfoBuyerName);
        txtOutSideSalerInfoBuyerMobile = findViewById(R.id.txtOutSideSalerInfoBuyerMobile);
        txtOutSideSalerInfoBuyerEmail = findViewById(R.id.txtOutSideSalerInfoBuyerEmail);
        txtOutSideSalerInfoBuyerDuration = findViewById(R.id.txtOutSideSalerInfoBuyerDuration);
        txtOutSideSalerInfoBuyerRent = findViewById(R.id.txtOutSideSalerInfoBuyerRent);
        txtOutSideSalerInfoBuyerBookingDate = findViewById(R.id.txtOutSideSalerInfoBuyerBookingDate);
        txtOutSideSalerInfoBuyerReturnDate = findViewById(R.id.txtOutSideSalerInfoBuyerReturnDate);


        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        txtOutSideSalerInfoCompany.setText(outSideProductDetails.getCompanyName());
        txtOutSideSalerInfoProdName.setText(outSideProductDetails.getProductName());
        txtOutSideSalerInfoProdPrice.setText(outSideProductDetails.getProductPrice()+" Rs.");
        txtOutSideSalerInfoBuyDate.setText(outSideProductDetails.getPurchaseDate());

        txtOutSideSalerInfoVendorName.setText(outSideProductDetails.getSalerName());
        txtOutSideSalerInfoVendorMobile.setText(outSideProductDetails.getSalerMobile());
        txtOutSideSalerInfoVendorShopName.setText(outSideProductDetails.getShopName());

        txtOutSideSalerInfoBuyerName.setText(outSideProductDetails.getOutSideCustName());
        txtOutSideSalerInfoBuyerMobile.setText(outSideProductDetails.getOutSideCustMobile());
        txtOutSideSalerInfoBuyerEmail.setText(outSideProductDetails.getOutSideCustEmail());
        txtOutSideSalerInfoBuyerRent.setText(outSideProductDetails.getOutSideTotalRent()+" Rs.");
        txtOutSideSalerInfoBuyerBookingDate.setText(outSideProductDetails.getOutSideFromDate());
        txtOutSideSalerInfoBuyerReturnDate.setText(outSideProductDetails.getOutSideToDate());

        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // here set the pattern as you date in string was containing like date/month/year
            date1 = sdf.parse(outSideProductDetails.getOutSideFromDate());
            date2 = sdf.parse(outSideProductDetails.getOutSideToDate());
            dayCount = (date2.getTime() - date1.getTime())/(24*60*60*1000);
            //Toast.makeText(BookingActivity.this, "To Date"+date2+"\n From Date"+date1, Toast.LENGTH_LONG).show();
            //Toast.makeText(BookingActivity.this, "Day Count"+dayCount, Toast.LENGTH_SHORT).show();
            if(dayCount==0.0f){
                dayCount = 1.0f;
            }
            int daysCount = Math.round(dayCount);

            txtOutSideSalerInfoBuyerDuration.setText(""+daysCount+" Days");

        }catch(ParseException ex){
            // handle parsing exception if date string was different from the pattern applying into the SimpleDateFormat contructor
        }


    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(this, OutSideInventoryListActivity.class);
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
    }
}
