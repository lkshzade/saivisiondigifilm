package com.microtron.cvinventorymanagmentapp;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.microtron.cvinventorymanagmentapp.Helper.ApiClient;
import com.microtron.cvinventorymanagmentapp.Helper.StaticClass;
import com.microtron.cvinventorymanagmentapp.NetworkModel.BookingItem.BookingDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.BookingItem.BookingDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.BookingItem.BookingDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.OutSideInventoryBooking.OutSideBookingDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.OutSideInventoryBooking.OutSideBookingDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.OutSideInventoryBooking.OutSideBookingDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.UpdatBookedItem.UpdatedBookingDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.UpdatBookedItem.UpdatedBookingDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.UpdatBookedItem.UpdatedBookingDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataItem;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class RentProductDetailsActivity extends AppCompatActivity {

    private TextView txtRentDetailsCustName, txtRentDetailsCustEmail, txtRentDetailsCustMobile, txtRentDetailsProductName, txtRentDetailsProductDetails, txtRentDetailsDays, txtRentDetailsRent, txtRentDetailsBookingDate, txtRentDetailsReturnDate, txtRentDetailsPending, txtRentTitle;
    private EditText edtRentDetailsAdvance, edtRentDetailsDayOrMonthRent;
    private Button btnReceived, btnPendingReturnOnly, btnProductReturnOnly, btnUpdate, btnCancelBooking;
    private ImageView imgBack;
    BookedItemDataItem bookedProductDetails;
    private ProgressDialog addDialog;
    private int finalQuantity;
    private String finalInStock, strPaidAmount, strTotalRent, strRentPending;
    private float dayCount = 0.0f, rentAdvance = 1.0f, rentPending = 0.0f, totalRent = 0.0f;
    private int mToYear, mToMonth, mToDay;
    private String strToDay, strToYear, strToMonth, strToDate, strType, strDayRent, strMonthRent, strRentTitle;
    private Date date1, date2;
    private Date fromDate, currentDate;
    private String strDaysCount, strBookingDate, strReturnDate, strBookingStatus, strRentOnly, strDayOrMonthRent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rent_product_details);

        txtRentDetailsCustName = findViewById(R.id.txtRentDetailsCustName);
        txtRentDetailsCustEmail = findViewById(R.id.txtRentDetailsCustEmail);
        txtRentDetailsCustMobile = findViewById(R.id.txtRentDetailsCustMobile);

        txtRentDetailsProductName = findViewById(R.id.txtRentDetailsProductName);
        txtRentDetailsProductDetails = findViewById(R.id.txtRentDetailsProductDescription);

        txtRentDetailsDays = findViewById(R.id.txtRentDetailsDays);
        txtRentTitle = findViewById(R.id.txtRentTitle);
        edtRentDetailsDayOrMonthRent = findViewById(R.id.edtRentDetailsDayOrMonthRent);
        txtRentDetailsRent = findViewById(R.id.txtRentDetailsRent);
        txtRentDetailsBookingDate = findViewById(R.id.txtRentDetailsBookingDate);
        txtRentDetailsReturnDate = findViewById(R.id.txtRentDetailsReturnDate);
        edtRentDetailsAdvance = findViewById(R.id.edtRentDetailsAdvance);
        txtRentDetailsPending = findViewById(R.id.txtRentDetailsPending);

        bookedProductDetails = StaticClass.bookedProduct;

        txtRentDetailsCustName.setText(bookedProductDetails.getCustomerName());
        txtRentDetailsCustEmail.setText(bookedProductDetails.getCustomerEmail());
        txtRentDetailsCustMobile.setText(bookedProductDetails.getCustomerMobile());

        txtRentDetailsProductName.setText(bookedProductDetails.getProductName());
        txtRentDetailsProductDetails.setText(bookedProductDetails.getProductDetails());


        final String type = bookedProductDetails.getBookingType();
        int finaldaysCount = Math.round(Float.parseFloat(bookedProductDetails.getDaysCount()));
        if (type.equals("Monthly")) {
            txtRentDetailsDays.setText("1 Month ("+finaldaysCount+" Days)");
            txtRentTitle.setText("Month Rent");
            edtRentDetailsDayOrMonthRent.setText(bookedProductDetails.getMonthRent());
        } else {
            int daysCount = Math.round(Float.parseFloat(bookedProductDetails.getDaysCount()));
            txtRentDetailsDays.setText(daysCount + " Days");
            txtRentTitle.setText("Daily Rent");
            edtRentDetailsDayOrMonthRent.setText(bookedProductDetails.getDailyRent());
        }


        txtRentDetailsRent.setText(bookedProductDetails.getTotalRent());
        strRentOnly = bookedProductDetails.getTotalRent();
        txtRentDetailsBookingDate.setText(bookedProductDetails.getBookingFromDate());
        txtRentDetailsReturnDate.setText(bookedProductDetails.getBookingToDate());
        edtRentDetailsAdvance.setText(bookedProductDetails.getRentAdvance());
        strRentPending = bookedProductDetails.getRentPending();
        txtRentDetailsPending.setText(strRentPending);

        txtRentDetailsProductDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(RentProductDetailsActivity.this);
                dialog.setContentView(R.layout.text_pop_up);
                TextView txtMessage = (TextView) dialog.findViewById(R.id.dialogMessage);

                if(bookedProductDetails.getProductDetails().equals("")){
                    txtMessage.setText("No Description Available");
                }
                else{
                    txtMessage.setText(bookedProductDetails.getProductDetails());
                }

                txtMessage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();

            }
        });

        edtRentDetailsDayOrMonthRent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                strDayOrMonthRent = edtRentDetailsDayOrMonthRent.getText().toString().trim();
                strBookingDate = txtRentDetailsBookingDate.getText().toString().trim();
                strReturnDate = txtRentDetailsReturnDate.getText().toString().trim();

                if(strDayOrMonthRent.equals("")){
                    strDayOrMonthRent = "0";
                }

                try{
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // here set the pattern as you date in string was containing like date/month/year
                    date1 = sdf.parse(strBookingDate);
                    date2 = sdf.parse(strReturnDate);
                    dayCount = (date2.getTime() - date1.getTime())/(24*60*60*1000);
                    //Toast.makeText(BookingActivity.this, "To Date"+date2+"\n From Date"+date1, Toast.LENGTH_LONG).show();
                    //Toast.makeText(BookingActivity.this, "Day Count"+dayCount, Toast.LENGTH_SHORT).show();
                    if(dayCount==0.0f){
                        dayCount = 1.0f;
                    }
                    /*if(type.equals("Monthly")){
                        txtRentDetailsDays.setText("1 Month");
                        float Rent = Float.parseFloat(strDayOrMonthRent);
                        totalRent = Rent * dayCount;
                        txtRentDetailsRent.setText(""+totalRent);
                        strRentOnly = String.valueOf(totalRent);
                        String advance = edtRentDetailsAdvance.getText().toString().trim();
                        if(advance.equals("")){
                            advance = "0";
                        }
                        float pending = (totalRent)-(Float.parseFloat(advance));
                        txtRentDetailsPending.setText(""+pending);
                    }
                    else*/ if(dayCount>=31 && dayCount<=45){

                            int finaldaysCount = Math.round(dayCount);
                            txtRentDetailsDays.setText("1 Month ("+finaldaysCount+" Days)");
                            txtRentTitle.setText("Month Rent");
                            float Rent = Float.parseFloat(strDayOrMonthRent);
                            totalRent = Rent;
                            txtRentDetailsRent.setText(""+totalRent);
                            strRentOnly = String.valueOf(totalRent);
                            String advance = edtRentDetailsAdvance.getText().toString().trim();
                            if(advance.equals("")){
                                advance = "0";
                            }
                            float pending = (totalRent)-(Float.parseFloat(advance));
                            txtRentDetailsPending.setText(""+pending);

                        }
                        else{
                            int daysCount = Math.round(dayCount);
                            txtRentTitle.setText("Daily Rent");
                            txtRentDetailsDays.setText(daysCount+" Days");
                            totalRent = (Float.parseFloat(strDayOrMonthRent)) * (dayCount);
                            txtRentDetailsRent.setText(""+totalRent);
                            strRentOnly = String.valueOf(totalRent);

                            String advance = edtRentDetailsAdvance.getText().toString().trim();
                            if(advance.equals("")){
                                advance = "0";
                            }
                            float pending = (totalRent)-(Float.parseFloat(advance));
                            txtRentDetailsPending.setText(""+pending);
                        }

                }catch(ParseException e){
                    // handle parsing exception if date string was different from the pattern applying into the SimpleDateFormat contructor
                }


            }
        });

        txtRentDetailsReturnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Calendar c = Calendar.getInstance();
                mToYear = c.get(Calendar.YEAR);
                mToMonth = c.get(Calendar.MONTH);
                mToDay = c.get(Calendar.DAY_OF_MONTH);

                strDayOrMonthRent = edtRentDetailsDayOrMonthRent.getText().toString().trim();

                DatePickerDialog datePickerDialog = new DatePickerDialog(RentProductDetailsActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                if (dayOfMonth <= 9) {
                                    strToDay = "0" + dayOfMonth;
                                } else {
                                    strToDay = "" + dayOfMonth;
                                }
                                monthOfYear = monthOfYear + 1;
                                strToYear = String.valueOf(year);

                                if (monthOfYear <= 9) {
                                    strToMonth = "0" + monthOfYear;
                                } else {
                                    strToMonth = "" + monthOfYear;
                                }

                                txtRentDetailsReturnDate.setText(strToYear + "-" + strToMonth + "-" + strToDay);

                                strToDate = txtRentDetailsReturnDate.getText().toString().trim();

                                try{
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // here set the pattern as you date in string was containing like date/month/year
                                    date1 = sdf.parse(bookedProductDetails.getBookingFromDate());
                                    date2 = sdf.parse(strToDate);
                                    dayCount = (date2.getTime() - date1.getTime())/(24*60*60*1000);
                                    //Toast.makeText(BookingActivity.this, "To Date"+date2+"\n From Date"+date1, Toast.LENGTH_LONG).show();
                                    //Toast.makeText(BookingActivity.this, "Day Count"+dayCount, Toast.LENGTH_SHORT).show();
                                    if(dayCount==0.0f){
                                        dayCount = 1.0f;
                                    }
                                    /*if(type.equals("Monthly")){
                                        txtRentDetailsDays.setText("1 Month");
                                        float Rent = Float.parseFloat(bookedProductDetails.getTotalRent())/31;
                                        totalRent = Rent * dayCount;
                                        txtRentDetailsRent.setText(""+totalRent);
                                        strRentOnly = String.valueOf(totalRent);
                                        String advance = edtRentDetailsAdvance.getText().toString().trim();
                                        if(advance.equals("")){
                                            advance = "0";
                                        }
                                        float pending = (totalRent)-(Float.parseFloat(advance));
                                        txtRentDetailsPending.setText(""+pending);
                                    }
                                    else */if(dayCount>=31){

                                            int finaldaysCount = Math.round(dayCount);
                                            txtRentDetailsDays.setText("1 Month ("+finaldaysCount+" Days)");
                                            txtRentTitle.setText("Month Rent");
                                            //strType = "Monthly";
                                            float Rent = Float.parseFloat(strDayOrMonthRent);
                                            totalRent = Rent;
                                            txtRentDetailsRent.setText(""+totalRent);
                                            strRentOnly = String.valueOf(totalRent);
                                            String advance = edtRentDetailsAdvance.getText().toString().trim();
                                            if(advance.equals("")){
                                                advance = "0";
                                            }
                                            float pending = (totalRent)-(Float.parseFloat(advance));
                                            txtRentDetailsPending.setText(""+pending);

                                        }
                                        else{
                                            int daysCount = Math.round(dayCount);
                                            txtRentTitle.setText("Daily Rent");
                                            txtRentDetailsDays.setText(daysCount+" Days");
                                            //strType = "Daily";
                                            totalRent = (Float.parseFloat(strDayOrMonthRent)) * (dayCount);
                                            txtRentDetailsRent.setText(""+totalRent);
                                            strRentOnly = String.valueOf(totalRent);

                                            String advance = edtRentDetailsAdvance.getText().toString().trim();
                                            if(advance.equals("")){
                                                advance = "0";
                                            }
                                            float pending = (totalRent)-(Float.parseFloat(advance));
                                            txtRentDetailsPending.setText(""+pending);
                                        }
                                        /*int daysCount = Math.round(dayCount);
                                        txtRentDetailsDays.setText(daysCount+" Days");
                                        totalRent = (Float.parseFloat(bookedProductDetails.getDailyRent())) * (dayCount);
                                        txtRentDetailsRent.setText(totalRent+" Rs.");
                                        strRentOnly = String.valueOf(totalRent);

                                        String advance = edtRentDetailsAdvance.getText().toString().trim();
                                        if(advance.equals("")){
                                            advance = "0";
                                        }
                                        float pending = (totalRent)-(Float.parseFloat(advance));
                                        txtRentDetailsPending.setText(""+pending);*/

                                }catch(ParseException e){
                                    // handle parsing exception if date string was different from the pattern applying into the SimpleDateFormat contructor
                                }

                            }
                        }, mToYear, mToMonth, mToDay);

                datePickerDialog.show();

            }
        });

        edtRentDetailsAdvance.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                strPaidAmount = edtRentDetailsAdvance.getText().toString().trim();
                strTotalRent = txtRentDetailsRent.getText().toString().trim();
                totalRent = Float.parseFloat(strTotalRent);

                if(strPaidAmount.equals("")){
                    rentAdvance = 0.0f;
                    rentPending = totalRent - rentAdvance;
                    txtRentDetailsPending.setText(""+rentPending);
                    strRentPending = ""+rentPending;
                }
                else{
                    rentAdvance = Float.parseFloat(strPaidAmount);
                    rentPending = totalRent - rentAdvance;
                    txtRentDetailsPending.setText(""+rentPending);
                    strRentPending = ""+rentPending;
                }

            }
        });

        btnReceived = findViewById(R.id.btnReceived);
        btnUpdate = findViewById(R.id.btnUpdate);
        btnPendingReturnOnly = findViewById(R.id.btnPendingReturnOnly);
        btnProductReturnOnly = findViewById(R.id.btnReturnProductOnly);
        imgBack = findViewById(R.id.imgBack);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        strBookingStatus = bookedProductDetails.getBookingStatus();

        if(strBookingStatus.equals("3")){
            btnProductReturnOnly.setBackgroundResource(R.drawable.solid_gray_round_button);
            btnProductReturnOnly.setEnabled(false);
        }

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strToDate = txtRentDetailsReturnDate.getText().toString().trim();
                strPaidAmount = edtRentDetailsAdvance.getText().toString().trim();
                strTotalRent = txtRentDetailsRent.getText().toString().trim();

                if(!strTotalRent.equals(bookedProductDetails.getTotalRent())){
                    onlyUpdate();
                    Toast.makeText(RentProductDetailsActivity.this, "Enable", Toast.LENGTH_SHORT).show();
                }
                else if(!strPaidAmount.equals(bookedProductDetails.getRentAdvance())){
                    onlyUpdate();
                    Toast.makeText(RentProductDetailsActivity.this, "Enable", Toast.LENGTH_SHORT).show();
                }
                else if(!strToDate.equals(bookedProductDetails.getBookingToDate())){
                    onlyUpdate();
                    Toast.makeText(RentProductDetailsActivity.this, "Enable", Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(RentProductDetailsActivity.this, "No Changes Made Please Select Another Option", Toast.LENGTH_SHORT).show();
                }

            }

        });

        btnReceived.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strToDate = txtRentDetailsReturnDate.getText().toString().trim();
                strPaidAmount = edtRentDetailsAdvance.getText().toString().trim();
                strTotalRent = txtRentDetailsRent.getText().toString().trim();
                if(!strTotalRent.equals(bookedProductDetails.getTotalRent())){
                    Toast.makeText(RentProductDetailsActivity.this, "Please Make Changes Update First", Toast.LENGTH_SHORT).show();
                }
                else if(!strPaidAmount.equals(bookedProductDetails.getRentAdvance())){
                    Toast.makeText(RentProductDetailsActivity.this, "Please Make Changes Update First", Toast.LENGTH_SHORT).show();
                }
                else if(!strToDate.equals(bookedProductDetails.getBookingToDate())){
                    Toast.makeText(RentProductDetailsActivity.this, "Please Make Changes Update First", Toast.LENGTH_SHORT).show();
                }
                else{
                    //Toast.makeText(RentProductDetailsActivity.this, "Button Enable", Toast.LENGTH_SHORT).show();
                    strBookingStatus = "0";
                strPaidAmount = edtRentDetailsAdvance.getText().toString().trim();

                strRentPending = txtRentDetailsPending.getText().toString().trim();
                float pendingAmount = Float.parseFloat(strRentPending);

                if(pendingAmount == 0.0){
                    //Toast.makeText(RentProductDetailsActivity.this, "Success", Toast.LENGTH_SHORT).show();
                    addDialog = new ProgressDialog(RentProductDetailsActivity.this);
                    addDialog.setCancelable(true);
                    addDialog.setMessage("Receiving Item...");
                    addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    addDialog.show();

                    finalQuantity = Integer.parseInt(bookedProductDetails.getProductQuantity()) + 1;

                    if(finalQuantity == 0){

                        finalInStock = "No";
                    }
                    else{
                        finalInStock = "Yes";
                    }

                    try{
                        BookingDataRequest bookingItemRequest = new BookingDataRequest();

                        bookingItemRequest.setBookingId(bookedProductDetails.getBookingId());
                        bookingItemRequest.setProductId(bookedProductDetails.getProductId());
                        bookingItemRequest.setProductQuantity(String.valueOf(finalQuantity));
                        bookingItemRequest.setProductInStock(finalInStock);
                        bookingItemRequest.setRentAdvance(strPaidAmount);
                        bookingItemRequest.setRentPending(strRentPending);
                        bookingItemRequest.setBookingStatus(strBookingStatus);
                        bookingItemRequest.setBookingType(strBookingStatus);
                        bookingItemRequest.setBookingCustId(bookedProductDetails.getBookingCustId());
                        bookingItemRequest.setTotalRent(bookedProductDetails.getTotalRent());

                        Call<BookingDataResponse> responceCall = ApiClient.getApiClient().receivedBookingStatus(bookingItemRequest);

                        responceCall.enqueue(new Callback<BookingDataResponse>() {
                            @Override
                            public void onResponse(Call<BookingDataResponse> call, retrofit2.Response<BookingDataResponse> response) {

                                //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                                if (response.body().getSuccess().equals("1")) {

                                    List<BookingDataItem> items =response.body().getBookingData();

                                    Intent intent = new Intent(RentProductDetailsActivity.this, RentListActivity.class);
                                    startActivity(intent);
                                    overridePendingTransition(0,0);
                                    addDialog.dismiss();


                                } else {

                                    Toast.makeText(RentProductDetailsActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                                    addDialog.dismiss();
                                }
                            }

                            @Override
                            public void onFailure(Call<BookingDataResponse> call, Throwable t) {
                                //Log.d("erorr",t.getMessage());
                                //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                                addDialog.dismiss();

                            }
                        });
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }

                }
                else{
                    txtRentDetailsPending.setError("Account not Clear yet please Use Update");
                    Toast.makeText(RentProductDetailsActivity.this, "Account not Clear yet please Use Update", Toast.LENGTH_SHORT).show();
                }
                    //Toast.makeText(RentProductDetailsActivity.this, "Enable", Toast.LENGTH_SHORT).show();
                }


            }
        });

        btnPendingReturnOnly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strToDate = txtRentDetailsReturnDate.getText().toString().trim();
                strPaidAmount = edtRentDetailsAdvance.getText().toString().trim();
                strTotalRent = txtRentDetailsRent.getText().toString().trim();
                if(!strTotalRent.equals(bookedProductDetails.getTotalRent())){
                    Toast.makeText(RentProductDetailsActivity.this, "Please Make Changes Update First", Toast.LENGTH_SHORT).show();
                }
                else if(!strPaidAmount.equals(bookedProductDetails.getRentAdvance())){
                    Toast.makeText(RentProductDetailsActivity.this, "Please Make Changes Update First", Toast.LENGTH_SHORT).show();
                }
                else if(!strToDate.equals(bookedProductDetails.getBookingToDate())){
                    Toast.makeText(RentProductDetailsActivity.this, "Please Make Changes Update First", Toast.LENGTH_SHORT).show();
                }
                else{

                    strRentPending = txtRentDetailsPending.getText().toString().trim();
                    float pendingAmount = Float.parseFloat(strRentPending);
                    strBookingStatus = bookedProductDetails.getBookingStatus();

                    if(strRentPending.equals("")){
                        strRentPending = "0.0";
                    }
                    else if(strRentPending.equals("0")){
                        strRentPending = "0.0";
                    }
                    else if(strRentPending.equals("0.0")){
                        strRentPending = "0.0";
                    }

                    if(strBookingStatus.equals("3") && !strRentPending.equals("0.0")){
                        strBookingStatus = "3";
                    }
                    else if(strBookingStatus.equals("3") && strRentPending.equals("0.0")){
                        strBookingStatus = "0";
                    }
                    else{
                        strBookingStatus = "2";
                    }

                    if(pendingAmount == 0.0){
                        //Toast.makeText(OutSideProductDetailsActivity.this, "Success", Toast.LENGTH_SHORT).show();
                        //strOutSideInventoryStatus = "0";
                        //updateOutSideProduct();
                        Toast.makeText(RentProductDetailsActivity.this, "Please Take as a Received", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        strTotalRent = txtRentDetailsRent.getText().toString().trim();
                        strBookingDate = txtRentDetailsBookingDate.getText().toString().trim();
                        strReturnDate = txtRentDetailsReturnDate.getText().toString().trim();
                        strPaidAmount = edtRentDetailsAdvance.getText().toString().trim();
                        strRentPending = txtRentDetailsPending.getText().toString().trim();
                        strRentTitle = txtRentTitle.getText().toString().trim();
                        if(strRentTitle.equals("Daily Rent")){
                            strDayRent = edtRentDetailsDayOrMonthRent.getText().toString().trim();
                            strMonthRent = "0";
                            strType = "Daily";
                        }
                        else{
                            strMonthRent = edtRentDetailsDayOrMonthRent.getText().toString().trim();
                            strDayRent = "0";
                            strType = "Monthly";
                        }
                        try {
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // here set the pattern as you date in string was containing like date/month/year
                            date1 = sdf.parse(strBookingDate);
                            date2 = sdf.parse(strReturnDate);
                            dayCount = (date2.getTime() - date1.getTime()) / (24 * 60 * 60 * 1000);
                            //Toast.makeText(BookingActivity.this, "To Date"+date2+"\n From Date"+date1, Toast.LENGTH_LONG).show();
                            //Toast.makeText(BookingActivity.this, "Day Count"+dayCount, Toast.LENGTH_SHORT).show();
                            if (dayCount == 0.0f) {
                                dayCount = 1.0f;
                            }
                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }
                        strDaysCount = String.valueOf(dayCount);
                        //strBookingStatus = "2";

                        //Toast.makeText(RentProductDetailsActivity.this, "Rent "+strTotalRent+" \n Total Days "+strDaysCount+" \n Booking Date "+strBookingDate+" \n Return Date "+strReturnDate+" \n Pending "+strRentPending+" \n Advance "+strPaidAmount+" \nStatus "+strBookingStatus, Toast.LENGTH_LONG).show();

                        addDialog = new ProgressDialog(RentProductDetailsActivity.this);
                        addDialog.setCancelable(true);
                        addDialog.setMessage("Updating Item...");
                        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        addDialog.show();

                        try{
                            UpdatedBookingDataRequest updateItemRequest = new UpdatedBookingDataRequest();

                            updateItemRequest.setBookingId(bookedProductDetails.getBookingId());
                            updateItemRequest.setDaysCount(strDaysCount);
                            updateItemRequest.setTotalRent(strRentOnly);
                            updateItemRequest.setBookingFromDate(strBookingDate);
                            updateItemRequest.setBookingToDate(strReturnDate);
                            updateItemRequest.setRentAdvance(strPaidAmount);
                            updateItemRequest.setRentPending(strRentPending);
                            updateItemRequest.setBookingStatus(strBookingStatus);
                            updateItemRequest.setBookingType(strType);
                            updateItemRequest.setDailyRate(strDayRent);
                            updateItemRequest.setMonthlyRate(strMonthRent);
                            updateItemRequest.setBookingProdId(bookedProductDetails.getBookingProdId());
                            updateItemRequest.setBookingCustId(bookedProductDetails.getBookingCustId());

                            Call<UpdatedBookingDataResponse> responceCall = ApiClient.getApiClient().updateBookingPending(updateItemRequest);

                            responceCall.enqueue(new Callback<UpdatedBookingDataResponse>() {
                                @Override
                                public void onResponse(Call<UpdatedBookingDataResponse> call, retrofit2.Response<UpdatedBookingDataResponse> response) {

                                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                                    if (response.body().getSuccess().equals("1")) {

                                        List<UpdatedBookingDataItem> items =response.body().getUpdatedBookingData();

                                        Intent intent = new Intent(RentProductDetailsActivity.this, RentListActivity.class);
                                        startActivity(intent);
                                        overridePendingTransition(0,0);
                                        addDialog.dismiss();


                                    } else {

                                        Toast.makeText(RentProductDetailsActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                                        addDialog.dismiss();
                                    }
                                }

                                @Override
                                public void onFailure(Call<UpdatedBookingDataResponse> call, Throwable t) {
                                    //Log.d("erorr",t.getMessage());
                                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                                    addDialog.dismiss();

                                }
                            });
                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }

                    }

                    //Toast.makeText(RentProductDetailsActivity.this, "Enable", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnProductReturnOnly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                strToDate = txtRentDetailsReturnDate.getText().toString().trim();
                strPaidAmount = edtRentDetailsAdvance.getText().toString().trim();
                strTotalRent = txtRentDetailsRent.getText().toString().trim();
                if (!strTotalRent.equals(bookedProductDetails.getTotalRent())) {
                    Toast.makeText(RentProductDetailsActivity.this, "Please Make Changes Update First", Toast.LENGTH_SHORT).show();
                } else if (!strPaidAmount.equals(bookedProductDetails.getRentAdvance())) {
                    Toast.makeText(RentProductDetailsActivity.this, "Please Make Changes Update First", Toast.LENGTH_SHORT).show();
                } else if (!strToDate.equals(bookedProductDetails.getBookingToDate())) {
                    Toast.makeText(RentProductDetailsActivity.this, "Please Make Changes Update First", Toast.LENGTH_SHORT).show();
                } else {

                    strTotalRent = txtRentDetailsRent.getText().toString().trim();
                    strBookingDate = txtRentDetailsBookingDate.getText().toString().trim();
                    strReturnDate = txtRentDetailsReturnDate.getText().toString().trim();
                    strPaidAmount = edtRentDetailsAdvance.getText().toString().trim();
                    strRentPending = txtRentDetailsPending.getText().toString().trim();
                    try {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // here set the pattern as you date in string was containing like date/month/year
                        date1 = sdf.parse(strBookingDate);
                        date2 = sdf.parse(strReturnDate);
                        dayCount = (date2.getTime() - date1.getTime()) / (24 * 60 * 60 * 1000);
                        //Toast.makeText(BookingActivity.this, "To Date"+date2+"\n From Date"+date1, Toast.LENGTH_LONG).show();
                        //Toast.makeText(BookingActivity.this, "Day Count"+dayCount, Toast.LENGTH_SHORT).show();
                        if (dayCount == 0.0f) {
                            dayCount = 1.0f;
                        }
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                    strDaysCount = String.valueOf(dayCount);
                    strBookingStatus = "3";

                    addDialog = new ProgressDialog(RentProductDetailsActivity.this);
                    addDialog.setCancelable(true);
                    addDialog.setMessage("Receiving Item...");
                    addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    addDialog.show();

                    finalQuantity = 1;

                    if(finalQuantity == 0){

                        finalInStock = "No";
                    }
                    else{
                        finalInStock = "Yes";
                    }

                    try{
                        BookingDataRequest bookingItemRequest = new BookingDataRequest();

                        bookingItemRequest.setBookingId(bookedProductDetails.getBookingId());
                        bookingItemRequest.setDaysCount(strDaysCount);
                        bookingItemRequest.setTotalRent(strRentOnly);
                        bookingItemRequest.setBookingFromDate(strBookingDate);
                        bookingItemRequest.setBookingToDate(strReturnDate);
                        bookingItemRequest.setRentAdvance(strPaidAmount);
                        bookingItemRequest.setRentPending(strRentPending);
                        bookingItemRequest.setBookingStatus(strBookingStatus);
                        bookingItemRequest.setProductId(bookedProductDetails.getProductId());
                        bookingItemRequest.setProductQuantity(String.valueOf(finalQuantity));
                        bookingItemRequest.setProductInStock(finalInStock);
                        bookingItemRequest.setBookingCustId(bookedProductDetails.getBookingCustId());
                        bookingItemRequest.setTotalRent(bookedProductDetails.getTotalRent());

                        Call<BookingDataResponse> responceCall = ApiClient.getApiClient().updateBookingProductStatus(bookingItemRequest);

                        responceCall.enqueue(new Callback<BookingDataResponse>() {
                            @Override
                            public void onResponse(Call<BookingDataResponse> call, retrofit2.Response<BookingDataResponse> response) {

                                //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                                if (response.body().getSuccess().equals("1")) {

                                    List<BookingDataItem> items =response.body().getBookingData();

                                    Intent intent = new Intent(RentProductDetailsActivity.this, RentListActivity.class);
                                    startActivity(intent);
                                    overridePendingTransition(0,0);
                                    addDialog.dismiss();


                                } else {

                                    Toast.makeText(RentProductDetailsActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                                    addDialog.dismiss();
                                }
                            }

                            @Override
                            public void onFailure(Call<BookingDataResponse> call, Throwable t) {
                                //Log.d("erorr",t.getMessage());
                                //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                                addDialog.dismiss();

                            }
                        });
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                    //Toast.makeText(RentProductDetailsActivity.this, "Enable", Toast.LENGTH_SHORT).show();

                }
            }
        });

        btnCancelBooking = findViewById(R.id.btnCancelBooking);
        btnCancelBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    Date c = Calendar.getInstance().getTime();
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    String formattedDate = df.format(c);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // here set the pattern as you date in string was containing like date/month/year
                    currentDate = sdf.parse(formattedDate);
                    fromDate = sdf.parse(bookedProductDetails.getBookingFromDate());

                    /*if(currentDate.equals(fromDate)){
                        txtRentDetailsBookingDate.setError("You Can Not Cancel Booking On Same Day");
                        Toast.makeText(RentProductDetailsActivity.this, "You Can Not Cancel Booking On Same Day", Toast.LENGTH_SHORT).show();
                    }
                    else */if(currentDate.after(fromDate)){
                        txtRentDetailsBookingDate.setError("You Can Not Cancel Booking Now Rent Started...");
                        Toast.makeText(RentProductDetailsActivity.this, "You Can Not Cancel Booking Now Rent Started...", Toast.LENGTH_SHORT).show();
                    }
                    else{

                        //Toast.makeText(RentProductDetailsActivity.this, "Id"+bookedProductDetails.getBookingId(), Toast.LENGTH_SHORT).show();
                        AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(RentProductDetailsActivity.this);
                        alertDialog2.setTitle("Cancel Booking...");
                        alertDialog2.setMessage("Are you sure to cancel this booking ?");

                        alertDialog2.setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        deleteBooking();
                                        dialog.cancel();
                                    }
                                });
                        alertDialog2.setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                        dialog.cancel();
                                        //Toast.makeText(context, "Product not Received", Toast.LENGTH_SHORT).show();
                                    }
                                });

                        alertDialog2.show();

                    }


                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });

    }

    private void deleteBooking() {

        addDialog = new ProgressDialog(RentProductDetailsActivity.this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Canceling booking...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        try{
            UpdatedBookingDataRequest deleteBookingRequest = new UpdatedBookingDataRequest();

            deleteBookingRequest.setBookingId(bookedProductDetails.getBookingId());

            Call<UpdatedBookingDataResponse> responceCall = ApiClient.getApiClient().cancelBooking(deleteBookingRequest);

            responceCall.enqueue(new Callback<UpdatedBookingDataResponse>() {
                @Override
                public void onResponse(Call<UpdatedBookingDataResponse> call, retrofit2.Response<UpdatedBookingDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        //List<UpdatedBookingDataItem> items =response.body().getUpdatedBookingData();

                        Toast.makeText(RentProductDetailsActivity.this, "Booking Cancel Successfully...!", Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(RentProductDetailsActivity.this, RentListActivity.class);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        addDialog.dismiss();


                    } else {

                        Toast.makeText(RentProductDetailsActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                        addDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<UpdatedBookingDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialog.dismiss();

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    private void onlyUpdate() {

        strTotalRent = txtRentDetailsRent.getText().toString().trim();
        strBookingDate = txtRentDetailsBookingDate.getText().toString().trim();
        strReturnDate = txtRentDetailsReturnDate.getText().toString().trim();
        strPaidAmount = edtRentDetailsAdvance.getText().toString().trim();
        strRentPending = txtRentDetailsPending.getText().toString().trim();
        strRentTitle = txtRentTitle.getText().toString().trim();

        if(strRentTitle.equals("Daily Rent")){
            strDayRent = edtRentDetailsDayOrMonthRent.getText().toString().trim();
            strMonthRent = "0";
            strType = "Daily";
        }
        else{
            strMonthRent = edtRentDetailsDayOrMonthRent.getText().toString().trim();
            strDayRent = "0";
            strType = "Monthly";
        }
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // here set the pattern as you date in string was containing like date/month/year
            date1 = sdf.parse(strBookingDate);
            date2 = sdf.parse(strReturnDate);
            dayCount = (date2.getTime() - date1.getTime()) / (24 * 60 * 60 * 1000);
            //Toast.makeText(BookingActivity.this, "To Date"+date2+"\n From Date"+date1, Toast.LENGTH_LONG).show();
            //Toast.makeText(BookingActivity.this, "Day Count"+dayCount, Toast.LENGTH_SHORT).show();
            if (dayCount == 0.0f) {
                dayCount = 1.0f;
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        strDaysCount = String.valueOf(dayCount);
        //strBookingStatus = "2";

        //Toast.makeText(RentProductDetailsActivity.this, "Rent "+strTotalRent+" \n Total Days "+strDaysCount+" \n Booking Date "+strBookingDate+" \n Return Date "+strReturnDate+" \n Pending "+strRentPending+" \n Advance "+strPaidAmount+" \nStatus "+strBookingStatus, Toast.LENGTH_LONG).show();

        addDialog = new ProgressDialog(RentProductDetailsActivity.this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Updating Item...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        try{
            UpdatedBookingDataRequest updateItemRequest = new UpdatedBookingDataRequest();

            updateItemRequest.setBookingId(bookedProductDetails.getBookingId());
            updateItemRequest.setDaysCount(strDaysCount);
            updateItemRequest.setTotalRent(strRentOnly);
            updateItemRequest.setBookingFromDate(strBookingDate);
            updateItemRequest.setBookingToDate(strReturnDate);
            updateItemRequest.setRentAdvance(strPaidAmount);
            updateItemRequest.setRentPending(strRentPending);
            updateItemRequest.setBookingType(strType);
            updateItemRequest.setDailyRate(strDayRent);
            updateItemRequest.setMonthlyRate(strMonthRent);
            updateItemRequest.setBookingProdId(bookedProductDetails.getBookingProdId());
            updateItemRequest.setBookingCustId(bookedProductDetails.getBookingCustId());

            Call<UpdatedBookingDataResponse> responceCall = ApiClient.getApiClient().updateBookingOnly(updateItemRequest);

            responceCall.enqueue(new Callback<UpdatedBookingDataResponse>() {
                @Override
                public void onResponse(Call<UpdatedBookingDataResponse> call, retrofit2.Response<UpdatedBookingDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        List<UpdatedBookingDataItem> items =response.body().getUpdatedBookingData();

                        Intent intent = new Intent(RentProductDetailsActivity.this, RentListActivity.class);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        addDialog.dismiss();


                    } else {

                        Toast.makeText(RentProductDetailsActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                        addDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<UpdatedBookingDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialog.dismiss();

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
