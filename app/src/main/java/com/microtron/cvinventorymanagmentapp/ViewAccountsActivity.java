package com.microtron.cvinventorymanagmentapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.microtron.cvinventorymanagmentapp.Adapters.RentListAdapter;
import com.microtron.cvinventorymanagmentapp.Helper.ApiClient;
import com.microtron.cvinventorymanagmentapp.Helper.AppConstants;
import com.microtron.cvinventorymanagmentapp.Helper.Sp;
import com.microtron.cvinventorymanagmentapp.Helper.StaticClass;
import com.microtron.cvinventorymanagmentapp.NetworkModel.Customer.CustomerDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.Customer.CustomerDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.Customer.CustomerDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PendingAmounts.PendingAmountDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PendingAmounts.PendingAmountDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PendingAmounts.PendingAmountDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PreBookings.PreBookingsDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PreBookings.PreBookingsDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PreBookings.PreBookingsDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.SaleOrRentedProduct.SaleRentProductDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.SaleOrRentedProduct.SaleRentProductDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.SaleOrRentedProduct.SaleRentProductDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataResponse;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class ViewAccountsActivity extends AppCompatActivity {

    private TextView txtAllSaleProductCount, txtAllRentProductCount, txtAllCustomerCount, txtAllPendingCount;
    private CardView cardSaleProduct, cardRentProduct, cardCustomer, cardPending;
    private ImageView imgBack;
    private String strAllSaleProductCount, strAllRentProductCount, strAllCustCount, strAllPendingCount, strAllPreBooking;
    private ArrayList<String> listSaleProductCount = new ArrayList<String>();
    private ArrayList<String> listRentProductCount = new ArrayList<String>();
    private ArrayList<String> listCustomerCount = new ArrayList<String>();
    private ArrayList<String> listPendingCount = new ArrayList<String>();
    private ArrayList<String> listPreBookingCount = new ArrayList<String>();

    private List<SaleRentProductDataItem> listSaleProduct = new ArrayList<SaleRentProductDataItem>();
    private List<SaleRentProductDataItem> listRentProduct = new ArrayList<SaleRentProductDataItem>();
    private List<CustomerDataItem> listCustomer = new ArrayList<CustomerDataItem>();
    private List<PendingAmountDataItem> listPending = new ArrayList<PendingAmountDataItem>();
    private List<PreBookingsDataItem> listPreBooking = new ArrayList<PreBookingsDataItem>();

    private ProgressDialog addDialog;
    private String strProductSatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_accounts);

        txtAllSaleProductCount = findViewById(R.id.allSaleProductCount);
        txtAllRentProductCount = findViewById(R.id.allRentedProductCount);
        txtAllCustomerCount = findViewById(R.id.allCustomerCount);
        txtAllPendingCount = findViewById(R.id.allPendingCount);
        //txtAllPreBookingCount = findViewById(R.id.allPreBookingCount);

        imgBack = findViewById(R.id.imgBack);

        cardSaleProduct = findViewById(R.id.cardSaleProduct);
        cardRentProduct = findViewById(R.id.cardRentProduct);
        cardCustomer = findViewById(R.id.cardCustomer);
        cardPending = findViewById(R.id.cardPendings);
        //cardPreBooking = findViewById(R.id.cardPreBooking);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        getAllDetails();

        cardSaleProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Toast.makeText(ViewAccountsActivity.this, ""+listSaleProduct.get(1).getProductName(), Toast.LENGTH_SHORT).show();
                StaticClass.listSaleProducts = listSaleProduct;

                Intent intent = new Intent(ViewAccountsActivity.this, SaleProductsActivity.class);
                startActivity(intent);
                overridePendingTransition(0,0);
                finish();
            }
        });

        cardRentProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                StaticClass.listRentProduct = listRentProduct;
                Sp.saveToPreferences(ViewAccountsActivity.this, AppConstants.INTENT_TO, "RentViewAccount");
                Intent intent = new Intent(ViewAccountsActivity.this, RentListActivity.class);
                startActivity(intent);
                overridePendingTransition(0,0);
                finish();
            }
        });

        cardCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                StaticClass.listCustomerData = listCustomer;

                Intent intent = new Intent(ViewAccountsActivity.this, ViewCustomerActivity.class);
                startActivity(intent);
                overridePendingTransition(0,0);
                finish();
            }
        });

        cardPending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                StaticClass.listPendingAmount = listPending;

                Intent intent = new Intent(ViewAccountsActivity.this, ViewPendingAmountActivity.class);
                startActivity(intent);
                overridePendingTransition(0,0);
                finish();
            }
        });

        /*cardPreBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                StaticClass.listPreBooking = listPreBooking;

                *//*Intent intent = new Intent(ViewAccountsActivity.this, ViewPreBookingProductActivity.class);
                startActivity(intent);
                overridePendingTransition(0,0);
                finish();*//*
            }
        });*/

    }

    private void getAllDetails() {

        addDialog = new ProgressDialog(this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Getting Details...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        getSaleProduct();
        getRentProduct();
        getCustomer();
        getPendings();
        /*getPreBooking();*/

        addDialog.dismiss();

    }

    private void getSaleProduct() {

        strProductSatus = "Sale";

        try{
            SaleRentProductDataRequest saleRentProductDataRequest = new SaleRentProductDataRequest();
            saleRentProductDataRequest.setProductStatus(strProductSatus);

            Call<SaleRentProductDataResponse> responceCall = ApiClient.getApiClient().getSaleOrRentProduct(saleRentProductDataRequest);

            responceCall.enqueue(new Callback<SaleRentProductDataResponse>() {
                @Override
                public void onResponse(Call<SaleRentProductDataResponse> call, retrofit2.Response<SaleRentProductDataResponse> response) {

                    if (response.body().getSuccess().equals("1")) {

                        List<SaleRentProductDataItem> items =response.body().getSaleRentProductData();
                        //listSaleProduct = items;

                        for(int i=0; i<items.size(); i++){

                            listSaleProductCount.add(items.get(i).getProductId());

                        }
                        strAllSaleProductCount = String.valueOf(listSaleProductCount.size());

                        txtAllSaleProductCount.setText(strAllSaleProductCount);

                    } else {

                        //Toast.makeText(ViewAccountsActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<SaleRentProductDataResponse> call, Throwable t) {

                    //Toast.makeText(ViewAccountsActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    private void getRentProduct() {

        strProductSatus = "Rented";

        try{
            SaleRentProductDataRequest saleRentProductDataRequest = new SaleRentProductDataRequest();
            saleRentProductDataRequest.setProductStatus(strProductSatus);

            Call<SaleRentProductDataResponse> responceCall = ApiClient.getApiClient().getSaleOrRentProduct(saleRentProductDataRequest);

            responceCall.enqueue(new Callback<SaleRentProductDataResponse>() {
                @Override
                public void onResponse(Call<SaleRentProductDataResponse> call, retrofit2.Response<SaleRentProductDataResponse> response) {

                    if (response.body().getSuccess().equals("1")) {

                        List<SaleRentProductDataItem> items =response.body().getSaleRentProductData();
                        listRentProduct = items;
                        for(int i=0; i<items.size(); i++){

                            listRentProductCount.add(items.get(i).getProductId());

                        }
                        strAllRentProductCount = String.valueOf(listRentProductCount.size());

                        txtAllRentProductCount.setText(strAllRentProductCount);

                    } else {

                        //Toast.makeText(ViewAccountsActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<SaleRentProductDataResponse> call, Throwable t) {

                    //Toast.makeText(ViewAccountsActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    private void getCustomer() {

        try{
            CustomerDataRequest customerDataRequest = new CustomerDataRequest();

            Call<CustomerDataResponse> responceCall = ApiClient.getApiClient().getCustomer(customerDataRequest);

            responceCall.enqueue(new Callback<CustomerDataResponse>() {
                @Override
                public void onResponse(Call<CustomerDataResponse> call, retrofit2.Response<CustomerDataResponse> response) {

                    if (response.body().getSuccess().equals("1")) {

                        List<CustomerDataItem> items =response.body().getCustomerData();
                        listCustomer = items;

                        for(int i=0; i<items.size(); i++){

                            listCustomerCount.add(items.get(i).getCustomerId());

                        }
                        strAllCustCount = String.valueOf(listCustomerCount.size());

                        txtAllCustomerCount.setText(strAllCustCount);

                    } else {

                        //Toast.makeText(ViewAccountsActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<CustomerDataResponse> call, Throwable t) {

                    //Toast.makeText(ViewAccountsActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    private void getPendings() {

        try{
            PendingAmountDataRequest pendingAmountDataRequest = new PendingAmountDataRequest();

            Call<PendingAmountDataResponse> responceCall = ApiClient.getApiClient().getPendings(pendingAmountDataRequest);

            responceCall.enqueue(new Callback<PendingAmountDataResponse>() {
                @Override
                public void onResponse(Call<PendingAmountDataResponse> call, retrofit2.Response<PendingAmountDataResponse> response) {

                    if (response.body().getSuccess().equals("1")) {

                        List<PendingAmountDataItem> items =response.body().getPendingAmountData();
                        listPending = items;

                        for(int i=0; i<items.size(); i++){

                            listPendingCount.add(items.get(i).getCustomerId());

                        }
                        strAllPendingCount = String.valueOf(listPendingCount.size());

                        txtAllPendingCount.setText(strAllPendingCount);

                    } else {

                        //Toast.makeText(ViewAccountsActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<PendingAmountDataResponse> call, Throwable t) {

                    //Toast.makeText(ViewAccountsActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    /*private void getPreBooking() {

        try{
            PreBookingsDataRequest preBookingsDataRequest = new PreBookingsDataRequest();

            Call<PreBookingsDataResponse> responceCall = ApiClient.getApiClient().getPreBookings(preBookingsDataRequest);

            responceCall.enqueue(new Callback<PreBookingsDataResponse>() {
                @Override
                public void onResponse(Call<PreBookingsDataResponse> call, retrofit2.Response<PreBookingsDataResponse> response) {

                    if (response.body().getSuccess().equals("1")) {

                        List<PreBookingsDataItem> items =response.body().getPreBookingsData();
                        listPreBooking = items;

                        for(int i=0; i<items.size(); i++){

                            listPreBookingCount.add(items.get(i).getCustomerId());

                        }
                        strAllPreBooking = String.valueOf(listPreBookingCount.size());

                        txtAllPreBookingCount.setText(strAllPreBooking);

                    } else {

                        //Toast.makeText(ViewAccountsActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<PreBookingsDataResponse> call, Throwable t) {

                    //Toast.makeText(ViewAccountsActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }*/

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
    }
}
