package com.microtron.cvinventorymanagmentapp.Helper;

import com.microtron.cvinventorymanagmentapp.NetworkModel.Customer.CustomerDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.GetAllProducts.AllProductsDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.OutSideInventoryBooking.OutSideBookingDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PendingAmounts.PendingAmountDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PreBookings.PreBookingsDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.SaleOrRentedProduct.SaleRentProductDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.TransactionHistory.TransactionHistoryDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.TransactionHistory.TransactionHistoryDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataItem;

import java.util.List;

public class StaticClass {

    public static AllProductsDataItem product;
    public static BookedItemDataItem bookedProduct;
    public static OutSideBookingDataItem outSideDetailsProduct;
    public static List<BookedItemDataItem> listMonthBooking;
    public static List<SaleRentProductDataItem> listSaleProducts;
    public static List<SaleRentProductDataItem> listRentProduct;
    public static List<CustomerDataItem> listCustomerData;
    public static List<PendingAmountDataItem> listPendingAmount;
    public static List<PreBookingsDataItem> listPreBooking;

    public static TransactionHistoryDataItem transactionHistoryDetails;

}
