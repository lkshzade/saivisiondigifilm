package com.microtron.cvinventorymanagmentapp.Helper;

public class ConfigFile {

    public static final String domain = "http://aadishaktitelemedia.com";
    public static final String domaindir = "/ShopAppScanner/";

    public static final String sendOTP = "sendOTP.php";
    public static final String sendChangePassOTP = "sendChangePassOTP.php";
    public static final String sendPass = "sendPass.php";
    public static final String changePassUrl = "changePass.php";

    public static final String saveAdmin="ShopAppScanner/registration.php";
    public static final String logInAdmin="ShopAppScanner/login.php";
    public static final String uploadAdminProfileImg = "uploadAdminProfileImg.php";
    public static final String uploadProductImages = "uploadProductImages.php";
    public static final String updateAdminProfile = "ShopAppScanner/updateProfile.php";
    public static final String getCompany = "ShopAppScanner/getAddItemCompany.php";
    public static final String addCompany = "ShopAppScanner/addCompany.php";
    public static final String getCategory = "ShopAppScanner/getAddItemCategory.php";
    public static final String addCategory = "ShopAppScanner/addCategory.php";
    public static final String getSubCategory = "ShopAppScanner/getAddItemSubCategory.php";
    public static final String addSubCategory = "ShopAppScanner/addSubCategory.php";
    public static final String addItem = "ShopAppScanner/addItem.php";
    public static final String updateItem = "ShopAppScanner/updateItem.php";
    public static final String deleteItem = "ShopAppScanner/deleteItem.php";
    public static final String bookItem = "ShopAppScanner/bookItem.php";
    public static final String getScanProduct = "ShopAppScanner/getScanProduct.php";
    public static final String getRentList = "ShopAppScanner/getRentList.php";
    public static final String getTransactionHistory = "ShopAppScanner/getTransactionHistory.php";
    public static final String getRentListDates = "ShopAppScanner/getRentListDates.php";
    public static final String getRentMonthList = "ShopAppScanner/getListByMonth.php";
    public static final String getAllProducts = "ShopAppScanner/getAllProducts.php";
    public static final String updateBookingStatus = "ShopAppScanner/updatBookingStatusQuantity.php";
    public static final String updateBookingProductStatus = "ShopAppScanner/updateBookingProductStatus.php";
    public static final String receivedBookingStatus = "ShopAppScanner/receivedBookingStatus.php";
    public static final String bookOutSideItem = "ShopAppScanner/outSideInventoryItem.php";
    public static final String updateOutSideItem = "ShopAppScanner/updateOutSideInventoryItem.php";
    public static final String updateBookingItem = "ShopAppScanner/updateBookingItem.php";
    public static final String updateBookingPending = "ShopAppScanner/updateBookingPending.php";
    public static final String updateBookingOnly = "ShopAppScanner/updateBookingOnly.php";
    public static final String cancelBooking = "ShopAppScanner/cancelBooking.php";
    public static final String getOutSideItem = "ShopAppScanner/getOutSideInventoryItem.php";

    public static final String getSaleOrRentProduct = "ShopAppScanner/getSaleOrRentProduct.php";
    public static final String getSaleItemRequest = "ShopAppScanner/getSaleItemList.php";
    public static final String getCustomer = "ShopAppScanner/getCustomer.php";
    public static final String getPendingCustomer = "ShopAppScanner/getPendingCustomer.php";
    public static final String getPendings = "ShopAppScanner/getPendings.php";
    public static final String getPendingProducts = "ShopAppScanner/getPendingProducts.php";
    public static final String getPreBookings = "ShopAppScanner/getPreBookings.php";
    public static final String getPreFromDateData = "ShopAppScanner/getPreFromDateData.php";
    public static final String getPreToDateData = "ShopAppScanner/getPreToDateData.php";
    public static final String getSaleDateData = "ShopAppScanner/getSaleDateData.php";
    public static final String updatePendingsFromViewAccount = "ShopAppScanner/updatePendingAmtProd.php";
    public static final String updateProductFromViewAccount = "ShopAppScanner/updateCustProduct.php";
    public static final String receivedFromViewAccount = "ShopAppScanner/receivedFromViewAccount.php";
    public static final String getNotification = "ShopAppScanner/getNotificationData.php";
    public static final String updateNotification = "ShopAppScanner/updateNotification.php";

}
