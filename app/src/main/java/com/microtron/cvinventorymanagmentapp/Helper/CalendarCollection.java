package com.microtron.cvinventorymanagmentapp.Helper;

import java.util.ArrayList;

public class CalendarCollection {
    public String toDate="";
    public String fromDate="";
    public String custName="";
    public String rent="";
    public String prodName="";


    public static ArrayList<CalendarCollection> date_collection_arr;
    public CalendarCollection(String fromDate, String toDate, String custName, String rent, String prodName){

        this.toDate=toDate;
        this.fromDate=fromDate;
        this.custName=custName;
        this.rent= rent;
        this.prodName= prodName;

    }
}