package com.microtron.cvinventorymanagmentapp.Helper;

import com.microtron.cvinventorymanagmentapp.NetworkModel.AddItem.AddItemDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.AddItem.AddItemDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.BookingItem.BookingDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.BookingItem.BookingDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.Category.CategoryDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.Category.CategoryDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.Comapny.CompanyDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.Comapny.CompanyDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.Customer.CustomerDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.Customer.CustomerDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.GetAllProducts.AllProductsDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.GetAllProducts.AllProductsDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.GetScanProduct.GetScanProductDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.GetScanProduct.GetScanProductDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.NotificationData.NotificationDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.NotificationData.NotificationDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.OutSideInventoryBooking.OutSideBookingDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.OutSideInventoryBooking.OutSideBookingDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PendingAmounts.PendingAmountDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PendingAmounts.PendingAmountDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PreBookings.PreBookingsDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PreBookings.PreBookingsDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PreDateBookingData.PreDateDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PreDateBookingData.PreDateDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PreFromDateData.PreFromDateDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PreFromDateData.PreFromDateDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PreToDateData.PreToDateDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PreToDateData.PreToDateDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.RegisteLogInUpdate.RegisterDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.RegisteLogInUpdate.RegisterDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.SaleDateData.SaleDateDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.SaleDateData.SaleDateDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.SaleItemData.SaleItemDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.SaleItemData.SaleItemDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.SaleOrRentedProduct.SaleRentProductDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.SaleOrRentedProduct.SaleRentProductDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.SubCategory.SubCategoryDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.SubCategory.SubCategoryDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.TransactionHistory.TransactionHistoryDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.TransactionHistory.TransactionHistoryDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.UpdatBookedItem.UpdatedBookingDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.UpdatBookedItem.UpdatedBookingDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.UpdatePendings.UpdatedCustPendingsDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.UpdatePendings.UpdatedCustPendingsDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiInterface {

        /*@POST(AppConstants.getSubCat)
        Call<GettingSubCategoryResponse> getSubCat(@Body GettingSubCategoryRequest getSubCategoryRequest);

        @POST(AppConstants.getWishHeart)
        Call<GetDeleteWishResponse> getWish(@Body GetdeleteWishRequest getWishRequest);

        @POST(AppConstants.deleteWishHeart)
        Call<GetDeleteWishResponse> deleteWish(@Body GetdeleteWishRequest getWishRequest);

        @POST(AppConstants.getAdminNotification)
        Call<AdminNotificationResponse> getAdminNotification(@Body AdminNotificationRequest getUserNotificationRequest);*/


    @POST(ConfigFile.saveAdmin)
    Call<RegisterDataResponse> addAdmin(@Body RegisterDataRequest registerDataRequest);

    @POST(ConfigFile.logInAdmin)
    Call<RegisterDataResponse> logInAdmin(@Body RegisterDataRequest logInDataRequest);

    @POST(ConfigFile.updateAdminProfile)
    Call<RegisterDataResponse> updateAdmin(@Body RegisterDataRequest updateProfileRequest);

    @POST(ConfigFile.getCompany)
    Call<CompanyDataResponse> getCompany(@Body CompanyDataRequest companyDataRequest);

    @POST(ConfigFile.addCompany)
    Call<CompanyDataResponse> addCompany(@Body CompanyDataRequest companyDataRequest);

    @POST(ConfigFile.getCategory)
    Call<CategoryDataResponse> getCategory(@Body CategoryDataRequest categoryDataRequest);

    @POST(ConfigFile.addCategory)
    Call<CategoryDataResponse> addCategory(@Body CategoryDataRequest companyDataRequest);

    @POST(ConfigFile.getSubCategory)
    Call<SubCategoryDataResponse> getSubCategory(@Body SubCategoryDataRequest subCategoryDataRequest);

    @POST(ConfigFile.addSubCategory)
    Call<SubCategoryDataResponse> addSubCategory(@Body SubCategoryDataRequest subCategoryDataRequest);

    @POST(ConfigFile.addItem)
    Call<AddItemDataResponse> addItem(@Body AddItemDataRequest addItemRequest);

    @POST(ConfigFile.updateItem)
    Call<AddItemDataResponse> updateItem(@Body AddItemDataRequest addItemRequest);

    @POST(ConfigFile.deleteItem)
    Call<AddItemDataResponse> deleteItem(@Body AddItemDataRequest addItemRequest);

    @POST(ConfigFile.getScanProduct)
    Call<GetScanProductDataResponse> getScanProduct(@Body GetScanProductDataRequest getScanProductRequest);

    @POST(ConfigFile.bookItem)
    Call<BookingDataResponse> bookItem(@Body BookingDataRequest bookItemRequest);

    @POST(ConfigFile.getRentList)
    Call<BookedItemDataResponse> getRentList(@Body BookedItemDataRequest getRentListRequest);

    @POST(ConfigFile.getTransactionHistory)
    Call<TransactionHistoryDataResponse> getTransactionHistory(@Body TransactionHistoryDataRequest getTransactionHistoryRequest);

    @POST(ConfigFile.getRentListDates)
    Call<PreDateDataResponse> getRentListDates(@Body PreDateDataRequest getRentDateListRequest);

    @POST(ConfigFile.getRentMonthList)
    Call<BookedItemDataResponse> getRentMonthList(@Body BookedItemDataRequest getRentListRequest);

    @POST(ConfigFile.getAllProducts)
    Call<AllProductsDataResponse> getAllProducts(@Body AllProductsDataRequest getAllProductRequest);

    @POST(ConfigFile.updateBookingProductStatus)
    Call<BookingDataResponse> updateBookingProductStatus(@Body BookingDataRequest getAllProductRequest);

    @POST(ConfigFile.receivedBookingStatus)
    Call<BookingDataResponse> receivedBookingStatus(@Body BookingDataRequest getAllProductRequest);

    @POST(ConfigFile.updatePendingsFromViewAccount)
    Call<UpdatedCustPendingsDataResponse> updatePendingsFromViewAccount(@Body UpdatedCustPendingsDataRequest custPendingsDataRequest);

    @POST(ConfigFile.updateProductFromViewAccount)
    Call<UpdatedCustPendingsDataResponse> updateProductFromViewAccount(@Body UpdatedCustPendingsDataRequest custPendingsDataRequest);

    @POST(ConfigFile.receivedFromViewAccount)
    Call<UpdatedCustPendingsDataResponse> receivedFromViewAccount(@Body UpdatedCustPendingsDataRequest custPendingsDataRequest);

    @POST(ConfigFile.bookOutSideItem)
    Call<OutSideBookingDataResponse> bookOutSideItem(@Body OutSideBookingDataRequest outSideBooking);

    @POST(ConfigFile.updateOutSideItem)
    Call<OutSideBookingDataResponse> updateOutSideItem(@Body OutSideBookingDataRequest outSideUpdate);

    @POST(ConfigFile.getOutSideItem)
    Call<OutSideBookingDataResponse> getOutSideItem(@Body OutSideBookingDataRequest getoutSideBooking);

    @POST(ConfigFile.getNotification)
    Call<NotificationDataResponse> getNotification(@Body NotificationDataRequest notificationRequest);

    @POST(ConfigFile.updateNotification)
    Call<NotificationDataResponse> updateNotification(@Body NotificationDataRequest notificationRequest);

    @POST(ConfigFile.updateBookingPending)
    Call<UpdatedBookingDataResponse> updateBookingPending(@Body UpdatedBookingDataRequest updateBookingItem);

    @POST(ConfigFile.updateBookingOnly)
    Call<UpdatedBookingDataResponse> updateBookingOnly(@Body UpdatedBookingDataRequest updateBookingItem);

    @POST(ConfigFile.cancelBooking)
    Call<UpdatedBookingDataResponse> cancelBooking(@Body UpdatedBookingDataRequest updateBookingItem);

    @POST(ConfigFile.getSaleOrRentProduct)
    Call<SaleRentProductDataResponse> getSaleOrRentProduct(@Body SaleRentProductDataRequest saleRentProductDataRequest);

    @POST(ConfigFile.getSaleItemRequest)
    Call<SaleItemDataResponse> getSaleItemRequest(@Body SaleItemDataRequest saleRentProductDataRequest);

    @POST(ConfigFile.getCustomer)
    Call<CustomerDataResponse> getCustomer(@Body CustomerDataRequest customerDataRequest);

    @POST(ConfigFile.getPendingCustomer)
    Call<CustomerDataResponse> getPendingCustomer(@Body CustomerDataRequest customerDataRequest);

    @POST(ConfigFile.getPendings)
    Call<PendingAmountDataResponse> getPendings(@Body PendingAmountDataRequest pendingAmountDataRequest);

    @POST(ConfigFile.getPreBookings)
    Call<PreBookingsDataResponse> getPreBookings(@Body PreBookingsDataRequest preBookingsDataRequest);

    @POST(ConfigFile.getPreFromDateData)
    Call<PreFromDateDataResponse> getPreFromDateData(@Body PreFromDateDataRequest preFromDateDataRequest);

    @POST(ConfigFile.getPreToDateData)
    Call<PreToDateDataResponse> getPreToDateData(@Body PreToDateDataRequest preToDateDataRequest);

    @POST(ConfigFile.getSaleDateData)
    Call<SaleDateDataResponse> getSaleDateData(@Body SaleDateDataRequest saleDateDataRequest);

    @POST(ConfigFile.getPendingProducts)
    Call<PendingAmountDataResponse> getPendingProducts(@Body PendingAmountDataRequest pendingAmountDataRequest);

}
