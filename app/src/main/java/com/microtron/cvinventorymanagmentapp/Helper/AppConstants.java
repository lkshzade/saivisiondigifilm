package com.microtron.cvinventorymanagmentapp.Helper;

public class AppConstants {

    public static String ADMIN_ID = "admin_id";
    public static String ADMIN_NAME = "admin_name";
    public static String ADMIN_EMAIL= "admin_email";
    public static String ADMIN_MOBILE= "admin_mobile";
    public static String ADMIN_ADDRESS= "admin_address";
    public static String ADMIN_PASSWORD= "admin_pass";
    public static String ADMIN_SHOPNAME= "admin_shopName";
    public static String ADMIN_SHOPTYPE= "admin_shopType";
    public static String ADMIN_OTP= "admin_OTP";
    public static String ADMIN_SESSION= "CANCEL";
    public static String ADMIN_IMGURL= "admin_img";



    public static String PRODUCT_ID= "product_id";
    public static String PRODUCT_COMP_ID= "product_compid";
    public static String PRODUCT_CAT_ID= "product_catid";
    public static String PRODUCT_SUBCAT_ID= "product_subcatid";
    public static String PRODUCT_COMP_NAME= "product_compname";
    public static String PRODUCT_CAT_NAME= "product_catname";
    public static String PRODUCT_SUBCAT_NAME= "product_subcatname";
    public static String PRODUCT_NAME= "product_name";
    public static String PRODUCT_DESCRIPTION= "product_desc";
    public static String PRODUCT_INSTOCK= "product_instock";
    public static String PRODUCT_STOCKQTY= "product_stockqty";

    public static String CUST_ID = "cust_id";
    public static String CUST_NAME = "cust_name";
    public static String CUST_EMAIL= "cust_email";
    public static String CUST_MOBILE= "cust_mobile";


    public static String INTENT_TO= "";
    public static String NOTIFY_FROM= "";



    public static final String WEB_HOST_URL="http://aadishaktitelemedia.com/";



    public static String FCM_TOKEN = "firebase_token";

}
