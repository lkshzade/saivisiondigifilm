package com.microtron.cvinventorymanagmentapp;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.microtron.cvinventorymanagmentapp.Helper.ApiClient;
import com.microtron.cvinventorymanagmentapp.NetworkModel.BookingItem.BookingDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.BookingItem.BookingDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.BookingItem.BookingDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.OutSideInventoryBooking.OutSideBookingDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.OutSideInventoryBooking.OutSideBookingDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.OutSideInventoryBooking.OutSideBookingDataResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class OutSideInventoryActivity extends AppCompatActivity {

    private EditText edtSalerProductName, edtSalerCompanyName, edtSalerProductPrice, edtSalerShopName, edtSalerName, edtSalerMobile, edtRentMonthly, edtRentDaily, edtAdvance, edtCustName, edtCustEmail, edtCustMobile;
    private TextView txtSalerDate, txtFromDate, txtToDate, txtTotalRent, txtPending;
    private TextView txtValidCompanyName,txtValidProductName, txtValidProductPrice, txtValidProductDate, txtValidSalerShopName, txtValidSalerName, txtValidSalerMobile, txtValidRentMonthly, txtValidRentDaily, txtValidFromDate, txtValidToDate, txtValidAdvance, txtValidCustName, txtValidCustEmail, txtValidCustMobile;
    private RadioGroup rdgBookingType;
    private LinearLayout llMonthlyRent, llDailyRent, llFromDate, llToDate, llTotalRent, llAdvance, llPending;
    private ImageView imgBack;
    private Button btnBookItem;
    private Date date1, date2;
    private float dayCount = 0.0f;
    private String strSalerCompanyName = "", strSalerProductName = "", strSalerProductPrice = "", strSalerProductDate = "", strSalerShopName = "", strSalerName = "", strSalerMobile = "", strBookingType = "", strRentDaily = "", strRentMonthly = "", strFromDate = "0", strToDate = "0", strAdvanceAmount = "", strPendingAmount = "0", strCustName = "", strCustEmail = "", strCustMobile = "", strTotalRent;
    private int mSalerDateYear, mSalerDateMonth, mSalerDateDay, mFromYear, mFromMonth, mFromDay, mToYear, mToMonth, mToDay;
    private String strSalerDateYear, strSalerDateMonth, strSalerDateDay, strFromYear, strFromMonth, strFromDay, strToYear, strToMonth, strToDay, strToMonthChange, strToDayChange;
    private float rentAdvance = 1.0f, rentPending = 0.0f;
    float totalRent;
    ProgressDialog addDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_out_side_inventory);

        edtSalerCompanyName = findViewById(R.id.edtSalerCompName);
        edtSalerProductName = findViewById(R.id.edtSalerProdName);
        edtSalerProductPrice = findViewById(R.id.edtSalerPrice);
        txtSalerDate = findViewById(R.id.txtSalerDate);

        txtValidCompanyName = findViewById(R.id.txtValidSalerCompName);
        txtValidProductName = findViewById(R.id.txtValidSalerProdName);
        txtValidProductPrice = findViewById(R.id.txtValidSalerPrice);
        txtValidProductDate = findViewById(R.id.txtValidSalerDate);

        edtSalerShopName = findViewById(R.id.edtSalerShopName);
        edtSalerName = findViewById(R.id.edtSalerName);
        edtSalerMobile = findViewById(R.id.edtSalerMobile);

        txtValidSalerShopName = findViewById(R.id.txtValidSalerShopName);
        txtValidSalerName = findViewById(R.id.txtValidSalerName);
        txtValidSalerMobile = findViewById(R.id.txtValidSalerMobile);

        rdgBookingType = findViewById(R.id.rdgSalerBookingType);
        edtRentMonthly = findViewById(R.id.edtMonthRent);
        edtRentDaily = findViewById(R.id.edtDayRent);
        txtFromDate = findViewById(R.id.txtFromDate);
        txtToDate = findViewById(R.id.txtToDate);
        txtTotalRent = findViewById(R.id.txtTotalRent);
        edtAdvance = findViewById(R.id.edtAdvance);
        txtPending = findViewById(R.id.txtPending);

        txtValidRentMonthly = findViewById(R.id.txtValidMonthRent);
        txtValidRentDaily = findViewById(R.id.txtValidDayRent);
        txtValidFromDate = findViewById(R.id.txtValidFromDate);
        txtValidToDate = findViewById(R.id.txtValidToDate);
        txtValidAdvance= findViewById(R.id.txtValidAdvance);

        edtCustName = findViewById(R.id.edtCustName);
        edtCustMobile = findViewById(R.id.edtCustMobile);
        edtCustEmail = findViewById(R.id.edtCustEmail);

        txtValidCustName = findViewById(R.id.txtValidCustName);
        txtValidCustMobile = findViewById(R.id.txtValidCustMobile);
        txtValidCustEmail = findViewById(R.id.txtValidCustEmail);

        llMonthlyRent = findViewById(R.id.llMonthlyRent);
        llDailyRent = findViewById(R.id.llDailyRent);
        llFromDate = findViewById(R.id.llFromDate);
        llToDate = findViewById(R.id.llToDate);
        llTotalRent = findViewById(R.id.llTotalRent);
        llAdvance = findViewById(R.id.llAdvance);
        llPending = findViewById(R.id.llSalerPending);

        imgBack = findViewById(R.id.imgBack);
        btnBookItem = findViewById(R.id.btnBookItem);

        llMonthlyRent.setVisibility(View.GONE);
        llDailyRent.setVisibility(View.GONE);
        llFromDate.setVisibility(View.GONE);
        llToDate.setVisibility(View.GONE);
        llTotalRent.setVisibility(View.GONE);
        llAdvance.setVisibility(View.GONE);
        llPending.setVisibility(View.GONE);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        txtSalerDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Calendar c = Calendar.getInstance();
                mSalerDateYear = c.get(Calendar.YEAR);
                mSalerDateMonth = c.get(Calendar.MONTH);
                mSalerDateDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(OutSideInventoryActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                if (dayOfMonth <= 9) {
                                    strSalerDateDay = "0" + dayOfMonth;
                                } else {
                                    strSalerDateDay = "" + dayOfMonth;
                                }
                                monthOfYear = monthOfYear + 1;
                                strSalerDateYear = String.valueOf(year);

                                if (monthOfYear <= 9) {
                                    strSalerDateMonth = "0" + monthOfYear;
                                } else {
                                    strSalerDateMonth = "" + monthOfYear;
                                }

                                txtSalerDate.setText(strSalerDateYear + "-" + strSalerDateMonth + "-" + strSalerDateDay);

                            }
                        }, mSalerDateYear, mSalerDateMonth, mSalerDateDay);


                datePickerDialog.show();
            }
        });

        rdgBookingType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.rbSalerMonthly:

                        strBookingType = "Monthly";
                        llMonthlyRent.setVisibility(View.VISIBLE);
                        llDailyRent.setVisibility(View.GONE);
                        llFromDate.setVisibility(View.GONE);
                        llToDate.setVisibility(View.GONE);
                        llTotalRent.setVisibility(View.GONE);
                        llAdvance.setVisibility(View.GONE);
                        llPending.setVisibility(View.GONE);
                        txtFromDate.setText("");
                        txtToDate.setText("");
                        txtTotalRent.setText("");
                        edtAdvance.setText("");
                        txtTotalRent.setText("");
                        txtPending.setText("");
                        edtRentDaily.setText("0");
                        edtRentMonthly.setText("");
                        break;
                    case R.id.rbSalerDaily:

                        strBookingType = "Daily";
                        llMonthlyRent.setVisibility(View.GONE);
                        llDailyRent.setVisibility(View.VISIBLE);
                        llFromDate.setVisibility(View.GONE);
                        llToDate.setVisibility(View.GONE);
                        llTotalRent.setVisibility(View.GONE);
                        llAdvance.setVisibility(View.GONE);
                        llPending.setVisibility(View.GONE);
                        txtFromDate.setText("");
                        txtToDate.setText("");
                        txtTotalRent.setText("");
                        edtAdvance.setText("");
                        txtTotalRent.setText("");
                        txtPending.setText("");
                        edtRentDaily.setText("");
                        edtRentMonthly.setText("0");
                        break;
                }
            }
        });

        edtRentDaily.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                strRentDaily = edtRentDaily.getText().toString().trim();

                if(strRentDaily.equals("")){
                    llFromDate.setVisibility(View.GONE);
                    llToDate.setVisibility(View.GONE);
                    llTotalRent.setVisibility(View.GONE);
                    llAdvance.setVisibility(View.GONE);
                    llPending.setVisibility(View.GONE);
                }else if(strRentDaily.equals("0")){
                    llFromDate.setVisibility(View.GONE);
                    llToDate.setVisibility(View.GONE);
                    llTotalRent.setVisibility(View.GONE);
                    llAdvance.setVisibility(View.GONE);
                    llPending.setVisibility(View.GONE);
                }
                else{
                    llFromDate.setVisibility(View.VISIBLE);
                    llToDate.setVisibility(View.VISIBLE);
                    llTotalRent.setVisibility(View.VISIBLE);
                    llAdvance.setVisibility(View.VISIBLE);
                    llPending.setVisibility(View.VISIBLE);
                    txtPending.setEnabled(false);
                    txtTotalRent.setEnabled(false);
                }

            }
        });

        edtRentMonthly.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                strRentMonthly = edtRentMonthly.getText().toString().trim();

                if(strRentMonthly.equals("")){
                    llFromDate.setVisibility(View.GONE);
                    llToDate.setVisibility(View.GONE);
                    llTotalRent.setVisibility(View.GONE);
                    llAdvance.setVisibility(View.GONE);
                    llPending.setVisibility(View.GONE);
                }
                else if(strRentMonthly.equals("0")){
                    llFromDate.setVisibility(View.GONE);
                    llToDate.setVisibility(View.GONE);
                    llTotalRent.setVisibility(View.GONE);
                    llAdvance.setVisibility(View.GONE);
                    llPending.setVisibility(View.GONE);
                }
                else{
                    llFromDate.setVisibility(View.VISIBLE);
                    llToDate.setVisibility(View.VISIBLE);
                    llTotalRent.setVisibility(View.VISIBLE);
                    llAdvance.setVisibility(View.VISIBLE);
                    llPending.setVisibility(View.VISIBLE);
                    txtPending.setEnabled(false);
                    txtTotalRent.setEnabled(false);
                }

            }
        });


        txtFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                mFromYear = c.get(Calendar.YEAR);
                mFromMonth = c.get(Calendar.MONTH);
                mFromDay = c.get(Calendar.DAY_OF_MONTH);
                strToMonthChange = "";
                strToDayChange = "";

                DatePickerDialog datePickerDialog = new DatePickerDialog(OutSideInventoryActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                if (dayOfMonth <= 9) {
                                    strFromDay = "0" + dayOfMonth;
                                } else {
                                    strFromDay = "" + dayOfMonth;
                                }
                                monthOfYear = monthOfYear + 1;
                                strFromYear = String.valueOf(year);

                                if (monthOfYear <= 9) {
                                    strFromMonth = "0" + monthOfYear;
                                } else {
                                    strFromMonth = "" + monthOfYear;
                                }

                                txtFromDate.setText(strFromYear + "-" + strFromMonth + "-" + strFromDay);
                                strRentMonthly = edtRentMonthly.getText().toString().trim();
                                strFromDate = txtFromDate.getText().toString().trim();

                                if(strBookingType.equals("Monthly")){

                                    int fromYear = Integer.parseInt(strFromYear);
                                    int fromDay = Integer.parseInt(strFromDay);
                                    int fromMonth = Integer.parseInt(strFromMonth);


                                    Calendar calendar = Calendar.getInstance();
                                    calendar.set(fromYear, Calendar.FEBRUARY, 1);
                                    int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
                                    //Toast.makeText(BookingActivity.this, ""+maxDay, Toast.LENGTH_SHORT).show();

                                    int toDay = fromDay;
                                    int toMonth = fromMonth + 1;
                                    int toYear;

                                    if(fromMonth >=12){
                                        toMonth = 01;
                                        toYear = fromYear + 1;
                                    }
                                    else {
                                        toYear = fromYear;
                                    }

                                    if (toDay <= 9) {
                                        strToDayChange = "0" + toDay;
                                    } else {
                                        strToDayChange = "" + toDay;
                                    }

                                    if (toMonth <= 9) {
                                        strToMonthChange = "0" + toMonth;
                                    } else {
                                        strToMonthChange = "" + toMonth;
                                    }

                                    if(strToMonthChange.equals("02")){
                                        if(fromDay == 29 && maxDay ==29){
                                            strToDayChange = "29";
                                        }
                                        else if(fromDay == 30 && maxDay ==29){
                                            strToDayChange = "01";
                                            strToMonthChange = "03";
                                        }
                                        else if(fromDay == 31 && maxDay ==29){
                                            strToDayChange = "02";
                                            strToMonthChange = "03";
                                        }
                                        if(fromDay == 28 && maxDay ==28){
                                            strToDayChange = "28";
                                        }
                                        else if(fromDay == 29 && maxDay ==28){
                                            strToDayChange = "01";
                                            strToMonthChange = "03";
                                        }
                                        else if(fromDay == 30 && maxDay ==28){
                                            strToDayChange = "02";
                                            strToMonthChange = "03";
                                        }
                                        else if(fromDay == 31 && maxDay ==28){
                                            strToDayChange = "03";
                                            strToMonthChange = "03";
                                        }
                                    }


                                    txtToDate.setText(toYear + "-" + strToMonthChange + "-" + strToDayChange);
                                    strToDate = txtToDate.getText().toString().trim();

                                    totalRent = (Float.parseFloat(strRentMonthly))*(1);
                                    txtTotalRent.setText(""+totalRent);


                                }

                            }
                        }, mFromYear, mFromMonth, mFromDay);



                datePickerDialog.show();
            }
        });


        txtToDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                mToYear = c.get(Calendar.YEAR);
                mToMonth = c.get(Calendar.MONTH);
                mToDay = c.get(Calendar.DAY_OF_MONTH);

                strRentDaily = edtRentDaily.getText().toString().trim();

                DatePickerDialog datePickerDialog = new DatePickerDialog(OutSideInventoryActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                if (dayOfMonth <= 9) {
                                    strToDay = "0" + dayOfMonth;
                                } else {
                                    strToDay = "" + dayOfMonth;
                                }
                                monthOfYear = monthOfYear + 1;
                                strToYear = String.valueOf(year);

                                if (monthOfYear <= 9) {
                                    strToMonth = "0" + monthOfYear;
                                } else {
                                    strToMonth = "" + monthOfYear;
                                }

                                txtToDate.setText(strToYear + "-" + strToMonth + "-" + strToDay);

                                strToDate = txtToDate.getText().toString().trim();

                                try{
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // here set the pattern as you date in string was containing like date/month/year
                                    date1 = sdf.parse(strFromDate);
                                    date2 = sdf.parse(strToDate);
                                    dayCount = (date2.getTime() - date1.getTime())/(24*60*60*1000);
                                    //Toast.makeText(BookingActivity.this, "To Date"+date2+"\n From Date"+date1, Toast.LENGTH_LONG).show();
                                    //Toast.makeText(BookingActivity.this, "Day Count"+dayCount, Toast.LENGTH_SHORT).show();
                                    if(dayCount==0.0f){
                                        dayCount = 1.0f;
                                    }
                                     totalRent = (Float.parseFloat(strRentDaily))*(dayCount);
                                    txtTotalRent.setText(""+totalRent);
                                }catch(ParseException ex){
                                    // handle parsing exception if date string was different from the pattern applying into the SimpleDateFormat contructor
                                }

                            }
                        }, mToYear, mToMonth, mToDay);

                datePickerDialog.show();
            }
        });

        edtAdvance.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                strAdvanceAmount = edtAdvance.getText().toString().trim();

                if(strAdvanceAmount.equals("")){
                    llPending.setVisibility(View.GONE);
                    rentAdvance = 0.0f;
                    rentPending = totalRent - rentAdvance;
                    txtPending.setText(""+rentPending);
                }
                else{
                    rentAdvance = Float.parseFloat(strAdvanceAmount);
                    llPending.setVisibility(View.VISIBLE);
                    rentPending = totalRent - rentAdvance;
                    txtPending.setText(""+rentPending);
                }

            }
        });

        btnBookItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strSalerCompanyName = edtSalerCompanyName.getText().toString().trim();
                strSalerProductName = edtSalerProductName.getText().toString().trim();
                strSalerProductPrice = edtSalerProductPrice.getText().toString().trim();
                strSalerProductDate = txtSalerDate.getText().toString().trim();
                strSalerShopName = edtSalerShopName.getText().toString().trim();
                strSalerName = edtSalerName.getText().toString().trim();
                strSalerMobile = edtSalerMobile.getText().toString().trim();
                strRentMonthly = edtRentMonthly.getText().toString().trim();
                strRentDaily = edtRentDaily.getText().toString().trim();
                strFromDate = txtFromDate.getText().toString().trim();
                strToDate = txtToDate.getText().toString().trim();
                strTotalRent = txtTotalRent.getText().toString().trim();
                strAdvanceAmount = edtAdvance.getText().toString().trim();
                strPendingAmount = txtPending.getText().toString().trim();
                strCustName = edtCustName.getText().toString().trim();
                strCustMobile = edtCustMobile.getText().toString().trim();
                strCustEmail = edtCustEmail.getText().toString().trim();

                //strBookingType Generated Automatically when Selected//

                if (rdgBookingType.getCheckedRadioButtonId() == -1)
                {
                    Toast.makeText(OutSideInventoryActivity.this, "Please Select Monthly or Daily", Toast.LENGTH_SHORT).show();
                }

                if(strSalerCompanyName.equals("")){

                    edtSalerCompanyName.setError("Please Enter Company Name");
                    txtValidCompanyName.setVisibility(View.VISIBLE);
                    txtValidProductName.setVisibility(View.GONE);
                    txtValidProductPrice.setVisibility(View.GONE);
                    txtValidProductDate.setVisibility(View.GONE);
                    txtValidSalerShopName.setVisibility(View.GONE);
                    txtValidSalerName.setVisibility(View.GONE);
                    txtValidSalerMobile.setVisibility(View.GONE);
                    txtValidRentMonthly.setVisibility(View.GONE);
                    txtValidRentDaily.setVisibility(View.GONE);
                    txtValidFromDate.setVisibility(View.GONE);
                    txtValidToDate.setVisibility(View.GONE);
                    txtValidAdvance.setVisibility(View.GONE);
                    txtValidCustName.setVisibility(View.GONE);
                    txtValidCustMobile.setVisibility(View.GONE);
                    txtValidCustEmail.setVisibility(View.GONE);
                }
                else if(strSalerProductName.equals("")){

                    edtSalerProductName.setError("Please Enter Product Name");
                    txtValidCompanyName.setVisibility(View.GONE);
                    txtValidProductName.setVisibility(View.VISIBLE);
                    txtValidProductPrice.setVisibility(View.GONE);
                    txtValidProductDate.setVisibility(View.GONE);
                    txtValidSalerShopName.setVisibility(View.GONE);
                    txtValidSalerName.setVisibility(View.GONE);
                    txtValidSalerMobile.setVisibility(View.GONE);
                    txtValidRentMonthly.setVisibility(View.GONE);
                    txtValidRentDaily.setVisibility(View.GONE);
                    txtValidFromDate.setVisibility(View.GONE);
                    txtValidToDate.setVisibility(View.GONE);
                    txtValidAdvance.setVisibility(View.GONE);
                    txtValidCustName.setVisibility(View.GONE);
                    txtValidCustMobile.setVisibility(View.GONE);
                    txtValidCustEmail.setVisibility(View.GONE);
                }
                else if(strSalerProductPrice.equals("")){

                    edtSalerProductPrice.setError("Please Enter Product Price");
                    txtValidCompanyName.setVisibility(View.GONE);
                    txtValidProductName.setVisibility(View.GONE);
                    txtValidProductPrice.setVisibility(View.VISIBLE);
                    txtValidProductDate.setVisibility(View.GONE);
                    txtValidSalerShopName.setVisibility(View.GONE);
                    txtValidSalerName.setVisibility(View.GONE);
                    txtValidSalerMobile.setVisibility(View.GONE);
                    txtValidRentMonthly.setVisibility(View.GONE);
                    txtValidRentDaily.setVisibility(View.GONE);
                    txtValidFromDate.setVisibility(View.GONE);
                    txtValidToDate.setVisibility(View.GONE);
                    txtValidAdvance.setVisibility(View.GONE);
                    txtValidCustName.setVisibility(View.GONE);
                    txtValidCustMobile.setVisibility(View.GONE);
                    txtValidCustEmail.setVisibility(View.GONE);
                }
                else if(strSalerProductDate.equals("")){

                    txtSalerDate.setError("Please Enter Prchasing Date");
                    txtValidCompanyName.setVisibility(View.GONE);
                    txtValidProductName.setVisibility(View.GONE);
                    txtValidProductPrice.setVisibility(View.GONE);
                    txtValidProductDate.setVisibility(View.VISIBLE);
                    txtValidSalerShopName.setVisibility(View.GONE);
                    txtValidSalerName.setVisibility(View.GONE);
                    txtValidSalerMobile.setVisibility(View.GONE);
                    txtValidRentMonthly.setVisibility(View.GONE);
                    txtValidRentDaily.setVisibility(View.GONE);
                    txtValidFromDate.setVisibility(View.GONE);
                    txtValidToDate.setVisibility(View.GONE);
                    txtValidAdvance.setVisibility(View.GONE);
                    txtValidCustName.setVisibility(View.GONE);
                    txtValidCustMobile.setVisibility(View.GONE);
                    txtValidCustEmail.setVisibility(View.GONE);
                }
                else if(strSalerShopName.equals("")){

                    edtSalerShopName.setError("Please Enter Shop Name");
                    txtValidCompanyName.setVisibility(View.GONE);
                    txtValidProductName.setVisibility(View.GONE);
                    txtValidProductPrice.setVisibility(View.GONE);
                    txtValidProductDate.setVisibility(View.GONE);
                    txtValidSalerShopName.setVisibility(View.VISIBLE);
                    txtValidSalerName.setVisibility(View.GONE);
                    txtValidSalerMobile.setVisibility(View.GONE);
                    txtValidRentMonthly.setVisibility(View.GONE);
                    txtValidRentDaily.setVisibility(View.GONE);
                    txtValidFromDate.setVisibility(View.GONE);
                    txtValidToDate.setVisibility(View.GONE);
                    txtValidAdvance.setVisibility(View.GONE);
                    txtValidCustName.setVisibility(View.GONE);
                    txtValidCustMobile.setVisibility(View.GONE);
                    txtValidCustEmail.setVisibility(View.GONE);
                }
                else if(strSalerName.equals("")){

                    edtSalerName.setError("Please Enter Saler Name");
                    txtValidCompanyName.setVisibility(View.GONE);
                    txtValidProductName.setVisibility(View.GONE);
                    txtValidProductPrice.setVisibility(View.GONE);
                    txtValidProductDate.setVisibility(View.GONE);
                    txtValidSalerShopName.setVisibility(View.GONE);
                    txtValidSalerName.setVisibility(View.VISIBLE);
                    txtValidSalerMobile.setVisibility(View.GONE);
                    txtValidRentMonthly.setVisibility(View.GONE);
                    txtValidRentDaily.setVisibility(View.GONE);
                    txtValidFromDate.setVisibility(View.GONE);
                    txtValidToDate.setVisibility(View.GONE);
                    txtValidAdvance.setVisibility(View.GONE);
                    txtValidCustName.setVisibility(View.GONE);
                    txtValidCustMobile.setVisibility(View.GONE);
                    txtValidCustEmail.setVisibility(View.GONE);
                }
                else if(strSalerMobile.equals("")){

                    edtSalerMobile.setError("Please Enter Saler Mobile");
                    txtValidCompanyName.setVisibility(View.GONE);
                    txtValidProductName.setVisibility(View.GONE);
                    txtValidProductPrice.setVisibility(View.GONE);
                    txtValidProductDate.setVisibility(View.GONE);
                    txtValidSalerShopName.setVisibility(View.GONE);
                    txtValidSalerName.setVisibility(View.GONE);
                    txtValidSalerMobile.setVisibility(View.VISIBLE);
                    txtValidRentMonthly.setVisibility(View.GONE);
                    txtValidRentDaily.setVisibility(View.GONE);
                }
                else if(strRentMonthly.equals("")){

                    edtRentMonthly.setError("Please Enter Monthly Rent");
                    txtValidCompanyName.setVisibility(View.GONE);
                    txtValidProductName.setVisibility(View.GONE);
                    txtValidProductPrice.setVisibility(View.GONE);
                    txtValidProductDate.setVisibility(View.GONE);
                    txtValidSalerShopName.setVisibility(View.GONE);
                    txtValidSalerName.setVisibility(View.GONE);
                    txtValidSalerMobile.setVisibility(View.GONE);
                    txtValidRentMonthly.setVisibility(View.VISIBLE);
                    txtValidRentDaily.setVisibility(View.GONE);
                    txtValidFromDate.setVisibility(View.GONE);
                    txtValidToDate.setVisibility(View.GONE);
                    txtValidAdvance.setVisibility(View.GONE);
                    txtValidCustName.setVisibility(View.GONE);
                    txtValidCustMobile.setVisibility(View.GONE);
                    txtValidCustEmail.setVisibility(View.GONE);
                }
                else if(strRentDaily.equals("")){

                    edtRentDaily.setError("Please Enter Daily Rent");
                    txtValidCompanyName.setVisibility(View.GONE);
                    txtValidProductName.setVisibility(View.GONE);
                    txtValidProductPrice.setVisibility(View.GONE);
                    txtValidProductDate.setVisibility(View.GONE);
                    txtValidSalerShopName.setVisibility(View.GONE);
                    txtValidSalerName.setVisibility(View.GONE);
                    txtValidSalerMobile.setVisibility(View.GONE);
                    txtValidRentMonthly.setVisibility(View.GONE);
                    txtValidRentDaily.setVisibility(View.VISIBLE);
                    txtValidFromDate.setVisibility(View.GONE);
                    txtValidToDate.setVisibility(View.GONE);
                    txtValidAdvance.setVisibility(View.GONE);
                    txtValidCustName.setVisibility(View.GONE);
                    txtValidCustMobile.setVisibility(View.GONE);
                    txtValidCustEmail.setVisibility(View.GONE);
                }
                else if(strFromDate.equals("")){

                    txtFromDate.setError("Please Enter From Date");
                    txtValidCompanyName.setVisibility(View.GONE);
                    txtValidProductName.setVisibility(View.GONE);
                    txtValidProductPrice.setVisibility(View.GONE);
                    txtValidProductDate.setVisibility(View.GONE);
                    txtValidSalerShopName.setVisibility(View.GONE);
                    txtValidSalerName.setVisibility(View.GONE);
                    txtValidSalerMobile.setVisibility(View.GONE);
                    txtValidRentMonthly.setVisibility(View.GONE);
                    txtValidRentDaily.setVisibility(View.GONE);
                    txtValidFromDate.setVisibility(View.VISIBLE);
                    txtValidToDate.setVisibility(View.GONE);
                    txtValidAdvance.setVisibility(View.GONE);
                    txtValidCustName.setVisibility(View.GONE);
                    txtValidCustMobile.setVisibility(View.GONE);
                    txtValidCustEmail.setVisibility(View.GONE);
                }
                else if(strToDate.equals("")){

                    txtToDate.setError("Please Enter End Date");
                    txtValidCompanyName.setVisibility(View.GONE);
                    txtValidProductName.setVisibility(View.GONE);
                    txtValidProductPrice.setVisibility(View.GONE);
                    txtValidProductDate.setVisibility(View.GONE);
                    txtValidSalerShopName.setVisibility(View.GONE);
                    txtValidSalerName.setVisibility(View.GONE);
                    txtValidSalerMobile.setVisibility(View.GONE);
                    txtValidRentMonthly.setVisibility(View.GONE);
                    txtValidRentDaily.setVisibility(View.GONE);
                    txtValidFromDate.setVisibility(View.GONE);
                    txtValidToDate.setVisibility(View.VISIBLE);
                    txtValidAdvance.setVisibility(View.GONE);
                    txtValidCustName.setVisibility(View.GONE);
                    txtValidCustMobile.setVisibility(View.GONE);
                    txtValidCustEmail.setVisibility(View.GONE);
                }
                else if(strAdvanceAmount.equals("")){

                    edtAdvance.setError("Please Enter Advance");
                    txtValidCompanyName.setVisibility(View.GONE);
                    txtValidProductName.setVisibility(View.GONE);
                    txtValidProductPrice.setVisibility(View.GONE);
                    txtValidProductDate.setVisibility(View.GONE);
                    txtValidSalerShopName.setVisibility(View.GONE);
                    txtValidSalerName.setVisibility(View.GONE);
                    txtValidSalerMobile.setVisibility(View.GONE);
                    txtValidRentMonthly.setVisibility(View.GONE);
                    txtValidRentDaily.setVisibility(View.GONE);
                    txtValidFromDate.setVisibility(View.GONE);
                    txtValidToDate.setVisibility(View.GONE);
                    txtValidAdvance.setVisibility(View.VISIBLE);
                    txtValidCustName.setVisibility(View.GONE);
                    txtValidCustMobile.setVisibility(View.GONE);
                    txtValidCustEmail.setVisibility(View.GONE);
                }
                else if(strCustName.equals("")){

                    edtCustName.setError("Please Enter Customer Name");
                    txtValidCompanyName.setVisibility(View.GONE);
                    txtValidProductName.setVisibility(View.GONE);
                    txtValidProductPrice.setVisibility(View.GONE);
                    txtValidProductDate.setVisibility(View.GONE);
                    txtValidSalerShopName.setVisibility(View.GONE);
                    txtValidSalerName.setVisibility(View.GONE);
                    txtValidSalerMobile.setVisibility(View.GONE);
                    txtValidRentMonthly.setVisibility(View.GONE);
                    txtValidRentDaily.setVisibility(View.GONE);
                    txtValidFromDate.setVisibility(View.GONE);
                    txtValidToDate.setVisibility(View.GONE);
                    txtValidAdvance.setVisibility(View.GONE);
                    txtValidCustName.setVisibility(View.VISIBLE);
                    txtValidCustMobile.setVisibility(View.GONE);
                    txtValidCustEmail.setVisibility(View.GONE);
                }
                else if(strCustMobile.equals("")){

                    edtCustMobile.setError("Please Enter Customer Mobile");
                    txtValidCompanyName.setVisibility(View.GONE);
                    txtValidProductName.setVisibility(View.GONE);
                    txtValidProductPrice.setVisibility(View.GONE);
                    txtValidProductDate.setVisibility(View.GONE);
                    txtValidSalerShopName.setVisibility(View.GONE);
                    txtValidSalerName.setVisibility(View.GONE);
                    txtValidSalerMobile.setVisibility(View.GONE);
                    txtValidRentMonthly.setVisibility(View.GONE);
                    txtValidRentDaily.setVisibility(View.GONE);
                    txtValidFromDate.setVisibility(View.GONE);
                    txtValidToDate.setVisibility(View.GONE);
                    txtValidAdvance.setVisibility(View.GONE);
                    txtValidCustName.setVisibility(View.GONE);
                    txtValidCustMobile.setVisibility(View.VISIBLE);
                    txtValidCustEmail.setVisibility(View.GONE);
                }
                else if(strCustEmail.equals("")){

                    edtCustEmail.setError("Please Enter Customer Email");
                    txtValidCompanyName.setVisibility(View.GONE);
                    txtValidProductName.setVisibility(View.GONE);
                    txtValidProductPrice.setVisibility(View.GONE);
                    txtValidProductDate.setVisibility(View.GONE);
                    txtValidSalerShopName.setVisibility(View.GONE);
                    txtValidSalerName.setVisibility(View.GONE);
                    txtValidSalerMobile.setVisibility(View.GONE);
                    txtValidRentMonthly.setVisibility(View.GONE);
                    txtValidRentDaily.setVisibility(View.GONE);
                    txtValidFromDate.setVisibility(View.GONE);
                    txtValidToDate.setVisibility(View.GONE);
                    txtValidAdvance.setVisibility(View.GONE);
                    txtValidCustName.setVisibility(View.GONE);
                    txtValidCustMobile.setVisibility(View.GONE);
                    txtValidCustEmail.setVisibility(View.VISIBLE);
                }
                else{
                    txtValidCompanyName.setVisibility(View.GONE);
                    txtValidProductName.setVisibility(View.GONE);
                    txtValidProductPrice.setVisibility(View.GONE);
                    txtValidProductDate.setVisibility(View.GONE);
                    txtValidSalerShopName.setVisibility(View.GONE);
                    txtValidSalerName.setVisibility(View.GONE);
                    txtValidSalerMobile.setVisibility(View.GONE);
                    txtValidRentMonthly.setVisibility(View.GONE);
                    txtValidRentDaily.setVisibility(View.GONE);
                    txtValidFromDate.setVisibility(View.GONE);
                    txtValidToDate.setVisibility(View.GONE);
                    txtValidAdvance.setVisibility(View.GONE);
                    txtValidCustName.setVisibility(View.GONE);
                    txtValidCustMobile.setVisibility(View.GONE);
                    txtValidCustEmail.setVisibility(View.GONE);

                    bookOutSideItem();

                    //txtValidProductDate.setVisibility(View.GONE);
                }

            }
        });

    }

    private void bookOutSideItem() {

        addDialog = new ProgressDialog(this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Booking Item...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        try{
            OutSideBookingDataRequest outSideBooking = new OutSideBookingDataRequest();

            outSideBooking.setCompanyName(strSalerCompanyName);
            outSideBooking.setProductName(strSalerProductName);
            outSideBooking.setProductPrice(strSalerProductPrice);
            outSideBooking.setPurchaseDate(strSalerProductDate);
            outSideBooking.setShopName(strSalerShopName);
            outSideBooking.setSalerName(strSalerName);
            outSideBooking.setSalerMobile(strSalerMobile);
            outSideBooking.setOutSideBookingType(strBookingType);
            outSideBooking.setOutSideDayRent(strRentDaily);
            outSideBooking.setOutSideMonthRent(strRentMonthly);
            outSideBooking.setOutSideFromDate(strFromDate);
            outSideBooking.setOutSideToDate(strToDate);
            outSideBooking.setOutSideTotalRent(strTotalRent);
            outSideBooking.setOutSideAdvanceAmount(strAdvanceAmount);
            outSideBooking.setOutSidePendingAmount(strPendingAmount);
            outSideBooking.setOutSideCustName(strCustName);
            outSideBooking.setOutSideCustMobile(strCustMobile);
            outSideBooking.setOutSideCustEmail(strCustEmail);
            outSideBooking.setOutSideStatus("1");

            Call<OutSideBookingDataResponse> responceCall = ApiClient.getApiClient().bookOutSideItem(outSideBooking);

            responceCall.enqueue(new Callback<OutSideBookingDataResponse>() {
                @Override
                public void onResponse(Call<OutSideBookingDataResponse> call, retrofit2.Response<OutSideBookingDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        List<OutSideBookingDataItem> items =response.body().getOutSideBookingData();
                        Toast.makeText(OutSideInventoryActivity.this, "Item Booked", Toast.LENGTH_SHORT).show();
                        onBackPressed();
                        addDialog.dismiss();


                    } else {

                        Toast.makeText(OutSideInventoryActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                        addDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<OutSideBookingDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialog.dismiss();

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, OutSideInventoryListActivity.class);
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
    }
}
