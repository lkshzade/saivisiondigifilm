package com.microtron.cvinventorymanagmentapp.LogInSignUpSplash;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.microtron.cvinventorymanagmentapp.Helper.ApiClient;
import com.microtron.cvinventorymanagmentapp.Helper.AppConstants;
import com.microtron.cvinventorymanagmentapp.Helper.Sp;
import com.microtron.cvinventorymanagmentapp.MainActivity;
import com.microtron.cvinventorymanagmentapp.NetworkModel.RegisteLogInUpdate.RegisterDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.RegisteLogInUpdate.RegisterDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.RegisteLogInUpdate.RegisterDataResponse;
import com.microtron.cvinventorymanagmentapp.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class LoginActivity extends AppCompatActivity {

    private Button btnLogIn;
    private TextView btnSignUp;
    private EditText edtLogMobile, edtLogPass;
    private TextView btnForgetPass;
    private String strMobileNo,strPass;
    ProgressDialog addDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edtLogMobile = findViewById(R.id.edtLogMobile);
        edtLogPass = findViewById(R.id.edtLogPass);
        btnForgetPass = findViewById(R.id.txtForgetPassword);
        btnLogIn = findViewById(R.id.btnLogIn);
        btnSignUp = findViewById(R.id.btnSignUp);

        btnLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strMobileNo = edtLogMobile.getText().toString().trim();
                strPass = edtLogPass.getText().toString().trim();

                if(strMobileNo.length() < 10){
                    edtLogMobile.setError("Please Enter Valid Mobile Number");
                    Toast.makeText(LoginActivity.this, "Please Enter Valid Mobile Number", Toast.LENGTH_SHORT).show();
                }
                else if(strPass.length() < 6){
                    edtLogPass.setError("Please Enter Password More Than 5 Character");
                    Toast.makeText(LoginActivity.this, "Please Enter Password More Than 5 Character", Toast.LENGTH_SHORT).show();
                }
                else{
                    login();
                }


            }
        });

        btnForgetPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
                startActivity(intent);
                overridePendingTransition(0,0);

            }
        });

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(intent);
                overridePendingTransition(0,0);

            }
        });

    }

    private void login() {

        addDialog = new ProgressDialog(this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Please Wait...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        try {

            RegisterDataRequest logInDataRequest = new RegisterDataRequest();

            logInDataRequest.setAdminMobile(strMobileNo);
            logInDataRequest.setAdminPassword(strPass);


            Call<RegisterDataResponse> responceCall = ApiClient.getApiClient().logInAdmin(logInDataRequest);

            responceCall.enqueue(new Callback<RegisterDataResponse>() {
                @Override
                public void onResponse(Call<RegisterDataResponse> call, retrofit2.Response<RegisterDataResponse> response) {

                    if (response.body().getSuccess().equals("1")) {

                        List<RegisterDataItem> items = response.body().getRegisterData();

                        Sp.saveToPreferences(LoginActivity.this, AppConstants.ADMIN_ID, items.get(0).getAdminId());
                        Sp.saveToPreferences(LoginActivity.this, AppConstants.ADMIN_NAME, items.get(0).getAdminName());
                        Sp.saveToPreferences(LoginActivity.this, AppConstants.ADMIN_EMAIL, items.get(0).getAdminEmail());
                        Sp.saveToPreferences(LoginActivity.this, AppConstants.ADMIN_MOBILE, items.get(0).getAdminMobile());
                        Sp.saveToPreferences(LoginActivity.this, AppConstants.ADMIN_PASSWORD, items.get(0).getAdminPassword());
                        Sp.saveToPreferences(LoginActivity.this, AppConstants.ADMIN_ADDRESS, items.get(0).getAdminAddress());
                        Sp.saveToPreferences(LoginActivity.this, AppConstants.ADMIN_SHOPNAME, items.get(0).getAdminShopName());
                        Sp.saveToPreferences(LoginActivity.this, AppConstants.ADMIN_SHOPTYPE, items.get(0).getAdminShopType());
                        Sp.saveToPreferences(LoginActivity.this, AppConstants.ADMIN_IMGURL, items.get(0).getAdminImgURL());
                        
                        Sp.saveToPreferences(LoginActivity.this, AppConstants.ADMIN_SESSION, "OK");
                        addDialog.dismiss();

                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                        overridePendingTransition(0,0);


                    }
                    else if(response.body().getErrorMsg().equals("Record Not Found")){
                        addDialog.dismiss();

                        new AlertDialog.Builder(LoginActivity.this)
                                .setTitle("Failed")
                                .setMessage("Invalid Credentials")
                                .setPositiveButton(android.R.string.yes, null)
                                .create().show();

                    }
                    else {
                        addDialog.dismiss();
                        new AlertDialog.Builder(LoginActivity.this)
                                .setTitle("Failed")
                                .setMessage("Something Went Wrong")
                                .setPositiveButton(android.R.string.yes, null)
                                .create().show();
                    }
                }

                @Override
                public void onFailure(Call<RegisterDataResponse> call, Throwable t) {
                    addDialog.dismiss();
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(ProductDetails.this, "Error :" + t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });
        }
        catch (Exception e) {
            addDialog.dismiss();
            //Toast.makeText(ProductDetails.this, "Get Wish Exception", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onBackPressed() {

        new android.app.AlertDialog.Builder(this)
                .setTitle("Exit")
                .setMessage("Are you sure..! You want to Exit?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        Intent a = new Intent(Intent.ACTION_MAIN);
                        a.addCategory(Intent.CATEGORY_HOME);
                        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(a);

                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}
