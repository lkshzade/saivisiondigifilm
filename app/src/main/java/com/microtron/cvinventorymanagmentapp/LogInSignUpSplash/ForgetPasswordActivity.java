package com.microtron.cvinventorymanagmentapp.LogInSignUpSplash;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.microtron.cvinventorymanagmentapp.Helper.ConfigFile;
import com.microtron.cvinventorymanagmentapp.R;

import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ForgetPasswordActivity extends AppCompatActivity {

    EditText edtMobile;
    Button btnSendPass;
    String strmobile;
    ProgressDialog addDialog;
    ImageView imgBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        edtMobile = findViewById(R.id.edtMobileFP);
        btnSendPass = findViewById(R.id.btnSendPass);

        imgBack = findViewById(R.id.imgBack);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnSendPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strmobile = edtMobile.getText().toString().trim();
                if (!strmobile.equals("")) {
                    checkMobile();
                }
                else {
                    edtMobile.setError("Please Enter Mobile No. ");
                }
            }
        });
    }

    private void checkMobile() {

        /*ProgressDialog addDialog;*/

        addDialog = new ProgressDialog(this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Please Wait...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConfigFile.domain)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        final SendOTPService service = retrofit.create(SendOTPService.class);
        retrofit2.Call<String> callOTP = service.sendPass(strmobile);
        callOTP.enqueue(new Callback<String>() {
            @Override
            public void onResponse(retrofit2.Call<String> callOTP, retrofit2.Response<String> response) {

                try {
                    if (response.body().contains("success")) {

                        addDialog.dismiss();

                        new android.app.AlertDialog.Builder(ForgetPasswordActivity.this)
                                        .setTitle("Forget Password")
                                        .setMessage("Password has been sent to your registered mobile number")
                                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {

                                                Intent intent = new Intent(ForgetPasswordActivity.this, LoginActivity.class);
                                                startActivity(intent);
                                                overridePendingTransition(0, 0);

                                            }
                                        })
                                        .show();

                    } else if (response.body().contains("fail")) {

                        edtMobile.setError("Mobile Number is Not Register");
                        addDialog.dismiss();

                    }
                }catch (Exception e){

                    //Toast.makeText(ForgetPasswordActivity.this, "No Internet", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(retrofit2.Call<String> callOTP, Throwable t) {
                //Toast.makeText(ForgetPasswordActivity.this, t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(ForgetPasswordActivity.this, LoginActivity.class);
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
    }
}
