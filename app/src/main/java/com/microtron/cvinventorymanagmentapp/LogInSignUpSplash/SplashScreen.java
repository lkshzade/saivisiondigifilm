package com.microtron.cvinventorymanagmentapp.LogInSignUpSplash;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.microtron.cvinventorymanagmentapp.Helper.AppConstants;
import com.microtron.cvinventorymanagmentapp.Helper.Sp;
import com.microtron.cvinventorymanagmentapp.MainActivity;
import com.microtron.cvinventorymanagmentapp.R;

public class SplashScreen extends AppCompatActivity {

    private String strSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        ShowCustomProgressBarAsyncTask sc = new ShowCustomProgressBarAsyncTask();
        sc.execute();

    }

    public class ShowCustomProgressBarAsyncTask extends AsyncTask<Void, Integer, Void> {
        int myProgress = 0;

        @Override
        protected void onPostExecute(Void result) {
            //progressBar.setVisibility(View.INVISIBLE);

            strSession = Sp.readFromPreferences(SplashScreen.this, AppConstants.ADMIN_SESSION,"");

            if (strSession.equals("OK")) {

                Intent i = new Intent(SplashScreen.this, MainActivity.class);
                startActivity(i);
                finish();

            }
            else {
                Intent i = new Intent(SplashScreen.this, LoginActivity.class);
                startActivity(i);
                finish();
            }


        }

        @Override
        protected void onPreExecute() {


        }

        @Override
        protected Void doInBackground(Void... params) {
            while (myProgress < 100) {
                myProgress++;
                SystemClock.sleep(20);


            }
            return null;
        }
    }
}