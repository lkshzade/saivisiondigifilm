package com.microtron.cvinventorymanagmentapp.LogInSignUpSplash;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.microtron.cvinventorymanagmentapp.Helper.AppConstants;
import com.microtron.cvinventorymanagmentapp.Helper.ConfigFile;
import com.microtron.cvinventorymanagmentapp.Helper.Sp;
import com.microtron.cvinventorymanagmentapp.MainActivity;
import com.microtron.cvinventorymanagmentapp.R;

import java.util.Random;
import java.util.regex.Pattern;

import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class SignUpActivity extends AppCompatActivity {

    private EditText edtFullName, edtMobile, edtEmail, edtPassword, edtCnfPass, edtAddress, edtShopName, edtShopType;
    private TextView txtValidFullName, txtValidMobile, txtValidEmail, txtValidPassword, txtValidCnfPassword, txtValidAddress, txtValidShopName, txtValidShopType;
    private String strFullName, strMobile, strEmail, strPassword, strCnfPassword, strAddress, strShopName, strShopType;
    private Button btnRegister;
    private CheckBox checkBox;
    ImageView imgBack;
    Boolean value=false;

    String otp;
    ProgressDialog addDialog;

    public final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9+._%-+]{1,256}" +
                    "@" +
                    "[a-zA-Z0-9][a-zA-Z0-9-]{0,64}" +
                    "(" +
                    "." +
                    "[a-zA-Z0-9][a-zA-Z0-9-]{0,25}" +
                    ")+"
    );

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        edtFullName = findViewById(R.id.edtFullName);
        edtMobile = findViewById(R.id.edtMobile);
        edtEmail = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtPass);
        edtCnfPass = findViewById(R.id.edtCnfPass);
        edtAddress = findViewById(R.id.edtAddress);
        edtShopName = findViewById(R.id.edtShopName);
        edtShopType = findViewById(R.id.edtShopType);

        txtValidFullName = findViewById(R.id.txtValidFullName);
        txtValidMobile = findViewById(R.id.txtValidMobile);
        txtValidEmail = findViewById(R.id.txtValidEmail);
        txtValidPassword = findViewById(R.id.txtValidPass);
        txtValidCnfPassword = findViewById(R.id.txtValidCnfPass);
        txtValidAddress = findViewById(R.id.txtValidAddress);
        txtValidShopName = findViewById(R.id.txtValidShopName);
        txtValidShopType = findViewById(R.id.txtValidShopType);

        imgBack = findViewById(R.id.imgBack);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        checkBox = findViewById(R.id.chkBoxTnC);

        btnRegister = findViewById(R.id.btnRegister);


        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked){
                    value = true;
                    //Toast.makeText(getActivity(), "Terms and Condition OK", Toast.LENGTH_SHORT).show();

                }else {
                    value = false;
                    //Toast.makeText(getActivity(), "Please check Terms and Condition", Toast.LENGTH_SHORT).show();
                }
            }
        });


        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strFullName = edtFullName.getText().toString().trim();
                strMobile = edtMobile.getText().toString().trim();
                strEmail = edtEmail.getText().toString().trim();
                strPassword = edtPassword.getText().toString().trim();
                strCnfPassword = edtCnfPass.getText().toString().trim();
                strAddress = edtAddress.getText().toString().trim();
                strShopName = edtShopName.getText().toString().trim();
                strShopType = edtShopType.getText().toString().trim();

                if (strFullName.length() < 4) {
                    edtFullName.setError("Name Should be Greater than 3 Character");
                    Toast.makeText(SignUpActivity.this, "Enter Name Properly", Toast.LENGTH_SHORT).show();

                    txtValidFullName.setVisibility(View.VISIBLE);
                    txtValidMobile.setVisibility(View.GONE);
                    txtValidEmail.setVisibility(View.GONE);
                    txtValidPassword.setVisibility(View.GONE);
                    txtValidCnfPassword.setVisibility(View.GONE);
                    txtValidAddress.setVisibility(View.GONE);
                    txtValidShopName.setVisibility(View.GONE);
                    txtValidShopType.setVisibility(View.GONE);

                } else if (strMobile.length() < 10) {

                    edtMobile.setError("Please Enter Valid Mobile Number");
                    Toast.makeText(SignUpActivity.this, "Please Enter Valid Mobile Number", Toast.LENGTH_SHORT).show();

                    txtValidFullName.setVisibility(View.GONE);
                    txtValidMobile.setVisibility(View.VISIBLE);
                    txtValidEmail.setVisibility(View.GONE);
                    txtValidPassword.setVisibility(View.GONE);
                    txtValidCnfPassword.setVisibility(View.GONE);
                    txtValidAddress.setVisibility(View.GONE);
                    txtValidShopName.setVisibility(View.GONE);
                    txtValidShopType.setVisibility(View.GONE);

                }
                else if (!checkEmail(strEmail)) {

                    edtEmail.setError("Email Should contains @ and . symbols");
                    Toast.makeText(SignUpActivity.this, "Enter Email properly", Toast.LENGTH_SHORT).show();

                    txtValidFullName.setVisibility(View.GONE);
                    txtValidMobile.setVisibility(View.GONE);
                    txtValidEmail.setVisibility(View.VISIBLE);
                    txtValidPassword.setVisibility(View.GONE);
                    txtValidCnfPassword.setVisibility(View.GONE);
                    txtValidAddress.setVisibility(View.GONE);
                    txtValidShopName.setVisibility(View.GONE);
                    txtValidShopType.setVisibility(View.GONE);

                }
                else if (strPassword.length() < 6) {

                    edtPassword.setError("Password Should be Greater than 5 Character");
                    Toast.makeText(SignUpActivity.this, "Password Should be Greater than 5 Character", Toast.LENGTH_SHORT).show();
                    txtValidFullName.setVisibility(View.GONE);
                    txtValidMobile.setVisibility(View.GONE);
                    txtValidEmail.setVisibility(View.GONE);
                    txtValidPassword.setVisibility(View.VISIBLE);
                    txtValidCnfPassword.setVisibility(View.GONE);
                    txtValidAddress.setVisibility(View.GONE);
                    txtValidShopName.setVisibility(View.GONE);
                    txtValidShopType.setVisibility(View.GONE);

                }
                else if (!strCnfPassword.equals(strPassword)) {

                    edtCnfPass.setError("Please Enter Same Password");
                    Toast.makeText(SignUpActivity.this, "Please nter Same Password", Toast.LENGTH_SHORT).show();
                    txtValidFullName.setVisibility(View.GONE);
                    txtValidMobile.setVisibility(View.GONE);
                    txtValidEmail.setVisibility(View.GONE);
                    txtValidPassword.setVisibility(View.GONE);
                    txtValidCnfPassword.setVisibility(View.VISIBLE);
                    txtValidAddress.setVisibility(View.GONE);
                    txtValidShopName.setVisibility(View.GONE);
                    txtValidShopType.setVisibility(View.GONE);

                }
                else if (strAddress.length() < 4) {

                    edtAddress.setError("Address Must be Greater than 3 Character");
                    Toast.makeText(SignUpActivity.this, "Address Must be Greater than 3 Character", Toast.LENGTH_SHORT).show();
                    txtValidFullName.setVisibility(View.GONE);
                    txtValidMobile.setVisibility(View.GONE);
                    txtValidEmail.setVisibility(View.GONE);
                    txtValidPassword.setVisibility(View.GONE);
                    txtValidCnfPassword.setVisibility(View.GONE);
                    txtValidAddress.setVisibility(View.VISIBLE);
                    txtValidShopName.setVisibility(View.GONE);
                    txtValidShopType.setVisibility(View.GONE);

                }
                else if (strShopName.length() < 4) {

                    edtShopName.setError("Shop Name Must be Greater than 3 Character");
                    Toast.makeText(SignUpActivity.this, "Shop Name Must be Greater than 3 Character", Toast.LENGTH_SHORT).show();
                    txtValidFullName.setVisibility(View.GONE);
                    txtValidMobile.setVisibility(View.GONE);
                    txtValidEmail.setVisibility(View.GONE);
                    txtValidPassword.setVisibility(View.GONE);
                    txtValidCnfPassword.setVisibility(View.GONE);
                    txtValidAddress.setVisibility(View.GONE);
                    txtValidShopName.setVisibility(View.VISIBLE);
                    txtValidShopType.setVisibility(View.GONE);

                }
                else if (strShopType.length() < 4) {

                    edtShopType.setError("Shop Type Must be Greater than 3 Character");
                    Toast.makeText(SignUpActivity.this, "Please Enter Valid Mobile Number", Toast.LENGTH_SHORT).show();
                    txtValidFullName.setVisibility(View.GONE);
                    txtValidMobile.setVisibility(View.GONE);
                    txtValidEmail.setVisibility(View.GONE);
                    txtValidPassword.setVisibility(View.GONE);
                    txtValidCnfPassword.setVisibility(View.GONE);
                    txtValidAddress.setVisibility(View.GONE);
                    txtValidShopName.setVisibility(View.GONE);
                    txtValidShopType.setVisibility(View.VISIBLE);

                }
                else if(value==false){
                    Toast.makeText(SignUpActivity.this, "Please check Terms and Condition", Toast.LENGTH_SHORT).show();
                } else{
                    txtValidFullName.setVisibility(View.GONE);
                    txtValidMobile.setVisibility(View.GONE);
                    txtValidEmail.setVisibility(View.GONE);
                    txtValidPassword.setVisibility(View.GONE);
                    txtValidCnfPassword.setVisibility(View.GONE);
                    txtValidAddress.setVisibility(View.GONE);
                    txtValidShopName.setVisibility(View.GONE);
                    txtValidShopType.setVisibility(View.GONE);

                    sendOTP();
                }
            }
        });
    }

    private boolean checkEmail(String email) {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }

    private void sendOTP() {

        addDialog = new ProgressDialog(this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Please Wait...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        Random r = new Random();
        int randomNumber = r.nextInt(10000);
        otp = "" + randomNumber;

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConfigFile.domain)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        final SendOTPService service = retrofit.create(SendOTPService.class);
        retrofit2.Call<String> call = service.sendOTP(otp, strMobile);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(retrofit2.Call<String> call, retrofit2.Response<String> response) {

                if(response.body().contains("success")){
                    addDialog.dismiss();
                    new AlertDialog.Builder(SignUpActivity.this)
                            .setTitle("OTP ")
                            .setMessage("OTP Sent to your Mobile Number")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {


                                    Sp.saveToPreferences(SignUpActivity.this, AppConstants.ADMIN_NAME, strFullName);
                                    Sp.saveToPreferences(SignUpActivity.this, AppConstants.ADMIN_EMAIL, strEmail);
                                    Sp.saveToPreferences(SignUpActivity.this, AppConstants.ADMIN_MOBILE, strMobile);
                                    Sp.saveToPreferences(SignUpActivity.this, AppConstants.ADMIN_PASSWORD, strPassword);
                                    Sp.saveToPreferences(SignUpActivity.this, AppConstants.ADMIN_ADDRESS, strAddress);
                                    Sp.saveToPreferences(SignUpActivity.this, AppConstants.ADMIN_SHOPNAME, strShopName);
                                    Sp.saveToPreferences(SignUpActivity.this, AppConstants.ADMIN_SHOPTYPE, strShopType);
                                    Sp.saveToPreferences(SignUpActivity.this, AppConstants.ADMIN_OTP, otp);

                                    Intent intent = new Intent(SignUpActivity.this, RegisterActivity.class);
                                    startActivity(intent);
                                    overridePendingTransition(0,0);

                                    //Toast.makeText(SignUpActivity.this, "OTP Sent", Toast.LENGTH_SHORT).show();

                                }
                            })
                            .show();
                }
                else if(response.body().contains("Already Register")){
                    addDialog.dismiss();

                    new android.support.v7.app.AlertDialog.Builder(SignUpActivity.this)
                            .setTitle("Failed")
                            .setMessage("Mobile Number Already Register...!")
                            .setPositiveButton(android.R.string.yes, null)
                            .create().show();
                }

            }

            @Override
            public void onFailure(retrofit2.Call<String> call, Throwable t) {
                addDialog.dismiss();
                Toast.makeText(SignUpActivity.this, t.getLocalizedMessage(), Toast.LENGTH_LONG).show();

            }
        });



    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
    }
}



