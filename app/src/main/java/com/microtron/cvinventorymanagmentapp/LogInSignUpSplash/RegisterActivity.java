package com.microtron.cvinventorymanagmentapp.LogInSignUpSplash;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.microtron.cvinventorymanagmentapp.Helper.ApiClient;
import com.microtron.cvinventorymanagmentapp.Helper.AppConstants;
import com.microtron.cvinventorymanagmentapp.Helper.Sp;
import com.microtron.cvinventorymanagmentapp.MainActivity;
import com.microtron.cvinventorymanagmentapp.NetworkModel.RegisteLogInUpdate.RegisterDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.RegisteLogInUpdate.RegisterDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.RegisteLogInUpdate.RegisterDataResponse;
import com.microtron.cvinventorymanagmentapp.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class RegisterActivity extends AppCompatActivity {

    private EditText edtMobileOTP;
    private Button btnVerifyOTP;
    private String strEntredOTP;
    private String strName, strMobile, strEmail, strPass, strAddress, strShopName, strShoptype, strOTP;
    ProgressDialog addDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        edtMobileOTP = findViewById(R.id.edtMobileOTP);
        btnVerifyOTP = findViewById(R.id.btnVerifyOTP);

        strOTP = Sp.readFromPreferences(this, AppConstants.ADMIN_OTP,"");

        btnVerifyOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strEntredOTP = edtMobileOTP.getText().toString().trim();
                if(strEntredOTP.equals("")){
                    edtMobileOTP.setError("Please Enter OTP First");
                    Toast.makeText(RegisterActivity.this, "Please Enter OTP First", Toast.LENGTH_SHORT).show();
                }
                else if(!strEntredOTP.equals(strOTP)){
                    edtMobileOTP.setError("OTP is Incorrect");
                    Toast.makeText(RegisterActivity.this, "Please Enter Same OTP", Toast.LENGTH_SHORT).show();
                }
                else{
                    saveAdmin();
                }
            }
        });
    }

    private void saveAdmin() {

        addDialog = new ProgressDialog(this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Please Wait...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        strName = Sp.readFromPreferences(this, AppConstants.ADMIN_NAME,"");
        strMobile = Sp.readFromPreferences(this, AppConstants.ADMIN_MOBILE,"");
        strEmail = Sp.readFromPreferences(this, AppConstants.ADMIN_EMAIL,"");
        strPass = Sp.readFromPreferences(this, AppConstants.ADMIN_PASSWORD,"");
        strAddress = Sp.readFromPreferences(this, AppConstants.ADMIN_ADDRESS,"");
        strShopName = Sp.readFromPreferences(this, AppConstants .ADMIN_SHOPNAME,"");
        strShoptype = Sp.readFromPreferences(this, AppConstants.ADMIN_SHOPTYPE,"");

        try {

            RegisterDataRequest registerDataRequest = new RegisterDataRequest();

            registerDataRequest.setAdminName(strName);
            registerDataRequest.setAdminEmail(strEmail);
            registerDataRequest.setAdminMobile(strMobile);
            registerDataRequest.setAdminPassword(strPass);
            registerDataRequest.setAdminAddress(strAddress);
            registerDataRequest.setAdminShopName(strShopName);
            registerDataRequest.setAdminShopType(strShoptype);
            registerDataRequest.setAdminImgURL("IMG_"+strMobile);

            Call<RegisterDataResponse> responceCall = ApiClient.getApiClient().addAdmin(registerDataRequest);

            responceCall.enqueue(new Callback<RegisterDataResponse>() {
                @Override
                public void onResponse(Call<RegisterDataResponse> call, retrofit2.Response<RegisterDataResponse> response) {

                    if (response.body().getSuccess().equals("1")) {

                        List<RegisterDataItem> items = response.body().getRegisterData();

                        Sp.saveToPreferences(RegisterActivity.this, AppConstants.ADMIN_ID, items.get(0).getAdminId());
                        Sp.saveToPreferences(RegisterActivity.this, AppConstants.ADMIN_NAME, items.get(0).getAdminName());
                        Sp.saveToPreferences(RegisterActivity.this, AppConstants.ADMIN_EMAIL, items.get(0).getAdminEmail());
                        Sp.saveToPreferences(RegisterActivity.this, AppConstants.ADMIN_MOBILE, items.get(0).getAdminMobile());
                        Sp.saveToPreferences(RegisterActivity.this, AppConstants.ADMIN_PASSWORD, items.get(0).getAdminPassword());
                        Sp.saveToPreferences(RegisterActivity.this, AppConstants.ADMIN_ADDRESS, items.get(0).getAdminAddress());
                        Sp.saveToPreferences(RegisterActivity.this, AppConstants.ADMIN_SHOPNAME, items.get(0).getAdminShopName());
                        Sp.saveToPreferences(RegisterActivity.this, AppConstants.ADMIN_SHOPTYPE, items.get(0).getAdminShopType());
                        Sp.saveToPreferences(RegisterActivity.this, AppConstants.ADMIN_IMGURL, items.get(0).getAdminImgURL());
                        Sp.saveToPreferences(RegisterActivity.this, AppConstants.ADMIN_SESSION, "OK");
                        addDialog.dismiss();

                        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                        startActivity(intent);
                        overridePendingTransition(0,0);


                    }
                    else if (response.body().getErrorMsg().equals("1")) {

                        addDialog.dismiss();
                        Toast.makeText(RegisterActivity.this, "Failed", Toast.LENGTH_SHORT).show();

                    }
                    else {

                        addDialog.dismiss();
                        //Toast.makeText(ProductDetails.this, "Error :" + response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<RegisterDataResponse> call, Throwable t) {
                    addDialog.dismiss();
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(ProductDetails.this, "Error :" + t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });
        }
        catch (Exception e) {
            addDialog.dismiss();
            //Toast.makeText(ProductDetails.this, "Get Wish Exception", Toast.LENGTH_SHORT).show();
        }

        //Toast.makeText(this, "Suceess ", Toast.LENGTH_SHORT).show();

    }
}
