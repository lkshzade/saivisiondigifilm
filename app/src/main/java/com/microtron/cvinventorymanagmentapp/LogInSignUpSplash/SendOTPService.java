package com.microtron.cvinventorymanagmentapp.LogInSignUpSplash;

import com.microtron.cvinventorymanagmentapp.Helper.ConfigFile;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface SendOTPService {

    @FormUrlEncoded
    @POST(ConfigFile.domaindir + ConfigFile.sendOTP)
    Call<String> sendOTP(@Field("otp") String otp,
                         @Field("mobile") String mobile);

    @FormUrlEncoded
    @POST(ConfigFile.domaindir + ConfigFile.sendPass)
    Call<String> sendPass(@Field("mobile") String mobile);


    @FormUrlEncoded
    @POST(ConfigFile.domaindir + ConfigFile.sendChangePassOTP)
    Call<String> changePass(@Field("otp") String otp,
                            @Field("mobile") String mobile);

}
