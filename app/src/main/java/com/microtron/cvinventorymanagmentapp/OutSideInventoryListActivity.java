package com.microtron.cvinventorymanagmentapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;

import com.microtron.cvinventorymanagmentapp.Adapters.OutSideProductListAdapter;
import com.microtron.cvinventorymanagmentapp.Adapters.RecyclerItemClickListenerOutSideProductList;
import com.microtron.cvinventorymanagmentapp.Adapters.RecyclerItemClickListenerRentList;
import com.microtron.cvinventorymanagmentapp.Adapters.RentListAdapter;
import com.microtron.cvinventorymanagmentapp.Helper.ApiClient;
import com.microtron.cvinventorymanagmentapp.NetworkModel.OutSideInventoryBooking.OutSideBookingDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.OutSideInventoryBooking.OutSideBookingDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.OutSideInventoryBooking.OutSideBookingDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class OutSideInventoryListActivity extends AppCompatActivity {

    private ImageView createOutSideInventory, imgBack, imgNoProduct;
    private RecyclerView recyclerOutSideProductsList;
    private ProgressDialog addDialog;
    private OutSideProductListAdapter outSideProductListAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_out_side_inventory_list);

        createOutSideInventory = findViewById(R.id.createOutSideInventory);
        imgBack = findViewById(R.id.imgBack);
        recyclerOutSideProductsList = findViewById(R.id.recyclerOutSideProductsList);
        imgNoProduct = findViewById(R.id.imgNoProduct);
        imgNoProduct.setVisibility(View.GONE);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        createOutSideInventory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OutSideInventoryListActivity.this, OutSideInventoryActivity.class);
                startActivity(intent);
                overridePendingTransition(0,0);
                finish();
            }
        });

        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerOutSideProductsList.setLayoutAnimation(animation);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerOutSideProductsList.setLayoutManager(layoutManager);

        getOutSideProductList();
    }

    private void getOutSideProductList() {

        addDialog = new ProgressDialog(this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Getting List...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        try{
            OutSideBookingDataRequest getOutSideProductListRequest = new OutSideBookingDataRequest();


            Call<OutSideBookingDataResponse> responceCall = ApiClient.getApiClient().getOutSideItem(getOutSideProductListRequest);

            responceCall.enqueue(new Callback<OutSideBookingDataResponse>() {
                @Override
                public void onResponse(Call<OutSideBookingDataResponse> call, retrofit2.Response<OutSideBookingDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        List<OutSideBookingDataItem> items =response.body().getOutSideBookingData();
                        imgNoProduct.setVisibility(View.GONE);

                        outSideProductListAdapter = new OutSideProductListAdapter(OutSideInventoryListActivity.this, items, recyclerItemClickListener);
                        recyclerOutSideProductsList.setAdapter(outSideProductListAdapter);
                        outSideProductListAdapter.notifyDataSetChanged();

                        addDialog.dismiss();


                    } else {

                        //Toast.makeText(RentListActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                        imgNoProduct.setVisibility(View.VISIBLE);
                        addDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<OutSideBookingDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialog.dismiss();

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    private RecyclerItemClickListenerOutSideProductList recyclerItemClickListener = new RecyclerItemClickListenerOutSideProductList() {
        @Override
        public void onItemClick(final OutSideBookingDataItem notiItem) {
        }
    };

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
    }
}
