package com.microtron.cvinventorymanagmentapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import com.microtron.cvinventorymanagmentapp.Adapters.RentListAdapter;
import com.microtron.cvinventorymanagmentapp.Helper.ApiClient;
import com.microtron.cvinventorymanagmentapp.Helper.AppConstants;
import com.microtron.cvinventorymanagmentapp.Helper.Sp;
import com.microtron.cvinventorymanagmentapp.Helper.StaticClass;
import com.microtron.cvinventorymanagmentapp.NetworkModel.AddItem.AddItemDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.AddItem.AddItemDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.AddItem.AddItemDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.Category.CategoryDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.Category.CategoryDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.Category.CategoryDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.Comapny.CompanyDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.Comapny.CompanyDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.Comapny.CompanyDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.GetAllProducts.AllProductsDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PreDateBookingData.PreDateDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PreDateBookingData.PreDateDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PreDateBookingData.PreDateDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.SubCategory.SubCategoryDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.SubCategory.SubCategoryDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.SubCategory.SubCategoryDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataResponse;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

import static android.text.Layout.JUSTIFICATION_MODE_INTER_WORD;

public class ProductDetailsActivity extends AppCompatActivity {

    AllProductsDataItem productsDetails;
    private EditText edtSetProductName, edtSetProductDescription, edtSetProductStockQuantity;
    private AutoCompleteTextView txtSetProductCompany, txtSetProductCategory, txtSetProductSubCategory;
    private Spinner spSetInStock;
    private Button btnSetAddCompany, btnSetAddCategory, btnSetAddSubCategory, btnSetChooseImages;
    private LinearLayout llSetCat, llSetSubCat, llSetProduct, llInStock, llStockQuantity;
    private ImageView imgBack;
    private String strProductID, strProductCompany, strProductCategory, strProductSubCategory, strProductName, strProductDesc, strProductInStock, strProductStockQuantity;

    private LinearLayout llEditDelete;
    private Button btnEditProduct, btnDeleteProduct, btnUpdateProduct, btnBookManualItem, btnSaleManualItem, btnGenerateQR;
    private String strIntentTo;


    private ArrayList<String> editCompanyNameList = new ArrayList<String>();
    private ArrayList<String> editCompanyIdList = new ArrayList<String>();
    private ArrayList<String> editCategoryNameList = new ArrayList<String>();
    private ArrayList<String> editCategoryIdList = new ArrayList<String>();
    private ArrayList<String> editSubCategoryNameList = new ArrayList<String>();
    private ArrayList<String> editSubCategoryIdList = new ArrayList<String>();
    private ArrayList<String> editSubCategoryCatIdList = new ArrayList<String>();

    private List<CompanyDataItem> companyItems;
    private List<CategoryDataItem> categoryItems;
    private List<SubCategoryDataItem> subCategoryItems;

    private ArrayList<String> rentDateList = new ArrayList<String>();

    private String strCompanyId, strCompanyName, strCategoryId, strCategoryName, strSubCategoryId, strSubCategoryName, strFinalCatId, strFinalSubCatId;
    private String strFinalCompanyName, strFinalCompanyID, strFinalCategoryName, strFinalCategoryID, strFinalSubCategoryID, strFinalSubCategoryName, strFinalSubCategoryCatID;
    private ProgressDialog addDialog, addDialog1;

    Bitmap bitmap;
    int QRcodeWidth = 350;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

        llEditDelete = findViewById(R.id.llEditDelete);
        llEditDelete.setVisibility(View.GONE);
        btnUpdateProduct = findViewById(R.id.btnUpdateProduct);
        btnUpdateProduct.setVisibility(View.GONE);
        btnBookManualItem = findViewById(R.id.btnBookManualItem);
        btnBookManualItem.setVisibility(View.GONE);
        btnSaleManualItem = findViewById(R.id.btnSaleManualItem);
        btnSaleManualItem.setVisibility(View.GONE);
        btnEditProduct = findViewById(R.id.btnEditProduct);
        btnDeleteProduct = findViewById(R.id.btnDeleteProduct);
        btnGenerateQR = findViewById(R.id.btnGenerateQR);
        txtSetProductCompany = findViewById(R.id.txtSetCompanyName);
        txtSetProductCategory = findViewById(R.id.txtSetCategory);
        txtSetProductSubCategory = findViewById(R.id.txtSetSubCategory);
        edtSetProductName = findViewById(R.id.edtSetProductName);
        edtSetProductDescription = findViewById(R.id.edtSetProductDescription);
        spSetInStock = findViewById(R.id.spSetInStock);
        edtSetProductStockQuantity = findViewById(R.id.edtSetProductStockQuantity);
        imgBack = findViewById(R.id.imgBack);
        llInStock = findViewById(R.id.llInStock);
        llStockQuantity = findViewById(R.id.llStockQuantity);

        llSetSubCat = findViewById(R.id.llSetSubCategory);
        llSetCat = findViewById(R.id.llSetCategory);
        llSetProduct = findViewById(R.id.llSetProduct);
        btnSetAddCompany = findViewById(R.id.btnSetAddCompany);
        btnSetAddCategory = findViewById(R.id.btnSetAddCategory);
        btnSetAddSubCategory = findViewById(R.id.btnSetAddSubCategory);

        btnSetAddCompany.setVisibility(View.GONE);
        btnSetAddCategory.setVisibility(View.GONE);
        btnSetAddSubCategory.setVisibility(View.GONE);
        llInStock.setVisibility(View.GONE);
        llStockQuantity.setVisibility(View.GONE);

        /*getBookingDatesForPreBooking();*/

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        txtSetProductCompany.setEnabled(false);
        txtSetProductCategory.setEnabled(false);
        txtSetProductSubCategory.setEnabled(false);
        edtSetProductName.setEnabled(false);
        edtSetProductDescription.setEnabled(false);
        edtSetProductStockQuantity.setEnabled(false);
        spSetInStock.setEnabled(false);

        strIntentTo = Sp.readFromPreferences(this, AppConstants.INTENT_TO,"");

        if(strIntentTo.equals("ManageInventory")){
            llInStock.setVisibility(View.GONE);
            llStockQuantity.setVisibility(View.GONE);
            llEditDelete.setVisibility(View.VISIBLE);
        }
        if(strIntentTo.equals("ManualBooking")){
            llInStock.setVisibility(View.GONE);
            llStockQuantity.setVisibility(View.GONE);
            btnBookManualItem.setVisibility(View.VISIBLE);
            btnSaleManualItem.setVisibility(View.VISIBLE);
        }



        productsDetails = StaticClass.product;


        txtSetProductCompany.setText(productsDetails.getCompanyName());
        txtSetProductCategory.setText(productsDetails.getCatName());
        txtSetProductSubCategory.setText(productsDetails.getSubCatName());
        edtSetProductName.setText(productsDetails.getProductName());
        edtSetProductDescription.setText(productsDetails.getProductDetails());
        edtSetProductStockQuantity.setText(productsDetails.getProductQuantity());


        strFinalCompanyID = productsDetails.getCompanyId();
        strFinalSubCategoryCatID = productsDetails.getProductCategoryId();
        strFinalSubCategoryID = productsDetails.getProductSubcategoryId();




        if (productsDetails.getProductInStock().equals("Yes")){
            spSetInStock.setSelection(0);
        }else{
            spSetInStock.setSelection(1);
        }

        btnEditProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                txtSetProductCompany.setEnabled(true);
                txtSetProductCategory.setEnabled(true);
                txtSetProductSubCategory.setEnabled(true);
                edtSetProductName.setEnabled(true);
                edtSetProductDescription.setEnabled(true);
                edtSetProductStockQuantity.setEnabled(true);
                spSetInStock.setEnabled(true);

                llEditDelete.setVisibility(View.GONE);
                btnUpdateProduct.setVisibility(View.VISIBLE);
                getCompany();
                getCategory();
                getSubCategory();


            }
        });

        btnDeleteProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(ProductDetailsActivity.this);
                alertDialog2.setTitle("Delete Product...");
                alertDialog2.setMessage("Do you want to Delete this Product ?");

                alertDialog2.setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                deleteItem();
                                //Toast.makeText(context, "Received", Toast.LENGTH_SHORT).show();
                            }
                        });
                alertDialog2.setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                //Toast.makeText(context, "Product not Received", Toast.LENGTH_SHORT).show();
                            }
                        });

                alertDialog2.show();

            }
        });
        
        btnBookManualItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*if(productsDetails.getProductInStock().equals("No")){

                    Toast.makeText(ProductDetailsActivity.this, "Product Not in Stock", Toast.LENGTH_SHORT).show();
                }
                else{*/
                    Sp.saveToPreferences(ProductDetailsActivity.this, AppConstants.PRODUCT_COMP_ID, ""+productsDetails.getProductCompnyId());
                    Sp.saveToPreferences(ProductDetailsActivity.this, AppConstants.PRODUCT_CAT_ID, ""+productsDetails.getProductCategoryId());
                    Sp.saveToPreferences(ProductDetailsActivity.this, AppConstants.PRODUCT_SUBCAT_ID, ""+productsDetails.getProductSubcategoryId());
                    Sp.saveToPreferences(ProductDetailsActivity.this, AppConstants.PRODUCT_ID, ""+productsDetails.getProductId());
                    Sp.saveToPreferences(ProductDetailsActivity.this, AppConstants.PRODUCT_COMP_NAME, ""+productsDetails.getCompanyName());
                    Sp.saveToPreferences(ProductDetailsActivity.this, AppConstants.PRODUCT_CAT_NAME, ""+productsDetails.getCatName());
                    Sp.saveToPreferences(ProductDetailsActivity.this, AppConstants.PRODUCT_SUBCAT_NAME, ""+productsDetails.getSubCatName());
                    Sp.saveToPreferences(ProductDetailsActivity.this, AppConstants.PRODUCT_NAME, ""+productsDetails.getProductName());
                    Sp.saveToPreferences(ProductDetailsActivity.this, AppConstants.PRODUCT_DESCRIPTION, ""+productsDetails.getProductDetails());
                    Sp.saveToPreferences(ProductDetailsActivity.this, AppConstants.PRODUCT_INSTOCK, ""+productsDetails.getProductInStock());
                    Sp.saveToPreferences(ProductDetailsActivity.this, AppConstants.PRODUCT_STOCKQTY, ""+productsDetails.getProductQuantity());

                    Sp.saveToPreferences(ProductDetailsActivity.this, AppConstants.INTENT_TO, "BookProduct");

                    Intent intent = new Intent(ProductDetailsActivity.this,BookingActivity.class);
                    startActivity(intent);
                    overridePendingTransition(0,0);
                    finish();
                /*}*/

                
            }
        });

        btnSaleManualItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(productsDetails.getProductInStock().equals("No")){

                    Toast.makeText(ProductDetailsActivity.this, "Product Not in Stock", Toast.LENGTH_SHORT).show();
                }
                else{
                    Sp.saveToPreferences(ProductDetailsActivity.this, AppConstants.PRODUCT_COMP_ID, ""+productsDetails.getProductCompnyId());
                    Sp.saveToPreferences(ProductDetailsActivity.this, AppConstants.PRODUCT_CAT_ID, ""+productsDetails.getProductCategoryId());
                    Sp.saveToPreferences(ProductDetailsActivity.this, AppConstants.PRODUCT_SUBCAT_ID, ""+productsDetails.getProductSubcategoryId());
                    Sp.saveToPreferences(ProductDetailsActivity.this, AppConstants.PRODUCT_ID, ""+productsDetails.getProductId());
                    Sp.saveToPreferences(ProductDetailsActivity.this, AppConstants.PRODUCT_COMP_NAME, ""+productsDetails.getCompanyName());
                    Sp.saveToPreferences(ProductDetailsActivity.this, AppConstants.PRODUCT_CAT_NAME, ""+productsDetails.getCatName());
                    Sp.saveToPreferences(ProductDetailsActivity.this, AppConstants.PRODUCT_SUBCAT_NAME, ""+productsDetails.getSubCatName());
                    Sp.saveToPreferences(ProductDetailsActivity.this, AppConstants.PRODUCT_NAME, ""+productsDetails.getProductName());
                    Sp.saveToPreferences(ProductDetailsActivity.this, AppConstants.PRODUCT_DESCRIPTION, ""+productsDetails.getProductDetails());
                    Sp.saveToPreferences(ProductDetailsActivity.this, AppConstants.PRODUCT_INSTOCK, ""+productsDetails.getProductInStock());
                    Sp.saveToPreferences(ProductDetailsActivity.this, AppConstants.PRODUCT_STOCKQTY, ""+productsDetails.getProductQuantity());

                    Sp.saveToPreferences(ProductDetailsActivity.this, AppConstants.INTENT_TO, "SaleProduct");

                    Intent intent = new Intent(ProductDetailsActivity.this,BookingActivity.class);
                    startActivity(intent);
                    overridePendingTransition(0,0);
                    finish();
                }


            }
        });

        btnGenerateQR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                try {
                    bitmap = TextToImageEncode(productsDetails.getProductId());

                    addDialog.dismiss();

                } catch (Exception e) {
                    e.printStackTrace();
                }

                addDialog1 = new ProgressDialog(ProductDetailsActivity.this);
                addDialog1.setCancelable(true);
                addDialog1.setMessage("Saving QR Code...");
                addDialog1.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                addDialog1.show();

                String root = Environment.getExternalStorageDirectory().toString();
                File myDir = new File(root + "/QRCodes");
                myDir.mkdirs();
                String fname = "QR_" + productsDetails.getProductName() + ".jpg";
                File file = new File(myDir, fname);
                if (file.exists())
                    file.delete();
                try {
                    FileOutputStream out = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                    out.flush();
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                addDialog1.dismiss();

                new AlertDialog.Builder(ProductDetailsActivity.this)
                        .setTitle("Storage Path")
                        .setMessage("Internal Storage/QRCodes/QR_" + productsDetails.getProductName() + ".jpg")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();

                            }
                        })
                        .setNegativeButton(android.R.string.no, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();



            }
        });

        txtSetProductCompany.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(ProductDetailsActivity.this, android.R.layout.select_dialog_item, editCompanyNameList);
                txtSetProductCompany.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
                txtSetProductCompany.setTextColor(getResources().getColor(R.color.black));
                txtSetProductCompany.setThreshold(1);
                llSetProduct.setVisibility(View.GONE);

                strProductCompany = txtSetProductCompany.getText().toString().trim();

                for (int j = 0; j < editCompanyNameList.size(); j++) {

                    if (strProductCompany.equals(editCompanyNameList.get(j).trim())) {

                        strFinalCompanyID = editCompanyIdList.get(j);
                        strFinalCompanyName = editCompanyNameList.get(j);
                        btnSetAddCompany.setVisibility(View.GONE);
                        llSetCat.setVisibility(View.VISIBLE);
                        llSetSubCat.setVisibility(View.VISIBLE);
                        llSetProduct.setVisibility(View.VISIBLE);
                        break;
                    } else {
                        strCompanyName = txtSetProductCompany.getText().toString().trim();
                        btnSetAddCompany.setVisibility(View.VISIBLE);
                        llSetCat.setVisibility(View.GONE);
                        llSetSubCat.setVisibility(View.GONE);
                        llSetProduct.setVisibility(View.GONE);

                    }

                }
            }
        });

        btnSetAddCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strProductCompany = txtSetProductCompany.getText().toString().trim();
                if (strProductCompany.length() < 3) {
                    txtSetProductCompany.setError("Please Enter Company Name First");
                } else {
                    addCompany();
                }
            }
        });



        txtSetProductCategory.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(ProductDetailsActivity.this, android.R.layout.select_dialog_item, editCategoryNameList);
                txtSetProductCategory.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
                txtSetProductCategory.setTextColor(getResources().getColor(R.color.black));
                txtSetProductCategory.setThreshold(1);

                llSetProduct.setVisibility(View.GONE);

                strProductCategory = txtSetProductCategory.getText().toString().trim();

                for (int j = 0; j < editCategoryNameList.size(); j++) {

                    if (strProductCategory.equals(editCategoryNameList.get(j).trim())) {

                        strFinalSubCategoryCatID = editCategoryIdList.get(j);
                        strFinalCategoryName = editCategoryNameList.get(j);
                        btnSetAddCategory.setVisibility(View.GONE);
                        llSetSubCat.setVisibility(View.VISIBLE);
                        llSetProduct.setVisibility(View.VISIBLE);
                        getSubCategory();
                        break;
                    } else {
                        strCategoryName = txtSetProductCategory.getText().toString().trim();
                        btnSetAddCategory.setVisibility(View.VISIBLE);
                        llSetSubCat.setVisibility(View.GONE);
                        llSetProduct.setVisibility(View.GONE);
                    }

                }


            }
        });

        btnSetAddCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strProductCategory = txtSetProductCategory.getText().toString().trim();
                if (strProductCategory.length() < 3) {
                    txtSetProductCategory.setError("Plase Enter Proper Category Name");
                } else {
                    addCategory();
                }

            }
        });

        txtSetProductSubCategory.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                btnSetAddSubCategory.setVisibility(View.VISIBLE);
                llSetProduct.setVisibility(View.GONE);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(ProductDetailsActivity.this, android.R.layout.select_dialog_item, editSubCategoryNameList);
                txtSetProductSubCategory.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
                txtSetProductSubCategory.setTextColor(getResources().getColor(R.color.black));
                txtSetProductSubCategory.setThreshold(1);

                strProductSubCategory = txtSetProductSubCategory.getText().toString().trim();

                for (int j = 0; j < editSubCategoryNameList.size(); j++) {

                    if (strProductSubCategory.equals(editSubCategoryNameList.get(j).trim())) {

                        strFinalSubCategoryName = editSubCategoryNameList.get(j);
                        strFinalSubCategoryID = editSubCategoryIdList.get(j);
                        strFinalSubCategoryCatID = editSubCategoryCatIdList.get(j);
                        btnSetAddSubCategory.setVisibility(View.GONE);
                        llSetProduct.setVisibility(View.VISIBLE);
                        break;
                    } else {
                        strProductSubCategory = txtSetProductSubCategory.getText().toString().trim();
                        btnSetAddSubCategory.setVisibility(View.VISIBLE);
                        llSetProduct.setVisibility(View.GONE);

                    }

                }


            }
        });

        btnSetAddSubCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strProductSubCategory = txtSetProductSubCategory.getText().toString().trim();
                if (strProductSubCategory.length() < 3) {
                    txtSetProductSubCategory.setError("Please Enter Proper SubCategory Name");
                } else {
                    addSubCategory();
                }

            }
        });

        spSetInStock.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Object item = parent.getItemAtPosition(position);
                strProductInStock = item.toString();

                if (strProductInStock.equals("No")) {

                    strProductStockQuantity = "0";
                    edtSetProductStockQuantity.setText("0");
                    edtSetProductStockQuantity.setEnabled(false);
                } else {
                    edtSetProductStockQuantity.setText("1");
                    edtSetProductStockQuantity.setEnabled(false);
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnUpdateProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strProductID = productsDetails.getProductId();
                strProductName = edtSetProductName.getText().toString().trim();
                strProductDesc = edtSetProductDescription.getText().toString().trim();
                strProductStockQuantity = edtSetProductStockQuantity.getText().toString().trim();
                strFinalSubCategoryName = txtSetProductSubCategory.getText().toString().trim();
                strFinalCompanyName = txtSetProductCompany.getText().toString().trim();
                strFinalCategoryName = txtSetProductCategory.getText().toString().trim();

                if (strFinalCompanyName.length() < 3) {
                    txtSetProductCompany.setError("Company Name Should be Greater Than 2 Character");
                    Toast.makeText(ProductDetailsActivity.this, "Company Name Should be Greater Than 2 Character", Toast.LENGTH_SHORT).show();

                } else if (strFinalCategoryName.length() < 3) {
                    txtSetProductCategory.setError("Category Name Should be Greater Than 2 Character");
                    Toast.makeText(ProductDetailsActivity.this, "Category Name Should be Greater Than 2 Character", Toast.LENGTH_SHORT).show();
                } else if (strFinalSubCategoryName.length() < 3) {
                    txtSetProductSubCategory.setError("Sub Category Name Should be Greater Than 2 Character");
                    Toast.makeText(ProductDetailsActivity.this, "Sub Category Name Should be Greater Than 2 Character", Toast.LENGTH_SHORT).show();
                } else if (strProductName.length() < 3) {
                    txtSetProductCategory.setError("Product Name Should be Greater Than 2 Character");
                    Toast.makeText(ProductDetailsActivity.this, "Product Name Should be Greater Than 2 Character", Toast.LENGTH_SHORT).show();
                } else if (strProductDesc.length() < 3) {
                    txtSetProductCategory.setError("Description Should be Greater Than 2 Character");
                    Toast.makeText(ProductDetailsActivity.this, "Description Should be Greater Than 2 Character", Toast.LENGTH_SHORT).show();
                }
                else {
                    UpdateItem();

                }
            }
        });


    }

    /*private void getBookingDatesForPreBooking() {

        addDialog = new ProgressDialog(this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Getting List...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        try{
            PreDateDataRequest getRentDateListRequest = new PreDateDataRequest();
            getRentDateListRequest.setBookingProdId(productsDetails.getProductId());


            Call<PreDateDataResponse> responceCall = ApiClient.getApiClient().getRentListDates(getRentDateListRequest);

            responceCall.enqueue(new Callback<PreDateDataResponse>() {
                @Override
                public void onResponse(Call<PreDateDataResponse> call, retrofit2.Response<PreDateDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        List<PreDateDataItem> items =response.body().getPreDateData();

                        for(int i=0; i<items.size(); i++){
                            rentDateList.add(items.get(i).getBookingFromDate());
                        }

                        addDialog.dismiss();


                    } else {

                        //Toast.makeText(RentListActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                        //imgNoProduct.setVisibility(View.VISIBLE);
                        addDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<PreDateDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialog.dismiss();

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }*/

    private void addSubCategory() {

        addDialog = new ProgressDialog(this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Adding SubCategory...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        try {
            SubCategoryDataRequest subCategoryDataRequest = new SubCategoryDataRequest();
            subCategoryDataRequest.setCatId(strFinalCategoryID);
            subCategoryDataRequest.setSubCatName(strProductSubCategory);

            Call<SubCategoryDataResponse> responceCall = ApiClient.getApiClient().addSubCategory(subCategoryDataRequest);

            responceCall.enqueue(new Callback<SubCategoryDataResponse>() {
                @Override
                public void onResponse(Call<SubCategoryDataResponse> call, retrofit2.Response<SubCategoryDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        List<SubCategoryDataItem> items = response.body().getSubCategoryData();

                        strFinalSubCategoryCatID = items.get(0).getCatId();
                        strFinalSubCategoryID = items.get(0).getSubCatId();
                        strFinalSubCategoryName = items.get(0).getSubCatName();
                        getSubCategory();
                        btnSetAddSubCategory.setVisibility(View.GONE);
                        llSetProduct.setVisibility(View.VISIBLE);
                        addDialog.dismiss();


                    } else {

                        Toast.makeText(ProductDetailsActivity.this, "Error :" + response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                        addDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<SubCategoryDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialog.dismiss();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addCategory() {

        addDialog = new ProgressDialog(this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Adding Category...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        try {
            CategoryDataRequest categoryDataRequest = new CategoryDataRequest();
            categoryDataRequest.setCatName(strProductCategory);

            Call<CategoryDataResponse> responceCall = ApiClient.getApiClient().addCategory(categoryDataRequest);

            responceCall.enqueue(new Callback<CategoryDataResponse>() {
                @Override
                public void onResponse(Call<CategoryDataResponse> call, retrofit2.Response<CategoryDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        List<CategoryDataItem> items = response.body().getCategoryData();

                        strFinalCategoryName = items.get(0).getCatName();
                        strFinalCategoryID = items.get(0).getCatId();
                        getCategory();
                        getSubCategory();
                        llSetSubCat.setVisibility(View.VISIBLE);
                        btnSetAddCategory.setVisibility(View.GONE);
                        addDialog.dismiss();


                    } else {

                        Toast.makeText(ProductDetailsActivity.this, "Error :" + response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                        addDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<CategoryDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialog.dismiss();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void addCompany() {

        addDialog = new ProgressDialog(this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Adding Company...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        try {
            CompanyDataRequest companyDataRequest = new CompanyDataRequest();
            companyDataRequest.setCompanyName(strProductCompany);

            Call<CompanyDataResponse> responceCall = ApiClient.getApiClient().addCompany(companyDataRequest);

            responceCall.enqueue(new Callback<CompanyDataResponse>() {
                @Override
                public void onResponse(Call<CompanyDataResponse> call, retrofit2.Response<CompanyDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        companyItems = response.body().getCompanyData();

                        strFinalCompanyName = companyItems.get(0).getCompanyName();
                        strFinalCompanyID = companyItems.get(0).getCompanyId();
                        getCompany();
                        llSetCat.setVisibility(View.VISIBLE);
                        llSetSubCat.setVisibility(View.VISIBLE);
                        btnSetAddCompany.setVisibility(View.GONE);
                        addDialog.dismiss();


                    } else {

                        Toast.makeText(ProductDetailsActivity.this, "Error :" + response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                        addDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<CompanyDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialog.dismiss();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getSubCategory() {

        editSubCategoryIdList.clear();
        editSubCategoryNameList.clear();
        editSubCategoryCatIdList.clear();

        SubCategoryDataRequest subCategoryDataRequest = new SubCategoryDataRequest();
        subCategoryDataRequest.setCatId(strFinalSubCategoryCatID);

        Call<SubCategoryDataResponse> responceCall = ApiClient.getApiClient().getSubCategory(subCategoryDataRequest);

        responceCall.enqueue(new Callback<SubCategoryDataResponse>() {
            @Override
            public void onResponse(Call<SubCategoryDataResponse> call, retrofit2.Response<SubCategoryDataResponse> response) {

                //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                if (response.body().getSuccess().equals("1")) {

                    subCategoryItems = response.body().getSubCategoryData();

                    //Toast.makeText(AdminMainActivity.this, ""+items, Toast.LENGTH_SHORT).show();

                    for (int i = 0; i < subCategoryItems.size(); i++) {

                        editSubCategoryNameList.add(subCategoryItems.get(i).getSubCatName());
                        editSubCategoryIdList.add(subCategoryItems.get(i).getSubCatId());
                        editSubCategoryCatIdList.add(subCategoryItems.get(i).getCatId());

                    }

                } else {

                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SubCategoryDataResponse> call, Throwable t) {
                //Log.d("erorr",t.getMessage());
                //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void getCategory() {

        editCategoryIdList.clear();
        editCategoryNameList.clear();

        CategoryDataRequest categoryDataRequest = new CategoryDataRequest();
        //catRequest.setCatName(""+strCategory);

        Call<CategoryDataResponse> responceCall = ApiClient.getApiClient().getCategory(categoryDataRequest);

        responceCall.enqueue(new Callback<CategoryDataResponse>() {
            @Override
            public void onResponse(Call<CategoryDataResponse> call, retrofit2.Response<CategoryDataResponse> response) {

                //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                if (response.body().getSuccess().equals("1")) {

                    categoryItems = response.body().getCategoryData();

                    //Toast.makeText(AdminMainActivity.this, ""+items, Toast.LENGTH_SHORT).show();

                    for (int i = 0; i < categoryItems.size(); i++) {

                        editCategoryNameList.add(categoryItems.get(i).getCatName());
                        editCategoryIdList.add(categoryItems.get(i).getCatId());

                    }

                } else {

                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CategoryDataResponse> call, Throwable t) {
                //Log.d("erorr",t.getMessage());
                //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void getCompany() {

        editCompanyIdList.clear();
        editCompanyNameList.clear();

        CompanyDataRequest companyDataRequest = new CompanyDataRequest();
        //catRequest.setCatName(""+strCategory);

        Call<CompanyDataResponse> responceCall = ApiClient.getApiClient().getCompany(companyDataRequest);

        responceCall.enqueue(new Callback<CompanyDataResponse>() {
            @Override
            public void onResponse(Call<CompanyDataResponse> call, retrofit2.Response<CompanyDataResponse> response) {

                //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                if (response.body().getSuccess().equals("1")) {

                    companyItems = response.body().getCompanyData();

                    //Toast.makeText(AdminMainActivity.this, ""+items, Toast.LENGTH_SHORT).show();

                    for (int i = 0; i < companyItems.size(); i++) {

                        editCompanyNameList.add(companyItems.get(i).getCompanyName());
                        editCompanyIdList.add(companyItems.get(i).getCompanyId());

                    }

                } else {

                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CompanyDataResponse> call, Throwable t) {
                //Log.d("erorr",t.getMessage());
                //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }


    private void UpdateItem() {

        addDialog = new ProgressDialog(ProductDetailsActivity.this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Updating Item...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        try {
            AddItemDataRequest addItemRequest = new AddItemDataRequest();

            addItemRequest.setProductId(strProductID);
            addItemRequest.setProductCompnyId(strFinalCompanyID);
            addItemRequest.setProductCategoryId(strFinalSubCategoryCatID);
            addItemRequest.setProductSubcategoryId(strFinalSubCategoryID);
            addItemRequest.setProductName(strProductName);
            addItemRequest.setProductDetails(strProductDesc);
            addItemRequest.setProductInStock(strProductInStock);
            addItemRequest.setProductQuantity(strProductStockQuantity);


            Call<AddItemDataResponse> responceCall = ApiClient.getApiClient().updateItem(addItemRequest);

            responceCall.enqueue(new Callback<AddItemDataResponse>() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void onResponse(Call<AddItemDataResponse> call, retrofit2.Response<AddItemDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        List<AddItemDataItem> items = response.body().getAddItemData();
                        onBackPressed();
                        addDialog.dismiss();


                    } else {

                        Toast.makeText(ProductDetailsActivity.this, "Error :" + response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                        addDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<AddItemDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialog.dismiss();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void deleteItem() {

        addDialog = new ProgressDialog(ProductDetailsActivity.this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Deleting Item...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        strProductID = productsDetails.getProductId();

        try {
            AddItemDataRequest addItemRequest = new AddItemDataRequest();

            addItemRequest.setProductId(strProductID);

            Call<AddItemDataResponse> responceCall = ApiClient.getApiClient().deleteItem(addItemRequest);

            responceCall.enqueue(new Callback<AddItemDataResponse>() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void onResponse(Call<AddItemDataResponse> call, retrofit2.Response<AddItemDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        List<AddItemDataItem> items = response.body().getAddItemData();
                        onBackPressed();
                        addDialog.dismiss();


                    } else {

                        Toast.makeText(ProductDetailsActivity.this, "Error :" + response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                        addDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<AddItemDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialog.dismiss();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private Bitmap TextToImageEncode(String qrProductID) throws Exception {
        BitMatrix bitMatrix;
        try {
            bitMatrix = new MultiFormatWriter().encode(
                    qrProductID,
                    BarcodeFormat.DATA_MATRIX.QR_CODE,
                    QRcodeWidth, QRcodeWidth, null
            );

        } catch (IllegalArgumentException Illegalargumentexception) {

            return null;
        }
        int bitMatrixWidth = bitMatrix.getWidth();

        int bitMatrixHeight = bitMatrix.getHeight();

        int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];

        for (int y = 0; y < bitMatrixHeight; y++) {
            int offset = y * bitMatrixWidth;

            for (int x = 0; x < bitMatrixWidth; x++) {

                pixels[offset + x] = bitMatrix.get(x, y) ?
                        getResources().getColor(R.color.black) : getResources().getColor(R.color.white);
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);

        bitmap.setPixels(pixels, 0, 350, 0, 0, bitMatrixWidth, bitMatrixHeight);
        return bitmap;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this,AllProductsActivity.class);
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
    }
}
