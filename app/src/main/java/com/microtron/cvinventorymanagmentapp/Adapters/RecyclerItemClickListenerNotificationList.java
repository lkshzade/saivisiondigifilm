package com.microtron.cvinventorymanagmentapp.Adapters;


import com.microtron.cvinventorymanagmentapp.NetworkModel.NotificationData.NotificationDataItem;

public interface RecyclerItemClickListenerNotificationList {
    void onItemClick(NotificationDataItem notice);
}
