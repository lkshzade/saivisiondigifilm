package com.microtron.cvinventorymanagmentapp.Adapters;


import com.microtron.cvinventorymanagmentapp.NetworkModel.OutSideInventoryBooking.OutSideBookingDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataItem;

public interface RecyclerItemClickListenerOutSideProductList {
    void onItemClick(OutSideBookingDataItem notice);
}
