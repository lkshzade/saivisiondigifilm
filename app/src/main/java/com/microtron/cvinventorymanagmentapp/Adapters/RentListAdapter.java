package com.microtron.cvinventorymanagmentapp.Adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.microtron.cvinventorymanagmentapp.Helper.ConfigFile;
import com.microtron.cvinventorymanagmentapp.Helper.StaticClass;
import com.microtron.cvinventorymanagmentapp.NetworkModel.GetAllProducts.AllProductsDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataItem;
import com.microtron.cvinventorymanagmentapp.ProductDetailsActivity;
import com.microtron.cvinventorymanagmentapp.R;
import com.microtron.cvinventorymanagmentapp.RentProductDetailsActivity;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;
import java.util.List;


public class RentListAdapter extends RecyclerView.Adapter<RentListAdapter.EmployeeViewHolder> implements Filterable {

private List<BookedItemDataItem> dataList;
private List<BookedItemDataItem> tempdataList;
private RecyclerItemClickListenerRentList recyclerItemClickListener;
        Context context;
        String name;
        float constantWeight;

public interface OnItemCheckListener {
    void onItemCheck(int item, float weight);

}


    public RentListAdapter(Context context, List<BookedItemDataItem> dataList, RecyclerItemClickListenerRentList recyclerItemClickListener) {
        this.dataList = dataList;
        this.tempdataList = dataList;
        this.context = context;
        this.recyclerItemClickListener = recyclerItemClickListener;
    }


    @Override
    public EmployeeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.rent_row, parent, false);
        return new EmployeeViewHolder(view);

    }


    @Override
    public void onBindViewHolder(final EmployeeViewHolder holder, @SuppressLint("RecyclerView") final int position) {


        final BookedItemDataItem subjects = tempdataList.get(position);
        holder.txtCustName.setText(subjects.getCustomerName());
        holder.txtBookingDate.setText(subjects.getBookingFromDate());
        holder.txtProductName.setText(subjects.getProductName());
        holder.txtDetails.setText(subjects.getProductDetails());
        int daysCount = Math.round(Float.parseFloat(subjects.getDaysCount()));
        holder.txtDays.setText(daysCount+" Days");
        holder.txtRent.setText(subjects.getTotalRent()+" Rs.");
        holder.txtReturnDate.setText(subjects.getBookingToDate());

        holder.txtDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final BookedItemDataItem bookedItem = tempdataList.get(position);

                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.text_pop_up);
                TextView txtMessage = (TextView) dialog.findViewById(R.id.dialogMessage);

                if(subjects.getProductDetails().equals("")){
                    txtMessage.setText("No Description Available");
                }
                else{
                    txtMessage.setText(subjects.getProductDetails());
                }

                txtMessage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                recyclerItemClickListener.onItemClick(tempdataList.get(position));

                BookedItemDataItem bookedProduct = tempdataList.get(position);

                BookedItemDataItem detailsBookedProduct = new BookedItemDataItem();

                detailsBookedProduct.setBookingId(bookedProduct.getBookingId());
                detailsBookedProduct.setCustomerName(bookedProduct.getCustomerName());
                detailsBookedProduct.setCustomerEmail(bookedProduct.getCustomerEmail());
                detailsBookedProduct.setCustomerMobile(bookedProduct.getCustomerMobile());
                detailsBookedProduct.setProductId(bookedProduct.getProductId());
                detailsBookedProduct.setProductName(bookedProduct.getProductName());
                detailsBookedProduct.setProductDetails(bookedProduct.getProductDetails());
                detailsBookedProduct.setProductInStock(bookedProduct.getProductInStock());
                detailsBookedProduct.setProductQuantity(bookedProduct.getProductQuantity());
                detailsBookedProduct.setBookingFromDate(bookedProduct.getBookingFromDate());
                detailsBookedProduct.setBookingToDate(bookedProduct.getBookingToDate());
                detailsBookedProduct.setDaysCount(bookedProduct.getDaysCount());
                detailsBookedProduct.setTotalRent(bookedProduct.getTotalRent());

                detailsBookedProduct.setBookingType(bookedProduct.getBookingType());
                detailsBookedProduct.setMonthRent(bookedProduct.getMonthRent());
                detailsBookedProduct.setDailyRent(bookedProduct.getDailyRent());
                detailsBookedProduct.setRentAdvance(bookedProduct.getRentAdvance());
                detailsBookedProduct.setRentPending(bookedProduct.getRentPending());
                detailsBookedProduct.setBookingStatus(bookedProduct.getBookingStatus());
                detailsBookedProduct.setBookingProdId(bookedProduct.getBookingProdId());
                detailsBookedProduct.setBookingCustId(bookedProduct.getBookingCustId());
                detailsBookedProduct.setTotalRent(bookedProduct.getTotalRent());

                StaticClass.bookedProduct = detailsBookedProduct;

                Activity activity = (Activity) context;
                Intent intent = new Intent(activity, RentProductDetailsActivity.class);
                activity.startActivity(intent);
                activity.overridePendingTransition(0,0);


            }
        });

        Picasso.get()
                .load(ConfigFile.domain+ConfigFile.domaindir+"uploads/productImages/"+subjects.getProductImgUrl1()+".jpg")
                .error(R.drawable.no_image)
                .resize(400,400)
                .onlyScaleDown()
                .into(holder.imgProduct);

    }



    @Override
    public int getItemCount() {
        return tempdataList.size();
    }


class EmployeeViewHolder extends RecyclerView.ViewHolder {

    TextView txtProductName, txtDetails, txtDays, txtRent, txtReturnDate, txtCustName, txtBookingDate;
    ImageView imgProduct;


    EmployeeViewHolder(View itemView) {
        super(itemView);


        imgProduct = itemView.findViewById(R.id.imgRowProduct);
        txtCustName = itemView.findViewById(R.id.txtCustName);
        txtBookingDate = itemView.findViewById(R.id.txtBookingDate);
        txtProductName = itemView.findViewById(R.id.txtRowProductName);
        txtDetails = itemView.findViewById(R.id.txtRowDetails);
        txtDays = itemView.findViewById(R.id.txtRowDays);
        txtRent = itemView.findViewById(R.id.txtRowRent);
        txtReturnDate = itemView.findViewById(R.id.txtRowReturnDate);
    }
}

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString().toLowerCase();
                if (charString.isEmpty()) {
                    tempdataList = dataList;
                } else {
                    ArrayList<BookedItemDataItem> filteredList = new ArrayList<>();
                    for (BookedItemDataItem row : dataList) {

                        if (row.toString().toLowerCase().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }
                    tempdataList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = tempdataList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                tempdataList = (ArrayList<BookedItemDataItem>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
