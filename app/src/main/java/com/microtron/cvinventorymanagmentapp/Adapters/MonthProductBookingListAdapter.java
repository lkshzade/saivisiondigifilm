package com.microtron.cvinventorymanagmentapp.Adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.microtron.cvinventorymanagmentapp.Helper.ConfigFile;
import com.microtron.cvinventorymanagmentapp.Helper.StaticClass;
import com.microtron.cvinventorymanagmentapp.NetworkModel.GetAllProducts.AllProductsDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataItem;
import com.microtron.cvinventorymanagmentapp.ProductDetailsActivity;
import com.microtron.cvinventorymanagmentapp.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class MonthProductBookingListAdapter extends RecyclerView.Adapter<MonthProductBookingListAdapter.EmployeeViewHolder> implements Filterable {

private List<BookedItemDataItem> dataList;
private List<BookedItemDataItem> tempdataList;
private RecyclerItemClickListenerMonthProductBookingList recyclerItemClickListener;
Context context;

public interface OnItemCheckListener {
    void onItemCheck(int item, float weight);

}


    public MonthProductBookingListAdapter(Context context, List<BookedItemDataItem> dataList, RecyclerItemClickListenerMonthProductBookingList recyclerItemClickListener) {
        this.dataList = dataList;
        this.tempdataList = dataList;
        this.context = context;
        this.recyclerItemClickListener = recyclerItemClickListener;
    }


    @Override
    public EmployeeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.month_item, parent, false);
        return new EmployeeViewHolder(view);

    }


    @Override
    public void onBindViewHolder(final EmployeeViewHolder holder, @SuppressLint("RecyclerView") final int position) {


        final BookedItemDataItem subjects = tempdataList.get(position);
        holder.txtRentMonthProductName.setText(subjects.getProductName());
        holder.txtRentMonthCustName.setText(subjects.getCustomerName());
        holder.txtRentMonthFromDate.setText(subjects.getBookingFromDate());
        holder.txtRentMonthToDate.setText(subjects.getBookingToDate());

       /* holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                recyclerItemClickListener.onItemClick(tempdataList.get(position));

                *//*BookedItemDataItem allProduct = tempdataList.get(position);

                AllProductsDataItem detailsProduct = new AllProductsDataItem();

                detailsProduct.setProductId(allProduct.getProductId());
                detailsProduct.setCompanyId(allProduct.getCompanyId());
                detailsProduct.setCompanyName(allProduct.getCompanyName());
                detailsProduct.setProductCategoryId(allProduct.getProductCategoryId());
                detailsProduct.setCatName(allProduct.getCatName());
                detailsProduct.setProductSubcategoryId(allProduct.getProductSubcategoryId());
                detailsProduct.setSubCatName(allProduct.getSubCatName());
                detailsProduct.setProductName(allProduct.getProductName());
                detailsProduct.setProductDetails(allProduct.getProductDetails());
                detailsProduct.setProductPrice(allProduct.getProductPrice());
                detailsProduct.setProductInStock(allProduct.getProductInStock());
                detailsProduct.setProductQuantity(allProduct.getProductQuantity());

                StaticClass.product = detailsProduct;

                Activity activity = (Activity) context;
                Intent intent = new Intent(activity, ProductDetailsActivity.class);
                activity.startActivity(intent);
                activity.overridePendingTransition(0,0);*//*


            }
        });*/

        Picasso.get()
                .load(ConfigFile.domain+ConfigFile.domaindir+"uploads/productImages/"+subjects.getProductImgUrl1()+".jpg")
                .error(R.drawable.no_image)
                .resize(400,400)
                .onlyScaleDown()
                .into(holder.imgRentMonth);

    }

    @Override
    public int getItemCount() {
        return tempdataList.size();
    }


class EmployeeViewHolder extends RecyclerView.ViewHolder {

    TextView txtRentMonthProductName, txtRentMonthCustName, txtRentMonthFromDate, txtRentMonthToDate;
    ImageView imgRentMonth;
    LinearLayout viewDetails;


    EmployeeViewHolder(View itemView) {
        super(itemView);


        imgRentMonth = itemView.findViewById(R.id.imgRentMonth);
        txtRentMonthProductName = itemView.findViewById(R.id.txtRentMonthProductName);
        txtRentMonthCustName = itemView.findViewById(R.id.txtRentMonthCustName);
        txtRentMonthFromDate = itemView.findViewById(R.id.txtRentMonthFromDate);
        txtRentMonthToDate = itemView.findViewById(R.id.txtRentMonthToDate);
        viewDetails = itemView.findViewById(R.id.viewDetails);
    }
}

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString().toLowerCase();
                if (charString.isEmpty()) {
                    tempdataList = dataList;
                } else {
                    ArrayList<BookedItemDataItem> filteredList = new ArrayList<>();
                    for (BookedItemDataItem row : dataList) {

                        if (row.toString().toLowerCase().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }
                    tempdataList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = tempdataList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                tempdataList = (ArrayList<BookedItemDataItem>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
