package com.microtron.cvinventorymanagmentapp.Adapters;


import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataItem;

public interface RecyclerItemClickListenerCalendarEventsList {
    void onItemClick(BookedItemDataItem notice);
}
