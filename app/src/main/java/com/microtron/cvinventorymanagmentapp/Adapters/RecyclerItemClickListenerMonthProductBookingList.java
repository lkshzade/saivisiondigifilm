package com.microtron.cvinventorymanagmentapp.Adapters;


import com.microtron.cvinventorymanagmentapp.NetworkModel.GetAllProducts.AllProductsDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataItem;

public interface RecyclerItemClickListenerMonthProductBookingList {
    void onItemClick(BookedItemDataItem notice);
}
