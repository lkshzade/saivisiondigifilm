package com.microtron.cvinventorymanagmentapp.Adapters;


import com.microtron.cvinventorymanagmentapp.NetworkModel.Customer.CustomerDataItem;

public interface RecyclerItemClickListenerPendingCustomerList {
    void onItemClick(CustomerDataItem notice);
}
