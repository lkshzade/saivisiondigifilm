package com.microtron.cvinventorymanagmentapp.Adapters;


import com.microtron.cvinventorymanagmentapp.NetworkModel.TransactionHistory.TransactionHistoryDataItem;

public interface RecyclerItemClickListenerTransactionHistoryList {
    void onItemClick(TransactionHistoryDataItem notice);
}
