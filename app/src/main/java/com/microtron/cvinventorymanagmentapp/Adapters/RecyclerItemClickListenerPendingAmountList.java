package com.microtron.cvinventorymanagmentapp.Adapters;

import com.microtron.cvinventorymanagmentapp.NetworkModel.PendingAmounts.PendingAmountDataItem;

public interface RecyclerItemClickListenerPendingAmountList {
    void onItemClick(PendingAmountDataItem notice);
}
