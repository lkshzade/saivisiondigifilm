package com.microtron.cvinventorymanagmentapp.Adapters;


import com.microtron.cvinventorymanagmentapp.NetworkModel.Customer.CustomerDataItem;

public interface RecyclerItemClickListenerCustomerList {
    void onItemClick(CustomerDataItem notice);
}
