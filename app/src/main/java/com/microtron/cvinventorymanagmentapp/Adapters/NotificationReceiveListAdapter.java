package com.microtron.cvinventorymanagmentapp.Adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.microtron.cvinventorymanagmentapp.Helper.ApiClient;
import com.microtron.cvinventorymanagmentapp.Helper.AppConstants;
import com.microtron.cvinventorymanagmentapp.Helper.Sp;
import com.microtron.cvinventorymanagmentapp.Helper.StaticClass;
import com.microtron.cvinventorymanagmentapp.NetworkModel.NotificationData.NotificationDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.NotificationData.NotificationDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.NotificationData.NotificationDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataItem;
import com.microtron.cvinventorymanagmentapp.R;
import com.microtron.cvinventorymanagmentapp.RentProductDetailsActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;


public class NotificationReceiveListAdapter extends RecyclerView.Adapter<NotificationReceiveListAdapter.EmployeeViewHolder> implements Filterable {

private List<NotificationDataItem> dataList;
private List<NotificationDataItem> tempdataList;
private RecyclerItemClickListenerNotificationList recyclerItemClickListener;
        Context context;
        private String strNotifyFrom;
        private ProgressDialog addDialog;


public interface OnItemCheckListener {
    void onItemCheck(int item, float weight);

}


    public NotificationReceiveListAdapter(Context context, List<NotificationDataItem> dataList, RecyclerItemClickListenerNotificationList recyclerItemClickListener) {
        this.dataList = dataList;
        this.tempdataList = dataList;
        this.context = context;
        this.recyclerItemClickListener = recyclerItemClickListener;
    }


    @Override
    public EmployeeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.notification_row, parent, false);
        /*strNotifyFrom = Sp.readFromPreferences(context, AppConstants.NOTIFY_FROM,"");
        if(strNotifyFrom.equals("Booking Date")){
            title = "Booking Date";
        }else if(strNotifyFrom.equals("Return Date")){
            title = "Return Date";
        }*/
        return new EmployeeViewHolder(view);

    }


    @Override
    public void onBindViewHolder(final EmployeeViewHolder holder, @SuppressLint("RecyclerView") final int position) {


        final NotificationDataItem subjects = tempdataList.get(position);

        holder.txtNotificationDateTime.setText(subjects.getNotificationDateTime());
        holder.txtNotificationProdName.setText(subjects.getProductName());
        holder.txtNotificationCustName.setText(subjects.getCustomerName());
        holder.txtNotificationBookingDateTitle.setText("Return Date");
        holder.txtNotificationBookingDate.setText(subjects.getBookingToDate());

        holder.deleteNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final NotificationDataItem notificationItem = tempdataList.get(position);

                AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(context);
                alertDialog2.setTitle("Delete...");
                alertDialog2.setMessage("Do you want to Delete Notification?");

                alertDialog2.setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.cancel();

                                Toast.makeText(context, "Deleted...!", Toast.LENGTH_SHORT).show();

                                addDialog = new ProgressDialog(context);
                                addDialog.setCancelable(true);
                                addDialog.setMessage("Deleting Notification...");
                                addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                addDialog.show();

                                try{
                                    final NotificationDataRequest notificationRequest = new NotificationDataRequest();
                                    notificationRequest.setNotificationStatus("1");
                                    notificationRequest.setNotificationId(notificationItem.getNotificationId());
                                    Call<NotificationDataResponse> responceCall = ApiClient.getApiClient().updateNotification(notificationRequest);

                                    responceCall.enqueue(new Callback<NotificationDataResponse>() {
                                        @Override
                                        public void onResponse(Call<NotificationDataResponse> call, retrofit2.Response<NotificationDataResponse> response) {

                                            //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                                            if (response.body().getSuccess().equals("1")) {

                                                List<NotificationDataItem> items =response.body().getNotificationData();

                                                dataList.remove(position);
                                                notifyDataSetChanged();

                                                addDialog.dismiss();


                                            } else {

                                                //Toast.makeText(RentListActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                                                addDialog.dismiss();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<NotificationDataResponse> call, Throwable t) {
                                            //Log.d("erorr",t.getMessage());
                                            //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                                            addDialog.dismiss();

                                        }
                                    });
                                }
                                catch (Exception e){
                                    e.printStackTrace();
                                }
                            }
                        });
                alertDialog2.setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                //Toast.makeText(context, "Don't Delete Notification", Toast.LENGTH_SHORT).show();
                                dialog.cancel();
                            }
                        });

                alertDialog2.show();
            }
        });

        /*holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                recyclerItemClickListener.onItemClick(tempdataList.get(position));

                NotificationDataItem bookedProduct = tempdataList.get(position);

                BookedItemDataItem detailsBookedProduct = new BookedItemDataItem();

                detailsBookedProduct.setBookingId(bookedProduct.getBookingId());
                detailsBookedProduct.setCustomerName(bookedProduct.getCustomerName());
                detailsBookedProduct.setCustomerEmail(bookedProduct.getCustomerEmail());
                detailsBookedProduct.setCustomerMobile(bookedProduct.getCustomerMobile());
                detailsBookedProduct.setProductId(bookedProduct.getProductId());
                detailsBookedProduct.setProductName(bookedProduct.getProductName());
                detailsBookedProduct.setProductDetails(bookedProduct.getProductDetails());
                detailsBookedProduct.setProductInStock(bookedProduct.getProductInStock());
                detailsBookedProduct.setProductQuantity(bookedProduct.getProductQuantity());
                detailsBookedProduct.setBookingFromDate(bookedProduct.getBookingFromDate());
                detailsBookedProduct.setBookingToDate(bookedProduct.getBookingToDate());
                detailsBookedProduct.setDaysCount(bookedProduct.getDaysCount());
                detailsBookedProduct.setTotalRent(bookedProduct.getTotalRent());

                detailsBookedProduct.setBookingType(bookedProduct.getBookingType());
                detailsBookedProduct.setRentAdvance(bookedProduct.getRentAdvance());
                detailsBookedProduct.setRentPending(bookedProduct.getRentPending());
                detailsBookedProduct.setBookingStatus(bookedProduct.getBookingStatus());
                detailsBookedProduct.setBookingProdId(bookedProduct.getBookingProdId());
                detailsBookedProduct.setBookingCustId(bookedProduct.getBookingCustId());
                detailsBookedProduct.setTotalRent(bookedProduct.getTotalRent());

                StaticClass.bookedProduct = detailsBookedProduct;

                Activity activity = (Activity) context;
                Intent intent = new Intent(activity, RentProductDetailsActivity.class);
                activity.startActivity(intent);
                activity.overridePendingTransition(0,0);


            }
        });*/



    }



    @Override
    public int getItemCount() {
        return tempdataList.size();
    }


class EmployeeViewHolder extends RecyclerView.ViewHolder {

    TextView txtNotificationDateTime, txtNotificationProdName, txtNotificationCustName, txtNotificationBookingDateTitle, txtNotificationBookingDate;
    ImageView deleteNotification;


    EmployeeViewHolder(View itemView) {
        super(itemView);


        txtNotificationDateTime = itemView.findViewById(R.id.txtNotificationDateTime);
        txtNotificationProdName = itemView.findViewById(R.id.txtNotificationProdName);
        txtNotificationCustName = itemView.findViewById(R.id.txtNotificationCustName);
        txtNotificationBookingDateTitle = itemView.findViewById(R.id.txtNotificationBookingDateTitle);
        txtNotificationBookingDate = itemView.findViewById(R.id.txtNotificationBookingDate);
        deleteNotification = itemView.findViewById(R.id.deleteNotification);

    }
}

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString().toLowerCase();
                if (charString.isEmpty()) {
                    tempdataList = dataList;
                } else {
                    ArrayList<NotificationDataItem> filteredList = new ArrayList<>();
                    for (NotificationDataItem row : dataList) {

                        if (row.toString().toLowerCase().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }
                    tempdataList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = tempdataList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                tempdataList = (ArrayList<NotificationDataItem>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
