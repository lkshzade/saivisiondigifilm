package com.microtron.cvinventorymanagmentapp.Adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.microtron.cvinventorymanagmentapp.Helper.ConfigFile;
import com.microtron.cvinventorymanagmentapp.Helper.StaticClass;
import com.microtron.cvinventorymanagmentapp.NetworkModel.SaleItemData.SaleItemDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataItem;
import com.microtron.cvinventorymanagmentapp.R;
import com.microtron.cvinventorymanagmentapp.RentProductDetailsActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class SaleProductListAdapter extends RecyclerView.Adapter<SaleProductListAdapter.EmployeeViewHolder> implements Filterable {

private List<SaleItemDataItem> dataList;
private List<SaleItemDataItem> tempdataList;
private RecyclerItemClickListenerSaleProductList recyclerItemClickListener;
        Context context;
        String name;
        float constantWeight;

public interface OnItemCheckListener {
    void onItemCheck(int item, float weight);

}


    public SaleProductListAdapter(Context context, List<SaleItemDataItem> dataList, RecyclerItemClickListenerSaleProductList recyclerItemClickListener) {
        this.dataList = dataList;
        this.tempdataList = dataList;
        this.context = context;
        this.recyclerItemClickListener = recyclerItemClickListener;
    }


    @Override
    public EmployeeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.sale_row, parent, false);
        return new EmployeeViewHolder(view);

    }


    @Override
    public void onBindViewHolder(final EmployeeViewHolder holder, @SuppressLint("RecyclerView") final int position) {


        final SaleItemDataItem subjects = tempdataList.get(position);
        holder.txtSaleCustName.setText(subjects.getCustomerName());
        holder.txtSaleDate.setText(subjects.getSaleDate());
        holder.txtSaleProductName.setText(subjects.getProductName());
        holder.txtSaleDetails.setText(subjects.getProductDetails());

        holder.txtSaleRate.setText(subjects.getSaleRate());

        holder.txtSaleDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final SaleItemDataItem bookedItem = tempdataList.get(position);

                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.text_pop_up);
                TextView txtMessage = (TextView) dialog.findViewById(R.id.dialogMessage);

                if(subjects.getProductDetails().equals("")){
                    txtMessage.setText("No Description Available");
                }
                else{
                    txtMessage.setText(subjects.getProductDetails());
                }

                txtMessage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        /*holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                recyclerItemClickListener.onItemClick(tempdataList.get(position));

                SaleItemDataItem bookedProduct = tempdataList.get(position);

                BookedItemDataItem detailsBookedProduct = new BookedItemDataItem();

                detailsBookedProduct.setBookingId(bookedProduct.getBookingId());
                detailsBookedProduct.setCustomerName(bookedProduct.getCustomerName());
                detailsBookedProduct.setCustomerEmail(bookedProduct.getCustomerEmail());
                detailsBookedProduct.setCustomerMobile(bookedProduct.getCustomerMobile());
                detailsBookedProduct.setProductId(bookedProduct.getProductId());
                detailsBookedProduct.setProductName(bookedProduct.getProductName());
                detailsBookedProduct.setProductDetails(bookedProduct.getProductDetails());
                detailsBookedProduct.setProductInStock(bookedProduct.getProductInStock());
                detailsBookedProduct.setProductQuantity(bookedProduct.getProductQuantity());
                detailsBookedProduct.setBookingFromDate(bookedProduct.getBookingFromDate());
                detailsBookedProduct.setBookingToDate(bookedProduct.getBookingToDate());
                detailsBookedProduct.setDaysCount(bookedProduct.getDaysCount());
                detailsBookedProduct.setTotalRent(bookedProduct.getTotalRent());

                detailsBookedProduct.setBookingType(bookedProduct.getBookingType());
                detailsBookedProduct.setRentAdvance(bookedProduct.getRentAdvance());
                detailsBookedProduct.setRentPending(bookedProduct.getRentPending());
                detailsBookedProduct.setBookingStatus(bookedProduct.getBookingStatus());
                detailsBookedProduct.setBookingProdId(bookedProduct.getBookingProdId());
                detailsBookedProduct.setBookingCustId(bookedProduct.getBookingCustId());
                detailsBookedProduct.setTotalRent(bookedProduct.getTotalRent());

                StaticClass.bookedProduct = detailsBookedProduct;

                Activity activity = (Activity) context;
                Intent intent = new Intent(activity, RentProductDetailsActivity.class);
                activity.startActivity(intent);
                activity.overridePendingTransition(0,0);


            }
        });*/

        Picasso.get()
                .load(ConfigFile.domain+ConfigFile.domaindir+"uploads/productImages/"+subjects.getProductImgUrl1()+".jpg")
                .error(R.drawable.no_image)
                .resize(400,400)
                .onlyScaleDown()
                .into(holder.imgSaleProduct);

    }



    @Override
    public int getItemCount() {
        return tempdataList.size();
    }


    class EmployeeViewHolder extends RecyclerView.ViewHolder {

        TextView txtSaleCustName, txtSaleDate, txtSaleDetails, txtSaleProductName, txtSaleRate;
        ImageView imgSaleProduct;


        EmployeeViewHolder(View itemView) {
            super(itemView);


            imgSaleProduct = itemView.findViewById(R.id.imgSaleProduct);
            txtSaleCustName = itemView.findViewById(R.id.txtSaleCustName);
            txtSaleDate = itemView.findViewById(R.id.txtSaleDate);
            txtSaleDetails = itemView.findViewById(R.id.txtSaleDetails);
            txtSaleProductName = itemView.findViewById(R.id.txtSaleProductName);
            txtSaleRate = itemView.findViewById(R.id.txtSaleRate);


        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString().toLowerCase();
                if (charString.isEmpty()) {
                    tempdataList = dataList;
                } else {
                    ArrayList<SaleItemDataItem> filteredList = new ArrayList<>();
                    for (SaleItemDataItem row : dataList) {

                        if (row.toString().toLowerCase().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }
                    tempdataList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = tempdataList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                tempdataList = (ArrayList<SaleItemDataItem>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
