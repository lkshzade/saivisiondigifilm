package com.microtron.cvinventorymanagmentapp.Adapters;


import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataResponse;

public interface RecyclerItemClickListenerRentList {
    void onItemClick(BookedItemDataItem notice);
}
