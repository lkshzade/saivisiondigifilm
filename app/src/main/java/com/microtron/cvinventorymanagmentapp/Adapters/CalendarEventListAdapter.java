package com.microtron.cvinventorymanagmentapp.Adapters;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.microtron.cvinventorymanagmentapp.Helper.ConfigFile;
import com.microtron.cvinventorymanagmentapp.Helper.StaticClass;
import com.microtron.cvinventorymanagmentapp.MainActivity;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataItem;
import com.microtron.cvinventorymanagmentapp.R;
import com.microtron.cvinventorymanagmentapp.RentProductDetailsActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class CalendarEventListAdapter extends RecyclerView.Adapter<CalendarEventListAdapter.EmployeeViewHolder> implements Filterable {

private List<BookedItemDataItem> dataList;
private List<BookedItemDataItem> tempdataList;
private RecyclerItemClickListenerCalendarEventsList recyclerItemClickListener;
        Context context;
        String name;
        float constantWeight;

public interface OnItemCheckListener {
    void onItemCheck(int item, float weight);

}


    public CalendarEventListAdapter(Context context, List<BookedItemDataItem> dataList, RecyclerItemClickListenerCalendarEventsList recyclerItemClickListener) {
        this.dataList = dataList;
        this.tempdataList = dataList;
        this.context = context;
        this.recyclerItemClickListener = recyclerItemClickListener;
    }


    @Override
    public EmployeeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.calendar_event_row, parent, false);
        return new EmployeeViewHolder(view);

    }


    @Override
    public void onBindViewHolder(final EmployeeViewHolder holder, @SuppressLint("RecyclerView") final int position) {


        final BookedItemDataItem subjects = tempdataList.get(position);

        holder.txtEventProductName.setText(subjects.getProductName());
        holder.txtEventCustName.setText(subjects.getCustomerName());
        holder.txtEventCustMobile.setText(subjects.getCustomerMobile());
        holder.txtEventBookingDate.setText(subjects.getBookingFromDate());
        holder.txtEventReturnDate.setText(subjects.getBookingToDate());
        holder.txtEventTotalRent.setText(subjects.getBookingToDate());

        holder.btnEventCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkPermissions();

                Intent callIntent = new Intent(Intent.ACTION_CALL);

                callIntent.setData(Uri.parse("tel:" + subjects.getCustomerMobile()));

                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                    return;

                }
                context.startActivity(callIntent);

            }
        });


        /*holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                recyclerItemClickListener.onItemClick(tempdataList.get(position));

                BookedItemDataItem bookedProduct = tempdataList.get(position);

                BookedItemDataItem detailsBookedProduct = new BookedItemDataItem();

                detailsBookedProduct.setBookingId(bookedProduct.getBookingId());
                detailsBookedProduct.setCustomerName(bookedProduct.getCustomerName());
                detailsBookedProduct.setCustomerEmail(bookedProduct.getCustomerEmail());
                detailsBookedProduct.setCustomerMobile(bookedProduct.getCustomerMobile());
                detailsBookedProduct.setProductId(bookedProduct.getProductId());
                detailsBookedProduct.setProductName(bookedProduct.getProductName());
                detailsBookedProduct.setProductDetails(bookedProduct.getProductDetails());
                detailsBookedProduct.setProductInStock(bookedProduct.getProductInStock());
                detailsBookedProduct.setProductQuantity(bookedProduct.getProductQuantity());
                detailsBookedProduct.setBookingFromDate(bookedProduct.getBookingFromDate());
                detailsBookedProduct.setBookingToDate(bookedProduct.getBookingToDate());
                detailsBookedProduct.setDaysCount(bookedProduct.getDaysCount());
                detailsBookedProduct.setTotalRent(bookedProduct.getTotalRent());

                detailsBookedProduct.setBookingType(bookedProduct.getBookingType());
                detailsBookedProduct.setMonthRent(bookedProduct.getMonthRent());
                detailsBookedProduct.setDailyRent(bookedProduct.getDailyRent());
                detailsBookedProduct.setRentAdvance(bookedProduct.getRentAdvance());
                detailsBookedProduct.setRentPending(bookedProduct.getRentPending());

                StaticClass.bookedProduct = detailsBookedProduct;

                Activity activity = (Activity) context;
                Intent intent = new Intent(activity, RentProductDetailsActivity.class);
                activity.startActivity(intent);
                activity.overridePendingTransition(0,0);


            }
        });*/

        /*Picasso.get()
                .load(ConfigFile.domain+ConfigFile.domaindir+"uploads/productImages/"+subjects.getProductImgUrl1()+".jpg")
                .error(R.drawable.no_image)
                .resize(400,400)
                .onlyScaleDown()
                .into(holder.imgProduct);*/

    }

    private void checkPermissions() {

        Activity activity = (Activity) context;

        Dexter.withActivity(activity)
                .withPermissions(Manifest.permission.CALL_PHONE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            // Toast.makeText(getApplicationContext(), "All permissions are granted!", Toast.LENGTH_SHORT).show();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        //Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();

    }

    private void showSettingsDialog() {

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    // navigating user to app settings
    private void openSettings() {

        Activity activity = (Activity) context;

        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
        intent.setData(uri);
        context.startActivity(intent);

        /*Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);*/
    }

    @Override
    public int getItemCount() {
        return tempdataList.size();
    }


class EmployeeViewHolder extends RecyclerView.ViewHolder {

    TextView txtEventProductName, txtEventCustName, txtEventCustMobile, txtEventBookingDate, txtEventReturnDate, txtEventTotalRent;
    Button btnEventCall;


    EmployeeViewHolder(View itemView) {
        super(itemView);

        txtEventProductName = itemView.findViewById(R.id.txtEventProductName);
        txtEventCustName = itemView.findViewById(R.id.txtEventCustName);
        txtEventCustMobile = itemView.findViewById(R.id.txtEventCustMobile);
        txtEventBookingDate = itemView.findViewById(R.id.txtEventBookingDate);
        txtEventReturnDate = itemView.findViewById(R.id.txtEventReturningDate);
        txtEventTotalRent = itemView.findViewById(R.id.txtEventTotalRent);
        btnEventCall = itemView.findViewById(R.id.btnEventCall);

    }
}

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString().toLowerCase();
                if (charString.isEmpty()) {
                    tempdataList = dataList;
                } else {
                    ArrayList<BookedItemDataItem> filteredList = new ArrayList<>();
                    for (BookedItemDataItem row : dataList) {

                        if (row.toString().toLowerCase().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }
                    tempdataList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = tempdataList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                tempdataList = (ArrayList<BookedItemDataItem>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
