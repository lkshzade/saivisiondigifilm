package com.microtron.cvinventorymanagmentapp.Adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.camera2.TotalCaptureResult;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.microtron.cvinventorymanagmentapp.BookingActivity;
import com.microtron.cvinventorymanagmentapp.Helper.ApiClient;
import com.microtron.cvinventorymanagmentapp.NetworkModel.BookingItem.BookingDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.BookingItem.BookingDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.BookingItem.BookingDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.Customer.CustomerDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PendingAmounts.PendingAmountDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.UpdatePendings.UpdatedCustPendingsDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.UpdatePendings.UpdatedCustPendingsDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.UpdatePendings.UpdatedCustPendingsDataResponse;
import com.microtron.cvinventorymanagmentapp.R;
import com.microtron.cvinventorymanagmentapp.RentListActivity;
import com.microtron.cvinventorymanagmentapp.RentProductDetailsActivity;
import com.microtron.cvinventorymanagmentapp.ViewCustomerPendingActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;


public class PendingAmountListAdapter extends RecyclerView.Adapter<PendingAmountListAdapter.EmployeeViewHolder> implements Filterable {

private List<PendingAmountDataItem> dataList;
private List<PendingAmountDataItem> tempdataList;
private RecyclerItemClickListenerPendingAmountList recyclerItemClickListener;
Context context;
private String strAddAmount, strPending, strAdvance, strBookingStatus, strBookingId, strTransactionActivity, strTransactionAmount, strProductId, strCustId, strTotalRent;
private float pending, advance, addAmmount;
private ProgressDialog addDialog;

public interface OnItemCheckListener {
    void onItemCheck(int item, float weight);

}


    public PendingAmountListAdapter(Context context, List<PendingAmountDataItem> dataList, RecyclerItemClickListenerPendingAmountList recyclerItemClickListener) {
        this.dataList = dataList;
        this.tempdataList = dataList;
        this.context = context;
        this.recyclerItemClickListener = recyclerItemClickListener;
    }


    @Override
    public EmployeeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.pending_amount_row, parent, false);
        return new EmployeeViewHolder(view);

    }


    @Override
    public void onBindViewHolder(final EmployeeViewHolder holder, @SuppressLint("RecyclerView") final int position) {


        final PendingAmountDataItem subjects = tempdataList.get(position);

        holder.txtCustProductName.setText(subjects.getProductName());
        holder.txtCustBookingDate.setText(subjects.getBookingFromDate());
        holder.txtCustReturnDate.setText(subjects.getBookingToDate());
        holder.txtCustTotalRent.setText(subjects.getTotalRent());
        holder.txtCustAdvance.setText(subjects.getRentAdvance());
        holder.txtCustPendings.setText(subjects.getRentPending());

        strBookingStatus = subjects.getBookingStatus();
        strPending = holder.txtCustPendings.getText().toString().trim();

        /*pending = Float.parseFloat(subjects.getRentPending());
        advance = Float.parseFloat(subjects.getRentAdvance());*/

        if(strBookingStatus.equals("3")){
            holder.btnPendingAmount.setEnabled(true);
            holder.btnPendingProduct.setEnabled(false);
            holder.btnReceived.setEnabled(true);
            holder.btnPendingProduct.setBackgroundResource(R.drawable.solid_gray_round_button);
        }



        holder.edtCustAddAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                pending = Float.parseFloat(strPending);
                strAddAmount = holder.edtCustAddAmount.getText().toString();

                if(strAddAmount.equals("")){
                    strAddAmount = "0";
                    holder.txtCustAdvance.setText(subjects.getRentAdvance());
                    holder.txtCustPendings.setText(subjects.getRentPending());
                }
                float addAmt = Float.parseFloat(strAddAmount);
                
                /*if(addAmt == pending){
                    //Toast.makeText(context, "true", Toast.LENGTH_SHORT).show();


                    addAmmount = Float.parseFloat(strAddAmount);
                    advance = Float.parseFloat(subjects.getRentAdvance());
                    pending = Float.parseFloat(subjects.getRentPending());

                    pending = pending-addAmmount;
                    advance = advance+addAmmount;
                    holder.txtCustPendings.setText(""+pending);
                    holder.txtCustAdvance.setText(""+advance);

                }
                else*/ if(addAmt > pending){
                    //Toast.makeText(context, "Entered Amount is Greater than Pendings", Toast.LENGTH_SHORT).show();
                    holder.edtCustAddAmount.setError("Entered Amount is Greater than Pendings");
                }
                else{

                    addAmmount = Float.parseFloat(strAddAmount);
                    advance = Float.parseFloat(subjects.getRentAdvance());
                    pending = Float.parseFloat(subjects.getRentPending());

                    pending = pending-addAmmount;
                    advance = advance+addAmmount;
                    holder.txtCustPendings.setText(""+pending);
                    holder.txtCustAdvance.setText(""+advance);
                }
            }
        });



        holder.btnPendingAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PendingAmountDataItem dataItem = tempdataList.get(position);
                strBookingId = dataItem.getBookingId();
                strBookingStatus = dataItem.getBookingStatus();

                strPending = holder.txtCustPendings.getText().toString();
                strAdvance = holder.txtCustAdvance.getText().toString();

                strProductId = dataItem.getBookingProdId();
                strCustId= dataItem.getBookingCustId();
                strTotalRent = dataItem.getTotalRent();

                if(strPending.equals("")){
                    strPending = "0.0";
                }
                else if(strPending.equals("0")){
                    strPending = "0.0";
                }
                else if(strPending.equals("0.0")){
                    strPending = "0.0";
                }

                if(strBookingStatus.equals("3") && !strPending.equals("0.0")){
                    strBookingStatus = "3";
                }
                else if(strBookingStatus.equals("3") && strPending.equals("0.0")){
                    strBookingStatus = "0";
                }
                else{
                    strBookingStatus = "2";
                }
                updatePendingAmount();

            }
        });

        holder.btnPendingProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PendingAmountDataItem dataItem = tempdataList.get(position);
                strBookingId = dataItem.getBookingId();
                strBookingStatus = "3";
                strPending = holder.txtCustPendings.getText().toString();
                strAdvance = holder.txtCustAdvance.getText().toString();
                strProductId = dataItem.getBookingProdId();
                strCustId= dataItem.getBookingCustId();
                strTotalRent = dataItem.getTotalRent();
                /*strTransactionActivity = "Product Return";
                strTransactionAmount = "0.0";*/
                updatePendingProduct();

            }
        });

        holder.btnReceived.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PendingAmountDataItem dataItem = tempdataList.get(position);
                strBookingId = dataItem.getBookingId();


                strPending = holder.txtCustPendings.getText().toString();
                strAdvance = holder.txtCustAdvance.getText().toString();

                strPending = holder.txtCustPendings.getText().toString().trim();

                strProductId = dataItem.getBookingProdId();
                strCustId= dataItem.getBookingCustId();
                strTotalRent = dataItem.getTotalRent();

                if(strPending.equals("0.0")){
                    AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(context);
                    alertDialog2.setTitle("Product...");
                    alertDialog2.setMessage("Product is taken from User ?");

                    alertDialog2.setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    strBookingStatus = "0";
                                    totalReceived();
                                    //Toast.makeText(context, "Received", Toast.LENGTH_SHORT).show();
                                }
                            });
                    alertDialog2.setNegativeButton("No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    strBookingStatus = "2";
                                    updatePendingAmount();
                                    //Toast.makeText(context, "Product not Received", Toast.LENGTH_SHORT).show();
                                }
                            });

                    alertDialog2.show();
                }
                else if(strPending.equals("0")){
                    Toast.makeText(context, "All Pendings Are Not Cleared", Toast.LENGTH_SHORT).show();
                }
                else if(strPending.equals("")){
                    Toast.makeText(context, "All Pendings Are Not Cleared", Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(context, "All Pendings Are Not Cleared", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    private void totalReceived() {

        addDialog = new ProgressDialog(context);
        addDialog.setCancelable(true);
        addDialog.setMessage("Updating...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();


        try{
            UpdatedCustPendingsDataRequest custPendingsDataRequest = new UpdatedCustPendingsDataRequest();

            custPendingsDataRequest.setBookingId(strBookingId);
            custPendingsDataRequest.setRentAdvance(strAdvance);
            custPendingsDataRequest.setRentPending(strPending);
            custPendingsDataRequest.setBookingStatus(strBookingStatus);

            custPendingsDataRequest.setBookingProdId(strProductId);
            custPendingsDataRequest.setBookingCustId(strCustId);
            custPendingsDataRequest.setTotalRent(strTotalRent);

            Call<UpdatedCustPendingsDataResponse> responceCall = ApiClient.getApiClient().receivedFromViewAccount(custPendingsDataRequest);

            responceCall.enqueue(new Callback<UpdatedCustPendingsDataResponse>() {
                @Override
                public void onResponse(Call<UpdatedCustPendingsDataResponse> call, retrofit2.Response<UpdatedCustPendingsDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        List<UpdatedCustPendingsDataItem> items =response.body().getUpdatedCustPendingsData();

                        Activity activity = (Activity) context;
                        Intent intent = new Intent(activity, ViewCustomerPendingActivity.class);
                        activity.startActivity(intent);
                        activity.overridePendingTransition(0,0);
                        activity.finish();

                        addDialog.dismiss();


                    } else {

                        Toast.makeText(context, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                        addDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<UpdatedCustPendingsDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialog.dismiss();

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    private void updatePendingProduct() {

        addDialog = new ProgressDialog(context);
        addDialog.setCancelable(true);
        addDialog.setMessage("Updating...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();


        try{
            UpdatedCustPendingsDataRequest custPendingsDataRequest = new UpdatedCustPendingsDataRequest();

            custPendingsDataRequest.setBookingId(strBookingId);
            custPendingsDataRequest.setRentAdvance(strAdvance);
            custPendingsDataRequest.setRentPending(strPending);
            custPendingsDataRequest.setBookingStatus(strBookingStatus);
            custPendingsDataRequest.setBookingProdId(strProductId);
            custPendingsDataRequest.setBookingCustId(strCustId);
            custPendingsDataRequest.setTotalRent(strTotalRent);


            Call<UpdatedCustPendingsDataResponse> responceCall = ApiClient.getApiClient().updateProductFromViewAccount(custPendingsDataRequest);

            responceCall.enqueue(new Callback<UpdatedCustPendingsDataResponse>() {
                @Override
                public void onResponse(Call<UpdatedCustPendingsDataResponse> call, retrofit2.Response<UpdatedCustPendingsDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        List<UpdatedCustPendingsDataItem> items =response.body().getUpdatedCustPendingsData();

                        Activity activity = (Activity) context;
                        Intent intent = new Intent(activity, ViewCustomerPendingActivity.class);
                        activity.startActivity(intent);
                        activity.overridePendingTransition(0,0);
                        activity.finish();

                        addDialog.dismiss();


                    } else {

                        Toast.makeText(context, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                        addDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<UpdatedCustPendingsDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialog.dismiss();

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }


        //Toast.makeText(context, "Only Product Return", Toast.LENGTH_SHORT).show();
    }

    private void updatePendingAmount() {

        addDialog = new ProgressDialog(context);
        addDialog.setCancelable(true);
        addDialog.setMessage("Updating...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();


        try{
            UpdatedCustPendingsDataRequest custPendingsDataRequest = new UpdatedCustPendingsDataRequest();

            custPendingsDataRequest.setBookingId(strBookingId);
            custPendingsDataRequest.setRentAdvance(strAdvance);
            custPendingsDataRequest.setRentPending(strPending);
            custPendingsDataRequest.setBookingStatus(strBookingStatus);

            custPendingsDataRequest.setBookingProdId(strProductId);
            custPendingsDataRequest.setBookingCustId(strCustId);
            custPendingsDataRequest.setTotalRent(strTotalRent);

            Call<UpdatedCustPendingsDataResponse> responceCall = ApiClient.getApiClient().updatePendingsFromViewAccount(custPendingsDataRequest);

            responceCall.enqueue(new Callback<UpdatedCustPendingsDataResponse>() {
                @Override
                public void onResponse(Call<UpdatedCustPendingsDataResponse> call, retrofit2.Response<UpdatedCustPendingsDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        //List<UpdatedCustPendingsDataItem> items =response.body().getUpdatedCustPendingsData();

                        Activity activity = (Activity) context;
                        Intent intent = new Intent(activity, ViewCustomerPendingActivity.class);
                        activity.startActivity(intent);
                        activity.overridePendingTransition(0,0);
                        activity.finish();

                        addDialog.dismiss();


                    } else {

                        Toast.makeText(context, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                        addDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<UpdatedCustPendingsDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialog.dismiss();

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }
        //Toast.makeText(context, "Only Pendings Updated", Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getItemCount() {
        return tempdataList.size();
    }


class EmployeeViewHolder extends RecyclerView.ViewHolder {

    private TextView txtCustProductName, txtCustBookingDate, txtCustReturnDate, txtCustTotalRent, txtCustAdvance, txtCustPendings;
    private Button btnPendingAmount, btnPendingProduct, btnReceived;
    private EditText edtCustAddAmount;



    EmployeeViewHolder(View itemView) {
        super(itemView);

        txtCustProductName = itemView.findViewById(R.id.txtCustProdName);
        txtCustBookingDate = itemView.findViewById(R.id.txtCustBookingDate);
        txtCustReturnDate = itemView.findViewById(R.id.txtCustReturnDate);
        txtCustTotalRent = itemView.findViewById(R.id.txtCustTotalAmount);
        txtCustAdvance = itemView.findViewById(R.id.txtCustAdvanceAmount);
        txtCustPendings = itemView.findViewById(R.id.txtCustPendingAmount);
        edtCustAddAmount = itemView.findViewById(R.id.edtCustAddAmount);

        btnPendingAmount = itemView.findViewById(R.id.btnCustPendingAmount);
        btnPendingProduct = itemView.findViewById(R.id.btnCustPendingProduct);
        btnReceived = itemView.findViewById(R.id.btnCustReceived);


    }
}

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString().toLowerCase();
                if (charString.isEmpty()) {
                    tempdataList = dataList;
                } else {
                    ArrayList<PendingAmountDataItem> filteredList = new ArrayList<>();
                    for (PendingAmountDataItem row : dataList) {

                        if (row.toString().toLowerCase().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }
                    tempdataList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = tempdataList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                tempdataList = (ArrayList<PendingAmountDataItem>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
