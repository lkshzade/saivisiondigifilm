package com.microtron.cvinventorymanagmentapp.Adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.microtron.cvinventorymanagmentapp.Helper.ConfigFile;
import com.microtron.cvinventorymanagmentapp.Helper.StaticClass;
import com.microtron.cvinventorymanagmentapp.NetworkModel.OutSideInventoryBooking.OutSideBookingDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.TransactionHistory.TransactionHistoryDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.TransactionHistory.TransactionHistoryDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataItem;
import com.microtron.cvinventorymanagmentapp.OutSideProductDetailsActivity;
import com.microtron.cvinventorymanagmentapp.R;
import com.microtron.cvinventorymanagmentapp.RentProductDetailsActivity;
import com.microtron.cvinventorymanagmentapp.TransactionHistoryDetailsActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static com.microtron.cvinventorymanagmentapp.Helper.StaticClass.outSideDetailsProduct;


public class TransactionHistoryListAdapter extends RecyclerView.Adapter<TransactionHistoryListAdapter.EmployeeViewHolder> implements Filterable {

private List<TransactionHistoryDataItem> dataList;
private List<TransactionHistoryDataItem> tempdataList;
private RecyclerItemClickListenerTransactionHistoryList recyclerItemClickListener;
        Context context;
        String name;
        float constantWeight;

public interface OnItemCheckListener {
    void onItemCheck(int item, float weight);

}


    public TransactionHistoryListAdapter(Context context, List<TransactionHistoryDataItem> dataList, RecyclerItemClickListenerTransactionHistoryList recyclerItemClickListener) {
        this.dataList = dataList;
        this.tempdataList = dataList;
        this.context = context;
        this.recyclerItemClickListener = recyclerItemClickListener;
    }


    @Override
    public EmployeeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.transaction_history_row, parent, false);
        return new EmployeeViewHolder(view);

    }


    @Override
    public void onBindViewHolder(final EmployeeViewHolder holder, @SuppressLint("RecyclerView") final int position) {


        final TransactionHistoryDataItem subjects = tempdataList.get(position);

        holder.txtTransCustName.setText(subjects.getCustomerName());
        holder.txtTransProdName.setText(subjects.getProductName());
        holder.txtTransBookingDate.setText(subjects.getBookingToDate());
        holder.txtTransReturnDate.setText(subjects.getBookingFromDate());
        holder.txtTransStatus.setText(subjects.getTransactionActivity());
        holder.txtTransDateTime.setText(subjects.getTransactionDateTime());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                recyclerItemClickListener.onItemClick(tempdataList.get(position));
                TransactionHistoryDataItem transactionDetails = new TransactionHistoryDataItem();

                transactionDetails.setCustomerId(subjects.getCustomerId());
                transactionDetails.setCustomerName(subjects.getCustomerName());
                transactionDetails.setCustomerEmail(subjects.getCustomerEmail());
                transactionDetails.setCustomerMobile(subjects.getCustomerMobile());

                transactionDetails.setProductId(subjects.getProductId());
                transactionDetails.setProductCompnyId(subjects.getProductCompnyId());
                transactionDetails.setProductCategoryId(subjects.getProductCategoryId());
                transactionDetails.setProductSubcategoryId(subjects.getProductSubcategoryId());
                transactionDetails.setProductName(subjects.getProductName());
                transactionDetails.setProductDetails(subjects.getProductDetails());
                transactionDetails.setProductInStock(subjects.getProductInStock());
                transactionDetails.setProductStatus(subjects.getProductStatus());
                transactionDetails.setProductQuantity(subjects.getProductQuantity());
                transactionDetails.setProductImgUrl1(subjects.getProductImgUrl1());
                transactionDetails.setProductImgUrl2(subjects.getProductImgUrl2());
                transactionDetails.setProductImgUrl3(subjects.getProductImgUrl3());
                transactionDetails.setProductImgUrl4(subjects.getProductImgUrl4());

                transactionDetails.setBookingId(subjects.getBookingId());
                transactionDetails.setBookingProdId(subjects.getBookingProdId());
                transactionDetails.setBookingCustId(subjects.getBookingCustId());
                transactionDetails.setBookingFromDate(subjects.getBookingFromDate());
                transactionDetails.setBookingToDate(subjects.getBookingToDate());
                transactionDetails.setBookingType(subjects.getBookingType());
                transactionDetails.setMonthlyRate(subjects.getMonthlyRate());
                transactionDetails.setDailyRate(subjects.getDailyRate());
                transactionDetails.setSaleRate(subjects.getSaleRate());
                transactionDetails.setSaleDate(subjects.getSaleDate());
                transactionDetails.setDaysCount(subjects.getDaysCount());
                transactionDetails.setTotalRent(subjects.getTotalRent());
                transactionDetails.setRentAdvance(subjects.getRentAdvance());
                transactionDetails.setRentPending(subjects.getRentPending());
                transactionDetails.setBookingStatus(subjects.getBookingStatus());
                transactionDetails.setBookingMonth(subjects.getBookingMonth());
                transactionDetails.setBookingYear(subjects.getBookingYear());

                transactionDetails.setTransactionId(subjects.getTransactionId());
                transactionDetails.setTransactionBookingId(subjects.getTransactionBookingId());
                transactionDetails.setTransactionProductId(subjects.getTransactionProductId());
                transactionDetails.setTransactionCustId(subjects.getTransactionCustId());
                transactionDetails.setTransactionProductTotal(subjects.getTransactionProductTotal());
                transactionDetails.setTransactionProductPaid(subjects.getTransactionProductPaid());
                transactionDetails.setTransactionProductRemain(subjects.getTransactionProductRemain());
                transactionDetails.setTransactionActivity(subjects.getTransactionActivity());
                transactionDetails.setTransactionActivityAmount(subjects.getTransactionActivityAmount());
                transactionDetails.setTransactionDateTime(subjects.getTransactionDateTime());

                StaticClass.transactionHistoryDetails = transactionDetails;

                Activity activity = (Activity) context;
                Intent intent = new Intent(activity, TransactionHistoryDetailsActivity.class);
                activity.startActivity(intent);
                activity.overridePendingTransition(0,0);


            }
        });


    }



    @Override
    public int getItemCount() {
        return tempdataList.size();
    }


class EmployeeViewHolder extends RecyclerView.ViewHolder {

    TextView txtTransCustName, txtTransProdName, txtTransBookingDate, txtTransReturnDate, txtTransStatus, txtTransDateTime;


    EmployeeViewHolder(View itemView) {
        super(itemView);


        txtTransCustName = itemView.findViewById(R.id.txtTransCustName);
        txtTransProdName = itemView.findViewById(R.id.txtTransProdName);
        txtTransBookingDate = itemView.findViewById(R.id.txtTransBookingDate);
        txtTransReturnDate = itemView.findViewById(R.id.txtTransReturnDate);
        txtTransStatus = itemView.findViewById(R.id.txtTransStatus);
        txtTransDateTime = itemView.findViewById(R.id.txtTransDateTime);
    }
}

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString().toLowerCase();
                if (charString.isEmpty()) {
                    tempdataList = dataList;
                } else {
                    ArrayList<TransactionHistoryDataItem> filteredList = new ArrayList<>();
                    for (TransactionHistoryDataItem row : dataList) {

                        if (row.toString().toLowerCase().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }
                    tempdataList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = tempdataList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                tempdataList = (ArrayList<TransactionHistoryDataItem>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
