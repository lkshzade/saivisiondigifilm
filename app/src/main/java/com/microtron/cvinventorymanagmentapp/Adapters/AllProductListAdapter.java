package com.microtron.cvinventorymanagmentapp.Adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.microtron.cvinventorymanagmentapp.Helper.ConfigFile;
import com.microtron.cvinventorymanagmentapp.Helper.StaticClass;
import com.microtron.cvinventorymanagmentapp.NetworkModel.GetAllProducts.AllProductsDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.GetAllProducts.AllProductsDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataItem;
import com.microtron.cvinventorymanagmentapp.ProductDetailsActivity;
import com.microtron.cvinventorymanagmentapp.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class AllProductListAdapter extends RecyclerView.Adapter<AllProductListAdapter.EmployeeViewHolder> implements Filterable {

private List<AllProductsDataItem> dataList;
private List<AllProductsDataItem> tempdataList;
private RecyclerItemClickListenerAllProductList recyclerItemClickListener;
Context context;

public interface OnItemCheckListener {
    void onItemCheck(int item, float weight);

}


    public AllProductListAdapter(Context context, List<AllProductsDataItem> dataList, RecyclerItemClickListenerAllProductList recyclerItemClickListener) {
        this.dataList = dataList;
        this.tempdataList = dataList;
        this.context = context;
        this.recyclerItemClickListener = recyclerItemClickListener;
    }


    @Override
    public EmployeeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.all_product_row, parent, false);
        return new EmployeeViewHolder(view);

    }


    @Override
    public void onBindViewHolder(final EmployeeViewHolder holder, @SuppressLint("RecyclerView") final int position) {


        final AllProductsDataItem subjects = tempdataList.get(position);
        holder.txtAllProductName.setText(subjects.getProductName());
        holder.txtAllProductDetails.setText(subjects.getProductDetails());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                recyclerItemClickListener.onItemClick(tempdataList.get(position));

                AllProductsDataItem allProduct = tempdataList.get(position);

                AllProductsDataItem detailsProduct = new AllProductsDataItem();

                detailsProduct.setProductId(allProduct.getProductId());
                detailsProduct.setCompanyId(allProduct.getCompanyId());
                detailsProduct.setCompanyName(allProduct.getCompanyName());
                detailsProduct.setProductCategoryId(allProduct.getProductCategoryId());
                detailsProduct.setCatName(allProduct.getCatName());
                detailsProduct.setProductSubcategoryId(allProduct.getProductSubcategoryId());
                detailsProduct.setSubCatName(allProduct.getSubCatName());
                detailsProduct.setProductName(allProduct.getProductName());
                detailsProduct.setProductDetails(allProduct.getProductDetails());
                detailsProduct.setProductInStock(allProduct.getProductInStock());
                detailsProduct.setProductQuantity(allProduct.getProductQuantity());

                StaticClass.product = detailsProduct;

                Activity activity = (Activity) context;
                Intent intent = new Intent(activity, ProductDetailsActivity.class);
                activity.startActivity(intent);
                activity.overridePendingTransition(0,0);


            }
        });

        Picasso.get()
                .load(ConfigFile.domain+ConfigFile.domaindir+"uploads/productImages/"+subjects.getProductImgUrl1()+".jpg")
                .error(R.drawable.no_image)
                .resize(400,400)
                .onlyScaleDown()
                .into(holder.imgAllProduct);

    }

    @Override
    public int getItemCount() {
        return tempdataList.size();
    }


class EmployeeViewHolder extends RecyclerView.ViewHolder {

    TextView txtAllProductName, txtAllProductDetails;
    ImageView imgAllProduct;
    LinearLayout viewDetails;


    EmployeeViewHolder(View itemView) {
        super(itemView);


        imgAllProduct = itemView.findViewById(R.id.imgAllProduct);
        txtAllProductName = itemView.findViewById(R.id.txtAllProductName);
        txtAllProductDetails = itemView.findViewById(R.id.txtAllProductDetails);
        viewDetails = itemView.findViewById(R.id.viewDetails);
    }
}

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString().toLowerCase();
                if (charString.isEmpty()) {
                    tempdataList = dataList;
                } else {
                    ArrayList<AllProductsDataItem> filteredList = new ArrayList<>();
                    for (AllProductsDataItem row : dataList) {

                        if (row.toString().toLowerCase().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }
                    tempdataList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = tempdataList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                tempdataList = (ArrayList<AllProductsDataItem>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
