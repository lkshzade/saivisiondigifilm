package com.microtron.cvinventorymanagmentapp.Adapters;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.microtron.cvinventorymanagmentapp.NetworkModel.CalendarDialogModelClass;
import com.microtron.cvinventorymanagmentapp.R;

import java.util.ArrayList;

public class CalendarDialogAdapter extends BaseAdapter {
    Activity activity;

    private Activity context;
    private ArrayList<CalendarDialogModelClass> alCustom;
    private String sturl;


    public CalendarDialogAdapter(Activity context, ArrayList<CalendarDialogModelClass> alCustom) {
        this.context = context;
        this.alCustom = alCustom;

    }

    @Override
    public int getCount() {
        return alCustom.size();

    }

    @Override
    public Object getItem(int i) {
        return alCustom.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @TargetApi(Build.VERSION_CODES.O)
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.row_addapt, null, true);

        TextView tvProdName=(TextView)listViewItem.findViewById(R.id.tv_prodName);
        TextView tvDueDate=(TextView)listViewItem.findViewById(R.id.tv_dueDate);
        TextView tvCustName=(TextView)listViewItem.findViewById(R.id.tv_custName);
        TextView tvRent=(TextView)listViewItem.findViewById(R.id.tv_rent);


        tvProdName.setText(alCustom.get(position).getProductName());
        tvDueDate.setText("Return Date : "+alCustom.get(position).getTitles());
        tvCustName.setText("Customer Name : "+alCustom.get(position).getSubjects());
        tvRent.setText("Total Rent : "+alCustom.get(position).getDescripts());

        return  listViewItem;
    }

}