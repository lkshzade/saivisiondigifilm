package com.microtron.cvinventorymanagmentapp.Adapters;


import com.microtron.cvinventorymanagmentapp.NetworkModel.GetAllProducts.AllProductsDataItem;

public interface RecyclerItemClickListenerAllProductList {
    void onItemClick(AllProductsDataItem notice);
}
