package com.microtron.cvinventorymanagmentapp.Adapters;


import com.microtron.cvinventorymanagmentapp.NetworkModel.SaleItemData.SaleItemDataItem;

public interface RecyclerItemClickListenerSaleProductList {
    void onItemClick(SaleItemDataItem notice);
}
