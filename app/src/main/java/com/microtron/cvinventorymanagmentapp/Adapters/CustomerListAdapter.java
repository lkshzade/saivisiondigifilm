package com.microtron.cvinventorymanagmentapp.Adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.microtron.cvinventorymanagmentapp.Helper.StaticClass;
import com.microtron.cvinventorymanagmentapp.NetworkModel.Customer.CustomerDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.Customer.CustomerDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.OutSideInventoryBooking.OutSideBookingDataItem;
import com.microtron.cvinventorymanagmentapp.OutSideProductDetailsActivity;
import com.microtron.cvinventorymanagmentapp.R;

import java.util.ArrayList;
import java.util.List;


public class CustomerListAdapter extends RecyclerView.Adapter<CustomerListAdapter.EmployeeViewHolder> implements Filterable {

private List<CustomerDataItem> dataList;
private List<CustomerDataItem> tempdataList;
private RecyclerItemClickListenerCustomerList recyclerItemClickListener;
        Context context;
        String name;
        float constantWeight;

public interface OnItemCheckListener {
    void onItemCheck(int item, float weight);

}


    public CustomerListAdapter(Context context, List<CustomerDataItem> dataList, RecyclerItemClickListenerCustomerList recyclerItemClickListener) {
        this.dataList = dataList;
        this.tempdataList = dataList;
        this.context = context;
        this.recyclerItemClickListener = recyclerItemClickListener;
    }


    @Override
    public EmployeeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.customer_row, parent, false);
        return new EmployeeViewHolder(view);

    }


    @Override
    public void onBindViewHolder(final EmployeeViewHolder holder, @SuppressLint("RecyclerView") final int position) {


        final CustomerDataItem subjects = tempdataList.get(position);
        holder.txtCustName.setText(subjects.getCustomerName());
        holder.txtCustMobile.setText(subjects.getCustomerMobile());
        /*holder.txtProdName.setText(subjects.getProductName());
        holder.txtProdPrice.setText(subjects.getProductPrice()+" Rs.");
        holder.txtStartDate.setText(subjects.getOutSideFromDate());
        holder.txtEndDate.setText(subjects.getOutSideToDate());
        holder.txtTotalRent.setText(subjects.getOutSideTotalRent()+" Rs.");*/


        /*holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                recyclerItemClickListener.onItemClick(tempdataList.get(position));


                OutSideBookingDataItem outSideDetailsProduct = new OutSideBookingDataItem();

                outSideDetailsProduct.setOutSideInventoryId(subjects.getOutSideInventoryId());
                outSideDetailsProduct.setCompanyName(subjects.getCompanyName());
                outSideDetailsProduct.setProductName(subjects.getProductName());
                outSideDetailsProduct.setProductPrice(subjects.getProductPrice());
                outSideDetailsProduct.setPurchaseDate(subjects.getPurchaseDate());
                outSideDetailsProduct.setShopName(subjects.getShopName());
                outSideDetailsProduct.setSalerName(subjects.getSalerName());
                outSideDetailsProduct.setSalerMobile(subjects.getSalerMobile());
                outSideDetailsProduct.setOutSideBookingType(subjects.getOutSideBookingType());
                outSideDetailsProduct.setOutSideMonthRent(subjects.getOutSideMonthRent());
                outSideDetailsProduct.setOutSideDayRent(subjects.getOutSideDayRent());
                outSideDetailsProduct.setOutSideFromDate(subjects.getOutSideFromDate());
                outSideDetailsProduct.setOutSideToDate(subjects.getOutSideToDate());
                outSideDetailsProduct.setOutSideTotalRent(subjects.getOutSideTotalRent());
                outSideDetailsProduct.setOutSideAdvanceAmount(subjects.getOutSideAdvanceAmount());
                outSideDetailsProduct.setOutSidePendingAmount(subjects.getOutSidePendingAmount());
                outSideDetailsProduct.setOutSideCustName(subjects.getOutSideCustName());
                outSideDetailsProduct.setOutSideCustMobile(subjects.getOutSideCustMobile());
                outSideDetailsProduct.setOutSideCustEmail(subjects.getOutSideCustEmail());



                StaticClass.outSideDetailsProduct = outSideDetailsProduct;

                Activity activity = (Activity) context;
                Intent intent = new Intent(activity, OutSideProductDetailsActivity.class);
                activity.startActivity(intent);
                activity.overridePendingTransition(0,0);


            }
        });*/

        /*Picasso.get()
                .load(ConfigFile.domain+ConfigFile.domaindir+"uploads/productImages/"+subjects.getProductImgUrl1()+".jpg")
                .error(R.drawable.no_image)
                .resize(400,400)
                .onlyScaleDown()
                .into(holder.imgProduct);*/

    }

    @Override
    public int getItemCount() {
        return tempdataList.size();
    }


class EmployeeViewHolder extends RecyclerView.ViewHolder {

    TextView txtCustName, txtCustMobile, txtProdName, txtProdPrice, txtStartDate, txtEndDate, txtTotalRent;


    EmployeeViewHolder(View itemView) {
        super(itemView);

        txtCustName = itemView.findViewById(R.id.txtCustName);
        txtCustMobile = itemView.findViewById(R.id.txtCustMobile);
        txtProdName = itemView.findViewById(R.id.txtProdName);
        txtProdPrice = itemView.findViewById(R.id.txtProdPrice);
        txtTotalRent = itemView.findViewById(R.id.txtTotalRent);
        txtStartDate = itemView.findViewById(R.id.txtStartDate);
        txtEndDate = itemView.findViewById(R.id.txtEndDate);

    }
}

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString().toLowerCase();
                if (charString.isEmpty()) {
                    tempdataList = dataList;
                } else {
                    ArrayList<CustomerDataItem> filteredList = new ArrayList<>();
                    for (CustomerDataItem row : dataList) {

                        if (row.toString().toLowerCase().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }
                    tempdataList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = tempdataList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                tempdataList = (ArrayList<CustomerDataItem>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
