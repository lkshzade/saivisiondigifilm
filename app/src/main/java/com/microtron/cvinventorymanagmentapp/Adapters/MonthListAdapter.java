package com.microtron.cvinventorymanagmentapp.Adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.microtron.cvinventorymanagmentapp.CalendarActivity;
import com.microtron.cvinventorymanagmentapp.Helper.ConfigFile;
import com.microtron.cvinventorymanagmentapp.Helper.StaticClass;
import com.microtron.cvinventorymanagmentapp.NetworkModel.GetAllProducts.AllProductsDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataItem;
import com.microtron.cvinventorymanagmentapp.R;
import com.microtron.cvinventorymanagmentapp.RentProductDetailsActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class MonthListAdapter extends BaseAdapter{

    private Activity context;
    private List<BookedItemDataItem> listMonths;
    private ImageView imgRentMonth;
    private TextView txtRentMonthProductName, txtRentMonthCustName, txtRentMonthFromDate, txtRentMonthToDate;




    public MonthListAdapter(Activity context, List<BookedItemDataItem> listMonths) {

        this.context = context;
        this.listMonths = listMonths;
    }

    @Override
    public int getCount() {
        return listMonths.size();
    }

    @Override
    public Object getItem(int position) {
        return listMonths.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {

            if (view==null){
                LayoutInflater layoutInflater;
                view=LayoutInflater.from(context).inflate(R.layout.month_item,viewGroup,false);
            }

            imgRentMonth = view.findViewById(R.id.imgRentMonth);
            txtRentMonthProductName = view.findViewById(R.id.txtRentMonthProductName);
            txtRentMonthCustName = view.findViewById(R.id.txtRentMonthCustName);
            txtRentMonthFromDate = view.findViewById(R.id.txtRentMonthFromDate);
            txtRentMonthToDate = view.findViewById(R.id.txtRentMonthToDate);

            final BookedItemDataItem subjects = listMonths.get(position);

            txtRentMonthProductName.setText(subjects.getProductName());
            txtRentMonthCustName.setText(subjects.getCustomerName());
            txtRentMonthFromDate.setText(subjects.getBookingFromDate());
            txtRentMonthToDate.setText(subjects.getBookingToDate());

        Picasso.get()
                .load(ConfigFile.domain+ConfigFile.domaindir+"uploads/productImages/"+subjects.getProductImgUrl1()+".jpg")
                .error(R.drawable.no_image)
                .resize(400,400)
                .onlyScaleDown()
                .into(imgRentMonth);


            return view;
        }
}
