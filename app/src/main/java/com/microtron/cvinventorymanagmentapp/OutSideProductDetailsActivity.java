package com.microtron.cvinventorymanagmentapp;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.microtron.cvinventorymanagmentapp.Helper.ApiClient;
import com.microtron.cvinventorymanagmentapp.Helper.StaticClass;
import com.microtron.cvinventorymanagmentapp.NetworkModel.OutSideInventoryBooking.OutSideBookingDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.OutSideInventoryBooking.OutSideBookingDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.OutSideInventoryBooking.OutSideBookingDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataItem;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class OutSideProductDetailsActivity extends AppCompatActivity {

    private TextView txtOutSideDetailsCustName, txtOutSideDetailsCustMobile, txtOutSideDetailsCustEmail, txtOutSideDetailsShopName, txtOutSideDetailsSalerName, txtOutSideDetailsSalerMobile, txtOutSideDetailsDuration, txtOutSideDetailsRent, txtOutSideDetailsBookingDate, txtOutSideDetailsReturningDate, txtOutSideDetailsPending, txtOutSideProductName;
    private EditText edtOutSideDetailsAdvance;
    private Button btnOutSideDetailsUpdate, btnOutSideDetailsReceived;
    private OutSideBookingDataItem outSideProductDetails;
    private ImageView imgBack, imgMore;
    private Date date1, date2;
    private float dayCount = 0.0f, totalRent, rentAdvance, rentPending;
    private String strAdvanceAmount, strTotalRent, strPendingAmount, type;
    private int mToYear, mToMonth, mToDay;
    private String strToDay, strToYear, strToMonth, strToDate, strOutSideInventoryId, strOutSideInventoryStatus;
    private String custName, custEmail, custMObile, productName;
    private ProgressDialog addDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_out_side_product_details);

        txtOutSideDetailsCustName = findViewById(R.id.txtOutSideDetailsCustName);
        txtOutSideDetailsCustMobile = findViewById(R.id.txtOutSideDetailsCustMobile);
        txtOutSideDetailsCustEmail = findViewById(R.id.txtOutSideDetailsCustEmail);

        txtOutSideDetailsShopName = findViewById(R.id.txtOutSideDetailsShopName);
        txtOutSideDetailsSalerName = findViewById(R.id.txtOutSideDetailsSalerName);
        txtOutSideDetailsSalerMobile = findViewById(R.id.txtoutSideDetailsSalerMobile);

        txtOutSideProductName = findViewById(R.id.txtOutSideDetailsProductName);

        txtOutSideDetailsDuration = findViewById(R.id.txtOutSideDetailsDays);
        txtOutSideDetailsRent = findViewById(R.id.txtOutSideDetailsRent);
        txtOutSideDetailsBookingDate = findViewById(R.id.txtOutSideDetailsBookingDate);
        txtOutSideDetailsReturningDate = findViewById(R.id.txtOutSideDetailsReturnDate);
        edtOutSideDetailsAdvance = findViewById(R.id.edtOutSideDetailsAdvance);
        txtOutSideDetailsPending = findViewById(R.id.txtOutSideDetailsPending);

        btnOutSideDetailsReceived = findViewById(R.id.btnOutSideDetailsReceived);
        btnOutSideDetailsUpdate = findViewById(R.id.btnOutSideDetailsUpdate);

        imgBack = findViewById(R.id.imgBack);
        imgMore = findViewById(R.id.imgMore);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        imgMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(OutSideProductDetailsActivity.this, OutSideInventorySalerInfoActivity.class);
                startActivity(intent);
                overridePendingTransition(0,0);
                finish();

            }
        });


        outSideProductDetails = StaticClass.outSideDetailsProduct;

        txtOutSideProductName.setText(outSideProductDetails.getProductName());

        txtOutSideDetailsCustName.setText(outSideProductDetails.getOutSideCustName());
        txtOutSideDetailsCustMobile.setText(outSideProductDetails.getOutSideCustMobile());
        txtOutSideDetailsCustEmail.setText(outSideProductDetails.getOutSideCustEmail());

        txtOutSideDetailsShopName.setText(outSideProductDetails.getShopName());
        txtOutSideDetailsSalerName.setText(outSideProductDetails.getSalerName());
        txtOutSideDetailsSalerMobile.setText(outSideProductDetails.getSalerMobile());

        /*txtOutSideDetailsDuration.setText(outSideProductDetails.get());*/
        txtOutSideDetailsRent.setText(outSideProductDetails.getOutSideTotalRent());
        txtOutSideDetailsBookingDate.setText(outSideProductDetails.getOutSideFromDate());
        txtOutSideDetailsReturningDate.setText(outSideProductDetails.getOutSideToDate());
        edtOutSideDetailsAdvance.setText(outSideProductDetails.getOutSideAdvanceAmount());
        txtOutSideDetailsPending.setText(outSideProductDetails.getOutSidePendingAmount());

        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // here set the pattern as you date in string was containing like date/month/year
            date1 = sdf.parse(outSideProductDetails.getOutSideFromDate());
            date2 = sdf.parse(outSideProductDetails.getOutSideToDate());
            dayCount = (date2.getTime() - date1.getTime())/(24*60*60*1000);
            //Toast.makeText(BookingActivity.this, "To Date"+date2+"\n From Date"+date1, Toast.LENGTH_LONG).show();
            //Toast.makeText(BookingActivity.this, "Day Count"+dayCount, Toast.LENGTH_SHORT).show();
            if(dayCount==0.0f){
                dayCount = 1.0f;
            }
            }catch(ParseException ex){
            // handle parsing exception if date string was different from the pattern applying into the SimpleDateFormat contructor
            }

        type = outSideProductDetails.getOutSideBookingType();
        if(type.equals("Monthly")){
            txtOutSideDetailsDuration.setText("1 Month");
        }
        else{
            int daysCount = Math.round(dayCount);
            txtOutSideDetailsDuration.setText(daysCount+" Days");
        }

        edtOutSideDetailsAdvance.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                strAdvanceAmount = edtOutSideDetailsAdvance.getText().toString().trim();
                strTotalRent = txtOutSideDetailsRent.getText().toString().trim();
                totalRent = Float.parseFloat(strTotalRent);


                if(strAdvanceAmount.equals("")){
                    rentAdvance = 0.0f;
                    rentPending = totalRent - rentAdvance;
                    txtOutSideDetailsPending.setText(""+rentPending);
                    strPendingAmount = ""+rentPending;
                }
                else{
                    rentAdvance = Float.parseFloat(strAdvanceAmount);
                    rentPending = totalRent - rentAdvance;
                    txtOutSideDetailsPending.setText(""+rentPending);
                    strPendingAmount = ""+rentPending;
                }

            }
        });

        txtOutSideDetailsReturningDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Calendar c = Calendar.getInstance();
                mToYear = c.get(Calendar.YEAR);
                mToMonth = c.get(Calendar.MONTH);
                mToDay = c.get(Calendar.DAY_OF_MONTH);

                //strRentDaily = edtRentDaily.getText().toString().trim();

                DatePickerDialog datePickerDialog = new DatePickerDialog(OutSideProductDetailsActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                if (dayOfMonth <= 9) {
                                    strToDay = "0" + dayOfMonth;
                                } else {
                                    strToDay = "" + dayOfMonth;
                                }
                                monthOfYear = monthOfYear + 1;
                                strToYear = String.valueOf(year);

                                if (monthOfYear <= 9) {
                                    strToMonth = "0" + monthOfYear;
                                } else {
                                    strToMonth = "" + monthOfYear;
                                }

                                txtOutSideDetailsReturningDate.setText(strToYear + "-" + strToMonth + "-" + strToDay);

                                strToDate = txtOutSideDetailsReturningDate.getText().toString().trim();

                                try{
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // here set the pattern as you date in string was containing like date/month/year
                                    date1 = sdf.parse(outSideProductDetails.getOutSideFromDate());
                                    date2 = sdf.parse(strToDate);
                                    dayCount = (date2.getTime() - date1.getTime())/(24*60*60*1000);
                                    //Toast.makeText(BookingActivity.this, "To Date"+date2+"\n From Date"+date1, Toast.LENGTH_LONG).show();
                                    //Toast.makeText(BookingActivity.this, "Day Count"+dayCount, Toast.LENGTH_SHORT).show();
                                    if(dayCount==0.0f){
                                        dayCount = 1.0f;
                                    }
                                    if(type.equals("Monthly")){
                                        txtOutSideDetailsDuration.setText("1 Month");
                                        float Rent = Float.parseFloat(outSideProductDetails.getOutSideTotalRent())/31;
                                        totalRent = Rent * dayCount;
                                        txtOutSideDetailsRent.setText(""+totalRent);
                                        String advance = edtOutSideDetailsAdvance.getText().toString().trim();
                                        if(advance.equals("")){
                                            advance = "0";
                                        }
                                        float pending = (totalRent)-(Float.parseFloat(advance));
                                        txtOutSideDetailsPending.setText(""+pending);
                                    }
                                    else{
                                        int daysCount = Math.round(dayCount);
                                        txtOutSideDetailsDuration.setText(daysCount+" Days");
                                        totalRent = (Float.parseFloat(outSideProductDetails.getOutSideDayRent())) * (dayCount);
                                        txtOutSideDetailsRent.setText("" + totalRent);

                                        String advance = edtOutSideDetailsAdvance.getText().toString().trim();
                                        if(advance.equals("")){
                                            advance = "0";
                                        }
                                        float pending = (totalRent)-(Float.parseFloat(advance));
                                        txtOutSideDetailsPending.setText(""+pending);
                                    }

                                }catch(ParseException e){
                                    // handle parsing exception if date string was different from the pattern applying into the SimpleDateFormat contructor
                                }

                            }
                        }, mToYear, mToMonth, mToDay);

                datePickerDialog.show();

            }
        });

        btnOutSideDetailsReceived.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strPendingAmount = txtOutSideDetailsPending.getText().toString().trim();
                float pendingAmount = Float.parseFloat(strPendingAmount);
                if(pendingAmount == 0.0){
                    Toast.makeText(OutSideProductDetailsActivity.this, "Success", Toast.LENGTH_SHORT).show();
                    strOutSideInventoryStatus = "0";
                    updateOutSideProduct();
                }
                else{
                    txtOutSideDetailsPending.setError("Account not Clear yet please Use Update");
                    Toast.makeText(OutSideProductDetailsActivity.this, "Account not Clear yet please Use Update", Toast.LENGTH_SHORT).show();
                }

            }
        });

        btnOutSideDetailsUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strPendingAmount = txtOutSideDetailsPending.getText().toString().trim();
                float pendingAmount = Float.parseFloat(strPendingAmount);
                if(pendingAmount == 0.0){
                    //Toast.makeText(OutSideProductDetailsActivity.this, "Success", Toast.LENGTH_SHORT).show();
                    strOutSideInventoryStatus = "0";
                    //updateOutSideProduct();
                    Toast.makeText(OutSideProductDetailsActivity.this, "Please Take as a Received", Toast.LENGTH_SHORT).show();
                }
                else{
                    strOutSideInventoryStatus = "1";
                    updateOutSideProduct();
                }



            }
        });


    }

    private void updateOutSideProduct() {

        strOutSideInventoryId = outSideProductDetails.getOutSideInventoryId();
        strToDate = txtOutSideDetailsReturningDate.getText().toString().trim();
        strTotalRent = txtOutSideDetailsRent.getText().toString().trim();
        strAdvanceAmount = edtOutSideDetailsAdvance.getText().toString().trim();
        strPendingAmount = txtOutSideDetailsPending.getText().toString().trim();

        addDialog = new ProgressDialog(this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Updating...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        try{
            OutSideBookingDataRequest outSideBooking = new OutSideBookingDataRequest();

            outSideBooking.setOutSideInventoryId(strOutSideInventoryId);
            outSideBooking.setOutSideToDate(strToDate);
            outSideBooking.setOutSideTotalRent(strTotalRent);
            outSideBooking.setOutSideAdvanceAmount(strAdvanceAmount);
            outSideBooking.setOutSidePendingAmount(strPendingAmount);
            outSideBooking.setOutSideStatus(strOutSideInventoryStatus);


            Call<OutSideBookingDataResponse> responceCall = ApiClient.getApiClient().updateOutSideItem(outSideBooking);

            responceCall.enqueue(new Callback<OutSideBookingDataResponse>() {
                @Override
                public void onResponse(Call<OutSideBookingDataResponse> call, retrofit2.Response<OutSideBookingDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        List<OutSideBookingDataItem> items =response.body().getOutSideBookingData();
                        Toast.makeText(OutSideProductDetailsActivity.this, "Item Updated", Toast.LENGTH_SHORT).show();
                        onBackPressed();
                        addDialog.dismiss();


                    } else {

                        Toast.makeText(OutSideProductDetailsActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                        addDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<OutSideBookingDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialog.dismiss();

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, OutSideInventoryListActivity.class);
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
    }
}
