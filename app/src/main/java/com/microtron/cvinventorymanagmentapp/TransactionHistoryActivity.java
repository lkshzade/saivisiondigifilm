package com.microtron.cvinventorymanagmentapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;

import com.microtron.cvinventorymanagmentapp.Adapters.RecyclerItemClickListenerTransactionHistoryList;
import com.microtron.cvinventorymanagmentapp.Adapters.TransactionHistoryListAdapter;
import com.microtron.cvinventorymanagmentapp.Helper.ApiClient;
import com.microtron.cvinventorymanagmentapp.NetworkModel.TransactionHistory.TransactionHistoryDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.TransactionHistory.TransactionHistoryDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.TransactionHistory.TransactionHistoryDataResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class TransactionHistoryActivity extends AppCompatActivity {

    ImageView imgBack;
    RecyclerView recyclerTransactionHistoryList;
    private ProgressDialog addDialog;
    TransactionHistoryListAdapter transactionHistoryListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_history);

        imgBack = findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        recyclerTransactionHistoryList = findViewById(R.id.recyclerTransactionHistoryList);

        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerTransactionHistoryList.setLayoutAnimation(animation);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerTransactionHistoryList.setLayoutManager(layoutManager);

        getTransactionHistoryList();
    }

    private void getTransactionHistoryList() {

        addDialog = new ProgressDialog(this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Getting List...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        try{
            TransactionHistoryDataRequest getTransactionHistoryRequest = new TransactionHistoryDataRequest();


            Call<TransactionHistoryDataResponse> responceCall = ApiClient.getApiClient().getTransactionHistory(getTransactionHistoryRequest);

            responceCall.enqueue(new Callback<TransactionHistoryDataResponse>() {
                @Override
                public void onResponse(Call<TransactionHistoryDataResponse> call, retrofit2.Response<TransactionHistoryDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        List<TransactionHistoryDataItem> items =response.body().getTransactionHistoryData();

                        transactionHistoryListAdapter = new TransactionHistoryListAdapter(TransactionHistoryActivity.this, items, recyclerItemClickListener);
                        recyclerTransactionHistoryList.setAdapter(transactionHistoryListAdapter);
                        transactionHistoryListAdapter.notifyDataSetChanged();

                        addDialog.dismiss();


                    } else {
                        //Toast.makeText(RentListActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                        addDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<TransactionHistoryDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialog.dismiss();

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    private RecyclerItemClickListenerTransactionHistoryList recyclerItemClickListener = new RecyclerItemClickListenerTransactionHistoryList() {
        @Override
        public void onItemClick(final TransactionHistoryDataItem notiItem) {
        }
    };

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
    }
}
