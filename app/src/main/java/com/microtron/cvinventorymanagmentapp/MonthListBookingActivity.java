package com.microtron.cvinventorymanagmentapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;

import com.microtron.cvinventorymanagmentapp.Adapters.AllProductListAdapter;
import com.microtron.cvinventorymanagmentapp.Adapters.MonthProductBookingListAdapter;
import com.microtron.cvinventorymanagmentapp.Adapters.RecyclerItemClickListenerAllProductList;
import com.microtron.cvinventorymanagmentapp.Adapters.RecyclerItemClickListenerMonthProductBookingList;
import com.microtron.cvinventorymanagmentapp.Helper.StaticClass;
import com.microtron.cvinventorymanagmentapp.NetworkModel.GetAllProducts.AllProductsDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataItem;

import java.util.ArrayList;
import java.util.List;

public class MonthListBookingActivity extends AppCompatActivity {

    private RecyclerView recyclerListMonthProduct;
    private ImageView imgBack, imgNoProduct;
    private ProgressDialog addDialog;
    private List<BookedItemDataItem> items = new ArrayList<BookedItemDataItem>();
    private MonthProductBookingListAdapter monthProductBookingListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_month_list_booking);

        recyclerListMonthProduct = findViewById(R.id.recyclerListMonthProduct);
        imgBack = findViewById(R.id.imgBack);
        imgNoProduct = findViewById(R.id.imgNoProduct);
        imgNoProduct.setVisibility(View.GONE);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerListMonthProduct.setLayoutAnimation(animation);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerListMonthProduct.setLayoutManager(layoutManager);

        items = StaticClass.listMonthBooking;

        monthProductBookingListAdapter = new MonthProductBookingListAdapter(MonthListBookingActivity.this, items, recyclerItemClickListener);
        recyclerListMonthProduct.setAdapter(monthProductBookingListAdapter);
        monthProductBookingListAdapter.notifyDataSetChanged();

    }

    private RecyclerItemClickListenerMonthProductBookingList recyclerItemClickListener = new RecyclerItemClickListenerMonthProductBookingList() {
        @Override
        public void onItemClick(final BookedItemDataItem notiItem) {
        }
    };

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, CalendarActivity.class);
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
    }
}
