package com.microtron.cvinventorymanagmentapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.microtron.cvinventorymanagmentapp.Helper.StaticClass;
import com.microtron.cvinventorymanagmentapp.NetworkModel.TransactionHistory.TransactionHistoryDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataItem;

public class TransactionHistoryDetailsActivity extends AppCompatActivity {

    TransactionHistoryDataItem transactionDetails;
    ImageView imgBack;
    TextView txtTransDetailsTransType, txtTransDetailsTransDate, txtTransDetailsTransAmount;
    TextView txtTransDetailsCustName, txtTransDetailsCustEmail, txtTransDetailsCustMobile;
    TextView txtTransDetailsProdName, txtTransDetailsProdDetails, txtTransDetailsBookingDate, txtTransDetailsReturnDate, txtTransDetailsTotalRent, txtTransDetailsPaidAmount, txtTransDetailsPending;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_history_details);

        imgBack = findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        txtTransDetailsTransType = findViewById(R.id.txtTransDetailsTransType);
        txtTransDetailsTransDate = findViewById(R.id.txtTransDetailsTransDate);
        txtTransDetailsTransAmount = findViewById(R.id.txtTransDetailsTransAmount);

        txtTransDetailsCustName = findViewById(R.id.txtTransDetailsCustName);
        txtTransDetailsCustEmail = findViewById(R.id.txtTransDetailsCustEmail);
        txtTransDetailsCustMobile = findViewById(R.id.txtTransDetailsCustMobile);

        txtTransDetailsProdName = findViewById(R.id.txtTransDetailsProdName);
        txtTransDetailsProdDetails = findViewById(R.id.txtTransDetailsProdDetails);
        txtTransDetailsBookingDate = findViewById(R.id.txtTransDetailsBookingDate);
        txtTransDetailsReturnDate = findViewById(R.id.txtTransDetailsReturnDate);
        txtTransDetailsTotalRent = findViewById(R.id.txtTransDetailsTotalRent);
        txtTransDetailsPaidAmount = findViewById(R.id.txtTransDetailsPaid);
        txtTransDetailsPending = findViewById(R.id.txtTransDetailsPending);

        transactionDetails = StaticClass.transactionHistoryDetails;

        txtTransDetailsTransType.setText(transactionDetails.getTransactionActivity());
        txtTransDetailsTransDate.setText(transactionDetails.getTransactionDateTime());
        txtTransDetailsTransAmount.setText(transactionDetails.getTransactionActivityAmount()+" Rs.");

        txtTransDetailsCustName.setText(transactionDetails.getCustomerName());
        txtTransDetailsCustMobile.setText(transactionDetails.getCustomerMobile());
        txtTransDetailsCustEmail.setText(transactionDetails.getCustomerEmail());

        txtTransDetailsProdName.setText(transactionDetails.getProductName());
        txtTransDetailsProdDetails.setText(transactionDetails.getProductDetails());
        txtTransDetailsBookingDate.setText(transactionDetails.getBookingFromDate());
        txtTransDetailsReturnDate.setText(transactionDetails.getBookingToDate());
        txtTransDetailsTotalRent.setText(transactionDetails.getTotalRent());
        txtTransDetailsPaidAmount.setText(transactionDetails.getRentAdvance());
        txtTransDetailsPending.setText(transactionDetails.getRentPending());
    }
}
