package com.microtron.cvinventorymanagmentapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.CardView;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.microtron.cvinventorymanagmentapp.Helper.ApiClient;
import com.microtron.cvinventorymanagmentapp.Helper.AppConstants;
import com.microtron.cvinventorymanagmentapp.Helper.ConfigFile;
import com.microtron.cvinventorymanagmentapp.Helper.Sp;
import com.microtron.cvinventorymanagmentapp.HomeFragments.ScannerFragment;
import com.microtron.cvinventorymanagmentapp.LogInSignUpSplash.LoginActivity;
import com.microtron.cvinventorymanagmentapp.LogInSignUpSplash.RegisterActivity;
import com.microtron.cvinventorymanagmentapp.LogInSignUpSplash.SplashScreen;
import com.microtron.cvinventorymanagmentapp.NetworkModel.GetScanProduct.GetScanProductDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.GetScanProduct.GetScanProductDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.GetScanProduct.GetScanProductDataResponse;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private BottomNavigationView bottomNavigation;
    private String strSession;
    private Button btnBook, btnSale, btnManualBooking;
    private TextView txtCompanyName, txtCategoryName, txtSubCategoryName, txtProductName, txtProductDesc/*, txtProductprice, txtProducInStock, txtProductStockQty*/;
    private ImageView btnScanner;
    private CardView cardScanner;
    private LinearLayout llShowProductMain;
    private String strName, strMobile, strEmail, strPass, strAddress, strShopName, strShoptype, strImgURL, strAdminID;
    private ProgressDialog addDialog;
    private String qrScannedProductID;
    private String strCompanyName, strCategoryName, strSubCategoryName, strProductName, strProductDesc, strProductPrice, strProductInStock, strProductStockQty;
    private String strProductInStockSp;
    private Toolbar toolbar;


    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkPermissions();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("DashBoard");
        //toolbar.setTitleTextColor(R.color.black);
        //setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        navigationView.setBackgroundColor(getResources().getColor(R.color.white));

        int[][] state = new int[][] {
                new int[] {-android.R.attr.state_enabled}, // disabled
                new int[] {android.R.attr.state_enabled}, // enabled
                new int[] {-android.R.attr.state_checked}, // unchecked
                new int[] { android.R.attr.state_pressed}  // pressed

        };

        int[] color = new int[] {
                getResources().getColor(R.color.black),
                getResources().getColor(R.color.black),
                getResources().getColor(R.color.black),
                getResources().getColor(R.color.black)
        };


        int[] color2 = new int[] {
                getResources().getColor(R.color.button_color),
                getResources().getColor(R.color.button_color),
                getResources().getColor(R.color.button_color),
                Color.GREEN
        };


        ColorStateList ColorStateList1 = new ColorStateList(state, color);

        ColorStateList ColorStateList2 = new ColorStateList(state, color2);

        navigationView.setItemTextColor(ColorStateList1);
        navigationView.setItemIconTintList(ColorStateList2);


        Menu menu = navigationView.getMenu();
        MenuItem logInItem = menu.findItem(R.id.login);
        MenuItem profileItem = menu.findItem(R.id.profile);
        MenuItem AddItem = menu.findItem(R.id.addItem);
        MenuItem viewAccount = menu.findItem(R.id.viewAccount);
        MenuItem rentProducts = menu.findItem(R.id.rentProducts);
        MenuItem notification = menu.findItem(R.id.notification);
        MenuItem aboutUs = menu.findItem(R.id.aboutUs);
        MenuItem changePassItem = menu.findItem(R.id.changePass);
        MenuItem logoutItem = menu.findItem(R.id.logout);


        View header = navigationView.getHeaderView(0);

        ImageView adminImage = header.findViewById(R.id.adminNavigationProfileImg);
        TextView adminName = header.findViewById(R.id.adminNavigationName);
        TextView adminContact = header.findViewById(R.id.adminNavigationNumber);


        strSession = Sp.readFromPreferences(MainActivity.this, AppConstants.ADMIN_SESSION,"");



        if(strSession.equals("CANCEL")){
            logInItem.setVisible(true);
            profileItem.setVisible(false);
            AddItem.setVisible(false);
            notification.setVisible(false);
            rentProducts.setVisible(false);
            viewAccount.setVisible(false);
            aboutUs.setVisible(true);
            changePassItem.setVisible(false);
            logoutItem.setVisible(false);

        }
        if(strSession.equals("OK")){

            strAdminID = Sp.readFromPreferences(this, AppConstants.ADMIN_ID,"");
            strName = Sp.readFromPreferences(this, AppConstants.ADMIN_NAME,"");
            strMobile = Sp.readFromPreferences(this, AppConstants.ADMIN_MOBILE,"");
            strEmail = Sp.readFromPreferences(this, AppConstants.ADMIN_EMAIL,"");
            strPass = Sp.readFromPreferences(this, AppConstants.ADMIN_PASSWORD,"");
            strAddress = Sp.readFromPreferences(this, AppConstants.ADMIN_ADDRESS,"");
            strShopName = Sp.readFromPreferences(this, AppConstants .ADMIN_SHOPNAME,"");
            strShoptype = Sp.readFromPreferences(this, AppConstants.ADMIN_SHOPTYPE,"");
            strImgURL = Sp.readFromPreferences(this, AppConstants.ADMIN_IMGURL,"");

            logInItem.setVisible(false);
            profileItem.setVisible(true);
            AddItem.setVisible(true);
            rentProducts.setVisible(true);
            viewAccount.setVisible(true);
            notification.setVisible(true);
            aboutUs.setVisible(true);
            changePassItem.setVisible(true);
            logoutItem.setVisible(true);

            Picasso.get()
                    .load(ConfigFile.domain+ConfigFile.domaindir+"uploads/profile/"+strImgURL+".jpg")
                    .error(R.drawable.no_image)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .placeholder(R.drawable.no_image)
                    .resize(400,400)
                    .onlyScaleDown()
                    .into(adminImage);

            adminName.setText(""+strName);
            adminContact.setText(""+strMobile);

        }

        //txtScanResult = findViewById(R.id.txtScanResult);

        btnScanner = findViewById(R.id.btnScanner);
        cardScanner = findViewById(R.id.cardScanner);
        llShowProductMain = findViewById(R.id.llShowProductMain);

        txtCompanyName = findViewById(R.id.txtSetCompanyName);
        txtCategoryName = findViewById(R.id.txtSetCategory);
        txtSubCategoryName = findViewById(R.id.txtSetSubCategory);
        txtProductName = findViewById(R.id.txtSetProductName);
        txtProductDesc = findViewById(R.id.txtSetProductDescription);
        /*txtProductprice = findViewById(R.id.txtSetProductPrice);
        txtProducInStock = findViewById(R.id.txtSetInStock);
        txtProductStockQty = findViewById(R.id.txtSetProductStockQuantity);*/
        btnBook = findViewById(R.id.btnBook);
        btnSale = findViewById(R.id.btnSale);
        btnManualBooking = findViewById(R.id.btnManualBooking);

        btnManualBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Sp.saveToPreferences(MainActivity.this, AppConstants.INTENT_TO, "ManualBooking");
                Intent intent = new Intent(MainActivity.this, AllProductsActivity.class);
                startActivity(intent);
                overridePendingTransition(0,0);
                finish();
            }
        });

        //txtScanResult.setVisibility(View.GONE);
        llShowProductMain.setVisibility(View.GONE);

        btnScanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                IntentIntegrator integrator = new IntentIntegrator(MainActivity.this);
                integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
                integrator.setPrompt("Scanning.....");
                integrator.setCameraId(0);
                integrator.setOrientationLocked(true);
                integrator.setBeepEnabled(false);
                integrator.setBarcodeImageEnabled(false);
                integrator.setCaptureActivity(CaptureActivityPortrait.class);
                integrator.initiateScan();

            }
        });

        bottomNavigation = findViewById(R.id.bottomNavigation);
        bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.bottomQRScanner:
                        Intent intentScanner = new Intent(MainActivity.this, MainActivity.class);
                        startActivity(intentScanner);
                        overridePendingTransition(0,0);
                        finish();
                        return true;
                    case R.id.bottomRentList:
                        Intent intentRent = new Intent(MainActivity.this, RentListActivity.class);
                        startActivity(intentRent);
                        overridePendingTransition(0,0);
                        finish();
                        return true;
                    case R.id.bottomAddItem:
                        Intent intentAddItem = new Intent(MainActivity.this, AddItemActivity.class);
                        startActivity(intentAddItem);
                        overridePendingTransition(0,0);
                        finish();
                        return true;
                    case R.id.bottomCalender:
                        Intent intentCalender = new Intent(MainActivity.this, CalendarActivity.class);
                        startActivity(intentCalender);
                        overridePendingTransition(0,0);
                        finish();
                        //Toast.makeText(MainActivity.this, "Calender", Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.bottomInventory:
                        Sp.saveToPreferences(MainActivity.this, AppConstants.INTENT_TO, "ManageInventory");
                        Intent intentInventory = new Intent(MainActivity.this, AllProductsActivity.class);
                        startActivity(intentInventory);
                        overridePendingTransition(0,0);
                        finish();
                        return true;
                }

                return false;
            }
        });

        btnBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*strProductInStockSp = Sp.readFromPreferences(MainActivity.this, AppConstants.PRODUCT_INSTOCK,"");

                if(strProductInStockSp.equals("No")){

                    Toast.makeText(MainActivity.this, "Product Not in Stock", Toast.LENGTH_SHORT).show();
                }
                else{*/
                    Sp.saveToPreferences(MainActivity.this, AppConstants.INTENT_TO, "BookProduct");
                    Intent intent = new Intent(MainActivity.this,BookingActivity.class);
                    startActivity(intent);
                    overridePendingTransition(0,0);
                    finish();
               /* }*/
            }
        });

        btnSale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strProductInStockSp = Sp.readFromPreferences(MainActivity.this, AppConstants.PRODUCT_INSTOCK,"");

                /*if(strProductInStockSp.equals("No")){

                    Toast.makeText(MainActivity.this, "Product Not in Stock", Toast.LENGTH_SHORT).show();
                }
                else{*/
                    Sp.saveToPreferences(MainActivity.this, AppConstants.INTENT_TO, "SaleProduct");
                    Intent intent = new Intent(MainActivity.this,BookingActivity.class);
                    startActivity(intent);
                    overridePendingTransition(0,0);
                    finish();
                /*}*/
            }
        });



    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();

        if (id == R.id.profile) {

            Intent i = new Intent(MainActivity.this, ProfileActivity.class);
            startActivity(i);
            overridePendingTransition(0,0);
            finish();

        }  else if (id == R.id.login) {

            Intent i = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(i);
            overridePendingTransition(0,0);
            finish();

        }
        else if (id == R.id.addItem) {

            Intent i = new Intent(MainActivity.this, AddItemActivity.class);
            startActivity(i);
            overridePendingTransition(0,0);
            finish();

        }else if (id == R.id.allProducts) {

            Sp.saveToPreferences(MainActivity.this, AppConstants.INTENT_TO, "AllProducts");
            Intent i = new Intent(MainActivity.this, AllProductsActivity.class);
            startActivity(i);
            overridePendingTransition(0,0);
            finish();

        }
        else if (id == R.id.rentProducts) {

            Intent i = new Intent(MainActivity.this, RentListActivity.class);
            startActivity(i);
            overridePendingTransition(0,0);
            finish();

        }
        /*else if (id == R.id.bookItem) {

            Intent i = new Intent(MainActivity.this, BookItemActivity.class);
            startActivity(i);
            overridePendingTransition(0,0);
            finish();

        }*/
        else if (id == R.id.outSideInventory) {

            Intent i = new Intent(MainActivity.this, OutSideInventoryListActivity.class);
            startActivity(i);
            overridePendingTransition(0,0);
            finish();

        }
        else if (id == R.id.viewAccount) {

            Intent i = new Intent(MainActivity.this, ViewAccountsActivity.class);
            startActivity(i);
            overridePendingTransition(0,0);
            finish();


        }
        else if (id == R.id.transactionHistory) {

            Intent i = new Intent(MainActivity.this, TransactionHistoryActivity.class);
            startActivity(i);
            overridePendingTransition(0,0);
            finish();
        }
        else if (id == R.id.notification) {

            //Toast.makeText(this, "Notificaton", Toast.LENGTH_SHORT).show();

            Intent i = new Intent(MainActivity.this, NotificationActivity.class);
            startActivity(i);
            overridePendingTransition(0,0);
            finish();

        }
        else if (id == R.id.aboutUs) {

            Toast.makeText(this, "About US", Toast.LENGTH_SHORT).show();

            /*Intent i = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(i);*/

        }
        else if (id == R.id.changePass) {


            //Toast.makeText(this, "Change Password", Toast.LENGTH_SHORT).show();
            Intent i = new Intent(MainActivity.this, ChangePasswordActivity.class);
            startActivity(i);
            overridePendingTransition(0,0);
            finish();

        }
        else if (id == R.id.logout) {

            new AlertDialog.Builder(this)
                    .setTitle("Logout")
                    .setMessage("Are you sure..! You want to Logout?")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            Sp.saveToPreferences(MainActivity.this, AppConstants.ADMIN_NAME, "");
                            Sp.saveToPreferences(MainActivity.this, AppConstants.ADMIN_EMAIL, "");
                            Sp.saveToPreferences(MainActivity.this, AppConstants.ADMIN_MOBILE, "");
                            Sp.saveToPreferences(MainActivity.this, AppConstants.ADMIN_PASSWORD, "");
                            Sp.saveToPreferences(MainActivity.this, AppConstants.ADMIN_ADDRESS, "");
                            Sp.saveToPreferences(MainActivity.this, AppConstants.ADMIN_SHOPNAME, "");
                            Sp.saveToPreferences(MainActivity.this, AppConstants.ADMIN_SHOPTYPE, "");
                            Sp.saveToPreferences(MainActivity.this, AppConstants.ADMIN_SESSION, "CANCEL");

                            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                            startActivity(intent);

                        }
                    })
                    .setNegativeButton(android.R.string.no, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if(toolbar.getTitle().toString().trim().contains("Product Details")){
            Intent intent = new Intent(MainActivity.this, MainActivity.class);
            startActivity(intent);
            overridePendingTransition(0,0);
            finish();
        }
        else if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else {

            new AlertDialog.Builder(this)
                    .setTitle("Exit")
                    .setMessage("Are you sure..! You want to Exit?")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            Intent a = new Intent(Intent.ACTION_MAIN);
                            a.addCategory(Intent.CATEGORY_HOME);
                            a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(a);

                        }
                    })
                    .setNegativeButton(android.R.string.no, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
                Log.e("Scan*******", "Cancelled scan");

            } else {
                Log.e("Scan", "Scanned");

                //tv_qr_readTxt.setText(result.getContents());
                if (result.getContents().equals("")){
                    //Sp.saveToPreferences(BookItemActivity.this, AppConstants.INTENT_TO, "Scanner");
                    cardScanner.setVisibility(View.GONE);
                    onBackPressed();
                }else {
                    Sp.saveToPreferences(MainActivity.this, AppConstants.INTENT_TO, "Scanner");
                    cardScanner.setVisibility(View.GONE);
                    btnManualBooking.setVisibility(View.GONE);
                    qrScannedProductID = result.getContents();
                    getScanProduct();
                }

            }
        } else {

            super.onActivityResult(requestCode, resultCode, data);

        }
    }

    private void getScanProduct() {

        addDialog = new ProgressDialog(this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Please Wait...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        try{
            GetScanProductDataRequest getScanProductRequest = new GetScanProductDataRequest();
            getScanProductRequest.setProductId(qrScannedProductID);

            Call<GetScanProductDataResponse> responceCall = ApiClient.getApiClient().getScanProduct(getScanProductRequest);

            responceCall.enqueue(new Callback<GetScanProductDataResponse>() {
                @Override
                public void onResponse(Call<GetScanProductDataResponse> call, retrofit2.Response<GetScanProductDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        addDialog.dismiss();
                        List<GetScanProductDataItem> items =response.body().getGetScanProductData();

                        llShowProductMain.setVisibility(View.VISIBLE);

                        strCompanyName = items.get(0).getCompanyName();
                        strCategoryName = items.get(0).getCatName();
                        strSubCategoryName = items.get(0).getSubCatName();
                        strProductName = items.get(0).getProductName();
                        strProductDesc = items.get(0).getProductDetails();
                        strProductInStock = items.get(0).getProductInStock();
                        strProductStockQty = items.get(0).getProductQuantity();

                        Sp.saveToPreferences(MainActivity.this, AppConstants.PRODUCT_COMP_ID, ""+items.get(0).getProductCompnyId());
                        Sp.saveToPreferences(MainActivity.this, AppConstants.PRODUCT_CAT_ID, ""+items.get(0).getProductCategoryId());
                        Sp.saveToPreferences(MainActivity.this, AppConstants.PRODUCT_SUBCAT_ID, ""+items.get(0).getProductSubcategoryId());
                        Sp.saveToPreferences(MainActivity.this, AppConstants.PRODUCT_ID, ""+items.get(0).getProductId());
                        Sp.saveToPreferences(MainActivity.this, AppConstants.PRODUCT_COMP_NAME, ""+items.get(0).getCompanyName());
                        Sp.saveToPreferences(MainActivity.this, AppConstants.PRODUCT_CAT_NAME, ""+items.get(0).getCatName());
                        Sp.saveToPreferences(MainActivity.this, AppConstants.PRODUCT_SUBCAT_NAME, ""+items.get(0).getSubCatName());
                        Sp.saveToPreferences(MainActivity.this, AppConstants.PRODUCT_NAME, ""+items.get(0).getProductName());
                        Sp.saveToPreferences(MainActivity.this, AppConstants.PRODUCT_DESCRIPTION, ""+items.get(0).getProductDetails());
                        Sp.saveToPreferences(MainActivity.this, AppConstants.PRODUCT_INSTOCK, ""+items.get(0).getProductInStock());
                        Sp.saveToPreferences(MainActivity.this, AppConstants.PRODUCT_STOCKQTY, ""+items.get(0).getProductQuantity());


                        txtCompanyName.setText(strCompanyName);
                        txtCategoryName.setText(strCategoryName);
                        txtSubCategoryName.setText(strSubCategoryName);
                        txtProductName.setText(strProductName);
                        txtProductDesc.setText(strProductDesc);
                        //txtProductprice.setText(strProductPrice);
                        /*txtProducInStock.setText(strProductInStock);
                        txtProductStockQty.setText(strProductStockQty);*/
                        toolbar.setTitle("Product Details");


                        //Toast.makeText(MainActivity.this, "Company Name = "+strCompanyName+" Category Name = "+strCategoryName+" SubCategory Name = "+strSubCategoryName+" Product Name = "+strProductName, Toast.LENGTH_SHORT).show();


                    } else {

                        Toast.makeText(MainActivity.this, ""+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(MainActivity.this , MainActivity.class);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        finish();
                        addDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<GetScanProductDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialog.dismiss();

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    private void checkPermissions() {

        Dexter.withActivity(MainActivity.this)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA,
                        Manifest.permission.CALL_PHONE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            // Toast.makeText(getApplicationContext(), "All permissions are granted!", Toast.LENGTH_SHORT).show();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        //Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();

    }

    private void showSettingsDialog() {

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

}
