package com.microtron.cvinventorymanagmentapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.Toast;

import com.microtron.cvinventorymanagmentapp.Adapters.AllProductListAdapter;
import com.microtron.cvinventorymanagmentapp.Adapters.RecyclerItemClickListenerAllProductList;
import com.microtron.cvinventorymanagmentapp.Adapters.RecyclerItemClickListenerRentList;
import com.microtron.cvinventorymanagmentapp.Adapters.RentListAdapter;
import com.microtron.cvinventorymanagmentapp.Helper.ApiClient;
import com.microtron.cvinventorymanagmentapp.Helper.AppConstants;
import com.microtron.cvinventorymanagmentapp.Helper.Sp;
import com.microtron.cvinventorymanagmentapp.NetworkModel.GetAllProducts.AllProductsDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.GetAllProducts.AllProductsDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.GetAllProducts.AllProductsDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class AllProductsActivity extends AppCompatActivity {

    private RecyclerView recyclerAllProductsList;
    private ImageView imgBack, imgNoProduct;
    private ProgressDialog addDialog;
    private AllProductListAdapter allProductsAdapter;
    private String strIntentTo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_products);

        strIntentTo = Sp.readFromPreferences(this, AppConstants.INTENT_TO,"");

        recyclerAllProductsList = findViewById(R.id.recyclerAllProductList);
        imgBack = findViewById(R.id.imgBack);
        imgNoProduct = findViewById(R.id.imgNoProduct);
        imgNoProduct.setVisibility(View.GONE);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerAllProductsList.setLayoutAnimation(animation);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerAllProductsList.setLayoutManager(layoutManager);

        getAllProductList();

    }

    private void getAllProductList() {

        addDialog = new ProgressDialog(this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Getting List...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        try{
            AllProductsDataRequest getAllProductRequest = new AllProductsDataRequest();


            Call<AllProductsDataResponse> responceCall = ApiClient.getApiClient().getAllProducts(getAllProductRequest);

            responceCall.enqueue(new Callback<AllProductsDataResponse>() {
                @Override
                public void onResponse(Call<AllProductsDataResponse> call, retrofit2.Response<AllProductsDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        List<AllProductsDataItem> items =response.body().getAllProductsData();
                        imgNoProduct.setVisibility(View.GONE);

                        allProductsAdapter = new AllProductListAdapter(AllProductsActivity.this, items, recyclerItemClickListener);
                        recyclerAllProductsList.setAdapter(allProductsAdapter);
                        allProductsAdapter.notifyDataSetChanged();

                        addDialog.dismiss();


                    } else {

                        imgNoProduct.setVisibility(View.VISIBLE);
                        //Toast.makeText(AllProductsActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                        addDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<AllProductsDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialog.dismiss();

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    private RecyclerItemClickListenerAllProductList recyclerItemClickListener = new RecyclerItemClickListenerAllProductList() {
        @Override
        public void onItemClick(final AllProductsDataItem notiItem) {
        }
    };

    @Override
    public void onBackPressed() {
        if(strIntentTo.equals("ManualBooking")){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            overridePendingTransition(0,0);
            finish();
        }else{
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            overridePendingTransition(0,0);
            finish();
        }

    }

}
