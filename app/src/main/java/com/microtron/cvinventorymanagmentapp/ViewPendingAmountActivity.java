package com.microtron.cvinventorymanagmentapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;

import com.microtron.cvinventorymanagmentapp.Adapters.CustomerListAdapter;
import com.microtron.cvinventorymanagmentapp.Adapters.PendingAmountListAdapter;
import com.microtron.cvinventorymanagmentapp.Adapters.PendingCustomerListAdapter;
import com.microtron.cvinventorymanagmentapp.Adapters.RecyclerItemClickListenerCustomerList;
import com.microtron.cvinventorymanagmentapp.Adapters.RecyclerItemClickListenerPendingAmountList;
import com.microtron.cvinventorymanagmentapp.Adapters.RecyclerItemClickListenerPendingCustomerList;
import com.microtron.cvinventorymanagmentapp.Helper.ApiClient;
import com.microtron.cvinventorymanagmentapp.NetworkModel.Customer.CustomerDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.Customer.CustomerDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.Customer.CustomerDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PendingAmounts.PendingAmountDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PendingAmounts.PendingAmountDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PendingAmounts.PendingAmountDataResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class ViewPendingAmountActivity extends AppCompatActivity {

    private ImageView imgBack, imgNoProduct;
    private RecyclerView recyclerPendingCustomerList;
    private ProgressDialog addDialog;
    private PendingCustomerListAdapter pendingCustomerListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pending_amount);

        imgBack = findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        imgNoProduct = findViewById(R.id.imgNoProduct);
        imgNoProduct.setVisibility(View.GONE);

        recyclerPendingCustomerList = findViewById(R.id.recyclerPendingAmountList);

        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerPendingCustomerList.setLayoutAnimation(animation);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerPendingCustomerList.setLayoutManager(layoutManager);

        getCustomerList();


    }

    private void getCustomerList() {

        addDialog = new ProgressDialog(this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Getting List...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        try{
            CustomerDataRequest customerRequest = new CustomerDataRequest();


            Call<CustomerDataResponse> responceCall = ApiClient.getApiClient().getCustomer(customerRequest);

            responceCall.enqueue(new Callback<CustomerDataResponse>() {
                @Override
                public void onResponse(Call<CustomerDataResponse> call, retrofit2.Response<CustomerDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        List<CustomerDataItem> items =response.body().getCustomerData();
                        imgNoProduct.setVisibility(View.GONE);

                        pendingCustomerListAdapter = new PendingCustomerListAdapter(ViewPendingAmountActivity.this, items, recyclerItemClickListener);
                        recyclerPendingCustomerList.setAdapter(pendingCustomerListAdapter);
                        pendingCustomerListAdapter.notifyDataSetChanged();

                        addDialog.dismiss();


                    } else {

                        //Toast.makeText(RentListActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                        imgNoProduct.setVisibility(View.VISIBLE);
                        addDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<CustomerDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialog.dismiss();

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    private RecyclerItemClickListenerPendingCustomerList recyclerItemClickListener = new RecyclerItemClickListenerPendingCustomerList() {
        @Override
        public void onItemClick(final CustomerDataItem notiItem) {
        }
    };


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, ViewAccountsActivity.class);
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
    }
}
