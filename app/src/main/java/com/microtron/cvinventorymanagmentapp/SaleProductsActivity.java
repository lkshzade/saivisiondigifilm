package com.microtron.cvinventorymanagmentapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;

import com.microtron.cvinventorymanagmentapp.Adapters.RecyclerItemClickListenerSaleProductList;
import com.microtron.cvinventorymanagmentapp.Adapters.SaleProductListAdapter;
import com.microtron.cvinventorymanagmentapp.Helper.ApiClient;
import com.microtron.cvinventorymanagmentapp.NetworkModel.SaleItemData.SaleItemDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.SaleItemData.SaleItemDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.SaleItemData.SaleItemDataResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class SaleProductsActivity extends AppCompatActivity {

    private String strProductSatus;
    private ProgressDialog addDialog;
    private RecyclerView recyclerSaleProductList;
    private ImageView imgBack, imgNoProduct;
    private SaleProductListAdapter saleProductListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sale_products);

        recyclerSaleProductList = findViewById(R.id.recyclerSaleProductList);
        imgBack = findViewById(R.id.imgBack);
        imgNoProduct = findViewById(R.id.imgNoProduct);

        imgNoProduct.setVisibility(View.GONE);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerSaleProductList.setLayoutAnimation(animation);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerSaleProductList.setLayoutManager(layoutManager);

        getSaleProduct();
    }

    private void getSaleProduct() {

        addDialog = new ProgressDialog(this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Getting Details...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        strProductSatus = "Sale";

        try{
            SaleItemDataRequest saleItemDataRequest = new SaleItemDataRequest();

            Call<SaleItemDataResponse> responceCall = ApiClient.getApiClient().getSaleItemRequest(saleItemDataRequest);

            responceCall.enqueue(new Callback<SaleItemDataResponse>() {
                @Override
                public void onResponse(Call<SaleItemDataResponse> call, retrofit2.Response<SaleItemDataResponse> response) {

                    if (response.body().getSuccess().equals("1")) {

                        List<SaleItemDataItem> items =response.body().getSaleItemData();

                        imgNoProduct.setVisibility(View.GONE);

                        saleProductListAdapter = new SaleProductListAdapter(SaleProductsActivity.this, items, recyclerItemClickListener);
                        recyclerSaleProductList.setAdapter(saleProductListAdapter);
                        saleProductListAdapter.notifyDataSetChanged();

                        addDialog.dismiss();

                    } else {

                        //Toast.makeText(ViewAccountsActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                        imgNoProduct.setVisibility(View.VISIBLE);
                        addDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<SaleItemDataResponse> call, Throwable t) {

                    //Toast.makeText(ViewAccountsActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    private RecyclerItemClickListenerSaleProductList recyclerItemClickListener = new RecyclerItemClickListenerSaleProductList() {
        @Override
        public void onItemClick(final SaleItemDataItem notiItem) {
        }
    };


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, ViewAccountsActivity.class);
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
    }
}
