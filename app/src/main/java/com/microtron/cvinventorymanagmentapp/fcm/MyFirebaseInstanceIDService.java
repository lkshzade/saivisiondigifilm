package com.microtron.cvinventorymanagmentapp.fcm;

import android.content.Intent;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.microtron.cvinventorymanagmentapp.Helper.AppConstants;
import com.microtron.cvinventorymanagmentapp.Helper.Sp;


public class MyFirebaseInstanceIDService extends FirebaseMessagingService {

    Intent intent;
    String clickAction;
    int messageId;

   // private static final String TAG = "MyFirebaseInstanceIDService";


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        String title = remoteMessage.getData().get("title");
        String message = remoteMessage.getData().get("message");

        NotificationHelper notificationHelper = new NotificationHelper(this);
        notificationHelper.createNotification(title, message, "");

    }

    private void sendRegistrationToServer(String refreshedToken) {
        Log.d("TOKEN ", refreshedToken.toString());
    }

    public final static String infoTopicNmae="/topics/admin";
    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);

        Sp.saveToPreferences(this, AppConstants.FCM_TOKEN, s);
        FirebaseMessaging.getInstance().subscribeToTopic(infoTopicNmae);

        Log.w("tokken", s);

    }




}

