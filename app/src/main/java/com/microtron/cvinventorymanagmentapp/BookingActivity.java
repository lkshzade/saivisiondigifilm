package com.microtron.cvinventorymanagmentapp;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.microtron.cvinventorymanagmentapp.Adapters.CustomerListAdapter;
import com.microtron.cvinventorymanagmentapp.Helper.ApiClient;
import com.microtron.cvinventorymanagmentapp.Helper.AppConstants;
import com.microtron.cvinventorymanagmentapp.Helper.Sp;
import com.microtron.cvinventorymanagmentapp.NetworkModel.BookingItem.BookingDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.BookingItem.BookingDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.BookingItem.BookingDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.Customer.CustomerDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.Customer.CustomerDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.Customer.CustomerDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PreDateBookingData.PreDateDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PreDateBookingData.PreDateDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PreDateBookingData.PreDateDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PreFromDateData.PreFromDateDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PreFromDateData.PreFromDateDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PreFromDateData.PreFromDateDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PreToDateData.PreToDateDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PreToDateData.PreToDateDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PreToDateData.PreToDateDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.SaleDateData.SaleDateDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.SaleDateData.SaleDateDataResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;

import static com.microtron.cvinventorymanagmentapp.Helper.ConfigFile.getCustomer;

public class BookingActivity extends AppCompatActivity {

    private String strProductIdSP, strProductCompIdSP, strProductCatIdSP, strProductSubCatId, strProductCompNameSP, strProductCatNameSP, strProductSubCatNameSP, strProductNameSP, strProductDescSP, strProductPriceSP, strProductInStockSP, strProductStockQtySP;
    private TextView txtBookingCompanyName, txtBookingProductName, txtFromDate, txtToDate, txtRentPrice, txtSaleDate;
    private EditText edtMonthRent, edtDayRent, edtSaleRate;
    private AutoCompleteTextView txtCustomerName, txtCustomerMobile, txtCustomerEmail;
    private ImageView imgBack;
    private TextView txtValidCustName, txtValidCustMobile, txtValidCustEmail, txtValidMonthRent, txtValidDayRent, txtValidFromDate, txtValidToDate, txtValidSaleRate, txtValidSaleDate;
    private Button btnBookItem, btnSaleItem;
    private float totalRent = 0.0f;
    private String strCustomerName = "", strCustomerMobile = "", strCustomerEmail = "", strToDate = "", strFromDate = "", strSaleDate = "";
    private int mFromDay, mFromMonth, mFromYear, mToDay, mToMonth, mToYear, mSaleDay, mSaleMonth, mSaleYear;
    private Date date1, date2;
    /*String xyz;*/
    String finalQuantity;
    private float dayCount = 0.0f;
    private String strFromDay = "", strFromMonthOfYear = "", strFromYear = "", strToDay = "", strToMonthOfYear = "", strToYear = "", strSaleDay = "", strSaleMonthOfYear = "", strSaleYear = "";
    private ProgressDialog addDialog, addDialogFromDate, addDialogToDate, addDialogProductStatus;
    private String finalInStock = "", strIntentTo;
    private LinearLayout llMonthlyRent, llDailyRent, llFromDate, llToDate, llTotalRent, llAdvance, llPending, llSaleRate, llSaleDate;
    private EditText edtAdvance;
    private TextView txtPending;
    private RadioGroup rdgBookingType;
    private RadioButton rbMonthly, rbDaily;
    private String strBookingType = "Sale", strProductStatus = "Rented", strMonthRent = "", strDayRent = "", strSaleRate = "", strBookingSatus = "1";
    private String strToDayChange = "", strToMonthChange = "", strToYearChange = "", strAdvance = "", strPending = "";
    private float rentAdvance = 1.0f, rentPending = 0.0f;

    public final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9+._%-+]{1,256}" +
                    "@" +
                    "[a-zA-Z0-9][a-zA-Z0-9-]{0,64}" +
                    "(" +
                    "." +
                    "[a-zA-Z0-9][a-zA-Z0-9-]{0,25}" +
                    ")+"
    );

    private ArrayList<String> custNameList = new ArrayList<String>();
    private ArrayList<String> custMobileList = new ArrayList<String>();
    private ArrayList<String> custEmailList = new ArrayList<String>();
    private ArrayList<String> custIDList = new ArrayList<String>();


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);

        strProductIdSP = Sp.readFromPreferences(this, AppConstants.PRODUCT_ID,"");
        strProductCompIdSP = Sp.readFromPreferences(this, AppConstants.PRODUCT_COMP_ID,"");
        strProductCatIdSP = Sp.readFromPreferences(this, AppConstants.PRODUCT_CAT_ID,"");
        strProductSubCatId = Sp.readFromPreferences(this, AppConstants.PRODUCT_SUBCAT_ID,"");
        strProductCompNameSP = Sp.readFromPreferences(this, AppConstants.PRODUCT_COMP_NAME,"");
        strProductCatNameSP = Sp.readFromPreferences(this, AppConstants.PRODUCT_CAT_NAME,"");
        strProductSubCatNameSP = Sp.readFromPreferences(this, AppConstants.PRODUCT_SUBCAT_NAME,"");
        strProductNameSP = Sp.readFromPreferences(this, AppConstants.PRODUCT_NAME,"");
        strProductDescSP = Sp.readFromPreferences(this, AppConstants.PRODUCT_DESCRIPTION,"");
        strProductInStockSP = Sp.readFromPreferences(this, AppConstants.PRODUCT_INSTOCK,"");
        strProductStockQtySP = Sp.readFromPreferences(this, AppConstants.PRODUCT_STOCKQTY,"");

        strIntentTo = Sp.readFromPreferences(this, AppConstants.INTENT_TO,"");

        txtBookingCompanyName = findViewById(R.id.txtBookingCompany);
        txtBookingProductName = findViewById(R.id.txtBookingProductName);
        txtCustomerName = findViewById(R.id.txtCustName);
        txtCustomerMobile = findViewById(R.id.txtCustMobile);
        txtCustomerEmail = findViewById(R.id.txtCustEmail);
        rdgBookingType = findViewById(R.id.rdgBookingType);
        rbMonthly = findViewById(R.id.rbMonthly);
        rbDaily = findViewById(R.id.rbDaily);
        edtMonthRent = findViewById(R.id.edtMonthRent);
        edtDayRent = findViewById(R.id.edtDayRent);
        edtAdvance = findViewById(R.id.edtAdvance);
        txtPending = findViewById(R.id.txtPending);
        txtPending.setEnabled(false);
        txtFromDate = findViewById(R.id.txtFromDate);
        txtToDate = findViewById(R.id.txtToDate);
        txtRentPrice = findViewById(R.id.txtRentPrice);
        txtSaleDate = findViewById(R.id.txtSaleDate);
        edtSaleRate = findViewById(R.id.edtSaleRate);
        imgBack = findViewById(R.id.imgBack);
        txtValidCustName = findViewById(R.id.txtValidCustName);
        txtValidCustMobile = findViewById(R.id.txtValidCustMobile);
        txtValidCustEmail = findViewById(R.id.txtValidCustEmail);
        txtValidMonthRent = findViewById(R.id.txtValidMonthRent);
        txtValidDayRent = findViewById(R.id.txtValidDayRent);
        txtValidFromDate = findViewById(R.id.txtValidFromDate);
        txtValidToDate = findViewById(R.id.txtValidToDate);
        txtValidSaleRate = findViewById(R.id.txtValidSaleRate);
        txtValidSaleDate = findViewById(R.id.txtValidSaleDate);
        llSaleDate = findViewById(R.id.llSaleDate);
        llSaleRate = findViewById(R.id.llSaleRate);
        llMonthlyRent = findViewById(R.id.llMonthlyRent);
        llDailyRent = findViewById(R.id.llDailyRent);
        llFromDate = findViewById(R.id.llFromDate);
        llToDate = findViewById(R.id.llToDate);
        llTotalRent = findViewById(R.id.llTotalRent);
        llAdvance = findViewById(R.id.llAdvance);
        llPending = findViewById(R.id.llPending);
        btnBookItem = findViewById(R.id.btnBookItem);
        btnSaleItem = findViewById(R.id.btnSaleItem);

        llMonthlyRent.setVisibility(View.GONE);
        llDailyRent.setVisibility(View.GONE);
        llFromDate.setVisibility(View.GONE);
        llToDate.setVisibility(View.GONE);
        llTotalRent.setVisibility(View.GONE);
        llAdvance.setVisibility(View.GONE);
        llPending.setVisibility(View.GONE);
        llSaleRate.setVisibility(View.GONE);
        llSaleDate.setVisibility(View.GONE);
        btnSaleItem.setVisibility(View.GONE);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        getCustomers();

        txtCustomerName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(BookingActivity.this, android.R.layout.select_dialog_item, custNameList);
                txtCustomerName.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
                txtCustomerName.setTextColor(getResources().getColor(R.color.black));
                txtCustomerName.setThreshold(1);

                strCustomerName = txtCustomerName.getText().toString().trim();

                for (int j = 0; j < custNameList.size(); j++) {

                    if (strCustomerName.equals(custNameList.get(j).trim())) {

                        strCustomerName = custNameList.get(j);
                        strCustomerMobile = custMobileList.get(j);
                        strCustomerEmail = custEmailList.get(j);

                        txtCustomerMobile.setText(strCustomerMobile);
                        txtCustomerEmail.setText(strCustomerEmail);

                        txtCustomerEmail.setEnabled(false);
                        txtCustomerMobile.setEnabled(false);

                        break;
                    } else {

                        txtCustomerMobile.setText("");
                        txtCustomerEmail.setText("");
                        txtCustomerEmail.setEnabled(true);
                        txtCustomerMobile.setEnabled(true);

                    }
                }

            }
        });

        if(strIntentTo.equals("SaleProduct")){
            rdgBookingType.setVisibility(View.GONE);
            llMonthlyRent.setVisibility(View.GONE);
            llDailyRent.setVisibility(View.GONE);
            llFromDate.setVisibility(View.GONE);
            llToDate.setVisibility(View.GONE);
            llTotalRent.setVisibility(View.GONE);
            llAdvance.setVisibility(View.GONE);
            llPending.setVisibility(View.GONE);
            btnBookItem.setVisibility(View.GONE);
            llSaleRate.setVisibility(View.VISIBLE);
            llSaleDate.setVisibility(View.VISIBLE);
            btnSaleItem.setVisibility(View.VISIBLE);
            btnBookItem.setText("Sale Item");
            strBookingSatus = "2";
            strProductStatus = "Sale";
        }

        txtBookingCompanyName.setText(strProductCompNameSP);
        txtBookingProductName.setText(strProductNameSP);


        txtSaleDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Calendar c = Calendar.getInstance();
                mSaleYear = c.get(Calendar.YEAR);
                mSaleMonth = c.get(Calendar.MONTH);
                mSaleDay = c.get(Calendar.DAY_OF_MONTH);

                strSaleRate = edtSaleRate.getText().toString().trim();

                DatePickerDialog datePickerDialog = new DatePickerDialog(BookingActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                if (dayOfMonth <= 9) {
                                    strSaleDay = "0" + dayOfMonth;
                                } else {
                                    strSaleDay = "" + dayOfMonth;
                                }
                                monthOfYear = monthOfYear + 1;
                                strSaleYear = String.valueOf(year);

                                if (monthOfYear <= 9) {
                                    strSaleMonthOfYear = "0" + monthOfYear;
                                } else {
                                    strSaleMonthOfYear = "" + monthOfYear;
                                }

                                txtSaleDate.setText(strSaleYear + "-" + strSaleMonthOfYear + "-" + strSaleDay);

                                strSaleDate = txtSaleDate.getText().toString().trim();

                                checkSaleDateFromDataBase();

                            }
                        }, mSaleYear, mSaleMonth, mSaleDay);


                datePickerDialog.show();
            }
        });


        rdgBookingType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.rbMonthly:
                        strBookingType = "Monthly";
                        llMonthlyRent.setVisibility(View.VISIBLE);
                        llDailyRent.setVisibility(View.GONE);
                        edtDayRent.setText("");
                        edtMonthRent.setText("");
                        txtFromDate.setText("");
                        txtToDate.setText("");
                        txtRentPrice.setText("");
                        txtToDate.setEnabled(false);
                        /*llFromDate.setVisibility(View.VISIBLE);
                        llToDate.setVisibility(View.VISIBLE);
                        llTotalRent.setVisibility(View.VISIBLE);*/
                        break;
                    case R.id.rbDaily:
                        strBookingType = "Daily";
                        llMonthlyRent.setVisibility(View.GONE);
                        llDailyRent.setVisibility(View.VISIBLE);
                        edtDayRent.setText("");
                        edtMonthRent.setText("");
                        txtFromDate.setText("");
                        txtToDate.setText("");
                        txtRentPrice.setText("");
                        txtToDate.setEnabled(true);
                        /*llFromDate.setVisibility(View.VISIBLE);
                        llToDate.setVisibility(View.VISIBLE);
                        llTotalRent.setVisibility(View.VISIBLE);*/
                        break;
                }
            }
        });

        edtDayRent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                strDayRent = edtDayRent.getText().toString().trim();

                if(strDayRent.equals("")){
                    llFromDate.setVisibility(View.GONE);
                    llToDate.setVisibility(View.GONE);
                    llTotalRent.setVisibility(View.GONE);
                    llAdvance.setVisibility(View.GONE);
                    llPending.setVisibility(View.GONE);
                }
                else{
                    llFromDate.setVisibility(View.VISIBLE);
                    llToDate.setVisibility(View.VISIBLE);
                    llTotalRent.setVisibility(View.VISIBLE);
                    llAdvance.setVisibility(View.VISIBLE);
                    llPending.setVisibility(View.VISIBLE);
                }

            }
        });

        edtMonthRent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                strMonthRent = edtMonthRent.getText().toString().trim();

                if(strMonthRent.equals("")){
                    llFromDate.setVisibility(View.GONE);
                    llToDate.setVisibility(View.GONE);
                    llTotalRent.setVisibility(View.GONE);
                    llAdvance.setVisibility(View.GONE);
                    llPending.setVisibility(View.GONE);
                }
                else{
                    llFromDate.setVisibility(View.VISIBLE);
                    llToDate.setVisibility(View.VISIBLE);
                    llTotalRent.setVisibility(View.VISIBLE);
                    llAdvance.setVisibility(View.VISIBLE);
                    llPending.setVisibility(View.VISIBLE);
                }

            }
        });


        txtFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                mFromYear = c.get(Calendar.YEAR);
                mFromMonth = c.get(Calendar.MONTH);
                mFromDay = c.get(Calendar.DAY_OF_MONTH);
                strToMonthChange = "";
                strToDayChange = "";

                DatePickerDialog datePickerDialog = new DatePickerDialog(BookingActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                if (dayOfMonth <= 9) {
                                    strFromDay = "0" + dayOfMonth;
                                } else {
                                    strFromDay = "" + dayOfMonth;
                                }
                                monthOfYear = monthOfYear + 1;
                                strFromYear = String.valueOf(year);

                                if (monthOfYear <= 9) {
                                    strFromMonthOfYear = "0" + monthOfYear;
                                } else {
                                    strFromMonthOfYear = "" + monthOfYear;
                                }

                                txtFromDate.setText(strFromYear + "-" + strFromMonthOfYear + "-" + strFromDay);
                                strMonthRent = edtMonthRent.getText().toString().trim();
                                strFromDate = txtFromDate.getText().toString().trim();

                                checkFromDateFromDataBase();

                                if(strBookingType.equals("Monthly")){

                                    int fromYear = Integer.parseInt(strFromYear);
                                    int fromDay = Integer.parseInt(strFromDay);
                                    int fromMonth = Integer.parseInt(strFromMonthOfYear);


                                    Calendar calendar = Calendar.getInstance();
                                    calendar.set(fromYear, Calendar.FEBRUARY, 1);
                                    int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
                                    //Toast.makeText(BookingActivity.this, ""+maxDay, Toast.LENGTH_SHORT).show();

                                    int toDay = fromDay;
                                    int toMonth = fromMonth + 1;
                                    int toYear;

                                    if(fromMonth >=12){
                                         toMonth = 01;
                                         toYear = fromYear + 1;
                                    }
                                    else {
                                        toYear = fromYear;
                                    }

                                    if (toDay <= 9) {
                                        strToDayChange = "0" + toDay;
                                    } else {
                                        strToDayChange = "" + toDay;
                                    }

                                    if (toMonth <= 9) {
                                        strToMonthChange = "0" + toMonth;
                                    } else {
                                        strToMonthChange = "" + toMonth;
                                    }

                                    if(strToMonthChange.equals("02")){
                                        if(fromDay == 29 && maxDay ==29){
                                            strToDayChange = "29";
                                        }
                                        else if(fromDay == 30 && maxDay ==29){
                                            strToDayChange = "01";
                                            strToMonthChange = "03";
                                        }
                                        else if(fromDay == 31 && maxDay ==29){
                                            strToDayChange = "02";
                                            strToMonthChange = "03";
                                        }
                                        if(fromDay == 28 && maxDay ==28){
                                            strToDayChange = "28";
                                        }
                                        else if(fromDay == 29 && maxDay ==28){
                                            strToDayChange = "01";
                                            strToMonthChange = "03";
                                        }
                                        else if(fromDay == 30 && maxDay ==28){
                                            strToDayChange = "02";
                                            strToMonthChange = "03";
                                        }
                                        else if(fromDay == 31 && maxDay ==28){
                                            strToDayChange = "03";
                                            strToMonthChange = "03";
                                        }
                                    }


                                    txtToDate.setText(toYear + "-" + strToMonthChange + "-" + strToDayChange);
                                    strToDate = txtToDate.getText().toString().trim();

                                    totalRent = (Float.parseFloat(strMonthRent))*(1);
                                    txtRentPrice.setText(""+totalRent);

                                    checkToDateFromDataBase();

                                }

                            }
                        }, mFromYear, mFromMonth, mFromDay);



                datePickerDialog.show();
            }
        });


        txtToDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                mToYear = c.get(Calendar.YEAR);
                mToMonth = c.get(Calendar.MONTH);
                mToDay = c.get(Calendar.DAY_OF_MONTH);

                strDayRent = edtDayRent.getText().toString().trim();

                DatePickerDialog datePickerDialog = new DatePickerDialog(BookingActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                if (dayOfMonth <= 9) {
                                    strToDay = "0" + dayOfMonth;
                                } else {
                                    strToDay = "" + dayOfMonth;
                                }
                                monthOfYear = monthOfYear + 1;
                                strToYear = String.valueOf(year);

                                if (monthOfYear <= 9) {
                                    strToMonthOfYear = "0" + monthOfYear;
                                } else {
                                    strToMonthOfYear = "" + monthOfYear;
                                }

                                txtToDate.setText(strToYear + "-" + strToMonthOfYear + "-" + strToDay);

                                strToDate = txtToDate.getText().toString().trim();

                                try{
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // here set the pattern as you date in string was containing like date/month/year
                                    date1 = sdf.parse(strFromDate);
                                    date2 = sdf.parse(strToDate);
                                    dayCount = (date2.getTime() - date1.getTime())/(24*60*60*1000);
                                    //Toast.makeText(BookingActivity.this, "To Date"+date2+"\n From Date"+date1, Toast.LENGTH_LONG).show();
                                    //Toast.makeText(BookingActivity.this, "Day Count"+dayCount, Toast.LENGTH_SHORT).show();
                                    if(dayCount==0.0f){
                                        dayCount = 1.0f;
                                    }
                                    totalRent = (Float.parseFloat(strDayRent))*(dayCount);
                                    txtRentPrice.setText(""+totalRent);
                                }catch(ParseException ex){
                                    // handle parsing exception if date string was different from the pattern applying into the SimpleDateFormat contructor
                                }

                                checkToDateFromDataBase();

                            }
                        }, mToYear, mToMonth, mToDay);


                datePickerDialog.show();
            }
        });

        edtAdvance.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                strAdvance = edtAdvance.getText().toString().trim();

                if(strAdvance.equals("")){
                    rentAdvance = 0.0f;
                    rentPending = totalRent - rentAdvance;
                    txtPending.setText(""+rentPending);
                }
                else{
                    rentAdvance = Float.parseFloat(strAdvance);
                    llPending.setVisibility(View.VISIBLE);
                    rentPending = totalRent - rentAdvance;
                    txtPending.setText(""+rentPending);
                }

            }
        });

        btnBookItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strCustomerName = txtCustomerName.getText().toString().trim();
                strCustomerMobile = txtCustomerMobile.getText().toString().trim();
                strCustomerEmail = txtCustomerEmail.getText().toString().trim();
                strFromDate = txtFromDate.getText().toString().trim();
                strToDate = txtToDate.getText().toString().trim();
                strMonthRent = edtMonthRent.getText().toString().trim();
                strDayRent = edtDayRent.getText().toString().trim();
                strAdvance = edtAdvance.getText().toString().trim();
                strPending = txtPending.getText().toString().trim();
                strSaleRate = edtSaleRate.getText().toString().trim();
                strSaleDate = txtSaleDate.getText().toString().trim();
                if (rdgBookingType.getCheckedRadioButtonId() == -1)
                {
                    Toast.makeText(BookingActivity.this, "Please Select Monthly or Daily", Toast.LENGTH_SHORT).show();
                }

                if(strCustomerName.length() < 3){
                    txtCustomerName.setError("Customer Name Should Be Greater Than 3 Character");
                    Toast.makeText(BookingActivity.this, "Customer Name Should Be Greater Than 3 Character", Toast.LENGTH_SHORT).show();
                    txtValidCustName.setVisibility(View.VISIBLE);
                    txtValidCustMobile.setVisibility(View.GONE);
                    txtValidCustEmail.setVisibility(View.GONE);
                    txtValidFromDate.setVisibility(View.GONE);
                    txtValidToDate.setVisibility(View.GONE);
                    txtValidMonthRent.setVisibility(View.GONE);
                    txtValidDayRent.setVisibility(View.GONE);
                }
                else if(strCustomerMobile.length() <= 9){
                    txtCustomerMobile.setError("Mobile No. contains 10 Character");
                    Toast.makeText(BookingActivity.this, "Mobile No. contains 10 Character", Toast.LENGTH_SHORT).show();
                    txtValidCustName.setVisibility(View.GONE);
                    txtValidCustMobile.setVisibility(View.VISIBLE);
                    txtValidCustEmail.setVisibility(View.GONE);
                    txtValidFromDate.setVisibility(View.GONE);
                    txtValidToDate.setVisibility(View.GONE);
                    txtValidMonthRent.setVisibility(View.GONE);
                    txtValidDayRent.setVisibility(View.GONE);
                }
                else if(!checkEmail(strCustomerEmail)){
                    txtCustomerEmail.setError("Email Should contains @ and . symbols");
                    Toast.makeText(BookingActivity.this, "Email Should contains @ and . symbols", Toast.LENGTH_SHORT).show();
                    txtValidCustName.setVisibility(View.GONE);
                    txtValidCustMobile.setVisibility(View.GONE);
                    txtValidCustEmail.setVisibility(View.VISIBLE);
                    txtValidFromDate.setVisibility(View.GONE);
                    txtValidToDate.setVisibility(View.GONE);
                    txtValidMonthRent.setVisibility(View.GONE);
                    txtValidDayRent.setVisibility(View.GONE);
                }
                else if(strBookingType.equals("Monthly") && strMonthRent.equals("")){
                    edtMonthRent.setError("Please Enter Monthly Rent First");
                    Toast.makeText(BookingActivity.this, "Please Enter Monthly Rent First", Toast.LENGTH_SHORT).show();
                    txtValidCustName.setVisibility(View.GONE);
                    txtValidCustMobile.setVisibility(View.GONE);
                    txtValidCustEmail.setVisibility(View.GONE);
                    txtValidFromDate.setVisibility(View.GONE);
                    txtValidToDate.setVisibility(View.GONE);
                    txtValidMonthRent.setVisibility(View.VISIBLE);
                    txtValidDayRent.setVisibility(View.GONE);
                }
                else if(strBookingType.equals("Daily") && strDayRent.equals("")){
                    edtDayRent.setError("Please Enter Per Day Rent First");
                    Toast.makeText(BookingActivity.this, "Please Enter Per Day Rent First", Toast.LENGTH_SHORT).show();
                    txtValidCustName.setVisibility(View.GONE);
                    txtValidCustMobile.setVisibility(View.GONE);
                    txtValidCustEmail.setVisibility(View.GONE);
                    txtValidFromDate.setVisibility(View.GONE);
                    txtValidToDate.setVisibility(View.GONE);
                    txtValidMonthRent.setVisibility(View.GONE);
                    txtValidDayRent.setVisibility(View.VISIBLE);
                }
                else if(strFromDate.equals("")){
                    txtFromDate.setError("Please Select Date First");
                    Toast.makeText(BookingActivity.this, "Please Select Date First", Toast.LENGTH_SHORT).show();
                    txtValidCustName.setVisibility(View.GONE);
                    txtValidCustMobile.setVisibility(View.GONE);
                    txtValidCustEmail.setVisibility(View.GONE);
                    txtValidFromDate.setVisibility(View.VISIBLE);
                    txtValidToDate.setVisibility(View.GONE);
                    txtValidMonthRent.setVisibility(View.GONE);
                    txtValidDayRent.setVisibility(View.GONE);
                }
                else if(strToDate.equals("")){
                    txtToDate.setError("Please Select Date First");
                    Toast.makeText(BookingActivity.this, "Please Select Date First", Toast.LENGTH_SHORT).show();
                    txtValidCustName.setVisibility(View.GONE);
                    txtValidCustMobile.setVisibility(View.GONE);
                    txtValidCustEmail.setVisibility(View.GONE);
                    txtValidFromDate.setVisibility(View.GONE);
                    txtValidToDate.setVisibility(View.VISIBLE);
                    txtValidMonthRent.setVisibility(View.GONE);
                    txtValidDayRent.setVisibility(View.GONE);
                }
                else {
                    txtValidCustName.setVisibility(View.GONE);
                    txtValidCustMobile.setVisibility(View.GONE);
                    txtValidCustEmail.setVisibility(View.GONE);
                    txtValidFromDate.setVisibility(View.GONE);
                    txtValidToDate.setVisibility(View.GONE);
                    txtValidMonthRent.setVisibility(View.GONE);
                    txtValidDayRent.setVisibility(View.GONE);
                    txtValidSaleRate.setVisibility(View.GONE);
                    txtValidSaleDate.setVisibility(View.GONE);
                    bookItem();
                }
            }
        });

        btnSaleItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strCustomerName = txtCustomerName.getText().toString().trim();
                strCustomerMobile = txtCustomerMobile.getText().toString().trim();
                strCustomerEmail = txtCustomerEmail.getText().toString().trim();
                strSaleRate = edtSaleRate.getText().toString().trim();
                strSaleDate = txtSaleDate.getText().toString().trim();

                if(strCustomerName.length() < 3){
                    txtCustomerName.setError("Customer Name Should Be Greater Than 3 Character");
                    Toast.makeText(BookingActivity.this, "Customer Name Should Be Greater Than 3 Character", Toast.LENGTH_SHORT).show();
                    txtValidCustName.setVisibility(View.VISIBLE);
                    txtValidCustMobile.setVisibility(View.GONE);
                    txtValidCustEmail.setVisibility(View.GONE);
                    txtValidSaleRate.setVisibility(View.GONE);
                    txtValidSaleDate.setVisibility(View.GONE);
                }
                else if(strCustomerMobile.length() <= 9){
                    txtCustomerMobile.setError("Mobile No. contains 10 Character");
                    Toast.makeText(BookingActivity.this, "Mobile No. contains 10 Character", Toast.LENGTH_SHORT).show();
                    txtValidCustName.setVisibility(View.GONE);
                    txtValidCustMobile.setVisibility(View.VISIBLE);
                    txtValidCustEmail.setVisibility(View.GONE);
                    txtValidSaleRate.setVisibility(View.GONE);
                    txtValidSaleDate.setVisibility(View.GONE);
                }
                else if(!checkEmail(strCustomerEmail)){
                    txtCustomerEmail.setError("Email Should contains @ and . symbols");
                    Toast.makeText(BookingActivity.this, "Email Should contains @ and . symbols", Toast.LENGTH_SHORT).show();
                    txtValidCustName.setVisibility(View.GONE);
                    txtValidCustMobile.setVisibility(View.GONE);
                    txtValidCustEmail.setVisibility(View.VISIBLE);
                    txtValidSaleRate.setVisibility(View.GONE);
                    txtValidSaleDate.setVisibility(View.GONE);
                }
                else if(strSaleRate.equals("")){
                    edtSaleRate.setError("Enter Price First");
                    Toast.makeText(BookingActivity.this, "Enter Price First", Toast.LENGTH_SHORT).show();
                    txtValidCustName.setVisibility(View.GONE);
                    txtValidCustMobile.setVisibility(View.GONE);
                    txtValidCustEmail.setVisibility(View.GONE);
                    txtValidSaleRate.setVisibility(View.VISIBLE);
                    txtValidSaleDate.setVisibility(View.GONE);
                }
                else if(strSaleDate.equals("")){
                    txtSaleDate.setError("Please Select Date First");
                    Toast.makeText(BookingActivity.this, "Please Select Date First", Toast.LENGTH_SHORT).show();
                    txtValidCustName.setVisibility(View.GONE);
                    txtValidCustMobile.setVisibility(View.GONE);
                    txtValidCustEmail.setVisibility(View.GONE);
                    txtValidSaleRate.setVisibility(View.GONE);
                    txtValidSaleDate.setVisibility(View.VISIBLE);
                }
                else {
                    txtValidCustName.setVisibility(View.GONE);
                    txtValidCustMobile.setVisibility(View.GONE);
                    txtValidCustEmail.setVisibility(View.GONE);
                    txtValidSaleRate.setVisibility(View.GONE);
                    txtValidSaleDate.setVisibility(View.GONE);
                    /*getProductStatus();*/
                    saleItem();
                }
            }
        });
    }

    /*private void getProductStatus() {

        addDialogProductStatus = new ProgressDialog(this);
        addDialogProductStatus.setCancelable(true);
        addDialogProductStatus.setMessage("Checking Product In Inventory...");
        addDialogProductStatus.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialogProductStatus.show();

        try{
            PreFromDateDataRequest fromDateDataRequest = new PreFromDateDataRequest();
            fromDateDataRequest.setBookingProdId(strProductIdSP);

            Call<BookingDataResponse> responceCall = ApiClient.getApiClient().bookItem(bookingItemRequest);

            responceCall.enqueue(new Callback<BookingDataResponse>() {
                @Override
                public void onResponse(Call<BookingDataResponse> call, retrofit2.Response<BookingDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        List<BookingDataItem> items =response.body().getBookingData();
                        Toast.makeText(BookingActivity.this, "Item Saled", Toast.LENGTH_SHORT).show();
                        onBackPressed();
                        addDialogProductStatus.dismiss();


                    } else {

                        Toast.makeText(BookingActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                        addDialogProductStatus.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<BookingDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialogProductStatus.dismiss();

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }



    }*/


    private void checkFromDateFromDataBase() {

        addDialogFromDate = new ProgressDialog(this);
        addDialogFromDate.setCancelable(true);
        addDialogFromDate.setMessage("Getting Start Date...");
        addDialogFromDate.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialogFromDate.show();

        try{
            PreFromDateDataRequest fromDateDataRequest = new PreFromDateDataRequest();
            fromDateDataRequest.setBookingProdId(strProductIdSP);
            fromDateDataRequest.setBookingFromDate(strFromDate);

            Toast.makeText(this, ""+strProductIdSP, Toast.LENGTH_SHORT).show();

            Call<PreFromDateDataResponse> responceCall = ApiClient.getApiClient().getPreFromDateData(fromDateDataRequest);

            responceCall.enqueue(new Callback<PreFromDateDataResponse>() {
                @Override
                public void onResponse(Call<PreFromDateDataResponse> call, retrofit2.Response<PreFromDateDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        //List<PreFromDateDataItem> items =response.body().getPreFromDateData();
                        addDialogFromDate.dismiss();

                        llToDate.setVisibility(View.VISIBLE);
                        /*llTotalRent.setVisibility(View.VISIBLE);
                        llAdvance.setVisibility(View.VISIBLE);
                        llPending.setVisibility(View.VISIBLE);
                        btnBookItem.setVisibility(View.VISIBLE);*/

                    } else {

                        llToDate.setVisibility(View.GONE);
                        llTotalRent.setVisibility(View.GONE);
                        llAdvance.setVisibility(View.GONE);
                        llPending.setVisibility(View.GONE);
                        btnBookItem.setVisibility(View.GONE);

                        addDialogFromDate.dismiss();

                        AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(BookingActivity.this);
                        alertDialog2.setTitle("Product is Booked...");
                        alertDialog2.setMessage("Product Book on Start Date.....!");

                        alertDialog2.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // Write your code here to execute after dialog
                                        //Toast.makeText(getApplicationContext(), "You clicked on YES", Toast.LENGTH_SHORT).show();
                                        dialog.cancel();
                                        addDialogFromDate.dismiss();
                                    }
                                });

                        alertDialog2.show();
                    }
                }

                @Override
                public void onFailure(Call<PreFromDateDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialogFromDate.dismiss();

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    private void checkSaleDateFromDataBase() {

        addDialogToDate = new ProgressDialog(this);
        addDialogToDate.setCancelable(true);
        addDialogToDate.setMessage("Getting Date...");
        addDialogToDate.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialogToDate.show();

        try{
            SaleDateDataRequest saleDateDataRequest = new SaleDateDataRequest();
            saleDateDataRequest.setBookingProdId(strProductIdSP);
            saleDateDataRequest.setSaleDate(strSaleDate);


            Call<SaleDateDataResponse> responceCall = ApiClient.getApiClient().getSaleDateData(saleDateDataRequest);

            responceCall.enqueue(new Callback<SaleDateDataResponse>() {
                @Override
                public void onResponse(Call<SaleDateDataResponse> call, retrofit2.Response<SaleDateDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        btnSaleItem.setVisibility(View.VISIBLE);
                        addDialogToDate.dismiss();


                        //Toast.makeText(RentListActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();


                    } else {

                        addDialogToDate.dismiss();
                        btnSaleItem.setVisibility(View.GONE);

                        AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(BookingActivity.this);
                        alertDialog2.setTitle("Product is Booked...");
                        alertDialog2.setMessage("Product Book on This Date.....!");

                        alertDialog2.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();

                                        addDialogToDate.dismiss();
                                    }
                                });

                        alertDialog2.show();


                    }
                }

                @Override
                public void onFailure(Call<SaleDateDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialogToDate.dismiss();

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    private void checkToDateFromDataBase() {

        addDialogToDate = new ProgressDialog(this);
        addDialogToDate.setCancelable(true);
        addDialogToDate.setMessage("Getting End Date...");
        addDialogToDate.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialogToDate.show();

        try{
            PreToDateDataRequest toDateDataRequest = new PreToDateDataRequest();
            toDateDataRequest.setBookingProdId(strProductIdSP);
            toDateDataRequest.setBookingToDate(strToDate);


            Call<PreToDateDataResponse> responceCall = ApiClient.getApiClient().getPreToDateData(toDateDataRequest);

            responceCall.enqueue(new Callback<PreToDateDataResponse>() {
                @Override
                public void onResponse(Call<PreToDateDataResponse> call, retrofit2.Response<PreToDateDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        addDialogToDate.dismiss();

                        //List<PreToDateDataItem> items =response.body().getPreToDateData();

                        llTotalRent.setVisibility(View.VISIBLE);
                        llAdvance.setVisibility(View.VISIBLE);
                        llPending.setVisibility(View.VISIBLE);
                        btnBookItem.setVisibility(View.VISIBLE);
                        //Toast.makeText(RentListActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();


                    } else {

                        addDialogToDate.dismiss();


                        AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(BookingActivity.this);
                        alertDialog2.setTitle("Product is Booked...");
                        alertDialog2.setMessage("Product Book on End Date.....!");

                        alertDialog2.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                        llTotalRent.setVisibility(View.GONE);
                                        llAdvance.setVisibility(View.GONE);
                                        llPending.setVisibility(View.GONE);
                                        btnBookItem.setVisibility(View.GONE);
                                        addDialogToDate.dismiss();
                                    }
                                });

                        alertDialog2.show();


                    }
                }

                @Override
                public void onFailure(Call<PreToDateDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialogToDate.dismiss();

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    private void getCustomers() {

        custIDList.clear();
        custNameList.clear();
        custMobileList.clear();
        custEmailList.clear();

        addDialog = new ProgressDialog(this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Getting List...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        try{
            CustomerDataRequest customerRequest = new CustomerDataRequest();


            Call<CustomerDataResponse> responceCall = ApiClient.getApiClient().getCustomer(customerRequest);

            responceCall.enqueue(new Callback<CustomerDataResponse>() {
                @Override
                public void onResponse(Call<CustomerDataResponse> call, retrofit2.Response<CustomerDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        List<CustomerDataItem> items =response.body().getCustomerData();
                        for (int i=0; i<items.size(); i++){
                            custIDList.add(items.get(i).getCustomerId());
                            custNameList.add(items.get(i).getCustomerName());
                            custMobileList.add(items.get(i).getCustomerMobile());
                            custEmailList.add(items.get(i).getCustomerEmail());
                        }
                        addDialog.dismiss();


                    } else {

                        //Toast.makeText(RentListActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                        addDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<CustomerDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialog.dismiss();

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    private void bookItem() {

        addDialog = new ProgressDialog(this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Booking Item...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

         finalInStock = "Yes";
        finalQuantity = "1";
        if(strAdvance.equals("")){
            strAdvance = "0.0";
            strPending = txtRentPrice.getText().toString().trim();
        }
        if(strBookingType.equals("Monthly")){
            dayCount = 31f;
        }

        try{
            BookingDataRequest bookingItemRequest = new BookingDataRequest();

            bookingItemRequest.setCustomerName(strCustomerName);
            bookingItemRequest.setCustomerMobile(strCustomerMobile);
            bookingItemRequest.setCustomerEmail(strCustomerEmail);
            bookingItemRequest.setBookingProdId(strProductIdSP);
            bookingItemRequest.setBookingFromDate(strFromDate);
            bookingItemRequest.setBookingToDate(strToDate);
            bookingItemRequest.setTotalRent(String.valueOf(totalRent));
            bookingItemRequest.setDaysCount(String.valueOf(dayCount));
            bookingItemRequest.setProductQuantity(finalQuantity);
            bookingItemRequest.setProductInStock(finalInStock);
            bookingItemRequest.setBookingType(strBookingType);
            bookingItemRequest.setMonthRent(strMonthRent);
            bookingItemRequest.setDailyRent(strDayRent);
            bookingItemRequest.setRentAdvance(strAdvance);
            bookingItemRequest.setRentPending(strPending);
            bookingItemRequest.setBookingMonth(strFromMonthOfYear);
            bookingItemRequest.setBookingYear(strFromYear);
            bookingItemRequest.setBookingStatus(strBookingSatus);
            bookingItemRequest.setSaleRate(strSaleRate);
            bookingItemRequest.setSaleDate(strSaleDate);
            bookingItemRequest.setProductStatus(strProductStatus);

            Call<BookingDataResponse> responceCall = ApiClient.getApiClient().bookItem(bookingItemRequest);

            responceCall.enqueue(new Callback<BookingDataResponse>() {
                @Override
                public void onResponse(Call<BookingDataResponse> call, retrofit2.Response<BookingDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        List<BookingDataItem> items =response.body().getBookingData();
                        Toast.makeText(BookingActivity.this, "Item Booked", Toast.LENGTH_SHORT).show();
                        onBackPressed();
                        addDialog.dismiss();


                    } else {

                        Toast.makeText(BookingActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                        addDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<BookingDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialog.dismiss();

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    private void saleItem() {

        addDialog = new ProgressDialog(this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Saling Item...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();
        strBookingSatus = "5";

        finalQuantity = "1";
        finalInStock = "No";

        if(strAdvance.equals("")){
            strAdvance = "0.0";
            strPending = txtRentPrice.getText().toString().trim();
        }
        if(strBookingType.equals("Monthly")){
            dayCount = 31f;
        }

        try{
            BookingDataRequest bookingItemRequest = new BookingDataRequest();

            bookingItemRequest.setCustomerName(strCustomerName);
            bookingItemRequest.setCustomerMobile(strCustomerMobile);
            bookingItemRequest.setCustomerEmail(strCustomerEmail);
            bookingItemRequest.setBookingProdId(strProductIdSP);
            bookingItemRequest.setBookingFromDate(strFromDate);
            bookingItemRequest.setBookingToDate(strToDate);
            bookingItemRequest.setTotalRent(String.valueOf(totalRent));
            bookingItemRequest.setDaysCount(String.valueOf(dayCount));
            bookingItemRequest.setProductQuantity(finalQuantity);
            bookingItemRequest.setProductInStock(finalInStock);
            bookingItemRequest.setBookingType(strBookingType);
            bookingItemRequest.setMonthRent(strMonthRent);
            bookingItemRequest.setDailyRent(strDayRent);
            bookingItemRequest.setRentAdvance(strAdvance);
            bookingItemRequest.setRentPending(strPending);
            bookingItemRequest.setBookingMonth(strFromMonthOfYear);
            bookingItemRequest.setBookingYear(strFromYear);
            bookingItemRequest.setBookingStatus(strBookingSatus);
            bookingItemRequest.setSaleRate(strSaleRate);
            bookingItemRequest.setSaleDate(strSaleDate);
            bookingItemRequest.setProductStatus(strProductStatus);

            Call<BookingDataResponse> responceCall = ApiClient.getApiClient().bookItem(bookingItemRequest);

            responceCall.enqueue(new Callback<BookingDataResponse>() {
                @Override
                public void onResponse(Call<BookingDataResponse> call, retrofit2.Response<BookingDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        List<BookingDataItem> items =response.body().getBookingData();
                        Toast.makeText(BookingActivity.this, "Item Saled", Toast.LENGTH_SHORT).show();
                        onBackPressed();
                        addDialog.dismiss();


                    } else {

                        Toast.makeText(BookingActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                        addDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<BookingDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialog.dismiss();

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }


    }

    private boolean checkEmail(String email) {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }

    @Override
    public void onBackPressed() {
        if(strIntentTo.equals("ManualBooking")){
            Intent intent = new Intent(this, AllProductsActivity.class);
            startActivity(intent);
            overridePendingTransition(0,0);
            finish();
        }
        else{
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            overridePendingTransition(0,0);
            finish();
        }

    }
}
