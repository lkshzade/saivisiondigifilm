package com.microtron.cvinventorymanagmentapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.microtron.cvinventorymanagmentapp.Helper.ApiClient;
import com.microtron.cvinventorymanagmentapp.Helper.AppConstants;
import com.microtron.cvinventorymanagmentapp.Helper.Sp;
import com.microtron.cvinventorymanagmentapp.NetworkModel.GetScanProduct.GetScanProductDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.GetScanProduct.GetScanProductDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.GetScanProduct.GetScanProductDataResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class BookItemActivity extends AppCompatActivity {

    private Button btnBook, btnManualBooking, btnSale;
    private TextView txtCompanyName, txtCategoryName, txtSubCategoryName, txtProductName, txtProductDesc, txtProductprice, txtProducInStock, txtProductStockQty;
    private ImageView btnScanner;
    private CardView cardScanner;
    private LinearLayout llShowProductMain;
    private String strName, strMobile, strEmail, strPass, strAddress, strShopName, strShoptype, strImgURL, strAdminID;
    private ProgressDialog addDialog;
    private ImageView imgback;
    private String qrScannedProductID;
    private String strCompanyName, strCategoryName, strSubCategoryName, strProductName, strProductDesc, strProductPrice, strProductInStock, strProductStockQty;
    private String strProductInStockSp, strIntentTo;
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_item);

        Sp.saveToPreferences(BookItemActivity.this, AppConstants.INTENT_TO, "BookItem");

        btnScanner = findViewById(R.id.btnScanner);
        cardScanner = findViewById(R.id.cardScanner);
        llShowProductMain = findViewById(R.id.llShowProductMain);

        txtCompanyName = findViewById(R.id.txtSetCompanyName);
        txtCategoryName = findViewById(R.id.txtSetCategory);
        txtSubCategoryName = findViewById(R.id.txtSetSubCategory);
        txtProductName = findViewById(R.id.txtSetProductName);
        txtProductDesc = findViewById(R.id.txtSetProductDescription);
        txtProductprice = findViewById(R.id.txtSetProductPrice);
        txtProducInStock = findViewById(R.id.txtSetInStock);
        txtProductStockQty = findViewById(R.id.txtSetProductStockQuantity);
        btnBook = findViewById(R.id.btnBook);
        btnSale = findViewById(R.id.btnSale);
        btnManualBooking = findViewById(R.id.btnManualBooking);
        imgback = findViewById(R.id.imgBack);

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        llShowProductMain.setVisibility(View.GONE);

        btnScanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                IntentIntegrator integrator = new IntentIntegrator(BookItemActivity.this);
                integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
                integrator.setPrompt("Scanning.....");
                integrator.setCameraId(0);
                integrator.setOrientationLocked(true);
                integrator.setBeepEnabled(false);
                integrator.setBarcodeImageEnabled(false);
                integrator.setCaptureActivity(CaptureActivityPortrait.class);
                integrator.initiateScan();

            }
        });

        btnManualBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Sp.saveToPreferences(BookItemActivity.this, AppConstants.INTENT_TO, "ManualBooking");
                Intent intent = new Intent(BookItemActivity.this, AllProductsActivity.class);
                startActivity(intent);
                overridePendingTransition(0,0);
                finish();
            }
        });

        btnBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strProductInStockSp = Sp.readFromPreferences(BookItemActivity.this, AppConstants.PRODUCT_INSTOCK,"");

                if(strProductInStockSp.equals("No")){

                    Toast.makeText(BookItemActivity.this, "Product Not in Stock", Toast.LENGTH_SHORT).show();
                }
                else{
                    Sp.saveToPreferences(BookItemActivity.this, AppConstants.INTENT_TO, "BookProduct");
                    Intent intent = new Intent(BookItemActivity.this,BookingActivity.class);
                    startActivity(intent);
                    overridePendingTransition(0,0);
                    finish();
                }
            }
        });

        btnSale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strProductInStockSp = Sp.readFromPreferences(BookItemActivity.this, AppConstants.PRODUCT_INSTOCK,"");

                if(strProductInStockSp.equals("No")){

                    Toast.makeText(BookItemActivity.this, "Product Not in Stock", Toast.LENGTH_SHORT).show();
                }
                else{
                    Sp.saveToPreferences(BookItemActivity.this, AppConstants.INTENT_TO, "SaleProduct");
                    Intent intent = new Intent(BookItemActivity.this,BookingActivity.class);
                    startActivity(intent);
                    overridePendingTransition(0,0);
                    finish();
                }
            }
        });

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
                Log.e("Scan*******", "Cancelled scan");

            } else {
                Log.e("Scan", "Scanned");

                //tv_qr_readTxt.setText(result.getContents());
                if (result.getContents().equals("")){
                    Sp.saveToPreferences(BookItemActivity.this, AppConstants.INTENT_TO, "Scanner");
                    cardScanner.setVisibility(View.GONE);
                    onBackPressed();
                }else {
                    Sp.saveToPreferences(BookItemActivity.this, AppConstants.INTENT_TO, "Scanner");
                    cardScanner.setVisibility(View.GONE);
                    btnManualBooking.setVisibility(View.GONE);
                    qrScannedProductID = result.getContents();
                    getScanProduct();
                }

            }
        } else {

            super.onActivityResult(requestCode, resultCode, data);

        }
    }

    private void getScanProduct() {

        addDialog = new ProgressDialog(this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Please Wait...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        try{
            GetScanProductDataRequest getScanProductRequest = new GetScanProductDataRequest();
            getScanProductRequest.setProductId(qrScannedProductID);

            Call<GetScanProductDataResponse> responceCall = ApiClient.getApiClient().getScanProduct(getScanProductRequest);

            responceCall.enqueue(new Callback<GetScanProductDataResponse>() {
                @Override
                public void onResponse(Call<GetScanProductDataResponse> call, retrofit2.Response<GetScanProductDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        addDialog.dismiss();
                        List<GetScanProductDataItem> items =response.body().getGetScanProductData();

                        llShowProductMain.setVisibility(View.VISIBLE);

                        strCompanyName = items.get(0).getCompanyName();
                        strCategoryName = items.get(0).getCatName();
                        strSubCategoryName = items.get(0).getSubCatName();
                        strProductName = items.get(0).getProductName();
                        strProductDesc = items.get(0).getProductDetails();
                        strProductInStock = items.get(0).getProductInStock();
                        strProductStockQty = items.get(0).getProductQuantity();

                        Sp.saveToPreferences(BookItemActivity.this, AppConstants.PRODUCT_COMP_ID, ""+items.get(0).getProductCompnyId());
                        Sp.saveToPreferences(BookItemActivity.this, AppConstants.PRODUCT_CAT_ID, ""+items.get(0).getProductCategoryId());
                        Sp.saveToPreferences(BookItemActivity.this, AppConstants.PRODUCT_SUBCAT_ID, ""+items.get(0).getProductSubcategoryId());
                        Sp.saveToPreferences(BookItemActivity.this, AppConstants.PRODUCT_ID, ""+items.get(0).getProductId());
                        Sp.saveToPreferences(BookItemActivity.this, AppConstants.PRODUCT_COMP_NAME, ""+items.get(0).getCompanyName());
                        Sp.saveToPreferences(BookItemActivity.this, AppConstants.PRODUCT_CAT_NAME, ""+items.get(0).getCatName());
                        Sp.saveToPreferences(BookItemActivity.this, AppConstants.PRODUCT_SUBCAT_NAME, ""+items.get(0).getSubCatName());
                        Sp.saveToPreferences(BookItemActivity.this, AppConstants.PRODUCT_NAME, ""+items.get(0).getProductName());
                        Sp.saveToPreferences(BookItemActivity.this, AppConstants.PRODUCT_DESCRIPTION, ""+items.get(0).getProductDetails());
                        Sp.saveToPreferences(BookItemActivity.this, AppConstants.PRODUCT_INSTOCK, ""+items.get(0).getProductInStock());
                        Sp.saveToPreferences(BookItemActivity.this, AppConstants.PRODUCT_STOCKQTY, ""+items.get(0).getProductQuantity());


                        txtCompanyName.setText(strCompanyName);
                        txtCategoryName.setText(strCategoryName);
                        txtSubCategoryName.setText(strSubCategoryName);
                        txtProductName.setText(strProductName);
                        txtProductDesc.setText(strProductDesc);
                        txtProductprice.setText(strProductPrice);
                        txtProducInStock.setText(strProductInStock);
                        txtProductStockQty.setText(strProductStockQty);


                        //Toast.makeText(MainActivity.this, "Company Name = "+strCompanyName+" Category Name = "+strCategoryName+" SubCategory Name = "+strSubCategoryName+" Product Name = "+strProductName, Toast.LENGTH_SHORT).show();


                    } else {

                        Toast.makeText(BookItemActivity.this, "No Product Available on this QR COde", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(BookItemActivity.this , BookItemActivity.class);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        finish();
                        addDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<GetScanProductDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialog.dismiss();

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        strIntentTo = Sp.readFromPreferences(this, AppConstants.INTENT_TO,"");
        if(strIntentTo.equals("Scanner"))
        {
            Intent intent = new Intent(this, BookItemActivity.class);
            startActivity(intent);
            overridePendingTransition(0,0);
            finish();
        }
        if(strIntentTo.equals("ManualBooking"))
        {
            Intent intent = new Intent(this, BookItemActivity.class);
            startActivity(intent);
            overridePendingTransition(0,0);
            finish();
        }
        else{
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            overridePendingTransition(0,0);
            finish();
        }

    }

}
