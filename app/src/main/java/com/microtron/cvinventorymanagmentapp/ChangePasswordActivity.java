package com.microtron.cvinventorymanagmentapp;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.microtron.cvinventorymanagmentapp.Helper.AppConstants;
import com.microtron.cvinventorymanagmentapp.Helper.ConfigFile;
import com.microtron.cvinventorymanagmentapp.Helper.Sp;
import com.microtron.cvinventorymanagmentapp.LogInSignUpSplash.SendOTPService;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ChangePasswordActivity extends AppCompatActivity {


    TextView txtUserName, txtCPUserName;
    EditText edtMobile;
    EditText edtOTP, edtNewPass, edtCnfNewPass;
    Button btnSendOTP, btnSetOTP, btnSetPass;
    CardView linear1, linear2, linear3;
    String  otp, otp_generated, strNewPass, strCnfPass;
    final static int REQUESTCODE_PERMISSION_SMS = 301;
    android.app.ProgressDialog ProgressDialog;
    String strMsg = "OTP SENT";
    ProgressDialog addDialog;
    ImageView imgBack;
    private String strAdminID, strName, strMobile, strPass, strEmail, strAddress, strShopName,strShoptype, strImgURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        strAdminID = Sp.readFromPreferences(this, AppConstants.ADMIN_ID,"");
        strName = Sp.readFromPreferences(this, AppConstants.ADMIN_NAME,"");
        strMobile = Sp.readFromPreferences(this, AppConstants.ADMIN_MOBILE,"");
        strEmail = Sp.readFromPreferences(this, AppConstants.ADMIN_EMAIL,"");
        strPass = Sp.readFromPreferences(this, AppConstants.ADMIN_PASSWORD,"");
        strAddress = Sp.readFromPreferences(this, AppConstants.ADMIN_ADDRESS,"");
        strShopName = Sp.readFromPreferences(this, AppConstants .ADMIN_SHOPNAME,"");
        strShoptype = Sp.readFromPreferences(this, AppConstants.ADMIN_SHOPTYPE,"");
        strImgURL = Sp.readFromPreferences(this, AppConstants.ADMIN_IMGURL,"");


        edtMobile = findViewById(R.id.edtMobile);
        txtCPUserName = findViewById(R.id.txtCPUserName);
        txtUserName = findViewById(R.id.txtUserName);
        edtOTP = findViewById(R.id.edtOTP);
        edtNewPass = findViewById(R.id.edtNewPassword);
        edtCnfNewPass = findViewById(R.id.edtcnfNewPassword);
        btnSendOTP = findViewById(R.id.btnSendOtp);
        btnSetOTP = findViewById(R.id.btnSetOtp);
        btnSetPass = findViewById(R.id.btnSetPassword);
        linear1 = findViewById(R.id.linear1);
        linear2 = findViewById(R.id.linear2);
        linear3 = findViewById(R.id.linear3);

        imgBack = findViewById(R.id.imgBack);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        if (Build.VERSION.SDK_INT >= 28) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS, Manifest.permission.READ_PHONE_STATE}, REQUESTCODE_PERMISSION_SMS);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, REQUESTCODE_PERMISSION_SMS);
        }

        edtMobile.setEnabled(false);
        edtMobile.setText(strMobile);
        txtCPUserName.setText(strName);
        linear2.setVisibility(View.GONE);
        linear3.setVisibility(View.GONE);

        btnSendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strMobile = edtMobile.getText().toString().trim();
                if (!strMobile.equals("")) {
                    Random r = new Random();
                    int randomNumber = r.nextInt(10000);
                    otp = "" + randomNumber;

                    SharedPreferences spotp = getSharedPreferences("myotp", MODE_PRIVATE);
                    SharedPreferences.Editor editor = spotp.edit();
                    editor.putString("keycontact", strMobile);
                    editor.putString("keyotp", otp);
                    editor.apply();

                    /*ProgressDialog addDialog;*/

                    addDialog = new ProgressDialog(ChangePasswordActivity.this);
                    addDialog.setCancelable(true);
                    addDialog.setMessage("Sending OTP...");
                    addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    addDialog.show();

                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(ConfigFile.domain)
                            .addConverterFactory(ScalarsConverterFactory.create())
                            .build();

                    final SendOTPService service = retrofit.create(SendOTPService.class);
                    retrofit2.Call<String> call = service.changePass(otp, strMobile);
                    call.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(retrofit2.Call<String> call, retrofit2.Response<String> response) {

                            new android.app.AlertDialog.Builder(ChangePasswordActivity.this)
                                    .setTitle("OTP ")
                                    .setMessage("OTP Sent to your Registered Mobile Number")
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {


                                            linear1.setVisibility(View.GONE);
                                            linear2.setVisibility(View.VISIBLE);
                                            linear3.setVisibility(View.GONE);

                                        }
                                    })
                                    .show();
                            addDialog.dismiss();

                        }

                        @Override
                        public void onFailure(retrofit2.Call<String> call, Throwable t) {
                            //Toast.makeText(ChangePasswordActivity.this, t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                            addDialog.dismiss();
                        }
                    });

                }
            }
        });

        btnSetOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addDialog = new ProgressDialog(ChangePasswordActivity.this);
                addDialog.setCancelable(true);
                addDialog.setMessage("Checking OTP...");
                addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                addDialog.show();

                if (edtOTP.getText().toString().equals(otp)) {
                    Toast.makeText(ChangePasswordActivity.this, "OTP Verified Successfully !", Toast.LENGTH_SHORT).show();
                    linear1.setVisibility(View.GONE);
                    linear2.setVisibility(View.GONE);
                    linear3.setVisibility(View.VISIBLE);
                    addDialog.dismiss();
                } else {
                    edtOTP.setError("Enter Correct OTP");
                    addDialog.dismiss();
                }
            }
        });
        btnSetPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strNewPass = edtNewPass.getText().toString().trim();
                strCnfPass = edtCnfNewPass.getText().toString().trim();
                if(strNewPass.equals("")){
                    edtNewPass.setError("Please Enter New Password");
                }else if(strCnfPass.equals("")){
                    edtCnfNewPass.setError("Please Enter Confirm Password");
                }else if(!strNewPass.equals(strCnfPass)){
                    edtCnfNewPass.setText("");
                    edtCnfNewPass.setError("Please Enter Same Password");
                }
                else{
                    changePass();

                }

            }
        });
    }

    private void changePass() {

        addDialog = new ProgressDialog(ChangePasswordActivity.this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Changing Password...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConfigFile.domain + ConfigFile.domaindir + ConfigFile.changePassUrl,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {


                        if (response.trim().equals("fail")) {
                            addDialog.dismiss();

                            new AlertDialog.Builder(ChangePasswordActivity.this)
                                    .setTitle("Failed")
                                    .setMessage("Please Try Again")
                                    .setPositiveButton(android.R.string.yes, null)
                                    .create().show();


                        } else if (response.trim().equals("exist")) {

                            addDialog.dismiss();

                            Toast.makeText(ChangePasswordActivity.this, "Number Is Already Exist", Toast.LENGTH_SHORT).show();


                        } else if (response.trim().equals("success")) {

                            addDialog.dismiss();

                            //Toast.makeText(ChangePasswordActivity.this, "Success", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(ChangePasswordActivity.this, MainActivity.class);
                            startActivity(intent);
                        } else {

                            addDialog.dismiss();

                            //Toast.makeText(ChangePasswordActivity.this, "Connection Failed", Toast.LENGTH_SHORT).show();
                        }
                    }


                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                addDialog.dismiss();

                //Toast.makeText(ChangePasswordActivity.this, "Internet connection Problem", Toast.LENGTH_SHORT).show();

            }

        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                HashMap<String, String> map = new HashMap<>();

                map.put("pass", strCnfPass);
                map.put("id", strAdminID);
                return map;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(ChangePasswordActivity.this);
        requestQueue.add(stringRequest);
    }


    public void onRequestPermissionsResult(int requestCode, @NonNull String[]
            permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUESTCODE_PERMISSION_SMS) {
            Log.d("TAG", "My permission request sms received successfully");
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ChangePasswordActivity.this, MainActivity.class);
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
    }
}