package com.microtron.cvinventorymanagmentapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.microtron.cvinventorymanagmentapp.Adapters.CalendarAdapter;
import com.microtron.cvinventorymanagmentapp.Adapters.CalendarEventListAdapter;
import com.microtron.cvinventorymanagmentapp.Adapters.RecyclerItemClickListenerCalendarEventsList;
import com.microtron.cvinventorymanagmentapp.Helper.ApiClient;
import com.microtron.cvinventorymanagmentapp.Helper.CalendarCollection;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataResponse;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class CalendarActivity extends AppCompatActivity {

    public GregorianCalendar cal_month, cal_month_copy;
    private CalendarAdapter calAdapter;
    private TextView tv_month;
    private GridView gridview;
    private ImageView imgBack;
    private String strMonth, strYear;

    private CalendarEventListAdapter calendarEventListAdapter;
    private RecyclerView recyclerCalenderEventsList;
    List<BookedItemDataItem> eventItems;


    private ProgressDialog addDialog, addDialog1;
    private ArrayList<String> fromDate = new ArrayList<String>();
    private ArrayList<String> toDate = new ArrayList<String>();
    private ArrayList<String> custName = new ArrayList<String>();
    private ArrayList<String> rent = new ArrayList<String>();
    private ArrayList<String> productName = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
        imgBack = findViewById(R.id.imgBack);

        recyclerCalenderEventsList = findViewById(R.id.recyclerCalenderEventsList);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerCalenderEventsList.setLayoutAnimation(animation);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerCalenderEventsList.setLayoutManager(layoutManager);

        Calendar c = Calendar.getInstance();
        int month = c.get(Calendar.MONTH)+1;
        int year = c.get(Calendar.YEAR);
        strMonth = String.valueOf(month);
        if(strMonth.length()<2){
            strMonth = "0"+strMonth;
        }
        strYear = String.valueOf(year);

        getRentDateList();

        cal_month = (GregorianCalendar) GregorianCalendar.getInstance();
        cal_month_copy = (GregorianCalendar) cal_month.clone();

        tv_month = (TextView) findViewById(R.id.tv_month);
        tv_month.setText(android.text.format.DateFormat.format("MMMM yyyy", cal_month));

        ImageButton previous = (ImageButton) findViewById(R.id.ib_prev);
        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cal_month.get(GregorianCalendar.MONTH) == 4&&cal_month.get(GregorianCalendar.YEAR)==2017) {
                    //cal_month.set((cal_month.get(GregorianCalendar.YEAR) - 1), cal_month.getActualMaximum(GregorianCalendar.MONTH), 1);
                    Toast.makeText(CalendarActivity.this, "Event Detail is available for current session only.", Toast.LENGTH_SHORT).show();
                }
                else {
                    //setPreviousMonth();
                    try{
                        setPreviousMonth();
                        refreshCalendar();
                    }
                    catch (Exception e){
                        AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(CalendarActivity.this);
                        alertDialog2.setTitle("No Entry...");
                        alertDialog2.setMessage("No Rented Item This Year.....!");

                        alertDialog2.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // Write your code here to execute after dialog
                                        //Toast.makeText(getApplicationContext(), "You clicked on YES", Toast.LENGTH_SHORT).show();
                                        dialog.cancel();
                                    }
                                });

                        alertDialog2.show();
                    }
                }


            }
        });
        ImageButton next = (ImageButton) findViewById(R.id.Ib_next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cal_month.get(GregorianCalendar.MONTH) == 5&&cal_month.get(GregorianCalendar.YEAR)==2018) {
                    Toast.makeText(CalendarActivity.this, "Event Detail is available for current session only.", Toast.LENGTH_SHORT).show();
                }
                else {
                    //setNextMonth();
                    try {
                        setNextMonth();
                        refreshCalendar();
                    }
                    catch (Exception e){

                        AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(CalendarActivity.this);
                        alertDialog2.setTitle("No Entry...");
                        alertDialog2.setMessage("No Rented Item This Year.....!");

                        alertDialog2.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // Write your code here to execute after dialog
                                        //Toast.makeText(getApplicationContext(), "You clicked on YES", Toast.LENGTH_SHORT).show();
                                        dialog.cancel();
                                    }
                                });

                        alertDialog2.show();
                        //Toast.makeText(CalendarActivity.this, "No Entries This Year...!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        gridview = findViewById(R.id.gv_calendar);

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                String selectedGridDate = CalendarAdapter.day_string.get(position);
                ((CalendarAdapter) parent.getAdapter()).getPositionList(selectedGridDate, CalendarActivity.this);
            }

        });
    }

    private void getRentDateList() {

        addDialog1 = new ProgressDialog(this);
        addDialog1.setCancelable(true);
        addDialog1.setMessage("Getting List...");
        addDialog1.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog1.show();

        try{
            BookedItemDataRequest getRentListRequest = new BookedItemDataRequest();


            Call<BookedItemDataResponse> responceCall = ApiClient.getApiClient().getRentList(getRentListRequest);

            responceCall.enqueue(new Callback<BookedItemDataResponse>() {
                @Override
                public void onResponse(Call<BookedItemDataResponse> call, retrofit2.Response<BookedItemDataResponse> response) {

                    if (response.body().getSuccess().equals("1")) {

                        List<BookedItemDataItem> items =response.body().getBookedItemData();
                        for(int i = 0; i < items.size(); i++){

                            fromDate.add(items.get(i).getBookingFromDate());
                            toDate.add(items.get(i).getBookingToDate());
                            custName.add(items.get(i).getCustomerName());
                            rent.add(items.get(i).getTotalRent());
                            productName.add(items.get(i).getProductName());
                        }
                        getRentDateListMonth();
                        addDialog1.dismiss();

                        CalendarCollection.date_collection_arr=new ArrayList<CalendarCollection>();

                        for(int j = 0; j < fromDate.size(); j++){

                            CalendarCollection.date_collection_arr.add( new CalendarCollection(""+fromDate.get(j) ,""+toDate.get(j),""+custName.get(j),""+rent.get(j), ""+productName.get(j)));

                        }

                        calAdapter = new CalendarAdapter(CalendarActivity.this, cal_month, CalendarCollection.date_collection_arr);
                        gridview.setAdapter(calAdapter);


                    } else {
                        addDialog1.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<BookedItemDataResponse> call, Throwable t) {

                    //Toast.makeText(CalendarActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialog1.dismiss();

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    protected void setNextMonth() {
        if (cal_month.get(GregorianCalendar.MONTH) == cal_month.getActualMaximum(GregorianCalendar.MONTH)) {
            cal_month.set((cal_month.get(GregorianCalendar.YEAR) + 1), cal_month.getActualMinimum(GregorianCalendar.MONTH), 1);
        } else {
            cal_month.set(GregorianCalendar.MONTH,
                    cal_month.get(GregorianCalendar.MONTH) + 1);
        }
    }

    protected void setPreviousMonth() {
        if (cal_month.get(GregorianCalendar.MONTH) == cal_month.getActualMinimum(GregorianCalendar.MONTH)) {
            cal_month.set((cal_month.get(GregorianCalendar.YEAR) - 1), cal_month.getActualMaximum(GregorianCalendar.MONTH), 1);
        } else {
            cal_month.set(GregorianCalendar.MONTH, cal_month.get(GregorianCalendar.MONTH) - 1);
            //Toast.makeText(this, "Month Else     "+android.text.format.DateFormat.format("MMMM", cal_month), Toast.LENGTH_SHORT).show();
        }
    }

    public void refreshCalendar() {
        calAdapter.refreshDays();
        calAdapter.notifyDataSetChanged();
        tv_month.setText(android.text.format.DateFormat.format("MMMM yyyy", cal_month));
        strMonth = String.valueOf(android.text.format.DateFormat.format("MM", cal_month));
        strYear = String.valueOf(android.text.format.DateFormat.format("yyyy", cal_month));
        getRentDateListMonth();
        //Toast.makeText(this, "Month "+strMonth+"\nYear "+strYear, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(CalendarActivity.this, MainActivity.class);
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
    }


    private void getRentDateListMonth() {

            addDialog = new ProgressDialog(this);
            addDialog.setCancelable(true);
            addDialog.setMessage("Getting Events...");
            addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            addDialog.show();

            try{
                BookedItemDataRequest getRentListMonthRequest = new BookedItemDataRequest();
                getRentListMonthRequest.setBookingMonth(strMonth);
                getRentListMonthRequest.setBookingYear(strYear);

                Call<BookedItemDataResponse> responceCall = ApiClient.getApiClient().getRentMonthList(getRentListMonthRequest);

                responceCall.enqueue(new Callback<BookedItemDataResponse>() {
                    @Override
                    public void onResponse(Call<BookedItemDataResponse> call, retrofit2.Response<BookedItemDataResponse> response) {

                        if (response.body().getSuccess().equals("1")) {

                            eventItems =response.body().getBookedItemData();
                            //Toast.makeText(CalendarActivity.this, "Success", Toast.LENGTH_SHORT).show();
                            recyclerCalenderEventsList.setVisibility(View.VISIBLE);
                            calendarEventListAdapter = new CalendarEventListAdapter(CalendarActivity.this, eventItems, recyclerItemClickListener);
                            recyclerCalenderEventsList.setAdapter(calendarEventListAdapter);
                            calendarEventListAdapter.notifyDataSetChanged();

                            addDialog.dismiss();


                        } else {
                            recyclerCalenderEventsList.setVisibility(View.GONE);
                            //Toast.makeText(CalendarActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                            addDialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<BookedItemDataResponse> call, Throwable t) {

                        addDialog.dismiss();

                    }
                });
            }
            catch (Exception e){
                e.printStackTrace();
            }

        }

    private RecyclerItemClickListenerCalendarEventsList recyclerItemClickListener = new RecyclerItemClickListenerCalendarEventsList() {
        @Override
        public void onItemClick(final BookedItemDataItem notiItem) {
        }
    };

}
