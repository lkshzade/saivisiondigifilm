package com.microtron.cvinventorymanagmentapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;

import com.microtron.cvinventorymanagmentapp.Adapters.CustomerListAdapter;
import com.microtron.cvinventorymanagmentapp.Adapters.OutSideProductListAdapter;
import com.microtron.cvinventorymanagmentapp.Adapters.RecyclerItemClickListenerCustomerList;
import com.microtron.cvinventorymanagmentapp.Adapters.RecyclerItemClickListenerOutSideProductList;
import com.microtron.cvinventorymanagmentapp.Helper.ApiClient;
import com.microtron.cvinventorymanagmentapp.NetworkModel.Customer.CustomerDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.Customer.CustomerDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.Customer.CustomerDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.OutSideInventoryBooking.OutSideBookingDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.OutSideInventoryBooking.OutSideBookingDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.OutSideInventoryBooking.OutSideBookingDataResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class ViewCustomerActivity extends AppCompatActivity {

    private ImageView imgBack, imgNoProduct;
    private RecyclerView recyclerCustomerList;
    private ProgressDialog addDialog;
    private CustomerListAdapter customerListAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_customer);

        imgBack = findViewById(R.id.imgBack);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        imgNoProduct = findViewById(R.id.imgNoProduct);
        imgNoProduct.setVisibility(View.GONE);

        recyclerCustomerList = findViewById(R.id.recyclerCustomerList);

        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerCustomerList.setLayoutAnimation(animation);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerCustomerList.setLayoutManager(layoutManager);

        getCustomerList();

    }

    private void getCustomerList() {

        addDialog = new ProgressDialog(this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Getting List...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        try{
            CustomerDataRequest customerRequest = new CustomerDataRequest();


            Call<CustomerDataResponse> responceCall = ApiClient.getApiClient().getCustomer(customerRequest);

            responceCall.enqueue(new Callback<CustomerDataResponse>() {
                @Override
                public void onResponse(Call<CustomerDataResponse> call, retrofit2.Response<CustomerDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        List<CustomerDataItem> items =response.body().getCustomerData();
                        imgNoProduct.setVisibility(View.GONE);

                        customerListAdapter = new CustomerListAdapter(ViewCustomerActivity.this, items, recyclerItemClickListener);
                        recyclerCustomerList.setAdapter(customerListAdapter);
                        customerListAdapter.notifyDataSetChanged();

                        addDialog.dismiss();


                    } else {

                        //Toast.makeText(RentListActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                        imgNoProduct.setVisibility(View.VISIBLE);
                        addDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<CustomerDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialog.dismiss();

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    private RecyclerItemClickListenerCustomerList recyclerItemClickListener = new RecyclerItemClickListenerCustomerList() {
        @Override
        public void onItemClick(final CustomerDataItem notiItem) {
        }
    };

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, ViewAccountsActivity.class);
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
    }
}
