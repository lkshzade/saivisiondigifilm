package com.microtron.cvinventorymanagmentapp.NetworkModel.BookingItem;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class BookingDataResponse{

	@SerializedName("error_msg")
	private String errorMsg;

	@SerializedName("success")
	private String success;

	@SerializedName("bookingData")
	private List<BookingDataItem> bookingData;

	public void setErrorMsg(String errorMsg){
		this.errorMsg = errorMsg;
	}

	public String getErrorMsg(){
		return errorMsg;
	}

	public void setSuccess(String success){
		this.success = success;
	}

	public String getSuccess(){
		return success;
	}

	public void setBookingData(List<BookingDataItem> bookingData){
		this.bookingData = bookingData;
	}

	public List<BookingDataItem> getBookingData(){
		return bookingData;
	}
}