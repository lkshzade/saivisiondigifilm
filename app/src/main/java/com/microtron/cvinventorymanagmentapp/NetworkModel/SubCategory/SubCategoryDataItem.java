package com.microtron.cvinventorymanagmentapp.NetworkModel.SubCategory;

import com.google.gson.annotations.SerializedName;

public class SubCategoryDataItem{

	@SerializedName("sub_cat_name")
	private String subCatName;

	@SerializedName("sub_cat_id")
	private String subCatId;

	@SerializedName("cat_id")
	private String catId;

	public void setSubCatName(String subCatName){
		this.subCatName = subCatName;
	}

	public String getSubCatName(){
		return subCatName;
	}

	public void setSubCatId(String subCatId){
		this.subCatId = subCatId;
	}

	public String getSubCatId(){
		return subCatId;
	}

	public void setCatId(String catId){
		this.catId = catId;
	}

	public String getCatId(){
		return catId;
	}
}