package com.microtron.cvinventorymanagmentapp.NetworkModel.Comapny;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class CompanyDataResponse{

	@SerializedName("error_msg")
	private String errorMsg;

	@SerializedName("success")
	private String success;

	@SerializedName("companyData")
	private List<CompanyDataItem> companyData;

	public void setErrorMsg(String errorMsg){
		this.errorMsg = errorMsg;
	}

	public String getErrorMsg(){
		return errorMsg;
	}

	public void setSuccess(String success){
		this.success = success;
	}

	public String getSuccess(){
		return success;
	}

	public void setCompanyData(List<CompanyDataItem> companyData){
		this.companyData = companyData;
	}

	public List<CompanyDataItem> getCompanyData(){
		return companyData;
	}
}