package com.microtron.cvinventorymanagmentapp.NetworkModel.SubCategory;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class SubCategoryDataResponse{

	@SerializedName("error_msg")
	private String errorMsg;

	@SerializedName("success")
	private String success;

	@SerializedName("subCategoryData")
	private List<SubCategoryDataItem> subCategoryData;

	public void setErrorMsg(String errorMsg){
		this.errorMsg = errorMsg;
	}

	public String getErrorMsg(){
		return errorMsg;
	}

	public void setSuccess(String success){
		this.success = success;
	}

	public String getSuccess(){
		return success;
	}

	public void setSubCategoryData(List<SubCategoryDataItem> subCategoryData){
		this.subCategoryData = subCategoryData;
	}

	public List<SubCategoryDataItem> getSubCategoryData(){
		return subCategoryData;
	}
}