package com.microtron.cvinventorymanagmentapp.NetworkModel.OutSideInventoryBooking;

import com.google.gson.annotations.SerializedName;


public class OutSideBookingDataRequest {

	@SerializedName("out_side_cust_mobile")
	private String outSideCustMobile;

	@SerializedName("out_side_inventory_id")
	private String outSideInventoryId;

	@SerializedName("saler_name")
	private String salerName;

	@SerializedName("product_price")
	private String productPrice;

	@SerializedName("out_side_total_rent")
	private String outSideTotalRent;

	@SerializedName("out_side_advance_amount")
	private String outSideAdvanceAmount;

	@SerializedName("10")
	private String jsonMember10;

	@SerializedName("11")
	private String jsonMember11;

	@SerializedName("12")
	private String jsonMember12;

	@SerializedName("13")
	private String jsonMember13;

	@SerializedName("out_side_from_date")
	private String outSideFromDate;

	@SerializedName("14")
	private String jsonMember14;

	@SerializedName("out_side_booking_type")
	private String outSideBookingType;

	@SerializedName("15")
	private String jsonMember15;

	@SerializedName("16")
	private String jsonMember16;

	@SerializedName("out_side_day_rent")
	private String outSideDayRent;

	@SerializedName("17")
	private String jsonMember17;

	@SerializedName("saler_mobile")
	private String salerMobile;

	@SerializedName("18")
	private String jsonMember18;

	@SerializedName("out_side_pending_amount")
	private String outSidePendingAmount;

	@SerializedName("out_side_inventory_status")
	private String outSideStatus;

	@SerializedName("shop_name")
	private String shopName;

	@SerializedName("product_name")
	private String productName;

	@SerializedName("0")
	private String jsonMember0;

	@SerializedName("out_side_month_rent")
	private String outSideMonthRent;

	@SerializedName("1")
	private String jsonMember1;

	@SerializedName("2")
	private String jsonMember2;

	@SerializedName("3")
	private String jsonMember3;

	@SerializedName("4")
	private String jsonMember4;

	@SerializedName("5")
	private String jsonMember5;

	@SerializedName("out_side_cust_name")
	private String outSideCustName;

	@SerializedName("6")
	private String jsonMember6;

	@SerializedName("company_name")
	private String companyName;

	@SerializedName("7")
	private String jsonMember7;

	@SerializedName("8")
	private String jsonMember8;

	@SerializedName("9")
	private String jsonMember9;

	@SerializedName("purchase_date")
	private String purchaseDate;

	@SerializedName("out_side_to_date")
	private String outSideToDate;

	@SerializedName("out_side_cust_email")
	private String outSideCustEmail;

	public void setOutSideCustMobile(String outSideCustMobile){
		this.outSideCustMobile = outSideCustMobile;
	}

	public String getOutSideCustMobile(){
		return outSideCustMobile;
	}

	public void setOutSideInventoryId(String outSideInventoryId){
		this.outSideInventoryId = outSideInventoryId;
	}

	public String getOutSideInventoryId(){
		return outSideInventoryId;
	}

	public void setSalerName(String salerName){
		this.salerName = salerName;
	}

	public String getSalerName(){
		return salerName;
	}

	public void setProductPrice(String productPrice){
		this.productPrice = productPrice;
	}

	public String getProductPrice(){
		return productPrice;
	}

	public void setOutSideTotalRent(String outSideTotalRent){
		this.outSideTotalRent = outSideTotalRent;
	}

	public String getOutSideTotalRent(){
		return outSideTotalRent;
	}

	public void setOutSideAdvanceAmount(String outSideAdvanceAmount){
		this.outSideAdvanceAmount = outSideAdvanceAmount;
	}

	public String getOutSideAdvanceAmount(){
		return outSideAdvanceAmount;
	}

	public void setJsonMember10(String jsonMember10){
		this.jsonMember10 = jsonMember10;
	}

	public String getJsonMember10(){
		return jsonMember10;
	}

	public void setJsonMember11(String jsonMember11){
		this.jsonMember11 = jsonMember11;
	}

	public String getJsonMember11(){
		return jsonMember11;
	}

	public void setJsonMember12(String jsonMember12){
		this.jsonMember12 = jsonMember12;
	}

	public String getJsonMember12(){
		return jsonMember12;
	}

	public void setJsonMember13(String jsonMember13){
		this.jsonMember13 = jsonMember13;
	}

	public String getJsonMember13(){
		return jsonMember13;
	}

	public void setOutSideFromDate(String outSideFromDate){
		this.outSideFromDate = outSideFromDate;
	}

	public String getOutSideFromDate(){
		return outSideFromDate;
	}

	public void setJsonMember14(String jsonMember14){
		this.jsonMember14 = jsonMember14;
	}

	public String getJsonMember14(){
		return jsonMember14;
	}

	public void setOutSideBookingType(String outSideBookingType){
		this.outSideBookingType = outSideBookingType;
	}

	public String getOutSideBookingType(){
		return outSideBookingType;
	}

	public void setJsonMember15(String jsonMember15){
		this.jsonMember15 = jsonMember15;
	}

	public String getJsonMember15(){
		return jsonMember15;
	}

	public void setJsonMember16(String jsonMember16){
		this.jsonMember16 = jsonMember16;
	}

	public String getJsonMember16(){
		return jsonMember16;
	}

	public void setOutSideDayRent(String outSideDayRent){
		this.outSideDayRent = outSideDayRent;
	}

	public String getOutSideDayRent(){
		return outSideDayRent;
	}

	public void setJsonMember17(String jsonMember17){
		this.jsonMember17 = jsonMember17;
	}

	public String getJsonMember17(){
		return jsonMember17;
	}

	public void setSalerMobile(String salerMobile){
		this.salerMobile = salerMobile;
	}

	public String getSalerMobile(){
		return salerMobile;
	}

	public void setJsonMember18(String jsonMember18){
		this.jsonMember18 = jsonMember18;
	}

	public String getJsonMember18(){
		return jsonMember18;
	}

	public void setOutSidePendingAmount(String outSidePendingAmount){
		this.outSidePendingAmount = outSidePendingAmount;
	}

	public String getOutSidePendingAmount(){
		return outSidePendingAmount;
	}

	public void setShopName(String shopName){
		this.shopName = shopName;
	}

	public String getShopName(){
		return shopName;
	}

	public void setProductName(String productName){
		this.productName = productName;
	}

	public String getProductName(){
		return productName;
	}

	public void setJsonMember0(String jsonMember0){
		this.jsonMember0 = jsonMember0;
	}

	public String getJsonMember0(){
		return jsonMember0;
	}

	public void setOutSideMonthRent(String outSideMonthRent){
		this.outSideMonthRent = outSideMonthRent;
	}

	public String getOutSideMonthRent(){
		return outSideMonthRent;
	}

	public void setJsonMember1(String jsonMember1){
		this.jsonMember1 = jsonMember1;
	}

	public String getJsonMember1(){
		return jsonMember1;
	}

	public void setJsonMember2(String jsonMember2){
		this.jsonMember2 = jsonMember2;
	}

	public String getJsonMember2(){
		return jsonMember2;
	}

	public void setJsonMember3(String jsonMember3){
		this.jsonMember3 = jsonMember3;
	}

	public String getJsonMember3(){
		return jsonMember3;
	}

	public void setJsonMember4(String jsonMember4){
		this.jsonMember4 = jsonMember4;
	}

	public String getJsonMember4(){
		return jsonMember4;
	}

	public void setJsonMember5(String jsonMember5){
		this.jsonMember5 = jsonMember5;
	}

	public String getJsonMember5(){
		return jsonMember5;
	}

	public void setOutSideCustName(String outSideCustName){
		this.outSideCustName = outSideCustName;
	}

	public String getOutSideCustName(){
		return outSideCustName;
	}

	public void setJsonMember6(String jsonMember6){
		this.jsonMember6 = jsonMember6;
	}

	public String getJsonMember6(){
		return jsonMember6;
	}

	public void setCompanyName(String companyName){
		this.companyName = companyName;
	}

	public String getCompanyName(){
		return companyName;
	}

	public void setJsonMember7(String jsonMember7){
		this.jsonMember7 = jsonMember7;
	}

	public String getJsonMember7(){
		return jsonMember7;
	}

	public void setJsonMember8(String jsonMember8){
		this.jsonMember8 = jsonMember8;
	}

	public String getJsonMember8(){
		return jsonMember8;
	}

	public void setJsonMember9(String jsonMember9){
		this.jsonMember9 = jsonMember9;
	}

	public String getJsonMember9(){
		return jsonMember9;
	}

	public void setPurchaseDate(String purchaseDate){
		this.purchaseDate = purchaseDate;
	}

	public String getPurchaseDate(){
		return purchaseDate;
	}

	public void setOutSideToDate(String outSideToDate){
		this.outSideToDate = outSideToDate;
	}

	public String getOutSideToDate(){
		return outSideToDate;
	}

	public void setOutSideCustEmail(String outSideCustEmail){
		this.outSideCustEmail = outSideCustEmail;
	}

	public String getOutSideCustEmail(){
		return outSideCustEmail;
	}

	public String getOutSideStatus() {
		return outSideStatus;
	}

	public void setOutSideStatus(String outSideStatus) {
		this.outSideStatus = outSideStatus;
	}
}