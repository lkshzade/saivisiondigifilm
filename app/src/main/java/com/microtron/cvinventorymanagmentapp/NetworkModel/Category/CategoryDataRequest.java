package com.microtron.cvinventorymanagmentapp.NetworkModel.Category;

import com.google.gson.annotations.SerializedName;

public class CategoryDataRequest {

	@SerializedName("cat_name")
	private String catName;

	@SerializedName("cat_id")
	private String catId;

	public void setCatName(String catName){
		this.catName = catName;
	}

	public String getCatName(){
		return catName;
	}

	public void setCatId(String catId){
		this.catId = catId;
	}

	public String getCatId(){
		return catId;
	}
}