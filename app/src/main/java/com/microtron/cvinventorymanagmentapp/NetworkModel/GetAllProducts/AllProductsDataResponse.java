package com.microtron.cvinventorymanagmentapp.NetworkModel.GetAllProducts;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class AllProductsDataResponse{

	@SerializedName("error_msg")
	private String errorMsg;

	@SerializedName("success")
	private String success;

	@SerializedName("allProductsData")
	private List<AllProductsDataItem> allProductsData;

	public void setErrorMsg(String errorMsg){
		this.errorMsg = errorMsg;
	}

	public String getErrorMsg(){
		return errorMsg;
	}

	public void setSuccess(String success){
		this.success = success;
	}

	public String getSuccess(){
		return success;
	}

	public void setAllProductsData(List<AllProductsDataItem> allProductsData){
		this.allProductsData = allProductsData;
	}

	public List<AllProductsDataItem> getAllProductsData(){
		return allProductsData;
	}
}