package com.microtron.cvinventorymanagmentapp.NetworkModel.UpdatePendings;

import com.google.gson.annotations.SerializedName;

public class UpdatedCustPendingsDataRequest {

	@SerializedName("daily_rate")
	private String dailyRate;

	@SerializedName("booking_cust_id")
	private String bookingCustId;

	@SerializedName("booking_from_date")
	private String bookingFromDate;

	@SerializedName("booking_month")
	private String bookingMonth;

	@SerializedName("booking_id")
	private String bookingId;

	@SerializedName("sale_date")
	private String saleDate;

	@SerializedName("rent_pending")
	private String rentPending;

	@SerializedName("days_count")
	private String daysCount;

	@SerializedName("booking_to_date")
	private String bookingToDate;

	@SerializedName("10")
	private String jsonMember10;

	@SerializedName("11")
	private String jsonMember11;

	@SerializedName("12")
	private String jsonMember12;

	@SerializedName("sale_rate")
	private String saleRate;

	@SerializedName("13")
	private String jsonMember13;

	@SerializedName("14")
	private String jsonMember14;

	@SerializedName("15")
	private String jsonMember15;

	@SerializedName("16")
	private String jsonMember16;

	@SerializedName("booking_prod_id")
	private String bookingProdId;

	@SerializedName("total_rent")
	private String totalRent;

	@SerializedName("booking_status")
	private String bookingStatus;

	@SerializedName("0")
	private String jsonMember0;

	@SerializedName("1")
	private String jsonMember1;

	@SerializedName("monthly_rate")
	private String monthlyRate;

	@SerializedName("2")
	private String jsonMember2;

	@SerializedName("3")
	private String jsonMember3;

	@SerializedName("4")
	private String jsonMember4;

	@SerializedName("5")
	private String jsonMember5;

	@SerializedName("6")
	private String jsonMember6;

	@SerializedName("7")
	private String jsonMember7;

	@SerializedName("8")
	private String jsonMember8;

	@SerializedName("rent_advance")
	private String rentAdvance;

	@SerializedName("9")
	private String jsonMember9;

	@SerializedName("booking_type")
	private String bookingType;

	@SerializedName("booking_year")
	private String bookingYear;

	public void setDailyRate(String dailyRate){
		this.dailyRate = dailyRate;
	}

	public String getDailyRate(){
		return dailyRate;
	}

	public void setBookingCustId(String bookingCustId){
		this.bookingCustId = bookingCustId;
	}

	public String getBookingCustId(){
		return bookingCustId;
	}

	public void setBookingFromDate(String bookingFromDate){
		this.bookingFromDate = bookingFromDate;
	}

	public String getBookingFromDate(){
		return bookingFromDate;
	}

	public void setBookingMonth(String bookingMonth){
		this.bookingMonth = bookingMonth;
	}

	public String getBookingMonth(){
		return bookingMonth;
	}

	public void setBookingId(String bookingId){
		this.bookingId = bookingId;
	}

	public String getBookingId(){
		return bookingId;
	}

	public void setSaleDate(String saleDate){
		this.saleDate = saleDate;
	}

	public String getSaleDate(){
		return saleDate;
	}

	public void setRentPending(String rentPending){
		this.rentPending = rentPending;
	}

	public String getRentPending(){
		return rentPending;
	}

	public void setDaysCount(String daysCount){
		this.daysCount = daysCount;
	}

	public String getDaysCount(){
		return daysCount;
	}

	public void setBookingToDate(String bookingToDate){
		this.bookingToDate = bookingToDate;
	}

	public String getBookingToDate(){
		return bookingToDate;
	}

	public void setJsonMember10(String jsonMember10){
		this.jsonMember10 = jsonMember10;
	}

	public String getJsonMember10(){
		return jsonMember10;
	}

	public void setJsonMember11(String jsonMember11){
		this.jsonMember11 = jsonMember11;
	}

	public String getJsonMember11(){
		return jsonMember11;
	}

	public void setJsonMember12(String jsonMember12){
		this.jsonMember12 = jsonMember12;
	}

	public String getJsonMember12(){
		return jsonMember12;
	}

	public void setSaleRate(String saleRate){
		this.saleRate = saleRate;
	}

	public String getSaleRate(){
		return saleRate;
	}

	public void setJsonMember13(String jsonMember13){
		this.jsonMember13 = jsonMember13;
	}

	public String getJsonMember13(){
		return jsonMember13;
	}

	public void setJsonMember14(String jsonMember14){
		this.jsonMember14 = jsonMember14;
	}

	public String getJsonMember14(){
		return jsonMember14;
	}

	public void setJsonMember15(String jsonMember15){
		this.jsonMember15 = jsonMember15;
	}

	public String getJsonMember15(){
		return jsonMember15;
	}

	public void setJsonMember16(String jsonMember16){
		this.jsonMember16 = jsonMember16;
	}

	public String getJsonMember16(){
		return jsonMember16;
	}

	public void setBookingProdId(String bookingProdId){
		this.bookingProdId = bookingProdId;
	}

	public String getBookingProdId(){
		return bookingProdId;
	}

	public void setTotalRent(String totalRent){
		this.totalRent = totalRent;
	}

	public String getTotalRent(){
		return totalRent;
	}

	public void setBookingStatus(String bookingStatus){
		this.bookingStatus = bookingStatus;
	}

	public String getBookingStatus(){
		return bookingStatus;
	}

	public void setJsonMember0(String jsonMember0){
		this.jsonMember0 = jsonMember0;
	}

	public String getJsonMember0(){
		return jsonMember0;
	}

	public void setJsonMember1(String jsonMember1){
		this.jsonMember1 = jsonMember1;
	}

	public String getJsonMember1(){
		return jsonMember1;
	}

	public void setMonthlyRate(String monthlyRate){
		this.monthlyRate = monthlyRate;
	}

	public String getMonthlyRate(){
		return monthlyRate;
	}

	public void setJsonMember2(String jsonMember2){
		this.jsonMember2 = jsonMember2;
	}

	public String getJsonMember2(){
		return jsonMember2;
	}

	public void setJsonMember3(String jsonMember3){
		this.jsonMember3 = jsonMember3;
	}

	public String getJsonMember3(){
		return jsonMember3;
	}

	public void setJsonMember4(String jsonMember4){
		this.jsonMember4 = jsonMember4;
	}

	public String getJsonMember4(){
		return jsonMember4;
	}

	public void setJsonMember5(String jsonMember5){
		this.jsonMember5 = jsonMember5;
	}

	public String getJsonMember5(){
		return jsonMember5;
	}

	public void setJsonMember6(String jsonMember6){
		this.jsonMember6 = jsonMember6;
	}

	public String getJsonMember6(){
		return jsonMember6;
	}

	public void setJsonMember7(String jsonMember7){
		this.jsonMember7 = jsonMember7;
	}

	public String getJsonMember7(){
		return jsonMember7;
	}

	public void setJsonMember8(String jsonMember8){
		this.jsonMember8 = jsonMember8;
	}

	public String getJsonMember8(){
		return jsonMember8;
	}

	public void setRentAdvance(String rentAdvance){
		this.rentAdvance = rentAdvance;
	}

	public String getRentAdvance(){
		return rentAdvance;
	}

	public void setJsonMember9(String jsonMember9){
		this.jsonMember9 = jsonMember9;
	}

	public String getJsonMember9(){
		return jsonMember9;
	}

	public void setBookingType(String bookingType){
		this.bookingType = bookingType;
	}

	public String getBookingType(){
		return bookingType;
	}

	public void setBookingYear(String bookingYear){
		this.bookingYear = bookingYear;
	}

	public String getBookingYear(){
		return bookingYear;
	}
}