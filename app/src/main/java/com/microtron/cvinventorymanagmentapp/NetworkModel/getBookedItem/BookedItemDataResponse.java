package com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class BookedItemDataResponse{

	@SerializedName("error_msg")
	private String errorMsg;

	@SerializedName("success")
	private String success;

	@SerializedName("bookedItemData")
	private List<BookedItemDataItem> bookedItemData;

	public void setErrorMsg(String errorMsg){
		this.errorMsg = errorMsg;
	}

	public String getErrorMsg(){
		return errorMsg;
	}

	public void setSuccess(String success){
		this.success = success;
	}

	public String getSuccess(){
		return success;
	}

	public void setBookedItemData(List<BookedItemDataItem> bookedItemData){
		this.bookedItemData = bookedItemData;
	}

	public List<BookedItemDataItem> getBookedItemData(){
		return bookedItemData;
	}
}