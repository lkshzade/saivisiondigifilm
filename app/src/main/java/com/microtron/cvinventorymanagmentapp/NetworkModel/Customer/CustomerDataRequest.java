package com.microtron.cvinventorymanagmentapp.NetworkModel.Customer;

import com.google.gson.annotations.SerializedName;

public class CustomerDataRequest {

	@SerializedName("customer_mobile")
	private String customerMobile;

	@SerializedName("total_amount")
	private String totalAmount;

	@SerializedName("customer_email")
	private String customerEmail;

	@SerializedName("paid_amount")
	private String paidAmount;

	@SerializedName("customer_name")
	private String customerName;

	@SerializedName("customer_id")
	private String customerId;

	@SerializedName("pending_amount")
	private String pendingAmount;

	public void setCustomerMobile(String customerMobile){
		this.customerMobile = customerMobile;
	}

	public String getCustomerMobile(){
		return customerMobile;
	}

	public void setTotalAmount(String totalAmount){
		this.totalAmount = totalAmount;
	}

	public String getTotalAmount(){
		return totalAmount;
	}

	public void setCustomerEmail(String customerEmail){
		this.customerEmail = customerEmail;
	}

	public String getCustomerEmail(){
		return customerEmail;
	}

	public void setPaidAmount(String paidAmount){
		this.paidAmount = paidAmount;
	}

	public String getPaidAmount(){
		return paidAmount;
	}

	public void setCustomerName(String customerName){
		this.customerName = customerName;
	}

	public String getCustomerName(){
		return customerName;
	}

	public void setCustomerId(String customerId){
		this.customerId = customerId;
	}

	public String getCustomerId(){
		return customerId;
	}

	public void setPendingAmount(String pendingAmount){
		this.pendingAmount = pendingAmount;
	}

	public String getPendingAmount(){
		return pendingAmount;
	}
}