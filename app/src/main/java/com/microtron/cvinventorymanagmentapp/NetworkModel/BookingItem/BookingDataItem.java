package com.microtron.cvinventorymanagmentapp.NetworkModel.BookingItem;

import com.google.gson.annotations.SerializedName;

public class BookingDataItem{

	@SerializedName("booking_cust_id")
	private String bookingCustId;

	@SerializedName("booking_from_date")
	private String bookingFromDate;

	@SerializedName("booking_month")
	private String bookingMonth;

	@SerializedName("booking_year")
	private String bookingYear;

	@SerializedName("booking_type")
	private String bookingType;

	@SerializedName("monthly_rate")
	private String monthRent;

	@SerializedName("daily_rate")
	private String dailyRent;

	@SerializedName("booking_status")
	private String bookingStatus;

	@SerializedName("sale_rate")
	private String saleRate;

	@SerializedName("sale_date")
	private String saleDate;

	@SerializedName("product_status")
	private String productStatus;

	@SerializedName("rent_advance")
	private String rentAdvance;

	@SerializedName("rent_pending")
	private String rentPending;

	@SerializedName("product_quantity")
	private String productQuantity;

	@SerializedName("product_category_id")
	private String productCategoryId;

	@SerializedName("booking_id")
	private String bookingId;

	@SerializedName("product_in_stock")
	private String productInStock;

	@SerializedName("product_id")
	private String productId;

	@SerializedName("product_compny_id")
	private String productCompnyId;

	@SerializedName("days_count")
	private String daysCount;

	@SerializedName("booking_to_date")
	private String bookingToDate;

	@SerializedName("10")
	private String jsonMember10;

	@SerializedName("product_img_url1")
	private String productImgUrl1;

	@SerializedName("product_img_url2")
	private String productImgUrl2;

	@SerializedName("product_img_url3")
	private String productImgUrl3;

	@SerializedName("product_img_url4")
	private String productImgUrl4;

	@SerializedName("11")
	private String jsonMember11;

	@SerializedName("12")
	private String jsonMember12;

	@SerializedName("customer_mobile")
	private String customerMobile;

	@SerializedName("13")
	private String jsonMember13;

	@SerializedName("14")
	private String jsonMember14;

	@SerializedName("15")
	private String jsonMember15;

	@SerializedName("product_subcategory_id")
	private String productSubcategoryId;

	@SerializedName("16")
	private String jsonMember16;

	@SerializedName("17")
	private String jsonMember17;

	@SerializedName("booking_prod_id")
	private String bookingProdId;

	@SerializedName("total_rent")
	private String totalRent;

	@SerializedName("18")
	private String jsonMember18;

	@SerializedName("19")
	private String jsonMember19;

	@SerializedName("product_name")
	private String productName;

	@SerializedName("0")
	private String jsonMember0;

	@SerializedName("1")
	private String jsonMember1;

	@SerializedName("2")
	private String jsonMember2;

	@SerializedName("3")
	private String jsonMember3;

	@SerializedName("4")
	private String jsonMember4;

	@SerializedName("5")
	private String jsonMember5;

	@SerializedName("6")
	private String jsonMember6;

	@SerializedName("7")
	private String jsonMember7;

	@SerializedName("customer_email")
	private String customerEmail;

	@SerializedName("8")
	private String jsonMember8;

	@SerializedName("9")
	private String jsonMember9;

	@SerializedName("customer_name")
	private String customerName;

	@SerializedName("customer_id")
	private String customerId;

	@SerializedName("product_details")
	private String productDetails;

	@SerializedName("20")
	private String jsonMember20;

	@SerializedName("21")
	private String jsonMember21;

	public void setBookingCustId(String bookingCustId){
		this.bookingCustId = bookingCustId;
	}

	public String getBookingCustId(){
		return bookingCustId;
	}

	public void setBookingFromDate(String bookingFromDate){
		this.bookingFromDate = bookingFromDate;
	}

	public String getBookingFromDate(){
		return bookingFromDate;
	}

	public void setProductQuantity(String productQuantity){
		this.productQuantity = productQuantity;
	}

	public String getProductQuantity(){
		return productQuantity;
	}

	public void setProductCategoryId(String productCategoryId){
		this.productCategoryId = productCategoryId;
	}

	public String getProductCategoryId(){
		return productCategoryId;
	}

	public void setBookingId(String bookingId){
		this.bookingId = bookingId;
	}

	public String getBookingId(){
		return bookingId;
	}

	public void setProductInStock(String productInStock){
		this.productInStock = productInStock;
	}

	public String getProductInStock(){
		return productInStock;
	}

	public void setProductId(String productId){
		this.productId = productId;
	}

	public String getProductId(){
		return productId;
	}

	public void setProductCompnyId(String productCompnyId){
		this.productCompnyId = productCompnyId;
	}

	public String getProductCompnyId(){
		return productCompnyId;
	}

	public void setDaysCount(String daysCount){
		this.daysCount = daysCount;
	}

	public String getDaysCount(){
		return daysCount;
	}

	public void setBookingToDate(String bookingToDate){
		this.bookingToDate = bookingToDate;
	}

	public String getBookingToDate(){
		return bookingToDate;
	}

	public void setJsonMember10(String jsonMember10){
		this.jsonMember10 = jsonMember10;
	}

	public String getJsonMember10(){
		return jsonMember10;
	}

	public String getProductImgUrl1() {
		return productImgUrl1;
	}

	public void setProductImgUrl1(String productImgUrl1) {
		this.productImgUrl1 = productImgUrl1;
	}

	public String getProductImgUrl2() {
		return productImgUrl2;
	}

	public void setProductImgUrl2(String productImgUrl2) {
		this.productImgUrl2 = productImgUrl2;
	}

	public String getProductImgUrl3() {
		return productImgUrl3;
	}

	public void setProductImgUrl3(String productImgUrl3) {
		this.productImgUrl3 = productImgUrl3;
	}

	public String getProductImgUrl4() {
		return productImgUrl4;
	}

	public void setProductImgUrl4(String productImgUrl4) {
		this.productImgUrl4 = productImgUrl4;
	}

	public void setJsonMember11(String jsonMember11){
		this.jsonMember11 = jsonMember11;
	}

	public String getJsonMember11(){
		return jsonMember11;
	}

	public void setJsonMember12(String jsonMember12){
		this.jsonMember12 = jsonMember12;
	}

	public String getJsonMember12(){
		return jsonMember12;
	}

	public void setCustomerMobile(String customerMobile){
		this.customerMobile = customerMobile;
	}

	public String getCustomerMobile(){
		return customerMobile;
	}

	public void setJsonMember13(String jsonMember13){
		this.jsonMember13 = jsonMember13;
	}

	public String getJsonMember13(){
		return jsonMember13;
	}

	public void setJsonMember14(String jsonMember14){
		this.jsonMember14 = jsonMember14;
	}

	public String getJsonMember14(){
		return jsonMember14;
	}

	public void setJsonMember15(String jsonMember15){
		this.jsonMember15 = jsonMember15;
	}

	public String getJsonMember15(){
		return jsonMember15;
	}

	public void setProductSubcategoryId(String productSubcategoryId){
		this.productSubcategoryId = productSubcategoryId;
	}

	public String getProductSubcategoryId(){
		return productSubcategoryId;
	}

	public void setJsonMember16(String jsonMember16){
		this.jsonMember16 = jsonMember16;
	}

	public String getJsonMember16(){
		return jsonMember16;
	}

	public void setJsonMember17(String jsonMember17){
		this.jsonMember17 = jsonMember17;
	}

	public String getJsonMember17(){
		return jsonMember17;
	}

	public void setBookingProdId(String bookingProdId){
		this.bookingProdId = bookingProdId;
	}

	public String getBookingProdId(){
		return bookingProdId;
	}

	public void setTotalRent(String totalRent){
		this.totalRent = totalRent;
	}

	public String getTotalRent(){
		return totalRent;
	}

	public void setJsonMember18(String jsonMember18){
		this.jsonMember18 = jsonMember18;
	}

	public String getJsonMember18(){
		return jsonMember18;
	}

	public void setJsonMember19(String jsonMember19){
		this.jsonMember19 = jsonMember19;
	}

	public String getJsonMember19(){
		return jsonMember19;
	}

	public void setBookingStatus(String bookingStatus){
		this.bookingStatus = bookingStatus;
	}

	public String getBookingStatus(){
		return bookingStatus;
	}

	public void setProductName(String productName){
		this.productName = productName;
	}

	public String getProductName(){
		return productName;
	}

	public void setJsonMember0(String jsonMember0){
		this.jsonMember0 = jsonMember0;
	}

	public String getJsonMember0(){
		return jsonMember0;
	}

	public void setJsonMember1(String jsonMember1){
		this.jsonMember1 = jsonMember1;
	}

	public String getJsonMember1(){
		return jsonMember1;
	}

	public void setJsonMember2(String jsonMember2){
		this.jsonMember2 = jsonMember2;
	}

	public String getJsonMember2(){
		return jsonMember2;
	}

	public void setJsonMember3(String jsonMember3){
		this.jsonMember3 = jsonMember3;
	}

	public String getJsonMember3(){
		return jsonMember3;
	}

	public void setJsonMember4(String jsonMember4){
		this.jsonMember4 = jsonMember4;
	}

	public String getJsonMember4(){
		return jsonMember4;
	}

	public void setJsonMember5(String jsonMember5){
		this.jsonMember5 = jsonMember5;
	}

	public String getJsonMember5(){
		return jsonMember5;
	}

	public void setJsonMember6(String jsonMember6){
		this.jsonMember6 = jsonMember6;
	}

	public String getJsonMember6(){
		return jsonMember6;
	}

	public void setJsonMember7(String jsonMember7){
		this.jsonMember7 = jsonMember7;
	}

	public String getJsonMember7(){
		return jsonMember7;
	}

	public void setCustomerEmail(String customerEmail){
		this.customerEmail = customerEmail;
	}

	public String getCustomerEmail(){
		return customerEmail;
	}

	public void setJsonMember8(String jsonMember8){
		this.jsonMember8 = jsonMember8;
	}

	public String getJsonMember8(){
		return jsonMember8;
	}

	public void setJsonMember9(String jsonMember9){
		this.jsonMember9 = jsonMember9;
	}

	public String getJsonMember9(){
		return jsonMember9;
	}

	public void setCustomerName(String customerName){
		this.customerName = customerName;
	}

	public String getCustomerName(){
		return customerName;
	}

	public void setCustomerId(String customerId){
		this.customerId = customerId;
	}

	public String getCustomerId(){
		return customerId;
	}

	public void setProductDetails(String productDetails){
		this.productDetails = productDetails;
	}

	public String getProductDetails(){
		return productDetails;
	}

	public void setJsonMember20(String jsonMember20){
		this.jsonMember20 = jsonMember20;
	}

	public String getJsonMember20(){
		return jsonMember20;
	}

	public void setJsonMember21(String jsonMember21){
		this.jsonMember21 = jsonMember21;
	}

	public String getJsonMember21(){
		return jsonMember21;
	}

	public String getBookingMonth() {
		return bookingMonth;
	}

	public void setBookingMonth(String bookingMonth) {
		this.bookingMonth = bookingMonth;
	}

	public String getBookingYear() {
		return bookingYear;
	}

	public void setBookingYear(String bookingYear) {
		this.bookingYear = bookingYear;
	}

	public String getBookingType() {
		return bookingType;
	}

	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}

	public String getMonthRent() {
		return monthRent;
	}

	public void setMonthRent(String monthRent) {
		this.monthRent = monthRent;
	}

	public String getDailyRent() {
		return dailyRent;
	}

	public void setDailyRent(String dailyRent) {
		this.dailyRent = dailyRent;
	}

	public String getRentAdvance() {
		return rentAdvance;
	}

	public void setRentAdvance(String rentAdvance) {
		this.rentAdvance = rentAdvance;
	}

	public String getRentPending() {
		return rentPending;
	}

	public void setRentPending(String rentPending) {
		this.rentPending = rentPending;
	}

	public String getSaleRate() {
		return saleRate;
	}

	public void setSaleRate(String saleRate) {
		this.saleRate = saleRate;
	}

	public String getSaleDate() {
		return saleDate;
	}

	public void setSaleDate(String saleDate) {
		this.saleDate = saleDate;
	}

	public String getProductStatus() {
		return productStatus;
	}

	public void setProductStatus(String productStatus) {
		this.productStatus = productStatus;
	}
}