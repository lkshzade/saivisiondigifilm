package com.microtron.cvinventorymanagmentapp.NetworkModel.AddItem;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class AddItemDataResponse{

	@SerializedName("error_msg")
	private String errorMsg;

	@SerializedName("success")
	private String success;

	@SerializedName("addItemData")
	private List<AddItemDataItem> addItemData;

	public void setErrorMsg(String errorMsg){
		this.errorMsg = errorMsg;
	}

	public String getErrorMsg(){
		return errorMsg;
	}

	public void setSuccess(String success){
		this.success = success;
	}

	public String getSuccess(){
		return success;
	}

	public void setAddItemData(List<AddItemDataItem> addItemData){
		this.addItemData = addItemData;
	}

	public List<AddItemDataItem> getAddItemData(){
		return addItemData;
	}
}