package com.microtron.cvinventorymanagmentapp.NetworkModel.GetScanProduct;

import com.google.gson.annotations.SerializedName;

public class GetScanProductDataItem{

	@SerializedName("product_subcategory_id")
	private String productSubcategoryId;

	@SerializedName("company_id")
	private String companyId;

	@SerializedName("cat_name")
	private String catName;

	@SerializedName("sub_cat_id")
	private String subCatId;

	@SerializedName("product_name")
	private String productName;

	@SerializedName("product_quantity")
	private String productQuantity;

	@SerializedName("product_category_id")
	private String productCategoryId;

	@SerializedName("product_in_stock")
	private String productInStock;

	@SerializedName("sub_cat_name")
	private String subCatName;

	@SerializedName("product_id")
	private String productId;

	@SerializedName("company_name")
	private String companyName;

	@SerializedName("product_compny_id")
	private String productCompnyId;

	@SerializedName("cat_id")
	private String catId;

	@SerializedName("product_details")
	private String productDetails;

	@SerializedName("product_img_url1")
	private String productImgUrl1;

	@SerializedName("product_img_url2")
	private String productImgUrl2;

	@SerializedName("product_img_url3")
	private String productImgUrl3;

	@SerializedName("product_img_url4")
	private String productImgUrl4;

	public void setProductSubcategoryId(String productSubcategoryId){
		this.productSubcategoryId = productSubcategoryId;
	}

	public String getProductSubcategoryId(){
		return productSubcategoryId;
	}

	public void setCompanyId(String companyId){
		this.companyId = companyId;
	}

	public String getCompanyId(){
		return companyId;
	}

	public void setCatName(String catName){
		this.catName = catName;
	}

	public String getCatName(){
		return catName;
	}

	public void setSubCatId(String subCatId){
		this.subCatId = subCatId;
	}

	public String getSubCatId(){
		return subCatId;
	}

	public void setProductName(String productName){
		this.productName = productName;
	}

	public String getProductName(){
		return productName;
	}

	public void setProductQuantity(String productQuantity){
		this.productQuantity = productQuantity;
	}

	public String getProductQuantity(){
		return productQuantity;
	}

	public void setProductCategoryId(String productCategoryId){
		this.productCategoryId = productCategoryId;
	}

	public String getProductCategoryId(){
		return productCategoryId;
	}

	public void setProductInStock(String productInStock){
		this.productInStock = productInStock;
	}

	public String getProductInStock(){
		return productInStock;
	}

	public void setSubCatName(String subCatName){
		this.subCatName = subCatName;
	}

	public String getSubCatName(){
		return subCatName;
	}

	public void setProductId(String productId){
		this.productId = productId;
	}

	public String getProductId(){
		return productId;
	}

	public void setCompanyName(String companyName){
		this.companyName = companyName;
	}

	public String getCompanyName(){
		return companyName;
	}

	public void setProductCompnyId(String productCompnyId){
		this.productCompnyId = productCompnyId;
	}

	public String getProductCompnyId(){
		return productCompnyId;
	}

	public void setCatId(String catId){
		this.catId = catId;
	}

	public String getCatId(){
		return catId;
	}

	public void setProductDetails(String productDetails){
		this.productDetails = productDetails;
	}

	public String getProductDetails(){
		return productDetails;
	}

	public String getProductImgUrl1() {
		return productImgUrl1;
	}

	public void setProductImgUrl1(String productImgUrl1) {
		this.productImgUrl1 = productImgUrl1;
	}

	public String getProductImgUrl2() {
		return productImgUrl2;
	}

	public void setProductImgUrl2(String productImgUrl2) {
		this.productImgUrl2 = productImgUrl2;
	}

	public String getProductImgUrl3() {
		return productImgUrl3;
	}

	public void setProductImgUrl3(String productImgUrl3) {
		this.productImgUrl3 = productImgUrl3;
	}

	public String getProductImgUrl4() {
		return productImgUrl4;
	}

	public void setProductImgUrl4(String productImgUrl4) {
		this.productImgUrl4 = productImgUrl4;
	}
}