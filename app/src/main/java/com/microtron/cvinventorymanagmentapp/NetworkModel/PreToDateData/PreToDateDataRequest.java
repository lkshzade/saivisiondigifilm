package com.microtron.cvinventorymanagmentapp.NetworkModel.PreToDateData;

import com.google.gson.annotations.SerializedName;


public class PreToDateDataRequest {

	@SerializedName("sale_rate")
	private String saleRate;

	@SerializedName("daily_rate")
	private String dailyRate;

	@SerializedName("booking_cust_id")
	private String bookingCustId;

	@SerializedName("booking_from_date")
	private String bookingFromDate;

	@SerializedName("booking_prod_id")
	private String bookingProdId;

	@SerializedName("total_rent")
	private String totalRent;

	@SerializedName("booking_status")
	private String bookingStatus;

	@SerializedName("booking_month")
	private String bookingMonth;

	@SerializedName("booking_id")
	private String bookingId;

	@SerializedName("monthly_rate")
	private String monthlyRate;

	@SerializedName("sale_date")
	private String saleDate;

	@SerializedName("rent_pending")
	private String rentPending;

	@SerializedName("rent_advance")
	private String rentAdvance;

	@SerializedName("booking_type")
	private String bookingType;

	@SerializedName("days_count")
	private String daysCount;

	@SerializedName("booking_to_date")
	private String bookingToDate;

	@SerializedName("booking_year")
	private String bookingYear;

	public void setSaleRate(String saleRate){
		this.saleRate = saleRate;
	}

	public String getSaleRate(){
		return saleRate;
	}

	public void setDailyRate(String dailyRate){
		this.dailyRate = dailyRate;
	}

	public String getDailyRate(){
		return dailyRate;
	}

	public void setBookingCustId(String bookingCustId){
		this.bookingCustId = bookingCustId;
	}

	public String getBookingCustId(){
		return bookingCustId;
	}

	public void setBookingFromDate(String bookingFromDate){
		this.bookingFromDate = bookingFromDate;
	}

	public String getBookingFromDate(){
		return bookingFromDate;
	}

	public void setBookingProdId(String bookingProdId){
		this.bookingProdId = bookingProdId;
	}

	public String getBookingProdId(){
		return bookingProdId;
	}

	public void setTotalRent(String totalRent){
		this.totalRent = totalRent;
	}

	public String getTotalRent(){
		return totalRent;
	}

	public void setBookingStatus(String bookingStatus){
		this.bookingStatus = bookingStatus;
	}

	public String getBookingStatus(){
		return bookingStatus;
	}

	public void setBookingMonth(String bookingMonth){
		this.bookingMonth = bookingMonth;
	}

	public String getBookingMonth(){
		return bookingMonth;
	}

	public void setBookingId(String bookingId){
		this.bookingId = bookingId;
	}

	public String getBookingId(){
		return bookingId;
	}

	public void setMonthlyRate(String monthlyRate){
		this.monthlyRate = monthlyRate;
	}

	public String getMonthlyRate(){
		return monthlyRate;
	}

	public void setSaleDate(String saleDate){
		this.saleDate = saleDate;
	}

	public String getSaleDate(){
		return saleDate;
	}

	public void setRentPending(String rentPending){
		this.rentPending = rentPending;
	}

	public String getRentPending(){
		return rentPending;
	}

	public void setRentAdvance(String rentAdvance){
		this.rentAdvance = rentAdvance;
	}

	public String getRentAdvance(){
		return rentAdvance;
	}

	public void setBookingType(String bookingType){
		this.bookingType = bookingType;
	}

	public String getBookingType(){
		return bookingType;
	}

	public void setDaysCount(String daysCount){
		this.daysCount = daysCount;
	}

	public String getDaysCount(){
		return daysCount;
	}

	public void setBookingToDate(String bookingToDate){
		this.bookingToDate = bookingToDate;
	}

	public String getBookingToDate(){
		return bookingToDate;
	}

	public void setBookingYear(String bookingYear){
		this.bookingYear = bookingYear;
	}

	public String getBookingYear(){
		return bookingYear;
	}
}