package com.microtron.cvinventorymanagmentapp.NetworkModel.GetScanProduct;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class GetScanProductDataResponse{

	@SerializedName("error_msg")
	private String errorMsg;

	@SerializedName("success")
	private String success;

	@SerializedName("getScanProductData")
	private List<GetScanProductDataItem> getScanProductData;

	public void setErrorMsg(String errorMsg){
		this.errorMsg = errorMsg;
	}

	public String getErrorMsg(){
		return errorMsg;
	}

	public void setSuccess(String success){
		this.success = success;
	}

	public String getSuccess(){
		return success;
	}

	public void setGetScanProductData(List<GetScanProductDataItem> getScanProductData){
		this.getScanProductData = getScanProductData;
	}

	public List<GetScanProductDataItem> getGetScanProductData(){
		return getScanProductData;
	}
}