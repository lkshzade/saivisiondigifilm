package com.microtron.cvinventorymanagmentapp.NetworkModel.Category;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class CategoryDataResponse{

	@SerializedName("error_msg")
	private String errorMsg;

	@SerializedName("success")
	private String success;

	@SerializedName("categoryData")
	private List<CategoryDataItem> categoryData;

	public void setErrorMsg(String errorMsg){
		this.errorMsg = errorMsg;
	}

	public String getErrorMsg(){
		return errorMsg;
	}

	public void setSuccess(String success){
		this.success = success;
	}

	public String getSuccess(){
		return success;
	}

	public void setCategoryData(List<CategoryDataItem> categoryData){
		this.categoryData = categoryData;
	}

	public List<CategoryDataItem> getCategoryData(){
		return categoryData;
	}
}