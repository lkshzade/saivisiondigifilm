package com.microtron.cvinventorymanagmentapp.NetworkModel.PendingAmounts;

import com.google.gson.annotations.SerializedName;

public class PendingAmountDataRequest {

	@SerializedName("daily_rate")
	private String dailyRate;

	@SerializedName("booking_cust_id")
	private String bookingCustId;

	@SerializedName("booking_from_date")
	private String bookingFromDate;

	@SerializedName("product_status")
	private String productStatus;

	@SerializedName("booking_month")
	private String bookingMonth;

	@SerializedName("product_quantity")
	private String productQuantity;

	@SerializedName("product_category_id")
	private String productCategoryId;

	@SerializedName("booking_id")
	private String bookingId;

	@SerializedName("product_in_stock")
	private String productInStock;

	@SerializedName("sale_date")
	private String saleDate;

	@SerializedName("rent_pending")
	private String rentPending;

	@SerializedName("product_id")
	private String productId;

	@SerializedName("product_compny_id")
	private String productCompnyId;

	@SerializedName("cat_id")
	private String catId;

	@SerializedName("days_count")
	private String daysCount;

	@SerializedName("booking_to_date")
	private String bookingToDate;

	@SerializedName("customer_mobile")
	private String customerMobile;

	@SerializedName("sale_rate")
	private String saleRate;

	@SerializedName("product_subcategory_id")
	private String productSubcategoryId;

	@SerializedName("company_id")
	private String companyId;

	@SerializedName("cat_name")
	private String catName;

	@SerializedName("booking_prod_id")
	private String bookingProdId;

	@SerializedName("total_rent")
	private String totalRent;

	@SerializedName("sub_cat_id")
	private String subCatId;

	@SerializedName("product_img_url1")
	private String productImgUrl1;

	@SerializedName("booking_status")
	private String bookingStatus;

	@SerializedName("product_img_url2")
	private String productImgUrl2;

	@SerializedName("product_name")
	private String productName;

	@SerializedName("pending_amount")
	private String pendingAmount;

	@SerializedName("monthly_rate")
	private String monthlyRate;

	@SerializedName("product_img_url3")
	private String productImgUrl3;

	@SerializedName("product_img_url4")
	private String productImgUrl4;

	@SerializedName("total_amount")
	private String totalAmount;

	@SerializedName("sub_cat_name")
	private String subCatName;

	@SerializedName("customer_email")
	private String customerEmail;

	@SerializedName("company_name")
	private String companyName;

	@SerializedName("paid_amount")
	private String paidAmount;

	@SerializedName("rent_advance")
	private String rentAdvance;

	@SerializedName("booking_type")
	private String bookingType;

	@SerializedName("customer_name")
	private String customerName;

	@SerializedName("product_details")
	private String productDetails;

	@SerializedName("customer_id")
	private String customerId;

	@SerializedName("booking_year")
	private String bookingYear;

	public void setDailyRate(String dailyRate){
		this.dailyRate = dailyRate;
	}

	public String getDailyRate(){
		return dailyRate;
	}

	public void setBookingCustId(String bookingCustId){
		this.bookingCustId = bookingCustId;
	}

	public String getBookingCustId(){
		return bookingCustId;
	}

	public void setBookingFromDate(String bookingFromDate){
		this.bookingFromDate = bookingFromDate;
	}

	public String getBookingFromDate(){
		return bookingFromDate;
	}

	public void setProductStatus(String productStatus){
		this.productStatus = productStatus;
	}

	public String getProductStatus(){
		return productStatus;
	}

	public void setBookingMonth(String bookingMonth){
		this.bookingMonth = bookingMonth;
	}

	public String getBookingMonth(){
		return bookingMonth;
	}

	public void setProductQuantity(String productQuantity){
		this.productQuantity = productQuantity;
	}

	public String getProductQuantity(){
		return productQuantity;
	}

	public void setProductCategoryId(String productCategoryId){
		this.productCategoryId = productCategoryId;
	}

	public String getProductCategoryId(){
		return productCategoryId;
	}

	public void setBookingId(String bookingId){
		this.bookingId = bookingId;
	}

	public String getBookingId(){
		return bookingId;
	}

	public void setProductInStock(String productInStock){
		this.productInStock = productInStock;
	}

	public String getProductInStock(){
		return productInStock;
	}

	public void setSaleDate(String saleDate){
		this.saleDate = saleDate;
	}

	public String getSaleDate(){
		return saleDate;
	}

	public void setRentPending(String rentPending){
		this.rentPending = rentPending;
	}

	public String getRentPending(){
		return rentPending;
	}

	public void setProductId(String productId){
		this.productId = productId;
	}

	public String getProductId(){
		return productId;
	}

	public void setProductCompnyId(String productCompnyId){
		this.productCompnyId = productCompnyId;
	}

	public String getProductCompnyId(){
		return productCompnyId;
	}

	public void setCatId(String catId){
		this.catId = catId;
	}

	public String getCatId(){
		return catId;
	}

	public void setDaysCount(String daysCount){
		this.daysCount = daysCount;
	}

	public String getDaysCount(){
		return daysCount;
	}

	public void setBookingToDate(String bookingToDate){
		this.bookingToDate = bookingToDate;
	}

	public String getBookingToDate(){
		return bookingToDate;
	}

	public void setCustomerMobile(String customerMobile){
		this.customerMobile = customerMobile;
	}

	public String getCustomerMobile(){
		return customerMobile;
	}

	public void setSaleRate(String saleRate){
		this.saleRate = saleRate;
	}

	public String getSaleRate(){
		return saleRate;
	}

	public void setProductSubcategoryId(String productSubcategoryId){
		this.productSubcategoryId = productSubcategoryId;
	}

	public String getProductSubcategoryId(){
		return productSubcategoryId;
	}

	public void setCompanyId(String companyId){
		this.companyId = companyId;
	}

	public String getCompanyId(){
		return companyId;
	}

	public void setCatName(String catName){
		this.catName = catName;
	}

	public String getCatName(){
		return catName;
	}

	public void setBookingProdId(String bookingProdId){
		this.bookingProdId = bookingProdId;
	}

	public String getBookingProdId(){
		return bookingProdId;
	}

	public void setTotalRent(String totalRent){
		this.totalRent = totalRent;
	}

	public String getTotalRent(){
		return totalRent;
	}

	public void setSubCatId(String subCatId){
		this.subCatId = subCatId;
	}

	public String getSubCatId(){
		return subCatId;
	}

	public void setProductImgUrl1(String productImgUrl1){
		this.productImgUrl1 = productImgUrl1;
	}

	public String getProductImgUrl1(){
		return productImgUrl1;
	}

	public void setBookingStatus(String bookingStatus){
		this.bookingStatus = bookingStatus;
	}

	public String getBookingStatus(){
		return bookingStatus;
	}

	public void setProductImgUrl2(String productImgUrl2){
		this.productImgUrl2 = productImgUrl2;
	}

	public String getProductImgUrl2(){
		return productImgUrl2;
	}

	public void setProductName(String productName){
		this.productName = productName;
	}

	public String getProductName(){
		return productName;
	}

	public void setPendingAmount(String pendingAmount){
		this.pendingAmount = pendingAmount;
	}

	public String getPendingAmount(){
		return pendingAmount;
	}

	public void setMonthlyRate(String monthlyRate){
		this.monthlyRate = monthlyRate;
	}

	public String getMonthlyRate(){
		return monthlyRate;
	}

	public void setProductImgUrl3(String productImgUrl3){
		this.productImgUrl3 = productImgUrl3;
	}

	public String getProductImgUrl3(){
		return productImgUrl3;
	}

	public void setProductImgUrl4(String productImgUrl4){
		this.productImgUrl4 = productImgUrl4;
	}

	public String getProductImgUrl4(){
		return productImgUrl4;
	}

	public void setTotalAmount(String totalAmount){
		this.totalAmount = totalAmount;
	}

	public String getTotalAmount(){
		return totalAmount;
	}

	public void setSubCatName(String subCatName){
		this.subCatName = subCatName;
	}

	public String getSubCatName(){
		return subCatName;
	}

	public void setCustomerEmail(String customerEmail){
		this.customerEmail = customerEmail;
	}

	public String getCustomerEmail(){
		return customerEmail;
	}

	public void setCompanyName(String companyName){
		this.companyName = companyName;
	}

	public String getCompanyName(){
		return companyName;
	}

	public void setPaidAmount(String paidAmount){
		this.paidAmount = paidAmount;
	}

	public String getPaidAmount(){
		return paidAmount;
	}

	public void setRentAdvance(String rentAdvance){
		this.rentAdvance = rentAdvance;
	}

	public String getRentAdvance(){
		return rentAdvance;
	}

	public void setBookingType(String bookingType){
		this.bookingType = bookingType;
	}

	public String getBookingType(){
		return bookingType;
	}

	public void setCustomerName(String customerName){
		this.customerName = customerName;
	}

	public String getCustomerName(){
		return customerName;
	}

	public void setProductDetails(String productDetails){
		this.productDetails = productDetails;
	}

	public String getProductDetails(){
		return productDetails;
	}

	public void setCustomerId(String customerId){
		this.customerId = customerId;
	}

	public String getCustomerId(){
		return customerId;
	}

	public void setBookingYear(String bookingYear){
		this.bookingYear = bookingYear;
	}

	public String getBookingYear(){
		return bookingYear;
	}
}