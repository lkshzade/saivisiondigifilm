package com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem;

import com.google.gson.annotations.SerializedName;


public class BookedItemDataRequest {

	@SerializedName("booking_cust_id")
	private String bookingCustId;

	@SerializedName("booking_from_date")
	private String bookingFromDate;

	@SerializedName("booking_month")
	private String bookingMonth;

	@SerializedName("booking_year")
	private String bookingYear;

	@SerializedName("booking_type")
	private String bookingType;

	@SerializedName("month_rent")
	private String monthRent;

	@SerializedName("daily_rent")
	private String dailyRent;

	@SerializedName("product_status")
	private String productStatus;

	@SerializedName("booking_status")
	private String bookingStatus;

	@SerializedName("sale_rate")
	private String saleRate;

	@SerializedName("sale_date")
	private String saleDate;

	@SerializedName("rent_advance")
	private String rentAdvance;

	@SerializedName("rent_pending")
	private String rentPending;

	@SerializedName("product_quantity")
	private String productQuantity;

	@SerializedName("product_category_id")
	private String productCategoryId;

	@SerializedName("booking_id")
	private String bookingId;

	@SerializedName("product_in_stock")
	private String productInStock;

	@SerializedName("product_id")
	private String productId;

	@SerializedName("product_compny_id")
	private String productCompnyId;

	@SerializedName("cat_id")
	private String catId;

	@SerializedName("days_count")
	private String daysCount;

	@SerializedName("product_img_url1")
	private String productImgUrl1;

	@SerializedName("product_img_url2")
	private String productImgUrl2;

	@SerializedName("product_img_url3")
	private String productImgUrl3;

	@SerializedName("product_img_url4")
	private String productImgUrl4;

	@SerializedName("booking_to_date")
	private String bookingToDate;

	@SerializedName("customer_mobile")
	private String customerMobile;

	@SerializedName("product_subcategory_id")
	private String productSubcategoryId;

	@SerializedName("company_id")
	private String companyId;

	@SerializedName("cat_name")
	private String catName;

	@SerializedName("booking_prod_id")
	private String bookingProdId;

	@SerializedName("total_rent")
	private String totalRent;

	@SerializedName("sub_cat_id")
	private String subCatId;

	@SerializedName("product_name")
	private String productName;

	@SerializedName("sub_cat_name")
	private String subCatName;

	@SerializedName("customer_email")
	private String customerEmail;

	@SerializedName("company_name")
	private String companyName;

	@SerializedName("customer_name")
	private String customerName;

	@SerializedName("product_details")
	private String productDetails;

	@SerializedName("customer_id")
	private String customerId;

	public void setBookingCustId(String bookingCustId){
		this.bookingCustId = bookingCustId;
	}

	public String getBookingCustId(){
		return bookingCustId;
	}

	public void setBookingFromDate(String bookingFromDate){
		this.bookingFromDate = bookingFromDate;
	}

	public String getBookingFromDate(){
		return bookingFromDate;
	}

	public void setProductQuantity(String productQuantity){
		this.productQuantity = productQuantity;
	}

	public String getProductQuantity(){
		return productQuantity;
	}

	public void setProductCategoryId(String productCategoryId){
		this.productCategoryId = productCategoryId;
	}

	public String getProductCategoryId(){
		return productCategoryId;
	}

	public void setBookingId(String bookingId){
		this.bookingId = bookingId;
	}

	public String getBookingId(){
		return bookingId;
	}

	public void setProductInStock(String productInStock){
		this.productInStock = productInStock;
	}

	public String getProductInStock(){
		return productInStock;
	}

	public void setProductId(String productId){
		this.productId = productId;
	}

	public String getProductId(){
		return productId;
	}

	public void setProductCompnyId(String productCompnyId){
		this.productCompnyId = productCompnyId;
	}

	public String getProductCompnyId(){
		return productCompnyId;
	}

	public void setCatId(String catId){
		this.catId = catId;
	}

	public String getCatId(){
		return catId;
	}

	public void setDaysCount(String daysCount){
		this.daysCount = daysCount;
	}

	public String getDaysCount(){
		return daysCount;
	}

	public String getProductImgUrl1() {
		return productImgUrl1;
	}

	public void setProductImgUrl1(String productImgUrl1) {
		this.productImgUrl1 = productImgUrl1;
	}

	public String getProductImgUrl2() {
		return productImgUrl2;
	}

	public void setProductImgUrl2(String productImgUrl2) {
		this.productImgUrl2 = productImgUrl2;
	}

	public String getProductImgUrl3() {
		return productImgUrl3;
	}

	public void setProductImgUrl3(String productImgUrl3) {
		this.productImgUrl3 = productImgUrl3;
	}

	public String getProductImgUrl4() {
		return productImgUrl4;
	}

	public void setProductImgUrl4(String productImgUrl4) {
		this.productImgUrl4 = productImgUrl4;
	}

	public void setBookingToDate(String bookingToDate){
		this.bookingToDate = bookingToDate;
	}

	public String getBookingToDate(){
		return bookingToDate;
	}

	public void setCustomerMobile(String customerMobile){
		this.customerMobile = customerMobile;
	}

	public String getCustomerMobile(){
		return customerMobile;
	}

	public void setProductSubcategoryId(String productSubcategoryId){
		this.productSubcategoryId = productSubcategoryId;
	}

	public String getProductSubcategoryId(){
		return productSubcategoryId;
	}

	public void setCompanyId(String companyId){
		this.companyId = companyId;
	}

	public String getCompanyId(){
		return companyId;
	}

	public void setCatName(String catName){
		this.catName = catName;
	}

	public String getCatName(){
		return catName;
	}

	public void setBookingProdId(String bookingProdId){
		this.bookingProdId = bookingProdId;
	}

	public String getBookingProdId(){
		return bookingProdId;
	}

	public void setTotalRent(String totalRent){
		this.totalRent = totalRent;
	}

	public String getTotalRent(){
		return totalRent;
	}

	public void setSubCatId(String subCatId){
		this.subCatId = subCatId;
	}

	public String getSubCatId(){
		return subCatId;
	}

	public void setProductName(String productName){
		this.productName = productName;
	}

	public String getProductName(){
		return productName;
	}

	public void setSubCatName(String subCatName){
		this.subCatName = subCatName;
	}

	public String getSubCatName(){
		return subCatName;
	}

	public void setCustomerEmail(String customerEmail){
		this.customerEmail = customerEmail;
	}

	public String getCustomerEmail(){
		return customerEmail;
	}

	public void setCompanyName(String companyName){
		this.companyName = companyName;
	}

	public String getCompanyName(){
		return companyName;
	}

	public void setCustomerName(String customerName){
		this.customerName = customerName;
	}

	public String getCustomerName(){
		return customerName;
	}

	public void setProductDetails(String productDetails){
		this.productDetails = productDetails;
	}

	public String getProductDetails(){
		return productDetails;
	}

	public void setCustomerId(String customerId){
		this.customerId = customerId;
	}

	public String getCustomerId(){
		return customerId;
	}

	public String getBookingMonth() {
		return bookingMonth;
	}

	public void setBookingMonth(String bookingMonth) {
		this.bookingMonth = bookingMonth;
	}

	public String getBookingYear() {
		return bookingYear;
	}

	public void setBookingYear(String bookingYear) {
		this.bookingYear = bookingYear;
	}

	public String getBookingType() {
		return bookingType;
	}

	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}

	public String getMonthRent() {
		return monthRent;
	}

	public void setMonthRent(String monthRent) {
		this.monthRent = monthRent;
	}

	public String getDailyRent() {
		return dailyRent;
	}

	public void setDailyRent(String dailyRent) {
		this.dailyRent = dailyRent;
	}

	public String getRentAdvance() {
		return rentAdvance;
	}

	public void setRentAdvance(String rentAdvance) {
		this.rentAdvance = rentAdvance;
	}

	public String getRentPending() {
		return rentPending;
	}

	public void setRentPending(String rentPending) {
		this.rentPending = rentPending;
	}

	public String getBookingStatus() {
		return bookingStatus;
	}

	public void setBookingStatus(String bookingStatus) {
		this.bookingStatus = bookingStatus;
	}

	public String getSaleRate() {
		return saleRate;
	}

	public void setSaleRate(String saleRate) {
		this.saleRate = saleRate;
	}

	public String getSaleDate() {
		return saleDate;
	}

	public void setSaleDate(String saleDate) {
		this.saleDate = saleDate;
	}

	public String getProductStatus() {
		return productStatus;
	}

	public void setProductStatus(String productStatus) {
		this.productStatus = productStatus;
	}
}