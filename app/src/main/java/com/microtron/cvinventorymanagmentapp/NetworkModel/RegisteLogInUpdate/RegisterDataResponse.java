package com.microtron.cvinventorymanagmentapp.NetworkModel.RegisteLogInUpdate;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class RegisterDataResponse{

	@SerializedName("error_msg")
	private String errorMsg;

	@SerializedName("registerData")
	private List<RegisterDataItem> registerData;

	@SerializedName("success")
	private String success;

	public void setErrorMsg(String errorMsg){
		this.errorMsg = errorMsg;
	}

	public String getErrorMsg(){
		return errorMsg;
	}

	public void setRegisterData(List<RegisterDataItem> registerData){
		this.registerData = registerData;
	}

	public List<RegisterDataItem> getRegisterData(){
		return registerData;
	}

	public void setSuccess(String success){
		this.success = success;
	}

	public String getSuccess(){
		return success;
	}
}