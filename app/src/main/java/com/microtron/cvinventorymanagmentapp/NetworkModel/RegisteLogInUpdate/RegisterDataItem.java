package com.microtron.cvinventorymanagmentapp.NetworkModel.RegisteLogInUpdate;


import com.google.gson.annotations.SerializedName;

public class RegisterDataItem{

	@SerializedName("admin_shopName")
	private String adminShopName;

	@SerializedName("admin_id")
	private String adminId;

	@SerializedName("admin_city")
	private String adminCity;

	@SerializedName("admin_mobile")
	private String adminMobile;

	@SerializedName("admin_name")
	private String adminName;

	@SerializedName("admin_address")
	private String adminAddress;

	@SerializedName("admin_shop_Type")
	private String adminShopType;

	@SerializedName("admin_password")
	private String adminPassword;

	@SerializedName("admin_email")
	private String adminEmail;

	@SerializedName("admin_imgURL")
	private String adminImgURL;

	public void setAdminShopName(String adminShopName){
		this.adminShopName = adminShopName;
	}

	public String getAdminShopName(){
		return adminShopName;
	}

	public void setAdminId(String adminId){
		this.adminId = adminId;
	}

	public String getAdminId(){
		return adminId;
	}

	public void setAdminCity(String adminCity){
		this.adminCity = adminCity;
	}

	public String getAdminCity(){
		return adminCity;
	}

	public void setAdminMobile(String adminMobile){
		this.adminMobile = adminMobile;
	}

	public String getAdminMobile(){
		return adminMobile;
	}

	public void setAdminName(String adminName){
		this.adminName = adminName;
	}

	public String getAdminName(){
		return adminName;
	}

	public void setAdminAddress(String adminAddress){
		this.adminAddress = adminAddress;
	}

	public String getAdminAddress(){
		return adminAddress;
	}

	public void setAdminShopType(String adminShopType){
		this.adminShopType = adminShopType;
	}

	public String getAdminShopType(){
		return adminShopType;
	}

	public void setAdminPassword(String adminPassword){
		this.adminPassword = adminPassword;
	}

	public String getAdminPassword(){
		return adminPassword;
	}

	public void setAdminEmail(String adminEmail){
		this.adminEmail = adminEmail;
	}

	public String getAdminEmail(){
		return adminEmail;
	}

	public String getAdminImgURL() {
		return adminImgURL;
	}

	public void setAdminImgURL(String adminImgURL) {
		this.adminImgURL = adminImgURL;
	}
}