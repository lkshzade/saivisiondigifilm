package com.microtron.cvinventorymanagmentapp.NetworkModel.Comapny;

import com.google.gson.annotations.SerializedName;

public class CompanyDataItem{

	@SerializedName("company_id")
	private String companyId;

	@SerializedName("company_name")
	private String companyName;

	public void setCompanyId(String companyId){
		this.companyId = companyId;
	}

	public String getCompanyId(){
		return companyId;
	}

	public void setCompanyName(String companyName){
		this.companyName = companyName;
	}

	public String getCompanyName(){
		return companyName;
	}
}