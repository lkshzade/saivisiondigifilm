package com.microtron.cvinventorymanagmentapp.NetworkModel.AddItem;

import com.google.gson.annotations.SerializedName;


public class AddItemDataRequest {

	@SerializedName("product_in_stock")
	private String productInStock;

	@SerializedName("product_subcategory_id")
	private String productSubcategoryId;

	@SerializedName("product_id")
	private String productId;

	@SerializedName("product_compny_id")
	private String productCompnyId;

	@SerializedName("product_details")
	private String productDetails;

	@SerializedName("product_name")
	private String productName;

	@SerializedName("product_quantity")
	private String productQuantity;

	@SerializedName("product_category_id")
	private String productCategoryId;

	@SerializedName("product_img_url1")
	private String productImgUrl1;

	@SerializedName("product_img_url2")
	private String productImgUrl2;

	@SerializedName("product_img_url3")
	private String productImgUrl3;

	@SerializedName("product_img_url4")
	private String productImgUrl4;

	public void setProductInStock(String productInStock){
		this.productInStock = productInStock;
	}

	public String getProductInStock(){
		return productInStock;
	}

	public void setProductSubcategoryId(String productSubcategoryId){
		this.productSubcategoryId = productSubcategoryId;
	}

	public String getProductSubcategoryId(){
		return productSubcategoryId;
	}

	public void setProductId(String productId){
		this.productId = productId;
	}

	public String getProductId(){
		return productId;
	}

	public void setProductCompnyId(String productCompnyId){
		this.productCompnyId = productCompnyId;
	}

	public String getProductCompnyId(){
		return productCompnyId;
	}

	public void setProductDetails(String productDetails){
		this.productDetails = productDetails;
	}

	public String getProductDetails(){
		return productDetails;
	}

	public void setProductName(String productName){
		this.productName = productName;
	}

	public String getProductName(){
		return productName;
	}

	public void setProductQuantity(String productQuantity){
		this.productQuantity = productQuantity;
	}

	public String getProductQuantity(){
		return productQuantity;
	}

	public void setProductCategoryId(String productCategoryId){
		this.productCategoryId = productCategoryId;
	}

	public String getProductCategoryId(){
		return productCategoryId;
	}

	public String getProductImgUrl1() {
		return productImgUrl1;
	}

	public void setProductImgUrl1(String productImgUrl1) {
		this.productImgUrl1 = productImgUrl1;
	}

	public String getProductImgUrl2() {
		return productImgUrl2;
	}

	public void setProductImgUrl2(String productImgUrl2) {
		this.productImgUrl2 = productImgUrl2;
	}

	public String getProductImgUrl3() {
		return productImgUrl3;
	}

	public void setProductImgUrl3(String productImgUrl3) {
		this.productImgUrl3 = productImgUrl3;
	}

	public String getProductImgUrl4() {
		return productImgUrl4;
	}

	public void setProductImgUrl4(String productImgUrl4) {
		this.productImgUrl4 = productImgUrl4;
	}
}