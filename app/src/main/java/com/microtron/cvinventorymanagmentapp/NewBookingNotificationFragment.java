package com.microtron.cvinventorymanagmentapp;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;

import com.microtron.cvinventorymanagmentapp.Adapters.NotificationListAdapter;
import com.microtron.cvinventorymanagmentapp.Adapters.OutSideProductListAdapter;
import com.microtron.cvinventorymanagmentapp.Adapters.RecyclerItemClickListenerNotificationList;
import com.microtron.cvinventorymanagmentapp.Adapters.RecyclerItemClickListenerRentList;
import com.microtron.cvinventorymanagmentapp.Helper.ApiClient;
import com.microtron.cvinventorymanagmentapp.Helper.AppConstants;
import com.microtron.cvinventorymanagmentapp.Helper.Sp;
import com.microtron.cvinventorymanagmentapp.LogInSignUpSplash.LoginActivity;
import com.microtron.cvinventorymanagmentapp.NetworkModel.NotificationData.NotificationDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.NotificationData.NotificationDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.NotificationData.NotificationDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.OutSideInventoryBooking.OutSideBookingDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.OutSideInventoryBooking.OutSideBookingDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.OutSideInventoryBooking.OutSideBookingDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataItem;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewBookingNotificationFragment extends Fragment {

    private ProgressDialog addDialog;
    private Context context;
    private Activity activity;
    private RecyclerView recyclerNewBookingNotificationList;
    private ImageView imgNoProduct;
    private NotificationListAdapter notificationListAdapter;


    public NewBookingNotificationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_booking, container, false);
        context = getContext();
        activity = (Activity) context;

        recyclerNewBookingNotificationList = view.findViewById(R.id.recyclerNewBookingNotificationList);
        imgNoProduct = view.findViewById(R.id.imgNoProduct);

        imgNoProduct.setVisibility(View.GONE);

        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(activity, resId);
        recyclerNewBookingNotificationList.setLayoutAnimation(animation);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(activity);
        recyclerNewBookingNotificationList.setLayoutManager(layoutManager);

        getNewBookingNotification();


        /*Intent intent = new Intent(activity, RentProductDetailsActivity.class);
        activity.startActivity(intent);
        activity.overridePendingTransition(0,0);*/

        return view;
    }

    private void getNewBookingNotification() {

        addDialog = new ProgressDialog(activity);
        addDialog.setCancelable(true);
        addDialog.setMessage("Getting Notification...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        try{
            final NotificationDataRequest notificationRequest = new NotificationDataRequest();
            notificationRequest.setNotificationTypeStatus("1");


            Call<NotificationDataResponse> responceCall = ApiClient.getApiClient().getNotification(notificationRequest);

            responceCall.enqueue(new Callback<NotificationDataResponse>() {
                @Override
                public void onResponse(Call<NotificationDataResponse> call, retrofit2.Response<NotificationDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        List<NotificationDataItem> items =response.body().getNotificationData();

                        imgNoProduct.setVisibility(View.GONE);
                        Sp.saveToPreferences(activity, AppConstants.NOTIFY_FROM, "Booking Date");
                        notificationListAdapter = new NotificationListAdapter(activity, items, recyclerItemClickListener);
                        recyclerNewBookingNotificationList.setAdapter(notificationListAdapter);
                        notificationListAdapter.notifyDataSetChanged();

                        addDialog.dismiss();


                    } else {

                        //Toast.makeText(RentListActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                        imgNoProduct.setVisibility(View.VISIBLE);
                        addDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<NotificationDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialog.dismiss();

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    private RecyclerItemClickListenerNotificationList recyclerItemClickListener = new RecyclerItemClickListenerNotificationList() {
        @Override
        public void onItemClick(final NotificationDataItem notiItem) {
        }
    };

}
