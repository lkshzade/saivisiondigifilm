package com.microtron.cvinventorymanagmentapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.microtron.cvinventorymanagmentapp.Helper.ApiClient;
import com.microtron.cvinventorymanagmentapp.NetworkModel.GetScanProduct.GetScanProductDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.GetScanProduct.GetScanProductDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.GetScanProduct.GetScanProductDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.SubCategory.SubCategoryDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.SubCategory.SubCategoryDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.SubCategory.SubCategoryDataResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class ScannerActivity extends AppCompatActivity {

    TextView txtScanResult;
    private BottomNavigationView bottomNavigation;
    /*private String qrScannedProductID;
    private String strCompanyName, strCategoryName, strSubCategoryName, strProductName;*/
    private ProgressDialog addDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);

        txtScanResult=findViewById(R.id.txtScanResult);

        IntentIntegrator integrator = new IntentIntegrator(ScannerActivity.this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt("Scanning.....");
        integrator.setCameraId(0);
        integrator.setOrientationLocked(true);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        integrator.setCaptureActivity(CaptureActivityPortrait.class);
        integrator.initiateScan();


        /*bottomNavigation = findViewById(R.id.bottomNavigation);
        bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.bottomQRScanner:

                        return true;
                    case R.id.bottomRentList:
                        Toast.makeText(ScannerActivity.this, "Rent List", Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.bottomAddItem:
                        Toast.makeText(ScannerActivity.this, "Add Item", Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.bottomCalender:
                        Toast.makeText(ScannerActivity.this, "Calender", Toast.LENGTH_SHORT).show();
                        return true;
                }

                return false;
            }
        });
*/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
                Log.e("Scan*******", "Cancelled scan");

            } else {
                Log.e("Scan", "Scanned");

                //tv_qr_readTxt.setText(result.getContents());
                if (result.getContents().equals("")){

                    txtScanResult.setText("Details Not Available");
                }else {
                    txtScanResult.setText(result.getContents());
                    /*qrScannedProductID = result.getContents();
                    getScanProduct();*/
                }

            }
        } else {
            // This is important, otherwise the result will not be passed to the fragment
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

   /* private void getScanProduct() {

        addDialog = new ProgressDialog(this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Please Wait...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        try{
            GetScanProductDataRequest getScanProductRequest = new GetScanProductDataRequest();
            getScanProductRequest.setProductId(qrScannedProductID);

            Call<GetScanProductDataResponse> responceCall = ApiClient.getApiClient().getScanProduct(getScanProductRequest);

            responceCall.enqueue(new Callback<GetScanProductDataResponse>() {
                @Override
                public void onResponse(Call<GetScanProductDataResponse> call, retrofit2.Response<GetScanProductDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        addDialog.dismiss();
                        List<GetScanProductDataItem> items =response.body().getGetScanProductData();

                        strCompanyName = items.get(0).getCompanyName();
                        strCategoryName = items.get(0).getCatName();
                        strSubCategoryName = items.get(0).getSubCatName();
                        strProductName = items.get(0).getProductName();

                        Toast.makeText(ScannerActivity.this, "Company Name = "+strCompanyName+" Category Name = "+strCategoryName+" SubCategory Name = "+strSubCategoryName+" Product Name = "+strProductName, Toast.LENGTH_SHORT).show();


                    } else {

                        Toast.makeText(ScannerActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                        addDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<GetScanProductDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialog.dismiss();

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }*/
}
