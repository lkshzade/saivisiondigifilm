package com.microtron.cvinventorymanagmentapp;

import android.Manifest;
import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.microtron.cvinventorymanagmentapp.Helper.ApiClient;
import com.microtron.cvinventorymanagmentapp.Helper.AppConstants;
import com.microtron.cvinventorymanagmentapp.Helper.ConfigFile;
import com.microtron.cvinventorymanagmentapp.Helper.Sp;
import com.microtron.cvinventorymanagmentapp.NetworkModel.RegisteLogInUpdate.RegisterDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.RegisteLogInUpdate.RegisterDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.RegisteLogInUpdate.RegisterDataResponse;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;

public class ProfileActivity extends AppCompatActivity {

    private String strSession, strID, strName, strMobile, strEmail, strPass, strAddress, strShopName, strShoptype, strImgURL;

    public final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9+._%-+]{1,256}" +
                    "@" +
                    "[a-zA-Z0-9][a-zA-Z0-9-]{0,64}" +
                    "(" +
                    "." +
                    "[a-zA-Z0-9][a-zA-Z0-9-]{0,25}" +
                    ")+"
    );

    private EditText edName, edEmail, edPhoneNo;
    private Button btnProceed, btnChooseImage;
    private CircularImageView profile_image;
    private ImageView imgBack;
    private LinearLayout capture, gallery;
    private final int requestCode = 20;
    private final int GALLERY = 1;

    private String strChangedName, strChangedEmail, image, type;

    JSONObject jsonObject;
    RequestQueue rQueue;
    Bitmap bitmap;
    private String upload_URL = ConfigFile.domain + ConfigFile.domaindir + ConfigFile.uploadAdminProfileImg;
    ProgressDialog addDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        strSession = Sp.readFromPreferences(this, AppConstants.ADMIN_SESSION,"");

        edName = findViewById(R.id.edName);
        edEmail = findViewById(R.id.edEmail);
        edPhoneNo = findViewById(R.id.edPhoneNo);
        btnChooseImage = findViewById(R.id.btnChooseImage);
        btnProceed = findViewById(R.id.btnProceed);
        profile_image = findViewById(R.id.profile_image);
        imgBack = findViewById(R.id.imgBack);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        if(strSession.equals("OK")){

            strID = Sp.readFromPreferences(this, AppConstants.ADMIN_ID,"");
            strName = Sp.readFromPreferences(this, AppConstants.ADMIN_NAME,"");
            strMobile = Sp.readFromPreferences(this, AppConstants.ADMIN_MOBILE,"");
            strEmail = Sp.readFromPreferences(this, AppConstants.ADMIN_EMAIL,"");
            strPass = Sp.readFromPreferences(this, AppConstants.ADMIN_PASSWORD,"");
            strAddress = Sp.readFromPreferences(this, AppConstants.ADMIN_ADDRESS,"");
            strShopName = Sp.readFromPreferences(this, AppConstants .ADMIN_SHOPNAME,"");
            strShoptype = Sp.readFromPreferences(this, AppConstants.ADMIN_SHOPTYPE,"");
            strImgURL = Sp.readFromPreferences(this, AppConstants.ADMIN_IMGURL,"");

            edName.setText(strName);
            edEmail.setText(strEmail);
            edPhoneNo.setText(strMobile);
            edPhoneNo.setEnabled(false);

            Picasso.get()
                    .load(ConfigFile.domain+ConfigFile.domaindir+"uploads/profile/"+strImgURL+".jpg")
                    .error(R.drawable.no_image)
                    .resize(400,400)
                    .onlyScaleDown()
                    .into(profile_image);


            btnChooseImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Dexter.withActivity(ProfileActivity.this)
                            .withPermissions(
                                    Manifest.permission.READ_EXTERNAL_STORAGE,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    Manifest.permission.CAMERA)
                            .withListener(new MultiplePermissionsListener() {
                                @Override
                                public void onPermissionsChecked(MultiplePermissionsReport report) {
                                    // check if all permissions are granted
                                    if (report.areAllPermissionsGranted()) {
                                        // Toast.makeText(getApplicationContext(), "All permissions are granted!", Toast.LENGTH_SHORT).show();

                                        final BottomSheetDialog dialog = new BottomSheetDialog(ProfileActivity.this);
                                        dialog.setContentView(R.layout.popup_layout);
                                        dialog.setTitle("Select Image...");

                                        capture = dialog.findViewById(R.id.dialogImgView1);
                                        gallery = dialog.findViewById(R.id.dialogImgView2);

                                        capture.setOnClickListener(new View.OnClickListener() {
                                            @RequiresApi(api = Build.VERSION_CODES.M)
                                            @Override
                                            public void onClick(View v) {

                                                SharedPreferences sharedPreferences = getSharedPreferences("imgNo", MODE_PRIVATE);
                                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                                editor.putString("img", strImgURL);
                                                editor.putString("type", "capture");
                                                editor.apply();

                                                Intent photoCaptureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                                startActivityForResult(photoCaptureIntent, requestCode);
                                                dialog.dismiss();
                                            }
                                        });

                                        gallery.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                                SharedPreferences sharedPreferences = getSharedPreferences("imgNo", MODE_PRIVATE);
                                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                                editor.putString("img", strImgURL);
                                                editor.putString("type", "gallery");
                                                editor.apply();

                                                Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                                                startActivityForResult(galleryIntent, GALLERY);
                                                dialog.dismiss();
                                            }
                                        });

                                        dialog.show();
                                        Window window = dialog.getWindow();
                                        window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);


                                    }

                                    // check for permanent denial of any permission
                                    if (report.isAnyPermissionPermanentlyDenied()) {
                                        // show alert dialog navigating to Settings
                                        showSettingsDialog();
                                    }
                                }

                                @Override
                                public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                                    token.continuePermissionRequest();
                                }
                            }).
                            withErrorListener(new PermissionRequestErrorListener() {
                                @Override
                                public void onError(DexterError error) {
                                    //Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .onSameThread()
                            .check();

                }
            });

            btnProceed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    strChangedName = edName.getText().toString().trim();
                    strChangedEmail = edEmail.getText().toString().trim();

                    if (!checkEmail(strChangedEmail)) {

                        edEmail.setError("Email Should contains @ and . symbols");
                        //Toast.makeText(ProfileActivity.this, "Enter Email properly", Toast.LENGTH_SHORT).show();

                    }else if(strChangedName.length()<4){
                        edName.setError("Name Should contains More than 3 Character");
                    }else{
                        updateProfile();
                    }


                }
            });

        }
    }
    private boolean checkEmail(String email) {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }

    private void showSettingsDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    private void updateProfile() {

        addDialog = new ProgressDialog(this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Updating...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        try {

            RegisterDataRequest updateProfileRequest = new RegisterDataRequest();

            updateProfileRequest.setAdminName(strChangedName);
            updateProfileRequest.setAdminEmail(strChangedEmail);
            updateProfileRequest.setAdminId(strID);

            Call<RegisterDataResponse> responceCall = ApiClient.getApiClient().updateAdmin(updateProfileRequest);

            responceCall.enqueue(new Callback<RegisterDataResponse>() {
                @Override
                public void onResponse(Call<RegisterDataResponse> call, retrofit2.Response<RegisterDataResponse> response) {

                    if (response.body().getSuccess().equals("1")) {

                        List<RegisterDataItem> items = response.body().getRegisterData();
                        
                        Sp.saveToPreferences(ProfileActivity.this, AppConstants.ADMIN_ID, items.get(0).getAdminId());
                        Sp.saveToPreferences(ProfileActivity.this, AppConstants.ADMIN_NAME, items.get(0).getAdminName());
                        Sp.saveToPreferences(ProfileActivity.this, AppConstants.ADMIN_EMAIL, items.get(0).getAdminEmail());
                        Sp.saveToPreferences(ProfileActivity.this, AppConstants.ADMIN_MOBILE, items.get(0).getAdminMobile());
                        Sp.saveToPreferences(ProfileActivity.this, AppConstants.ADMIN_PASSWORD, items.get(0).getAdminPassword());
                        Sp.saveToPreferences(ProfileActivity.this, AppConstants.ADMIN_ADDRESS, items.get(0).getAdminAddress());
                        Sp.saveToPreferences(ProfileActivity.this, AppConstants.ADMIN_SHOPNAME, items.get(0).getAdminShopName());
                        Sp.saveToPreferences(ProfileActivity.this, AppConstants.ADMIN_SHOPTYPE, items.get(0).getAdminShopType());
                        Sp.saveToPreferences(ProfileActivity.this, AppConstants.ADMIN_IMGURL, items.get(0).getAdminImgURL());

                        Toast.makeText(ProfileActivity.this, "Updated Successfully", Toast.LENGTH_SHORT).show();
                        addDialog.dismiss();
                    }
                    else if (response.body().getErrorMsg().equals("1")) {

                        addDialog.dismiss();
                        Toast.makeText(ProfileActivity.this, "Failed", Toast.LENGTH_SHORT).show();

                    }
                    else {

                        addDialog.dismiss();
                        //Toast.makeText(ProductDetails.this, "Error :" + response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<RegisterDataResponse> call, Throwable t) {
                    addDialog.dismiss();
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(ProductDetails.this, "Error :" + t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });
        }
        catch (Exception e) {
            addDialog.dismiss();
            //Toast.makeText(ProductDetails.this, "Get Wish Exception", Toast.LENGTH_SHORT).show();
        }

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        SharedPreferences sharedPreferences = getSharedPreferences("imgNo", MODE_PRIVATE);
        image = sharedPreferences.getString("img", "");
        type = sharedPreferences.getString("type", "");

            if (type.equals("capture")) {

                super.onActivityResult(requestCode, resultCode, data);
                if (this.requestCode == requestCode && resultCode == RESULT_OK) {
                    Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                    profile_image.setImageBitmap(bitmap);
                    uploadImage(bitmap);
                }

            } else {
                super.onActivityResult(requestCode, resultCode, data);
                if (resultCode == this.RESULT_CANCELED) {
                    return;
                }
                if (requestCode == GALLERY) {
                    if (data != null) {
                        Uri contentURI = data.getData();
                        try {

                            bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                            profile_image.setImageBitmap(bitmap);
                            uploadImage(bitmap);

                        } catch (IOException e) {
                            e.printStackTrace();
                            //Toast.makeText(CustomerRequirmentsActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
    }

    private void uploadImage(Bitmap bitmap) {

        /*ProgressDialog addDialog;*/

        addDialog = new ProgressDialog(this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Uploading Image...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
        try {
            jsonObject = new JSONObject();
            String imgname = strImgURL;
            jsonObject.put("name", imgname);
            //  Log.e("Image name", etxtUpload.getText().toString().trim());
            jsonObject.put("image", encodedImage);
            // jsonObject.put("aa", "aa");
        } catch (JSONException e) {
            Log.e("JSONObject Here", e.toString());
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, upload_URL, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        Log.e("aaaaaaa", jsonObject.toString());
                        rQueue.getCache().clear();
                        //Toast.makeText(getApplication(), "Image Uploaded Successfully", Toast.LENGTH_SHORT).show();
                        addDialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("aaaaaaa", volleyError.toString());

            }
        });

        rQueue = Volley.newRequestQueue(ProfileActivity.this);
        rQueue.add(jsonObjectRequest);

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
        overridePendingTransition(0,0);
    }
}
