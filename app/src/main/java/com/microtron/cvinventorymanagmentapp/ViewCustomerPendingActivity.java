package com.microtron.cvinventorymanagmentapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;

import com.microtron.cvinventorymanagmentapp.Adapters.PendingAmountListAdapter;
import com.microtron.cvinventorymanagmentapp.Adapters.PendingCustomerListAdapter;
import com.microtron.cvinventorymanagmentapp.Adapters.RecyclerItemClickListenerPendingAmountList;
import com.microtron.cvinventorymanagmentapp.Helper.ApiClient;
import com.microtron.cvinventorymanagmentapp.Helper.AppConstants;
import com.microtron.cvinventorymanagmentapp.Helper.Sp;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PendingAmounts.PendingAmountDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PendingAmounts.PendingAmountDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.PendingAmounts.PendingAmountDataResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class ViewCustomerPendingActivity extends AppCompatActivity {

    private ImageView imgBack, imgNoProduct;
    private RecyclerView recyclerCustPendingAmountList;
    private ProgressDialog addDialog;
    private PendingAmountListAdapter pendingAmountListAdapter;
    private String strCustId, strCustName, strCustMobile, strCustEmail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_customer_pending);

        strCustId = Sp.readFromPreferences(ViewCustomerPendingActivity.this, AppConstants.CUST_ID,"");
        strCustName= Sp.readFromPreferences(ViewCustomerPendingActivity.this, AppConstants.CUST_NAME,"");
        strCustMobile = Sp.readFromPreferences(ViewCustomerPendingActivity.this, AppConstants.CUST_MOBILE,"");
        strCustEmail = Sp.readFromPreferences(ViewCustomerPendingActivity.this, AppConstants.CUST_EMAIL,"");

        imgBack = findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        imgNoProduct = findViewById(R.id.imgNoProduct);
        imgNoProduct.setVisibility(View.GONE);

        recyclerCustPendingAmountList = findViewById(R.id.recyclerCustPendingAmountList);

        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerCustPendingAmountList.setLayoutAnimation(animation);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerCustPendingAmountList.setLayoutManager(layoutManager);

        getPendingAmountList();

    }

    private void getPendingAmountList() {

        addDialog = new ProgressDialog(this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Getting List...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        try{
            PendingAmountDataRequest pendingAmountRequest = new PendingAmountDataRequest();
            pendingAmountRequest.setBookingCustId(strCustId);


            Call<PendingAmountDataResponse> responceCall = ApiClient.getApiClient().getPendingProducts(pendingAmountRequest);

            responceCall.enqueue(new Callback<PendingAmountDataResponse>() {
                @Override
                public void onResponse(Call<PendingAmountDataResponse> call, retrofit2.Response<PendingAmountDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        List<PendingAmountDataItem> items =response.body().getPendingAmountData();
                        imgNoProduct.setVisibility(View.GONE);

                        pendingAmountListAdapter = new PendingAmountListAdapter(ViewCustomerPendingActivity.this, items, recyclerItemClickListener);
                        recyclerCustPendingAmountList.setAdapter(pendingAmountListAdapter);
                        pendingAmountListAdapter.notifyDataSetChanged();

                        addDialog.dismiss();


                    } else {

                        //Toast.makeText(RentListActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                        imgNoProduct.setVisibility(View.VISIBLE);
                        addDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<PendingAmountDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialog.dismiss();

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    private RecyclerItemClickListenerPendingAmountList recyclerItemClickListener = new RecyclerItemClickListenerPendingAmountList() {
        @Override
        public void onItemClick(final PendingAmountDataItem notiItem) {
        }
    };

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, ViewPendingAmountActivity.class);
        startActivity(intent);
        overridePendingTransition(0,0);
        finish();
    }
}
