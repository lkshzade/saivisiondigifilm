package com.microtron.cvinventorymanagmentapp;

import android.Manifest;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.microtron.cvinventorymanagmentapp.Helper.ApiClient;
import com.microtron.cvinventorymanagmentapp.Helper.ConfigFile;
import com.microtron.cvinventorymanagmentapp.Helper.FileUtils;
import com.microtron.cvinventorymanagmentapp.NetworkModel.AddItem.AddItemDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.AddItem.AddItemDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.AddItem.AddItemDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.Category.CategoryDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.Category.CategoryDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.Category.CategoryDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.Comapny.CompanyDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.Comapny.CompanyDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.Comapny.CompanyDataResponse;
import com.microtron.cvinventorymanagmentapp.NetworkModel.SubCategory.SubCategoryDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.SubCategory.SubCategoryDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.SubCategory.SubCategoryDataResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class AddItemActivity extends AppCompatActivity {

    private EditText edtProductName, edtProductDescription, edtProductStockQuantity;
    private AutoCompleteTextView txtProductCompany, txtProductCategory, txtProductSubCategory;
    private TextView txtValidProductCompany, txtValidProductCategory, txtValidProductSubCategory, txtValidProductName, txtValidProductDescription, txtValidProductStockQuantity;
    private Spinner spInStock;
    private Button btnAddItem, btnAddCompany, btnAddCategory, btnAddSubCategory, btnChooseImages;
    private LinearLayout llCat, llSubCat, llProduct;
    private ImageView imgBack;
    private String strProductCompany, strProductCategory, strProductSubCategory, strProductName, strProductDesc, strProductInStock, strProductStockQuantity;


    private ArrayList<String> companyNameList = new ArrayList<String>();
    private ArrayList<String> companyIdList = new ArrayList<String>();
    private ArrayList<String> categoryNameList = new ArrayList<String>();
    private ArrayList<String> categoryIdList = new ArrayList<String>();
    private ArrayList<String> subCategoryNameList = new ArrayList<String>();
    private ArrayList<String> subCategoryIdList = new ArrayList<String>();
    private ArrayList<String> subCategoryCatIdList = new ArrayList<String>();

    private List<CompanyDataItem> companyItems;
    private List<CategoryDataItem> categoryItems;
    private List<SubCategoryDataItem> subCategoryItems;

    private String strCompanyId, strCompanyName, strCategoryId, strCategoryName, strSubCategoryId, strSubCategoryName, strFinalCatId, strFinalSubCatId;
    private String strFinalCompanyName, strFinalCompanyID, strFinalCategoryName, strFinalCategoryID, strFinalSubCategoryID, strFinalSubCategoryName, strFinalSubCategoryCatID;
    private ProgressDialog addDialog, addDialog1;
    private String qrProductID, qrProductName;
    Bitmap bitmap;
    int QRcodeWidth = 350;
    final static int REQUESTCODE_PERMISSION_SMS = 301;
    private ImageView imgProduct1, imgProduct2, imgProduct3, imgProduct4;
    private LinearLayout capture, gallery;
    private String strImgURL = "", strImgURL1 = "", strImgURL2 = "", strImgURL3 = "", strImgURL4 = "", image, type;
    private final int requestCode = 20;
    private final int GALLERY = 1;
    private String upload_URL = ConfigFile.domain + ConfigFile.domaindir + ConfigFile.uploadProductImages;
    JSONObject jsonObject;
    RequestQueue rQueue;
    Bitmap bitmap1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);

        txtProductCompany = findViewById(R.id.txtCompanyName);
        txtProductCategory = findViewById(R.id.txtCategory);
        txtProductSubCategory = findViewById(R.id.txtSubCategory);
        txtValidProductCompany = findViewById(R.id.txtValidCompany);
        txtValidProductCategory = findViewById(R.id.txtValidCategory);
        txtValidProductSubCategory = findViewById(R.id.txtValidSubCategory);
        imgBack = findViewById(R.id.imgBack);
        llSubCat = findViewById(R.id.llSubCategory);
        llCat = findViewById(R.id.llCategory);
        llProduct = findViewById(R.id.llProduct);
        btnAddCompany = findViewById(R.id.btnAddCompany);
        btnAddCategory = findViewById(R.id.btnAddCategory);
        btnAddSubCategory = findViewById(R.id.btnAddSubCategory);
        btnChooseImages = findViewById(R.id.btnChooseImage);

        imgProduct1 = findViewById(R.id.imgProduct1);
        imgProduct2 = findViewById(R.id.imgProduct2);
        imgProduct3 = findViewById(R.id.imgProduct3);
        imgProduct4 = findViewById(R.id.imgProduct4);

        imgProduct2.setVisibility(View.GONE);
        imgProduct3.setVisibility(View.GONE);
        imgProduct4.setVisibility(View.GONE);

        btnAddCompany.setVisibility(View.GONE);
        btnAddCategory.setVisibility(View.GONE);
        btnAddSubCategory.setVisibility(View.GONE);
        llProduct.setVisibility(View.GONE);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        edtProductName = findViewById(R.id.edtProductName);
        edtProductDescription = findViewById(R.id.edtProductDescription);
        edtProductStockQuantity = findViewById(R.id.edtProductStockQuantity);
        txtValidProductName = findViewById(R.id.txtValidProductName);
        txtValidProductDescription = findViewById(R.id.txtValidProductDescription);
        txtValidProductStockQuantity = findViewById(R.id.txtValidProductStockQuantity);

        spInStock = findViewById(R.id.spInStock);
        btnAddItem = findViewById(R.id.btnAddItem);

        getCompany();

        txtProductCompany.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddItemActivity.this, android.R.layout.select_dialog_item, companyNameList);
                txtProductCompany.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
                txtProductCompany.setTextColor(getResources().getColor(R.color.black));
                txtProductCompany.setThreshold(1);

                strProductCompany = txtProductCompany.getText().toString().trim();

                if(companyNameList.size()==0){

                    strCompanyName = txtProductCompany.getText().toString().trim();
                    btnAddCompany.setVisibility(View.VISIBLE);
                    llCat.setVisibility(View.GONE);
                    llSubCat.setVisibility(View.GONE);

                }
                else {

                    for (int j = 0; j < companyNameList.size(); j++) {

                        if (strProductCompany.equals(companyNameList.get(j).trim())) {

                            strFinalCompanyID = companyIdList.get(j);
                            strFinalCompanyName = companyNameList.get(j);
                            btnAddCompany.setVisibility(View.GONE);
                            llCat.setVisibility(View.VISIBLE);
                            llSubCat.setVisibility(View.VISIBLE);
                            //break;
                        } else {
                            strCompanyName = txtProductCompany.getText().toString().trim();
                            btnAddCompany.setVisibility(View.VISIBLE);
                            llCat.setVisibility(View.GONE);
                            llSubCat.setVisibility(View.GONE);

                        }
                    }
                }
            }
        });

        btnAddCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strProductCompany = txtProductCompany.getText().toString().trim();
                if (strProductCompany.length() < 3) {
                    txtProductCompany.setError("Please Enter Company Name First");
                } else {
                    addCompany();
                }
            }
        });

        getCategory();

        txtProductCategory.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddItemActivity.this, android.R.layout.select_dialog_item, categoryNameList);
                txtProductCategory.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
                txtProductCategory.setTextColor(getResources().getColor(R.color.black));
                txtProductCategory.setThreshold(1);

                strProductCategory = txtProductCategory.getText().toString().trim();

                if(categoryNameList.size() == 0){

                    strCategoryName = txtProductCategory.getText().toString().trim();
                    btnAddCategory.setVisibility(View.VISIBLE);
                    llSubCat.setVisibility(View.GONE);

                }else{

                    for (int j = 0; j < categoryNameList.size(); j++) {

                        if (strProductCategory.equals(categoryNameList.get(j).trim())) {

                            strFinalCategoryID = categoryIdList.get(j);
                            strFinalCategoryName = categoryNameList.get(j);
                            btnAddCategory.setVisibility(View.GONE);
                            llSubCat.setVisibility(View.VISIBLE);
                            getSubCategory();
                            break;
                        } else {
                            strCategoryName = txtProductCategory.getText().toString().trim();
                            btnAddCategory.setVisibility(View.VISIBLE);
                            llSubCat.setVisibility(View.GONE);
                        }

                    }

                }
            }
        });

        btnAddCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strProductCategory = txtProductCategory.getText().toString().trim();
                if (strProductCategory.length() < 3) {
                    txtProductCategory.setError("Plase Enter Proper Category Name");
                } else {
                    addCategory();
                }

            }
        });

        txtProductSubCategory.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                btnAddSubCategory.setVisibility(View.VISIBLE);
                llProduct.setVisibility(View.GONE);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddItemActivity.this, android.R.layout.select_dialog_item, subCategoryNameList);
                txtProductSubCategory.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
                txtProductSubCategory.setTextColor(getResources().getColor(R.color.black));
                txtProductSubCategory.setThreshold(1);

                strProductSubCategory = txtProductSubCategory.getText().toString().trim();

                if(subCategoryNameList.size() == 0){

                    strProductSubCategory = txtProductSubCategory.getText().toString().trim();
                    btnAddSubCategory.setVisibility(View.VISIBLE);

                }
                else{

                    for (int j = 0; j < subCategoryNameList.size(); j++) {

                        if (strProductSubCategory.equals(subCategoryNameList.get(j).trim())) {

                            strFinalSubCategoryName = subCategoryNameList.get(j);
                            strFinalSubCategoryID = subCategoryIdList.get(j);
                            strFinalSubCategoryCatID = subCategoryCatIdList.get(j);
                            btnAddSubCategory.setVisibility(View.GONE);
                            llProduct.setVisibility(View.VISIBLE);
                            break;
                        } else {
                            strProductSubCategory = txtProductSubCategory.getText().toString().trim();
                            btnAddSubCategory.setVisibility(View.VISIBLE);

                        }

                    }

                }
            }
        });

        btnAddSubCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strProductSubCategory = txtProductSubCategory.getText().toString().trim();
                if (strProductSubCategory.length() < 3) {
                    txtProductSubCategory.setError("Please Enter Proper SubCategory Name");
                } else {
                    addSubCategory();
                }

            }
        });

        spInStock.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Object item = parent.getItemAtPosition(position);
                strProductInStock = item.toString();

                if (strProductInStock.equals("No")) {

                    strProductStockQuantity = "0";
                    edtProductStockQuantity.setEnabled(false);
                    txtValidProductStockQuantity.setVisibility(View.GONE);
                } else {
                    edtProductStockQuantity.setEnabled(true);
                    txtValidProductStockQuantity.setVisibility(View.VISIBLE);
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        imgProduct1.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {

                Dexter.withActivity(AddItemActivity.this)
                        .withPermissions(
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.CAMERA)
                        .withListener(new MultiplePermissionsListener() {
                            @Override
                            public void onPermissionsChecked(MultiplePermissionsReport report) {
                                // check if all permissions are granted
                                if (report.areAllPermissionsGranted()) {

                                    strProductName = edtProductName.getText().toString().trim();

                                    if (strProductName.equals("")) {
                                        txtValidProductName.setVisibility(View.VISIBLE);
                                        edtProductName.setError("Please Enter Above Details First");

                                        Toast.makeText(AddItemActivity.this, "Please Enter Above Details First", Toast.LENGTH_SHORT).show();
                                    } else {

                                        strImgURL = strProductName + "_1";

                                        final BottomSheetDialog dialog = new BottomSheetDialog(AddItemActivity.this);
                                        dialog.setContentView(R.layout.popup_layout);
                                        dialog.setTitle("Select Image...");

                                        capture = dialog.findViewById(R.id.dialogImgView1);
                                        gallery = dialog.findViewById(R.id.dialogImgView2);

                                        capture.setOnClickListener(new View.OnClickListener() {
                                            @RequiresApi(api = Build.VERSION_CODES.M)
                                            @Override
                                            public void onClick(View v) {

                                                SharedPreferences sharedPreferences = getSharedPreferences("imgNo", MODE_PRIVATE);
                                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                                editor.putString("img", strImgURL);
                                                editor.putString("type", "capture");
                                                editor.apply();

                                                Intent photoCaptureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                                startActivityForResult(photoCaptureIntent, requestCode);
                                                dialog.dismiss();
                                            }
                                        });

                                        gallery.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                                SharedPreferences sharedPreferences = getSharedPreferences("imgNo", MODE_PRIVATE);
                                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                                editor.putString("img", strImgURL);
                                                editor.putString("type", "gallery");
                                                editor.apply();
                                                //Toast.makeText(AddItemActivity.this, "Gallery", Toast.LENGTH_SHORT).show();
                                                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                                                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                                                startActivityForResult(galleryIntent, GALLERY);
                                                dialog.dismiss();
                                            }
                                        });

                                        dialog.show();
                                        Window window = dialog.getWindow();
                                        window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                                    }
                                }

                                // check for permanent denial of any permission
                                if (report.isAnyPermissionPermanentlyDenied()) {
                                    // show alert dialog navigating to Settings
                                    showSettingsDialog();
                                }
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                                token.continuePermissionRequest();
                            }
                        }).
                        withErrorListener(new PermissionRequestErrorListener() {
                            @Override
                            public void onError(DexterError error) {
                                //Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .onSameThread()
                        .check();
            }
        });

        imgProduct2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strImgURL = strProductName + "_2";

                final BottomSheetDialog dialog = new BottomSheetDialog(AddItemActivity.this);
                dialog.setContentView(R.layout.popup_layout);
                dialog.setTitle("Select Image...");

                capture = dialog.findViewById(R.id.dialogImgView1);
                gallery = dialog.findViewById(R.id.dialogImgView2);

                capture.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        SharedPreferences sharedPreferences = getSharedPreferences("imgNo", MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("img", strImgURL);
                        editor.putString("type", "capture");
                        editor.apply();

                        Intent photoCaptureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(photoCaptureIntent, requestCode);
                        dialog.dismiss();

                    }
                });

                gallery.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Toast.makeText(AddItemActivity.this, "Gallery", Toast.LENGTH_SHORT).show();
                        SharedPreferences sharedPreferences = getSharedPreferences("imgNo", MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("img", strImgURL);
                        editor.putString("type", "gallery");
                        editor.apply();
                        //Toast.makeText(AddItemActivity.this, "Gallery", Toast.LENGTH_SHORT).show();
                        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                        startActivityForResult(galleryIntent, GALLERY);
                        dialog.dismiss();
                    }
                });

                dialog.show();
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

            }
        });

        imgProduct3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strImgURL = strProductName + "_3";

                final BottomSheetDialog dialog = new BottomSheetDialog(AddItemActivity.this);
                dialog.setContentView(R.layout.popup_layout);
                dialog.setTitle("Select Image...");

                capture = dialog.findViewById(R.id.dialogImgView1);
                gallery = dialog.findViewById(R.id.dialogImgView2);

                capture.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        SharedPreferences sharedPreferences = getSharedPreferences("imgNo", MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("img", strImgURL);
                        editor.putString("type", "capture");
                        editor.apply();

                        Intent photoCaptureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(photoCaptureIntent, requestCode);
                        dialog.dismiss();
                    }
                });

                gallery.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        SharedPreferences sharedPreferences = getSharedPreferences("imgNo", MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("img", strImgURL);
                        editor.putString("type", "gallery");
                        editor.apply();
                        //Toast.makeText(AddItemActivity.this, "Gallery", Toast.LENGTH_SHORT).show();
                        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                        startActivityForResult(galleryIntent, GALLERY);
                        dialog.dismiss();
                    }
                });

                dialog.show();
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                /*}*/
            }
        });

        imgProduct4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strImgURL = strProductName + "_4";

                final BottomSheetDialog dialog = new BottomSheetDialog(AddItemActivity.this);
                dialog.setContentView(R.layout.popup_layout);
                dialog.setTitle("Select Image...");

                capture = dialog.findViewById(R.id.dialogImgView1);
                gallery = dialog.findViewById(R.id.dialogImgView2);

                capture.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        SharedPreferences sharedPreferences = getSharedPreferences("imgNo", MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("img", strImgURL);
                        editor.putString("type", "capture");
                        editor.apply();

                        Intent photoCaptureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(photoCaptureIntent, requestCode);
                        dialog.dismiss();
                    }
                });

                gallery.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        SharedPreferences sharedPreferences = getSharedPreferences("imgNo", MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("img", strImgURL);
                        editor.putString("type", "gallery");
                        editor.apply();
                        //Toast.makeText(AddItemActivity.this, "Gallery", Toast.LENGTH_SHORT).show();
                        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                        startActivityForResult(galleryIntent, GALLERY);
                        dialog.dismiss();

                    }
                });

                dialog.show();
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                /* }*/
            }
        });


        btnAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strProductName = edtProductName.getText().toString().trim();
                strProductDesc = edtProductDescription.getText().toString().trim();
                strProductStockQuantity = edtProductStockQuantity.getText().toString().trim();
                strFinalSubCategoryName = txtProductSubCategory.getText().toString().trim();
                strFinalCompanyName = txtProductCompany.getText().toString().trim();
                strFinalCategoryName = txtProductCategory.getText().toString().trim();

                if (strFinalCompanyName.length() < 3) {
                    txtProductCompany.setError("Company Name Should be Greater Than 2 Character");
                    Toast.makeText(AddItemActivity.this, "Company Name Should be Greater Than 2 Character", Toast.LENGTH_SHORT).show();
                    txtValidProductCompany.setVisibility(View.VISIBLE);
                    txtValidProductCategory.setVisibility(View.GONE);
                    txtValidProductSubCategory.setVisibility(View.GONE);
                    txtValidProductName.setVisibility(View.GONE);
                    txtValidProductDescription.setVisibility(View.GONE);
                    txtValidProductStockQuantity.setVisibility(View.GONE);

                } else if (strFinalCategoryName.length() < 3) {
                    txtProductCategory.setError("Category Name Should be Greater Than 2 Character");
                    Toast.makeText(AddItemActivity.this, "Category Name Should be Greater Than 2 Character", Toast.LENGTH_SHORT).show();
                    txtValidProductCompany.setVisibility(View.GONE);
                    txtValidProductCategory.setVisibility(View.VISIBLE);
                    txtValidProductSubCategory.setVisibility(View.GONE);
                    txtValidProductName.setVisibility(View.GONE);
                    txtValidProductDescription.setVisibility(View.GONE);
                    txtValidProductStockQuantity.setVisibility(View.GONE);
                } else if (strFinalSubCategoryName.length() < 3) {
                    txtProductSubCategory.setError("Sub Category Name Should be Greater Than 2 Character");
                    Toast.makeText(AddItemActivity.this, "Sub Category Name Should be Greater Than 2 Character", Toast.LENGTH_SHORT).show();
                    txtValidProductCompany.setVisibility(View.GONE);
                    txtValidProductCategory.setVisibility(View.GONE);
                    txtValidProductSubCategory.setVisibility(View.VISIBLE);
                    txtValidProductName.setVisibility(View.GONE);
                    txtValidProductDescription.setVisibility(View.GONE);
                    txtValidProductStockQuantity.setVisibility(View.GONE);
                } else if (strProductName.length() < 3) {
                    txtProductCategory.setError("Product Name Should be Greater Than 2 Character");
                    Toast.makeText(AddItemActivity.this, "Product Name Should be Greater Than 2 Character", Toast.LENGTH_SHORT).show();
                    txtValidProductCompany.setVisibility(View.GONE);
                    txtValidProductCategory.setVisibility(View.GONE);
                    txtValidProductSubCategory.setVisibility(View.GONE);
                    txtValidProductName.setVisibility(View.VISIBLE);
                    txtValidProductDescription.setVisibility(View.GONE);
                    txtValidProductStockQuantity.setVisibility(View.GONE);
                } else if (strProductDesc.length() < 3) {
                    txtProductCategory.setError("Description Should be Greater Than 2 Character");
                    Toast.makeText(AddItemActivity.this, "Description Should be Greater Than 2 Character", Toast.LENGTH_SHORT).show();
                    txtValidProductCompany.setVisibility(View.GONE);
                    txtValidProductCategory.setVisibility(View.GONE);
                    txtValidProductSubCategory.setVisibility(View.GONE);
                    txtValidProductName.setVisibility(View.GONE);
                    txtValidProductDescription.setVisibility(View.VISIBLE);
                    txtValidProductStockQuantity.setVisibility(View.GONE);
                }
                else {

                    txtValidProductCompany.setVisibility(View.GONE);
                    txtValidProductCategory.setVisibility(View.GONE);
                    txtValidProductSubCategory.setVisibility(View.GONE);
                    txtValidProductName.setVisibility(View.GONE);
                    txtValidProductDescription.setVisibility(View.GONE);
                    txtValidProductStockQuantity.setVisibility(View.GONE);
                    addItem();

                }


            }
        });


    }

    private void addItem() {

        Dexter.withActivity(AddItemActivity.this)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {

                            addDialog = new ProgressDialog(AddItemActivity.this);
                            addDialog.setCancelable(true);
                            addDialog.setMessage("Adding Item...");
                            addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                            addDialog.show();

                            try {
                                AddItemDataRequest addItemRequest = new AddItemDataRequest();

                                addItemRequest.setProductCompnyId(strFinalCompanyID);
                                addItemRequest.setProductCategoryId(strFinalSubCategoryCatID);
                                addItemRequest.setProductSubcategoryId(strFinalSubCategoryID);
                                addItemRequest.setProductName(strProductName);
                                addItemRequest.setProductDetails(strProductDesc);
                                addItemRequest.setProductInStock(strProductInStock);
                                addItemRequest.setProductQuantity(strProductStockQuantity);
                                addItemRequest.setProductImgUrl1(strProductName + "_1");
                                addItemRequest.setProductImgUrl2(strProductName + "_2");
                                addItemRequest.setProductImgUrl3(strProductName + "_3");
                                addItemRequest.setProductImgUrl4(strProductName + "_4");


                                Call<AddItemDataResponse> responceCall = ApiClient.getApiClient().addItem(addItemRequest);

                                responceCall.enqueue(new Callback<AddItemDataResponse>() {
                                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                                    @Override
                                    public void onResponse(Call<AddItemDataResponse> call, retrofit2.Response<AddItemDataResponse> response) {

                                        //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                                        if (response.body().getSuccess().equals("1")) {

                                            List<AddItemDataItem> items = response.body().getAddItemData();
                                            qrProductID = items.get(0).getProductId();
                                            qrProductName = items.get(0).getProductName();

                                            llProduct.setVisibility(View.GONE);
                                            llSubCat.setVisibility(View.GONE);
                                            llCat.setVisibility(View.GONE);
                                            txtProductCompany.setText("");
                                            txtProductCategory.setText("");
                                            txtProductSubCategory.setText("");
                                            edtProductName.setText("");
                                            edtProductDescription.setText("");
                                            edtProductStockQuantity.setText("");
                                            spInStock.setSelection(0);


                                            //Toast.makeText(AddItemActivity.this, "Product ID"+qrProductID+" Product Name"+qrProductName, Toast.LENGTH_SHORT).show();

                                            try {
                                                bitmap = TextToImageEncode(qrProductID);

                                                addDialog.dismiss();

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                            addDialog1 = new ProgressDialog(AddItemActivity.this);
                                            addDialog1.setCancelable(true);
                                            addDialog1.setMessage("Saving QR Code...");
                                            addDialog1.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                            addDialog1.show();

                                            String root = Environment.getExternalStorageDirectory().toString();
                                            File myDir = new File(root + "/QRCodes");
                                            myDir.mkdirs();
                                            String fname = "QR_" + qrProductName + ".jpg";
                                            File file = new File(myDir, fname);
                                            if (file.exists())
                                                file.delete();
                                            try {
                                                FileOutputStream out = new FileOutputStream(file);
                                                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                                                out.flush();
                                                out.close();
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            addDialog1.dismiss();

                                            new AlertDialog.Builder(AddItemActivity.this)
                                                    .setTitle("Storage Path")
                                                    .setMessage("Internal Storage/QRCodes/QR_" + qrProductName + ".jpg")
                                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int which) {

                                                            dialog.dismiss();

                                                        }
                                                    })
                                                    .setNegativeButton(android.R.string.no, null)
                                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                                    .show();


                                        } else {

                                            Toast.makeText(AddItemActivity.this, "Error :" + response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                                            addDialog.dismiss();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<AddItemDataResponse> call, Throwable t) {
                                        //Log.d("erorr",t.getMessage());
                                        //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                                        addDialog.dismiss();

                                    }
                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            // Toast.makeText(getApplicationContext(), "All permissions are granted!", Toast.LENGTH_SHORT).show();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        //Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }

    private Bitmap TextToImageEncode(String qrProductID) throws Exception {
        BitMatrix bitMatrix;
        try {
            bitMatrix = new MultiFormatWriter().encode(
                    qrProductID,
                    BarcodeFormat.DATA_MATRIX.QR_CODE,
                    QRcodeWidth, QRcodeWidth, null
            );

        } catch (IllegalArgumentException Illegalargumentexception) {

            return null;
        }
        int bitMatrixWidth = bitMatrix.getWidth();

        int bitMatrixHeight = bitMatrix.getHeight();

        int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];

        for (int y = 0; y < bitMatrixHeight; y++) {
            int offset = y * bitMatrixWidth;

            for (int x = 0; x < bitMatrixWidth; x++) {

                pixels[offset + x] = bitMatrix.get(x, y) ?
                        getResources().getColor(R.color.black) : getResources().getColor(R.color.white);
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);

        bitmap.setPixels(pixels, 0, 350, 0, 0, bitMatrixWidth, bitMatrixHeight);
        return bitmap;
    }

    private void addSubCategory() {

        addDialog = new ProgressDialog(this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Adding SubCategory...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        try {
            SubCategoryDataRequest subCategoryDataRequest = new SubCategoryDataRequest();
            subCategoryDataRequest.setCatId(strFinalCategoryID);
            subCategoryDataRequest.setSubCatName(strProductSubCategory);

            Call<SubCategoryDataResponse> responceCall = ApiClient.getApiClient().addSubCategory(subCategoryDataRequest);

            responceCall.enqueue(new Callback<SubCategoryDataResponse>() {
                @Override
                public void onResponse(Call<SubCategoryDataResponse> call, retrofit2.Response<SubCategoryDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        List<SubCategoryDataItem> items = response.body().getSubCategoryData();

                        strFinalSubCategoryCatID = items.get(0).getCatId();
                        strFinalSubCategoryID = items.get(0).getSubCatId();
                        strFinalSubCategoryName = items.get(0).getSubCatName();
                        getSubCategory();
                        btnAddSubCategory.setVisibility(View.GONE);
                        llProduct.setVisibility(View.VISIBLE);
                        addDialog.dismiss();


                    } else {

                        Toast.makeText(AddItemActivity.this, "Error :" + response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                        addDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<SubCategoryDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialog.dismiss();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addCategory() {

        addDialog = new ProgressDialog(this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Adding Category...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        try {
            CategoryDataRequest categoryDataRequest = new CategoryDataRequest();
            categoryDataRequest.setCatName(strProductCategory);

            Call<CategoryDataResponse> responceCall = ApiClient.getApiClient().addCategory(categoryDataRequest);

            responceCall.enqueue(new Callback<CategoryDataResponse>() {
                @Override
                public void onResponse(Call<CategoryDataResponse> call, retrofit2.Response<CategoryDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        List<CategoryDataItem> items = response.body().getCategoryData();

                        strFinalCategoryName = items.get(0).getCatName();
                        strFinalCategoryID = items.get(0).getCatId();
                        getCategory();
                        getSubCategory();
                        llSubCat.setVisibility(View.VISIBLE);
                        btnAddCategory.setVisibility(View.GONE);
                        addDialog.dismiss();


                    } else {

                        Toast.makeText(AddItemActivity.this, "Error :" + response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                        addDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<CategoryDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialog.dismiss();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void addCompany() {

        addDialog = new ProgressDialog(this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Adding Company...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        try {
            CompanyDataRequest companyDataRequest = new CompanyDataRequest();
            companyDataRequest.setCompanyName(strProductCompany);

            Call<CompanyDataResponse> responceCall = ApiClient.getApiClient().addCompany(companyDataRequest);

            responceCall.enqueue(new Callback<CompanyDataResponse>() {
                @Override
                public void onResponse(Call<CompanyDataResponse> call, retrofit2.Response<CompanyDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        companyItems = response.body().getCompanyData();

                        strFinalCompanyName = companyItems.get(0).getCompanyName();
                        strFinalCompanyID = companyItems.get(0).getCompanyId();
                        getCompany();
                        llCat.setVisibility(View.VISIBLE);
                        llSubCat.setVisibility(View.VISIBLE);
                        btnAddCompany.setVisibility(View.GONE);
                        addDialog.dismiss();


                    } else {

                        Toast.makeText(AddItemActivity.this, "Error :" + response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                        addDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<CompanyDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialog.dismiss();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getSubCategory() {

        subCategoryIdList.clear();
        subCategoryNameList.clear();
        subCategoryCatIdList.clear();

        SubCategoryDataRequest subCategoryDataRequest = new SubCategoryDataRequest();
        subCategoryDataRequest.setCatId(strFinalCategoryID);

        Call<SubCategoryDataResponse> responceCall = ApiClient.getApiClient().getSubCategory(subCategoryDataRequest);

        responceCall.enqueue(new Callback<SubCategoryDataResponse>() {
            @Override
            public void onResponse(Call<SubCategoryDataResponse> call, retrofit2.Response<SubCategoryDataResponse> response) {

                //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                if (response.body().getSuccess().equals("1")) {

                    subCategoryItems = response.body().getSubCategoryData();

                    //Toast.makeText(AdminMainActivity.this, ""+items, Toast.LENGTH_SHORT).show();

                    for (int i = 0; i < subCategoryItems.size(); i++) {

                        subCategoryNameList.add(subCategoryItems.get(i).getSubCatName());
                        subCategoryIdList.add(subCategoryItems.get(i).getSubCatId());
                        subCategoryCatIdList.add(subCategoryItems.get(i).getCatId());

                    }

                } else {

                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SubCategoryDataResponse> call, Throwable t) {
                //Log.d("erorr",t.getMessage());
                //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void getCategory() {

        categoryIdList.clear();
        categoryNameList.clear();

        CategoryDataRequest categoryDataRequest = new CategoryDataRequest();
        //catRequest.setCatName(""+strCategory);

        Call<CategoryDataResponse> responceCall = ApiClient.getApiClient().getCategory(categoryDataRequest);

        responceCall.enqueue(new Callback<CategoryDataResponse>() {
            @Override
            public void onResponse(Call<CategoryDataResponse> call, retrofit2.Response<CategoryDataResponse> response) {

                //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                if (response.body().getSuccess().equals("1")) {

                    categoryItems = response.body().getCategoryData();

                    //Toast.makeText(AdminMainActivity.this, ""+items, Toast.LENGTH_SHORT).show();

                    for (int i = 0; i < categoryItems.size(); i++) {

                        categoryNameList.add(categoryItems.get(i).getCatName());
                        categoryIdList.add(categoryItems.get(i).getCatId());

                    }

                } else {

                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CategoryDataResponse> call, Throwable t) {
                //Log.d("erorr",t.getMessage());
                //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void getCompany() {

        companyIdList.clear();
        companyNameList.clear();

        CompanyDataRequest companyDataRequest = new CompanyDataRequest();
        //catRequest.setCatName(""+strCategory);

        Call<CompanyDataResponse> responceCall = ApiClient.getApiClient().getCompany(companyDataRequest);

        responceCall.enqueue(new Callback<CompanyDataResponse>() {
            @Override
            public void onResponse(Call<CompanyDataResponse> call, retrofit2.Response<CompanyDataResponse> response) {

                //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                if (response.body().getSuccess().equals("1")) {

                    companyItems = response.body().getCompanyData();

                    //Toast.makeText(AdminMainActivity.this, ""+items, Toast.LENGTH_SHORT).show();

                    for (int i = 0; i < companyItems.size(); i++) {

                        companyNameList.add(companyItems.get(i).getCompanyName());
                        companyIdList.add(companyItems.get(i).getCompanyId());

                    }

                } else {

                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CompanyDataResponse> call, Throwable t) {
                //Log.d("erorr",t.getMessage());
                //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }


    private void showSettingsDialog() {

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(AddItemActivity.this);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        SharedPreferences sharedPreferences = getSharedPreferences("imgNo", MODE_PRIVATE);
        image = sharedPreferences.getString("img", "");
        type = sharedPreferences.getString("type", "");

        if (image.equals(strProductName + "_1")) {

            if (type.equals("capture")) {

                super.onActivityResult(requestCode, resultCode, data);
                if (this.requestCode == requestCode && resultCode == RESULT_OK) {
                    Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                    imgProduct1.setImageBitmap(bitmap);
                    uploadImage(bitmap);
                    imgProduct2.setVisibility(View.VISIBLE);
                }

            } else {
                super.onActivityResult(requestCode, resultCode, data);
                if (resultCode == this.RESULT_CANCELED) {
                    return;
                }
                if (requestCode == GALLERY) {
                    if (data != null) {
                        Uri contentURI = data.getData();
                        try {

                            bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                            imgProduct1.setImageBitmap(bitmap);
                            uploadImage(bitmap);
                            imgProduct2.setVisibility(View.VISIBLE);

                        } catch (IOException e) {
                            e.printStackTrace();
                            //Toast.makeText(AdminAddProductActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }

        }
        if (image.equals(strProductName + "_2")) {

            if (type.equals("capture")) {

                super.onActivityResult(requestCode, resultCode, data);
                if (this.requestCode == requestCode && resultCode == RESULT_OK) {
                    Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                    imgProduct2.setImageBitmap(bitmap);
                    uploadImage(bitmap);
                    imgProduct3.setVisibility(View.VISIBLE);
                }

            } else {
                super.onActivityResult(requestCode, resultCode, data);
                if (resultCode == this.RESULT_CANCELED) {
                    return;
                }
                if (requestCode == GALLERY) {
                    if (data != null) {
                        Uri contentURI = data.getData();
                        try {

                            bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                            imgProduct2.setImageBitmap(bitmap);
                            uploadImage(bitmap);
                            imgProduct3.setVisibility(View.VISIBLE);

                        } catch (IOException e) {
                            e.printStackTrace();
                            //Toast.makeText(AdminAddProductActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }

        }
        if (image.equals(strProductName + "_3")) {

            if (type.equals("capture")) {

                super.onActivityResult(requestCode, resultCode, data);
                if (this.requestCode == requestCode && resultCode == RESULT_OK) {
                    Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                    imgProduct3.setImageBitmap(bitmap);
                    uploadImage(bitmap);
                    imgProduct4.setVisibility(View.VISIBLE);
                }

            } else {
                super.onActivityResult(requestCode, resultCode, data);
                if (resultCode == this.RESULT_CANCELED) {
                    return;
                }
                if (requestCode == GALLERY) {
                    if (data != null) {
                        Uri contentURI = data.getData();
                        try {

                            bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                            imgProduct3.setImageBitmap(bitmap);
                            uploadImage(bitmap);
                            imgProduct4.setVisibility(View.VISIBLE);

                        } catch (IOException e) {
                            e.printStackTrace();
                            //Toast.makeText(AdminAddProductActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }

        }
        if (image.equals(strProductName + "_4")) {

            if (type.equals("capture")) {

                super.onActivityResult(requestCode, resultCode, data);
                if (this.requestCode == requestCode && resultCode == RESULT_OK) {
                    Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                    imgProduct4.setImageBitmap(bitmap);
                    uploadImage(bitmap);
                }

            } else {
                super.onActivityResult(requestCode, resultCode, data);
                if (resultCode == this.RESULT_CANCELED) {
                    return;
                }
                if (requestCode == GALLERY) {
                    if (data != null) {
                        Uri contentURI = data.getData();
                        try {

                            bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                            imgProduct4.setImageBitmap(bitmap);
                            uploadImage(bitmap);

                        } catch (IOException e) {
                            e.printStackTrace();
                            // Toast.makeText(AdminAddProductActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }

        }

    }

    private void uploadImage(Bitmap bitmap) {

        addDialog = new ProgressDialog(this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Uploading Image");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
        try {
            jsonObject = new JSONObject();
            String imgname = strImgURL;
            jsonObject.put("name", imgname);
            //  Log.e("Image name", etxtUpload.getText().toString().trim());
            jsonObject.put("image", encodedImage);
            // jsonObject.put("aa", "aa");
        } catch (JSONException e) {
            Log.e("JSONObject Here", e.toString());
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, upload_URL, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        Log.e("aaaaaaa", jsonObject.toString());
                        rQueue.getCache().clear();
                        //saveImageName();
                        //Toast.makeText(getApplication(), "Image Uploaded Successfully", Toast.LENGTH_SHORT).show();
                        addDialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("aaaaaaa", volleyError.toString());

            }
        });

        rQueue = Volley.newRequestQueue(AddItemActivity.this);
        rQueue.add(jsonObjectRequest);

    }
}


/*if(companyNameList.size()==0){

        strCompanyName = txtProductCompany.getText().toString().trim();
        btnAddCompany.setVisibility(View.VISIBLE);
        llCat.setVisibility(View.GONE);
        llSubCat.setVisibility(View.GONE);

        }
        else{

        for (int j = 0; j < companyNameList.size(); j++) {

        if (strProductCompany.equals(companyNameList.get(j).trim())) {

        strFinalCompanyID = companyIdList.get(j);
        strFinalCompanyName = companyNameList.get(j);
        btnAddCompany.setVisibility(View.GONE);
        llCat.setVisibility(View.VISIBLE);
        llSubCat.setVisibility(View.VISIBLE);
        //break;
        } else {
        strCompanyName = txtProductCompany.getText().toString().trim();
        btnAddCompany.setVisibility(View.VISIBLE);
        llCat.setVisibility(View.GONE);
        llSubCat.setVisibility(View.GONE);

        }

        }

        }*/
