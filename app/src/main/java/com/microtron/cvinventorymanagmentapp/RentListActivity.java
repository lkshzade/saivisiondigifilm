package com.microtron.cvinventorymanagmentapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.Toast;

import com.microtron.cvinventorymanagmentapp.Adapters.RecyclerItemClickListenerRentList;
import com.microtron.cvinventorymanagmentapp.Adapters.RentListAdapter;
import com.microtron.cvinventorymanagmentapp.Helper.ApiClient;
import com.microtron.cvinventorymanagmentapp.Helper.AppConstants;
import com.microtron.cvinventorymanagmentapp.Helper.Sp;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataItem;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataRequest;
import com.microtron.cvinventorymanagmentapp.NetworkModel.getBookedItem.BookedItemDataResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class RentListActivity extends AppCompatActivity {

    private RecyclerView recyclerRentList;
    private ImageView imgBack, imgNoProduct;
    private String strFromIntent;
    private ProgressDialog addDialog;
    private RentListAdapter rentListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rent_list);

        strFromIntent = Sp.readFromPreferences(this, AppConstants.INTENT_TO,"");

        recyclerRentList = findViewById(R.id.recyclerRentList);
        imgBack = findViewById(R.id.imgBack);
        imgNoProduct = findViewById(R.id.imgNoProduct);

        imgNoProduct.setVisibility(View.GONE);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerRentList.setLayoutAnimation(animation);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerRentList.setLayoutManager(layoutManager);

        getRentList();
    }

    private void getRentList() {

        addDialog = new ProgressDialog(this);
        addDialog.setCancelable(true);
        addDialog.setMessage("Getting List...");
        addDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        addDialog.show();

        try{
            BookedItemDataRequest getRentListRequest = new BookedItemDataRequest();


            Call<BookedItemDataResponse> responceCall = ApiClient.getApiClient().getRentList(getRentListRequest);

            responceCall.enqueue(new Callback<BookedItemDataResponse>() {
                @Override
                public void onResponse(Call<BookedItemDataResponse> call, retrofit2.Response<BookedItemDataResponse> response) {

                    //Toast.makeText(AdminMainActivity.this, "Success "+response.body().getSuccess(), Toast.LENGTH_SHORT).show();

                    if (response.body().getSuccess().equals("1")) {

                        List<BookedItemDataItem> items =response.body().getBookedItemData();
                        imgNoProduct.setVisibility(View.GONE);

                        rentListAdapter = new RentListAdapter(RentListActivity.this, items, recyclerItemClickListener);
                        recyclerRentList.setAdapter(rentListAdapter);
                        rentListAdapter.notifyDataSetChanged();

                        addDialog.dismiss();


                    } else {

                        //Toast.makeText(RentListActivity.this, "Error :"+response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                        imgNoProduct.setVisibility(View.VISIBLE);
                        addDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<BookedItemDataResponse> call, Throwable t) {
                    //Log.d("erorr",t.getMessage());
                    //Toast.makeText(AdminAddProductActivity.this, "Error :"+t.getMessage(), Toast.LENGTH_SHORT).show();
                    addDialog.dismiss();

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    private RecyclerItemClickListenerRentList recyclerItemClickListener = new RecyclerItemClickListenerRentList() {
        @Override
        public void onItemClick(final BookedItemDataItem notiItem) {
        }
    };

    @Override
    public void onBackPressed() {
        if(strFromIntent.equals("RentViewAccount")){
            Intent intent = new Intent(this, ViewAccountsActivity.class);
            startActivity(intent);
            overridePendingTransition(0,0);
            finish();
        }
        else{
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            overridePendingTransition(0,0);
            finish();
        }

    }

}
